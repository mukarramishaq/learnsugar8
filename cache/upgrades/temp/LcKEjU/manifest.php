<?php
$manifest = array (
  'author' => 'Rolustech',
  'published_date' => '2018-10-02 13:29:59',
  'type' => 'module',
  'key' => '92082d74e280f5ee',
  'name' => 'RT StikiNotes',
  'description' => 'RT StikiNotes, sticky notes for your Sugar.',
  'version' => 'v1.0',
  'is_uninstallable' => true,
  'acceptable_sugar_versions' => 
  array (
    'regex_matches' => 
    array (
      0 => '7.*.*',
      1 => '8.*.*',
    ),
  ),
  'acceptable_sugar_flavors' => 
  array (
    0 => 'PRO',
    1 => 'ENT',
    2 => 'ULT',
  ),
  'readme' => 'README.txt',
  'remove_tables' => 'prompt',
);
$installdefs = array (
  'id' => 'RT StikiNotes-v1.0',
  'custom_fields' => 
  array (
    0 => 
    array (
      'name' => 'stickyseen_c',
      'vname' => 'LBL_STICKYSEEN_C',
      'type' => 'bool',
      'source' => 'custom_fields',
      'studio' => false,
      'default' => 0,
      'reportable' => false,
      'massupdate' => false,
      'importable' => false,
      'module' => 'Notes',
    ),
    1 => 
    array (
      'name' => 'issticky_c',
      'vname' => 'LBL_ISSTICKY_C',
      'type' => 'bool',
      'source' => 'custom_fields',
      'studio' => false,
      'default' => 0,
      'reportable' => false,
      'massupdate' => false,
      'importable' => false,
      'module' => 'Notes',
    ),
  ),
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/custom',
      'to' => 'custom',
    ),
  ),
  'jsgroups' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/custom/Extension/application/Ext/JSGroupings/RTStikiGrouping.php',
      'to_module' => 'application',
    ),
  ),
);
