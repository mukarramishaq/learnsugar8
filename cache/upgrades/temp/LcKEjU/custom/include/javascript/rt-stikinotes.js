class StikiNote {
    // declare variables and sets default values
    constructor() {
        this.currentNote = this.getCookie("currentNote");
        this.totalNotes = this.getCookie("totalNotes");
        this.subject = this.getCookie("subject");
        this.description = this.getCookie("description");
        this.position_x = this.getCookie("position_x");
        this.position_y = this.getCookie("position_y");
        this.isMinimized = this.getCookie("isMinimized");
        this.defaultNote = SUGAR.App.lang.get("LBL_SN_DEFAULT_NOTE");
        this.defaultSubject = SUGAR.App.lang.get("LBL_SN_DEFAULT_SUBJECT");
        this.defaultDescription = SUGAR.App.lang.get("LBL_SN_DEFAULT_DESCRIPTION");
        this.expireDate = new Date();
        this.expireDate.setFullYear(this.expireDate.getFullYear() + 10);
        this.errorString = SUGAR.App.lang.get('LBL_SN_ERROR_FETCH');
        this.notesCollection = SUGAR.App.data.createBeanCollection('Notes');
        this.updatedNotice = SUGAR.App.lang.get('LBL_SN_UPDATED');
        this.createdNotice = SUGAR.App.lang.get('LBL_SN_CREATED');
        this.sharedNotice = SUGAR.App.lang.get('LBL_SN_SHARED');
        this.unassignedNotice = SUGAR.App.lang.get('LBL_SN_UNASSIGNED');
        this.linkedNotice = SUGAR.App.lang.get('LBL_SN_LINKED');
        this.closedNotice = SUGAR.App.lang.get('LBL_SN_CLOSED');
        this.notSelectedNotice = SUGAR.App.lang.get('LBL_SN_NOTSELECTED');
        this.alreadySharedNotice = SUGAR.App.lang.get('LBL_SN_ALREADYSHARED');
        this.notAvaiableNotice = SUGAR.App.lang.get('LBL_SN_NOTAVAILABLE')


        //if currentNote is null or undefined then set to default
        if (_.isNull(this.currentNote) || _.isUndefined(this.currentNote)) {
            this.setCookie("currentNote", 1);
            this.currentNote = 1;
        }
        //if totalNotes is null or undefined then set to default
        if (_.isNull(this.totalNotes) || _.isUndefined(this.totalNotes)) {
            this.setCookie("totalNotes", 1);
            this.totalNotes = 1;
        }
        //if isMinimized is null or undefined then set to default(not minimized)
        if (_.isNull(this.isMinimized) || _.isUndefined(this.isMinimized)) {
            this.setCookie("isMinimized", false);
            this.isMinimized = false;
        }
        //if container's position is null or undefined then set to default
        if (_.isNull(this.position_x) || _.isUndefined(this.position_x)) {
            this.position_x = this.position_y = 0;
            this.setCookie("position_x", 0);
            this.setCookie("position_y", 0);
        }
    }

    // creates/updates the cookie where cname is the cookie name and cvalue is cookie's value
    setCookie(cname, cvalue) {
        $.cookie(cname, cvalue, {
            expires: this.expireDate,
            path: '/'
        });
    }

    // returns the cookie where cname is the name of the cookie
    getCookie(cname) {
        return $.cookie(cname);
    }

    // gets the current position of stikinote
    getPosition(element) {
        var rect = element.getBoundingClientRect();
        return {
            x: rect.left,
            y: rect.top
        };
    }

    /*
        Checks if there isn't enough space for container to open then move the container to left
    */
    checkPosition() {
        var stikinotescontainer = document.getElementById('stikinotescontainer');
        var currentPosition = this.getPosition(stikinotescontainer);
        if($(window).width() - currentPosition.x - stikinotescontainer.offsetWidth - 210 < 0) {
            var negativePostion = $(window).width() - currentPosition.x - stikinotescontainer.offsetWidth - 210;
            negativePostion *= -1;
            stikinotescontainer.style.left = (currentPosition.x - negativePostion) +'px';
        }
    }

    /*
        SugarCRM Alert
        possible alert_levels are info , success , warning , error , process , confirmation
    */
    stikinoteAlert(alert_level, alert_message) {
        SUGAR.App.alert.show('rt_stikinotes', {
            level: alert_level,
            messages: alert_message,
            autoClose: true
        });
    }

    /*
        update a specific note in the databasse which accepts a bean and attributes to update
    */
    updateNoteBean(bean, attributes) {
        SUGAR.App.api.call('update', SUGAR.App.api.buildURL('Notes/' + bean.id), attributes, {
            success: _.bind(function(data) {
                this.currentNote = 1;
                this.setCookie("currentNote", 1);
                this.fetchNotes();
            }, this),
            error: function(err) {
                SUGAR.App.error.handleHttpError(err);
            }
        }, {
            async: false
        });
    }

    /*
        make subject field editable
    */
    focusSubject() {
        if($('#subjectinput span').length > 0) {
            if(this.notesCollection.length > 0){
                if(this.currentNote < 1 || this.currentNote > this.totalNotes || this.currentNote == this.defaultNote){
                    this.currentNote =1;
                    this.setCookie("currentNote", 1);
                }
                if(this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                    var value = $('#subjectinput span').text();
                    $('#subjectinput').html('<input id="subjectinput" type="text" autocomplete="off" value="' + value + '"  >');
                    $( "#subjectinput input" ).focus();
                    this.subjectClickEvent();
                } else {
                    this.stikinoteAlert('info', this.unassignedNotice);
                }
            } else {
                var value = "";
                $('#subjectinput').data("isnew","new");
                $('#subjectinput').html('<input id="subjectinput" type="text" autocomplete="off" placeholder="Subject" value="' + value + '"  >');
                $( "#subjectinput input" ).focus();
                this.subjectClickEvent();
            }
        }
    }

    /*
        make description field editable
    */
    focusDescription() {
        if($('#textareainput span').length > 0 ) {
            if(this.notesCollection.length > 0) {
                if(this.currentNote < 1 || this.currentNote > this.totalNotes || this.currentNote == this.defaultNote){
                    this.currentNote =1;
                    this.setCookie("currentNote", 1);
                }
                if(this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                    var value = $('#textareainput span').text();
                    if(value == this.defaultDescription){
                        value = "";
                    }
                    $('#textareainput').html('<textarea name="rtdescription" autocomplete="off" type="text" placeholder="'+ this.defaultDescription +'">' + value + '</textarea>');
                    $( "#textareainput textarea" ).focus();
                    this.descriptionClickEvent();
                } else {
                    this.stikinoteAlert('info', this.unassignedNotice);
                }
            } 
            else {
                var value = "";
                $('#subjectinput').data("isnew","new");
                $('#subjectinput').html('<input id="subjectinput" type="text" autocomplete="off" placeholder="Subject" value="' + value + '"  >');
                $( "#subjectinput input" ).focus();
                this.subjectClickEvent();
            }
        }
    }

    /*
        checks if the data is being updated then wait for description to focus
    */
    focusDescriptionUpdate() {
        if( $('#subjectinput').data("isclicked") == false) {
            if($('#subjectinput').data("isupdating") == true) {
                setTimeout(_.bind(function() {
                    this.focusDescriptionUpdate();
                },this),300);
            } else {
                this.focusDescription();
            }
        }
    }

    /*
        Register click event for subject focus out
    */
    subjectClickEvent() {
        $(document).on("click", _.bind(function(e) {
            if(e.srcElement.id != "subjectinput"){
                if($('#subjectinput input').length > 0 && (e.srcElement.className != "fa fa-plus")) {
                    $('#subjectinput').data("isclicked","true");
                    this.updateSubject();
                    $(document).off("click");
                }
            }
        },this));
    }

    /*
        Register click event for description focus out
    */
    descriptionClickEvent() {
        $(document).unbind('click').on("click", _.bind(function(e) {
            if(e.srcElement.name != "rtdescription"){
                if($('#textareainput textarea').length > 0) {
                    this.updateDescription();
                    $(document).off("click");
                }
            }
        },this));
    }

    /*
        update Subject field and save bean if updated
    */
    updateSubject() {
        var val = $('#subjectinput input').val();
        if ($('#subjectinput').data("isnew") == "new") { // new note
            if ( !_.isEmpty(val) && !_.isNull(val)) {
                $('#subjectinput').html('');
                $('#subjectinput').html('<span id="subjectinput" >' + val +'</span>');
                $('#subjectinput').data("isnew",null);
                $('#subjectinput').data("isupdating", true);
                this.createNote();
                this.updateList();
            } else {
                $('#subjectinput').data("isnew",null);
                $('#subjectinput').html('');
                $('#subjectinput').html('<span id="subjectinput" >' + val +'</span>');
                this.updateNote();
                this.updateList();
            }
            this.focusDescriptionUpdate();
            return;
        }
        else {
            if(this.subject != val && !_.isEmpty(val) && !_.isNull(val)){ //existing note
                this.updateNoteBean(this.notesCollection.models[this.currentNote - 1], {
                    name: val
                });
                this.stikinoteAlert('success', this.updatedNotice);    
                $('#subjectinput').html('');
                $('#subjectinput').html('<span id="subjectinput" >' + val +'</span>');
            } else {
                $('#subjectinput').html('');
                $('#subjectinput').html('<span id="subjectinput" >' + this.subject +'</span>');
            }
            this.focusDescriptionUpdate();
        }
    }
    /*
        update Description field and save bean if updated
    */
    updateDescription() {
        if($('#textareainput textarea').length > 0) {
            var val = $('#textareainput textarea').val();
            if(this.description != val) {
                if(!_.isNull($('#subjectinput input').val()) && val != ""){
                    this.updateNoteBean(this.notesCollection.models[this.currentNote - 1], {
                        description: val
                    });
                    this.stikinoteAlert('success', this.updatedNotice);
                    this.fetchNotes();
                }
                else {
                    val = this.description;
                }
            }
            $('#textareainput').html('');
            $('#textareainput').html('<span id="textareainput" placeholder="Description">' + val + '</span>');
        }
    }

    /*
        switch to the previous note
    */
    navigateLeft() {
        if ($( "#subjectinput input" ).length > 0){
            this.updateSubject();
        }
        if ($('#textareainput textarea').length > 0){
            this.updateDescription();
        }
        if (this.notesCollection.length > 0) {
            if (this.currentNote > 1 && this.currentNote != this.defaultNote) {
                $('#notesList li:nth-child('+ this.currentNote + ')').removeClass($('#notesList li:nth-child('+ this.currentNote + ')').attr('class'));
                this.setCookie("currentNote", --this.currentNote);
                if($('#notesList li').length > 0){
                    $('#notesList li:nth-child('+ this.currentNote + ')').addClass('selected');
                }
                this.updateNote();
            }
        } else {
            this.stikinoteAlert('info', this.notAvaiableNotice);
        }
    }

    /*
        switch to the next note
    */
    navigateRight() {
        if ($( "#subjectinput input" ).length > 0){
            this.updateSubject();
        }
        if ($('#textareainput textarea').length > 0){
            this.updateDescription();
        }
        if (this.notesCollection.length > 0) {
            if (this.currentNote < this.totalNotes && this.currentNote != this.defaultNote) {
                $('#notesList li:nth-child('+ this.currentNote + ')').removeClass($('#notesList li:nth-child('+ this.currentNote + ')').attr('class'));
                this.setCookie("currentNote", ++this.currentNote);
                if($('#notesList li').length > 0){
                    $('#notesList li:nth-child('+ this.currentNote + ')').addClass('selected');
                }
                this.updateNote();
            }
        } else {
            this.stikinoteAlert('info', this.notAvaiableNotice);
        }
    }

    /*
        fetch the shared team of the note and render it
    */
    renderSharedTeam() {
        var options = "";
        var teams = SUGAR.App.user.attributes.my_teams;
        for(var i = 0; i < teams.length; i++)
        {
            options += '<option value="' + teams[i].id + '"> ' + teams[i].name +' </option>';
        }
        $("#sharecontainer select[name='teams']").html('');
        $("#sharecontainer select[name='teams']").html(options);
        $("#sharecontainer select[name='teams']").select2('destroy');
        $("#sharecontainer select[name='teams']").select2({
            width: 'resolve'
        });
        $("#sharecontainer select[name='teams']").val(this.notesCollection.models[this.currentNote-1].attributes.team_id).trigger('change');
    }

    /*
        fetch the related record of the note and render it
    */
    renderRelatedRecordField() {
        var parent_type = this.notesCollection.models[this.currentNote-1].attributes.parent_type;
        var parent_id = this.notesCollection.models[this.currentNote-1].attributes.parent_id;
        if(!_.isNull(parent_id) && !_.isUndefined(parent_id) && parent_id != "") {
            var filteredCollection = SUGAR.App.data.createBeanCollection(parent_type);
            filteredCollection.filterDef = {
                'id' : parent_id
            };
            filteredCollection.fetch({
                relate: false,
                limit: 5,
                fields: ['id', 'name'],
                success: _.bind(function(response) {
                    if(response.length > 0){
                        var rr =  response.models[0].attributes.name;
                        var relatedRecordId = this.notesCollection.models[this.currentNote-1].attributes.parent_id;
                        $("#linkcontainer select[name='relatedModule']").val(this.notesCollection.models[this.currentNote-1].attributes.parent_type).trigger('change');
                        $("#linkcontainer input[name='relatedRecord']").select2({
                            initSelection: function(el, callback) {
                                var $el = $(el),
                                    id = parent_id,
                                    text = rr;
                                callback({id: id, text: text});
                            },
                            width: 'resolve',
                            query: this.updateSearch
                        });
                    }
                }, this),
                error: _.bind(function(err) {
                    console.error(err);
                }, this),
            });
        } 
        else {
            $("#linkcontainer input[name='relatedRecord']").select2({
                width: 'resolve',
                query: this.updateSearch
            });
        }
    }

    /*
        query function for select2 of $("#linkcontainer input[name='relatedRecord']")
    */    
    updateSearch (params) {
            var module = $("#linkcontainer select[name='relatedModule']").val();
            this.filteredCollection = SUGAR.App.data.createBeanCollection(module);
            this.filteredCollection.filterDef = {
                'name': {'$starts' : params.term}
            };
            this.filteredCollection.fetch({
                relate: false,
                limit: 5,
                fields: ['id', 'name'],
                success: _.bind(function(response) {
                    var data = {
                        "results": [],
                        "pagination": {
                            "more" : false
                        }
                    };
                    for(var i = 0; i < response.length; i++){
                        data.results.push({
                            "id" : response.models[i].attributes.id,
                            "text" : response.models[i].attributes.name
                        })
                    }

                    if (params.callback && _.isFunction(params.callback)) {
                         params.callback(data);
                    }
                }, this),
                error: _.bind(function(err) {
                    console.error(err);
                }, this),
            });
    }

    /*
        update a note on the UI
    */
    updateNote() {
        if($('#stikinotescontainer').length > 0 && !_.isNull(this.notesCollection) && !_.isEmpty(this.notesCollection) && this.notesCollection.length > 0){
            if (this.currentNote == 0 || this.currentNote == this.defaultNote) {
                this.renderdefaultNote();
                return;
            }
            if(this.currentNote > 0 && this.currentNote !== this.defaultNote){
                if (this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                    $('#notecontainer').removeClass($('#notecontainer').attr('class'));
                    $("#notecontainer").addClass("notecontainerassigned");
                } else {
                    $('#notecontainer').removeClass($('#notecontainer').attr('class'));
                    $("#notecontainer").addClass("notecontainerunassigned");
                }
                this.subject = this.notesCollection.models[this.currentNote - 1].attributes.name;
                this.description = this.notesCollection.models[this.currentNote - 1].attributes.description;
                $('#subjectinput span').text(this.subject);
                $('#textareainput span').text(this.description);
                $('#navigationbarcount').text(this.currentNote + '/' + this.totalNotes);
                if($("#linkcontainer").attr('class') == 'isvisible') {
                    this.renderRelatedRecordField();
                }
                if($("#sharecontainer").attr('class') == 'isvisible') {
                    this.renderSharedTeam();
                }
                return;
            }
        }
        this.renderdefaultNote();
    }

    /* create a new note */
    createNote() {
        var stikinote = SUGAR.App.data.createBean("Notes");
        stikinote.save({
            name: $('#subjectinput span').text(),
            description: $('#textareainput textarea').text(),
            assigned_user_id: App.user.id,
            issticky_c: true,
            stickyseen_c: false
        },
        {
            success: _.bind(function(){
                this.stikinoteAlert('success', this.createdNotice);
                this.currentNote = 1;
                this.setCookie("currentNote", 1);
                this.fetchNotes();
            },this),
            error: _.bind(function(){
                this.stikinoteAlert('error', this.errorString);
            },this),
        });
    }

    /*
        render the default values in the note UI
    */
    renderdefaultNote() {
        if($('#stikinotescontainer').length > 0) {
            this.setCookie("currentNote", 1);
            this.currentNote = this.defaultNote;
            this.setCookie("totalNotes", 1);
            this.totalNotes = this.defaultNote;
            this.subject = this.defaultSubject;
            this.description = this.defaultDescription;
            $('#notecontainer').removeClass($('#notecontainer').attr('class'));
            $("#notecontainer").addClass("notecontainerassigned");
            $('#subjectinput span').text(this.subject);
            $('#textareainput span').text(this.description);
            $('#navigationbarcount').text(this.currentNote + '/' + this.totalNotes);
        }
    }

    /*
        update the stikinotes list
    */
    updateList() {
        if($('#notesList').attr('class') == 'isvisible'){
            $('#notesList').html('');
            var list = document.createElement('ul');
            $('#notesList').append(list);
            for(var i = 0; i < this.notesCollection.length; i++) {
                $('#notesList ul').append('<li id="' + (i+1) + '">' + this.notesCollection.models[i].attributes.name + '</li>');
            }
            $( "#notesList li" ).first().css("border-top", "#dddddd 1px solid");
            $('#notesList li:nth-child('+ this.currentNote + ')').addClass('selected');
        }
    }

    // fetches the notes to be displayed as stikinotes
    fetchNotes() {
        this.notesCollection.filterDef = { //select only those notes that are sticky and whose sticky seen is false
            'issticky_c': '1',
            'stickyseen_c': '0'
        };

        this.notesCollection.fetch({
            relate: false,
            limit: 200,
            fields: ['id', 'date_entered', 'date_modified', 'modified_user_id', 'name', 'description', 'created_by', 'deleted', 'parent_name', 'parent_type', 'parent_id', 'contact_id', 'assigned_user_id', 'issticky_c', 'stickyseen_c', 'team_id'],
            success: _.bind(function(data) {
                $('#subjectinput').data("isupdating", false);
                if (data.length > 0) {
                    this.setCookie("totalNotes", data.length);
                    if (this.currentNote == this.defaultNote) {
                        this.setCookie("currentNote", 1);
                        this.currentNote = 1;
                    }
                    this.loadNotes(); // load the stikinote with fetched notes
                    this.updateList(); // update the stikinotes list
                } else {
                    this.setCookie("currentNote", 1);
                    this.currentNote =1;
                    this.setCookie("totalNotes", 1);
                    this.totalNotes =1;
                    this.loadNotes(); // load the stikinote with default values
                    this.updateList(); // update the stikinotes list
                }
            }, this),
            error: _.bind(function(err) {
                console.error(this.errorString);
                console.error(err);
                this.stikinoteAlert('error', this.errorString);
            }, this),
        });
    }

    //loads the fetched note to stikinote
    loadNotes() {
        if (this.notesCollection != null && this.notesCollection.length > 0) { // if there is any fetched note
            if ($("#stikinotescontainer").length > 0) { // if the UI is already rendered
                this.totalNotes = this.notesCollection.length;
                if (this.currentNote > this.totalNotes || _.isNull(this.currentNote) && this.currentNote != this.defaultNote) { // if currentNote index is invalid then set it to default
                    this.setCookie("currentNote", this.totalNotes);
                    this.currentNote = this.totalNotes;
                }
                this.updateNote(); // update the rendered note with updated note's values
            } else { // UI is not rendered
                if(this.currentNote <= 0 && this.currentNote !== this.defaultNote){
                    this.currentNote = 1;
                }
                this.totalNotes = this.notesCollection.length;
                this.subject = this.notesCollection.models[this.currentNote - 1].attributes.name;
                this.description = this.notesCollection.models[this.currentNote - 1].attributes.description;
                if (this.currentNote > this.totalNotes || _.isNull(this.currentNote) && this.currentNote != this.defaultNote) { // if currentNote index is invalid then set it to default
                    this.setCookie("currentNote", this.totalNotes);
                    this.currentNote = this.totalNotes;
                }
            }
        } else { // if there is no fetched note
            this.currentNote = this.defaultNote;
            this.subject = this.defaultSubject;
            this.description = this.defaultDescription;
            this.totalNotes = this.defaultNote;
            if ($("#stikinotescontainer").length > 0) {
                this.updateNote();
                return;
            }
        }
        // renders the stikinote UI
        this.render();
    }
    //renders the note UI on the screen
    render() {
        if ($("#stikinotescontainer").length == 0) {
            $('body').append(
                '<link rel="stylesheet" type="text/css" href="custom/themes/rt-stikinotes.css">' +
                '<div id="stikinotescontainer" class = "isvisible" style="left:' + this.getCookie("position_x") + 'px;top:' + this.getCookie("position_y") + 'px;">' +
                
                '<div id = "leftcontainer" class = "ishidden">' + //leftcontainer
                '<div id="notesList" class = "ishidden" style="list-style-type: none;">' +
                '</div>' +
                '</div>' + //leftcontainer

                '<div id = "maincontainer">' + //maincontainer
                '<div id="menubar"  class = "ishidden"> ' +
                '<div name="menuone" style="margin-right:20px">' +
                '<i class="fa fa-plus" aria-hidden="true" style="margin-right:10%" name="menuadd" rel = "tooltip" data-placement="top" data-title="' + SUGAR.App.lang.get('LBL_SN_ADD') + '"></i>' +
                '</div>' +
                '<div name="menutwo">' +
                '<i class="fa fa-list" aria-hidden="true" name="menulist" data-placement="top" rel = "tooltip" data-title="' + SUGAR.App.lang.get('LBL_SN_LIST') + '"></i>' +
                '<i class="fa fa-share-square-o" aria-hidden="true" margin-left="10%" name="menushare" rel = "tooltip" data-placement="top" data-title="' + SUGAR.App.lang.get('LBL_SN_SHARE') + '"></i>' +
                '<i class="fa fa-link" aria-hidden="true" name=menulink rel = "tooltip" data-placement="top" data-title="' + SUGAR.App.lang.get('LBL_SN_LINK') + '"></i>' +
                '</div>' +
                '<div id="menuthree">' +
                '<i class="fa fa-minus" style="padding-right:0px;" aria-hidden="true" name="menuminimize" rel = "tooltip" data-placement="top" data-title="' + SUGAR.App.lang.get('LBL_SN_MINIMIZE') + '"></i>' +
                '<i class="fa fa-times" aria-hidden="true" name="menuclose" rel = "tooltip" data-placement="top" data-title="' + SUGAR.App.lang.get('LBL_SN_CLOSE') + '"></i>' +
                '</div>' +
                '</div>' + //menubar

                '<div id="notecontainer" class = "isvisible" style="left:' + this.position_x + 'px;top:' + this.position_y + 'px;">' +
                '<form name="rtstikinote"> ' +
                '<div id="firstpane">' +
                '<div id="subjectinput">' +
                '<span id="subjectinput" name="rtsubject">' + this.subject + '</span>' +
                '</div>' +
                '<div id="menuellipses">' +
                '<i class="fa fa-ellipsis-v" aria-hidden="true"></i>' +
                '</div>' +
                '</div>' +
                '<br>' +
                '<div id="textareainput">' +
                '<span name="rtdescription" id="textareainput">' + this.description + '</span>' +
                '</div>' +
                '</form>' +
                '</div>' + //notecontainer

                '<div id="navigationbar">' +
                '<i id="navigateleft" class="fa fa-chevron-left" aria-hidden="true"></i>' +
                '<div id="navigationbarcount">' +
                '<span>' + this.currentNote + '/' + this.totalNotes + '</span>' +
                '</div>' +
                '<i id="navigateright" class="fa fa-chevron-right" aria-hidden="true"></i>' +
                '</div>' + // navigationbar
                '</div>' + //maincontainer
                '<div id="rightcontainer" class="isvisible">' + //rightcontainer
                '<div id="linkcontainer" class="ishidden">' + //linkcontainer
                '<div id="titlebar">' +
                '<i class="fa fa-link" aria-hidden="true"></i> <span> Link </span> </br>' +
                '</div>' +
                '<div id="linkbody" >' +
                '<span>Related To: </span> <br>' + 
                '<select name="relatedModule" class="form-control">' +
                '</select> <br>' +
                '<span>Related Record:</span> <br>' +
                '<input type="text" id="relatedRecord" name="relatedRecord" class="form-control">' +
                '</input> <br>' +
                '<input type="button" name="Link" value = "Link">'+
                '</div>' + //linkbody
                '</div>' + //linkcontainer

                '<div id="sharecontainer" class="ishidden">' +
                '<div id="titlebar">' +
                '<i class="fa fa-share-square-o" aria-hidden="true"></i> <span> Share </span> </br>' +
                '</div>' +
                '<div id="linkbody" >' + //linkbody
                '<span>Team: </span><br>' +
                '<select name="teams" id="teamsID">' +
                '<option value="">Search ...</option>' +
                '</select> <br>' +
                '<input type="button" value = "Share">' +
                '</div>' + //linkbody
                '</div>' + //sharecontainer
                '</div>' + //rightcontainer
                '</div>' + //container
                '<div id="minimizedbar" class = "ishidden">' +
                '<img src="custom/themes/images/logo.png">' +
                '<i class="fa fa-expand" aria-hidden="true" name="maximize" ></i>' +
                '</div>'
            );

            if (!_.isNull(this.notesCollection) && !_.isEmpty(this.notesCollection) && this.notesCollection.length > 0) {
                if (this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id != SUGAR.App.user.id) {
                    $('#notecontainer').removeClass($('#notecontainer').attr('class'));
                    $("#notecontainer").addClass("notecontainerunassigned");
                } else {
                    $('#notecontainer').removeClass($('#notecontainer').attr('class'));
                    $("#notecontainer").addClass("notecontainerassigned");
                }
            } else {
                $('#notecontainer').removeClass($('#notecontainer').attr('class'));
                $("#notecontainer").addClass("notecontainerassigned");
            }

            if (this.isMinimized == "true") {
                $('#minimizedbar').removeClass($('#minimizedbar').attr('class'));
                $('#minimizedbar').addClass('isvisible');
                $('#stikinotescontainer').removeClass($('#stikinotescontainer').attr('class'));
                $('#stikinotescontainer').addClass('ishidden');
                $('#menubar').removeClass($('#menubar').attr('class'));
                $('#menubar').addClass('ishidden');
            }
        }
        //Actions - Start

        /*
            show menu
        */
        $("#menuellipses").unbind('click').on('click', _.bind(function() {
            if ($('#menubar').attr('class') == 'ishidden') {
                $('#menubar').removeClass($('#menubar').attr('class'));
                $('#menubar').addClass('isvisible');
            } else {
                $('#menubar').removeClass($('#menubar').attr('class'));
                $('#menubar').addClass('ishidden');
            }
        }, this));

        $("#stikinotescontainer").draggable();
        $("#stikinotescontainer").draggable("option", "containment", "window");
        /*
            update the note position when dragged
        */
        $("#stikinotescontainer").on("dragstop", _.bind(function(event, ui) {
            var notePosition = this.getPosition(document.getElementById('stikinotescontainer'));
            this.setCookie("position_x", notePosition.x);
            this.setCookie("position_y", notePosition.y);
        }, this));

        // make subject field editable when double clicked
        $('#subjectinput').unbind("dblclick").on("dblclick",_.bind(function(e) {
            this.focusSubject();            
        },this));

        /*
            focus out from the subject input and focus into the description input when tab key is pressed and update/create the note
        */
        $('#subjectinput').unbind("keydown").keydown( _.bind(function(e) { 
            if (e.which == 9 || e.which == 13) {
                $('#subjectinput').data("isclicked",false);
                this.updateSubject();
                e.preventDefault();
            }
        },this));

        /*
            focus out of the description field and update the note
        */
        $('#textareainput').unbind("keydown").keydown( _.bind(function(e) {
            if (e.which == 9) {
                this.updateDescription();
                e.preventDefault();
            }
        },this));

        // make description field editable when double clicked
        $('#textareainput').unbind("dblclick").on("dblclick",_.bind(function(e) {
            this.focusDescription();
        },this));

        // switch to the previous note when clicked on thge left button
        $('#navigateleft').unbind('click').on('click', _.bind(function() {
            this.navigateLeft();
        }, this));

        // switch to the next note when clicked on thge right button
        $('#navigateright').unbind('click').on('click', _.bind(function() {
            this.navigateRight();
        }, this));

        // show the navigation bar when cursor is on the stikinote
        $("#stikinotescontainer").mouseover(function() {
            $('#navigationbar').show();
        });

        // hide the navigation bar when cursor is moved out of the stikinote
        $("#stikinotescontainer").mouseout(function() {
            $('#navigationbar').hide();
        });

        // display the default subject and description for new stikinote
        $("#menubar i[name='menuadd']").unbind('click').click(_.bind(function() {
            $('#notecontainer').removeClass($('#notecontainer').attr('class'));
            $("#notecontainer").addClass("notecontainerassigned");
            $('#subjectinput').html('<input id="subjectinput" type="text" autocomplete="off" placeholder="' + this.defaultSubject +  '">');
            $('#subjectinput').data("isnew","new");
            $('#textareainput span').text('Description');
            $('#textareainput span').val('');
            $('#textareainput span').attr("placeholder", "Description");
            this.subjectClickEvent();
        }, this));

        // list all the stikinotes
        $("#menubar i[name='menulist']").unbind('click').click(_.bind(function() {
            if(this.notesCollection.length > 0) {
                if($('#notesList').attr('class') == 'ishidden'){
                    this.checkPosition();
                    $('#notesList').removeClass($('#notesList').attr('class'));
                    $("#notesList").addClass("isvisible");
                    $('#leftcontainer').removeClass($('#leftcontainer').attr('class'));
                    $("#leftcontainer").addClass("isvisible");
                    this.updateList();
                } else {
                    $('#notesList').removeClass($('#notesList').attr('class'));
                    $("#notesList").addClass("ishidden");
                    $('#leftcontainer').removeClass($('#leftcontainer').attr('class'));
                    $("#leftcontainer").addClass("ishidden");
                }
            } else {
                this.stikinoteAlert('info', this.notAvaiableNotice);
            }   
        }, this));

        // load the clicked stikinote from the l
        $("#notesList").on('click', 'li', _.bind(function(e) {
            if ($( "#subjectinput input" ).length > 0){
                this.updateSubject();
            }
            if ($('#textareainput textarea').length > 0){
                this.updateDescription();
            }
            $('#notesList li:nth-child('+ this.currentNote + ')').removeClass($('#notesList li:nth-child('+ this.currentNote + ')').attr('class'));
            this.currentNote = e.srcElement.id; // get id of clicked li
            this.updateNote();
            $('#notesList li:nth-child('+ this.currentNote + ')').addClass('selected');
        },this));

        // hide the sticky note and show the minimized note when clicked on minimize button
        $("#menubar i[name='menuminimize']").unbind('click').click(_.bind(function() {
            $('#minimizedbar').removeClass($('#minimizedbar').attr('class'));
            $('#minimizedbar').addClass('isvisible');
            $('#stikinotescontainer').removeClass($('#stikinotescontainer').attr('class'));
            $('#stikinotescontainer').addClass('ishidden');
            $('#menubar').removeClass($('#menubar').attr('class'));
            $('#menubar').addClass('ishidden');
            $('#leftcontainer').removeClass($('#leftcontainer').attr('class'));
            $('#leftcontainer').addClass('ishidden');
            $('#rightcontainer').removeClass($('#rightcontainer').attr('class'));
            $('#rightcontainer').addClass('ishidden');
            this.isMinimized = true;
            this.setCookie("isMinimized", true);
        }, this));

        /*
          mark the current note as sticky seen
        */
        $("#menubar i[name='menuclose']").unbind('click').click(_.bind(function() {
                if(this.notesCollection.length > 0) {
                    if (this.currentNote > 0 && this.currentNote != this.defaultNote) {
                        if (this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                            this.updateNoteBean(this.notesCollection.models[this.currentNote - 1], {
                                stickyseen_c: true
                            });
                            this.fetchNotes();
                            this.updateList();
                            this.stikinoteAlert('success', this.closedNotice);
                        }
                        else {
                            this.stikinoteAlert('info', this.unassignedNotice);
                        }
                    } else {
                        this.stikinoteAlert('info',this.notSelectedNotice);
                        this.currentNote = 1;
                        this.updateNote();
                    }
                } else {
                    this.stikinoteAlert('info', this.notAvaiableNotice);
                }
        }, this));

        /*
            show the menu to link the note with a record
        */
        $("#menubar i[name='menulink']").unbind('click').click(_.bind(function() {
            if(this.notesCollection.length > 0) {
               if ($('#linkcontainer').attr('class') == 'ishidden') {
                    if ($('#sharecontainer').attr('class') == 'ishidden') {
                        this.checkPosition();
                    }
                    $('#linkcontainer').removeClass($('#linkcontainer').attr('class'));
                    $('#linkcontainer').addClass('isvisible');
                } else {
                    $('#linkcontainer').removeClass($('#linkcontainer').attr('class'));
                    $('#linkcontainer').addClass('ishidden');
                }
                var relatedModulesList = SUGAR.App.lang.getAppListStrings('record_type_display_notes');
                var options = "";
                for (var moduleKey in relatedModulesList) {
                    options += '<option value="' + relatedModulesList[moduleKey] + 's' + '"> ' + relatedModulesList[moduleKey] +' </option>';
                }
                $("#linkcontainer select[name='relatedModule']").html(options);
                $("#linkcontainer select[name='relatedModule']").select2({
                    width: 'resolve'
                });
                this.renderRelatedRecordField();
            } else {
                this.stikinoteAlert('info', this.notAvaiableNotice);
            }
        },this));

        /*
            link the note with the selected record when link button is clicked
        */
        $("#linkcontainer input[name='Link']").unbind('click').click(_.bind(function() {
            if(this.notesCollection.length > 0) {
                if(this.currentNote > 0 && this.currentNote !== this.defaultNote){
                    if (this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                        var selectedModule = $("#linkcontainer select[name='relatedModule']").val();
                        var selectedRecord = $("#linkcontainer input[name='relatedRecord']").val();
                        var selectedRecordName = $("#linkcontainer select[name='relatedRecord']").text();
                        if (!_.isNull(selectedModule) && !_.isEmpty(selectedModule) && !_.isNull(selectedRecord) && !_.isEmpty(selectedRecord)){
                            this.notesCollection.models[this.currentNote - 1].attributes.parent_id = selectedRecord;
                            this.updateNoteBean(this.notesCollection.models[this.currentNote - 1], {
                                parent_type: selectedModule,
                                parent_id: selectedRecord
                            });
                            this.stikinoteAlert('success',this.linkedNotice + selectedRecordName);
                        }
                    } else {
                        this.stikinoteAlert('info', this.unassignedNotice);
                    }
                } else {
                    this.stikinoteAlert('info',this.notSelectedNotice);
                    this.currentNote = 1;
                    this.updateNote();
                }
            } else {
                    this.stikinoteAlert('info',this.notAvaiableNotice);
            }
        },this));

        /*
            share the note with other teams
        */
        $("#menubar i[name='menushare']").unbind('click').click(_.bind(function() {
            if(this.notesCollection.length > 0) {
                if ($('#sharecontainer').attr('class') == 'ishidden') {
                    if ($('#linkcontainer').attr('class') == 'ishidden') {
                        this.checkPosition();
                    }
                    $('#sharecontainer').removeClass($('#sharecontainer').attr('class'));
                    $('#sharecontainer').addClass('isvisible');
                } else {
                    $('#sharecontainer').removeClass($('#sharecontainer').attr('class'));
                    $('#sharecontainer').addClass('ishidden');
                }
                this.renderSharedTeam();
           } else {
                this.stikinoteAlert('info',this.notAvaiableNotice);
           } 
        }, this));

        // display the sharecontainer
        $("#sharecontainer input").on('click', _.bind(function() {  
            if(this.currentNote > 0 && this.currentNote !== this.defaultNote){
                if (this.notesCollection.models[this.currentNote - 1].attributes.assigned_user_id == App.user.id) {
                    var selectedTeam = $("#sharecontainer select[name='teams'] :selected").val();
                    var selectedTeamName = $("#sharecontainer select[name='teams'] :selected").text();
                    if (this.notesCollection.models[this.currentNote - 1].attributes.team_id != selectedTeam && !_.isNull(selectedTeam) && !_.isEmpty(selectedTeam)){
                        this.notesCollection.models[this.currentNote - 1].attributes.team_id = selectedTeam;
                        this.updateNoteBean(this.notesCollection.models[this.currentNote - 1], {
                            team_id: selectedTeam
                        });
                        this.stikinoteAlert('success',this.sharedNotice + selectedTeamName);
                    } else {
                        this.stikinoteAlert('notice' , this.alreadySharedNotice);
                    }
                }
                else {
                    this.stikinoteAlert('info', this.unassignedNotice);
                }
            } else {
                this.stikinoteAlert('info',this.notSelectedNotice);
                this.currentNote = 1;
                this.updateNote();
            }
        }, this));

        $("#minimizedbar").draggable({
            start: function(event, ui){
                $(this).data('dragging', true);
            },
            stop: function(event, ui){
                setTimeout(function(){
                    $(event.target).data('dragging', false);
                }, 1);
            }
        });
        $("#minimizedbar").draggable("option", "containment", "window");

        // hide the minimized note and display the original note when clicked
        $("#minimizedbar").unbind('click').click(_.bind(function() { 
            if($("#minimizedbar").data('dragging')){
                return;
            }
            $('#minimizedbar').removeClass($('#minimizedbar').attr('class'));
            $('#minimizedbar').addClass('ishidden');
            $('#stikinotescontainer').removeClass($('#stikinotescontainer').attr('class'));
            $('#stikinotescontainer').addClass('isvisible');
            $('#menubar').removeClass($('#menubar').attr('class'));
            $('#menubar').addClass('isvisible');
            $('#rightcontainer').removeClass($('#rightcontainer').attr('class'));
            $('#rightcontainer').addClass('isvisible');
            $('#leftcontainer').removeClass($('#leftcontainer').attr('class'));
            $('#leftcontainer').addClass('isvisible');
            this.isMinimized = false;
            this.setCookie("isMinimized", false);
        }, this));
        // Actions - End
    }
}
SUGAR.App.events.on("app:sync:complete", function() {
    var user_id = SUGAR.App.user.get("id");
    if (SUGAR.App.config.platform != 'mobile') { // do not show the stikinote if user is in mobile view
        if (_.isEmpty(user_id)) {
            return;
        }
        $(document).ready(function() {
            //initialize the Note object
            const note = new StikiNote();
            //calling fetchNotes
            note.fetchNotes();
        });
    }
});
