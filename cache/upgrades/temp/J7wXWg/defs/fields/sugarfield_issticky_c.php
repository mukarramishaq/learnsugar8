<?php
 // created: 2018-06-26 16:18:31
$customfielddef =
 array(
    'name'       => 'issticky_c',
    'vname'      => 'LBL_ISSTICKY_C',
    'type'       => 'bool',
    'source'     => 'custom_fields',
    'studio'     => false,
    'default'    => 0,
    'reportable' => false,
    'massupdate' => false,
    'importable' => false,
    'module'     => 'Notes',
 );
return $customfielddef;
