<?php 

// License view in admin
$admin_option_defs=array();												       
$admin_option_defs['Administration']['rt_maps_license_view']= array('rt_maps','LBL_RT_MAPS_LICENSE','LBL_RT_MAPS_LICENSE_DESC','javascript:parent.SUGAR.App.router.navigate("rt_maps/layout/license", {trigger: true});');
$admin_option_defs['Administration']['rt_maps_configure_users']= array('rt_maps','LBL_RT_MAPS_CONFIGURE_USERS_TITLE','LBL_RT_MAPS_CONFIGURE_USERS','javascript:parent.SUGAR.App.router.navigate("#rt_maps/layout/userconfiguration", {trigger: true});');
$admin_option_defs['Administration']['rt_maps_apikey_view']= array('rt_maps','LBL_RT_MAPS_APIKEY','LBL_RT_MAPS_APIKEY_DESC','javascript:parent.SUGAR.App.router.navigate("rt_maps/layout/apikey", {trigger: true});');
$admin_group_header[]= array('License Verification','',false,$admin_option_defs, 'Sales Map License Verification');

?>
