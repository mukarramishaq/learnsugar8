({
    className: 'list-view tleft',
    _render: function () {

        var self = this;
        document.getElementsByName('create_button')[0].style.visibility = 'hidden';
        $(".search-name").hide();
        var prefsURL = app.api.buildURL('validateModuleLicense/prefs/', null, null, {
            oauth_token: app.api.getOAuthToken()
        });
        app.api.call('POST', prefsURL, null, {
            success: _.bind(function (result) {
                if (result == true) {
                    app.view.View.prototype._render.call(self);
                    // setTimeout(function () {
                    //    self.loadMap();
                    // }, 5000);
                    self.fetchGecodedInfo();
                } else {
                    var msg = {autoClose: false, level: 'error'};
                    msg.messages = result;
                    app.alert.show('rtGSync_config', msg);
                }
            }, this),
            error: _.bind(function (e) {
                this.configError(e);
            }, this)
        });
    },
    events: {
        'click #loadmap': 'loadMap',
        'mouseenter  [rel="tooltip"]': 'showAddressInfo',
        'mouseleave  [rel="tooltip"]': 'hideAddressInfo',
        'click #scroller': 'scrollerHandler',
    },
    configError: function (error) {
        var msg = {autoClose: false, level: 'error'};
        if (error && _.isString(error.message)) {
            msg.messages = error.message;
        }
        if (error.status == 412 && !error.request.metadataRetry) {
            msg.messages = 'If this page does not reload automatically, please try to reload manually';
        } else {
            app.alert.show('rtGSync_config', msg);
        }
        if (error && _.isString(error.message)) {
            msg.messages = error.message;
        }
        app.alert.dismiss('rtGSync_config');
        app.logger.error('Failed: ' + error);
        if (typeof error.status != 'undefined')
        {
            if (error.status == 400)
            {
                if (typeof error.responseText != 'undefined')
                {
                    var msg = {autoClose: false, level: 'error'};
                    msg.messages = error.responseText;
                    app.alert.show('salesmap_config', msg);
                }
            }
        }
    },
    fetchGecodedInfo: function () {
        var apiURL = app.api.buildURL('MyEndpoint/getSettingData/', null, null, {
            oauth_token: app.api.getOAuthToken()
        });
        app.api.call('GET', apiURL, null, {
            success: _.bind(function (result) {
                //total and non-geo coded stats
                $("#total_leads").append(result[0]['total_leads']);
                $("#total_leads_non_geo").append(result[0]['total_leads_non_geo']);
                $("#total_contacts").append(result[0]['total_contacts']);
                $("#total_contacts_non_geo").append(result[0]['total_contacts_non_geo']);
                $("#total_accounts").append(result[0]['total_accounts']);
                $("#total_accounts_non_geo").append(result[0]['total_accounts_non_geo']);
            }, this),
            error: _.bind(function (e) {
                console.log(e);
            }, this)

        });
    },
    scrollerHandler: function () {
        $("#result-panel").hide();
        $("#scroller").hide();
    },
    validateLicense: function () {
        app.alert.dismissAll();
        var prefsURL = app.api.buildURL('validateModuleLicense/prefs/', null, null, {
            oauth_token: app.api.getOAuthToken(),
            user_id: app.user.get('id'),
        });
        var self = this;
        app.api.call('POST', prefsURL, null, {
            success: _.bind(function (result) {
                if (result.data.isValidated === false) {
                    SUGAR.App.router.navigate(parent.SUGAR.App.router.buildRoute('rt_maps') + '/layout/license', {trigger: true});
                } else {
                    self.checkSettings();
                    self.render();
                    self.refreshLibs();
                }
            }, this),
            error: _.bind(function (e) {
                msg = {autoClose: false, level: 'error', messages: 'License API error'};
                app.alert.show('salesMap_config', msg);
            }, this)
        });
    },
    showAddressInfo: function () {
        $("#address_info").tooltip({
            html: true,
            placement: 'right',
            trigger: 'hover',
            title: 'For best results use the following address format </br> Postal code, City, Country  </br> e.g: SE1 1JW, London, United Kingdom',
        });
        $('#address_info').tooltip('show');

    },
    hideAddressInfo: function () {
        $('#address_info').tooltip('hide')
    },
    refreshLibs: function () {
        var loadpage = getCookie("loadpage");
        if (loadpage == 'yes') {
            del_cookie("loadpage");
            location.reload();
        } else {
            setCookie("loadpage", "yes", "1");
        }
    },
    loadMap: function () {
        if (typeof google == 'undefined') {
            var licenseURL = app.api.buildURL('salesMapLicense/loadGAPIkey', null, null);
            app.api.call('GET', licenseURL, null, {
                success: _.bind(function (result) {
                    if (result) {
                        var js = document.createElement("script");
                        js.type = "text/javascript";
                        var key = result.apibw_key;
                        js.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&callback=load_cstm&key=" + key;
                        document.body.appendChild(js);
                    } else {
                        SUGAR.App.router.navigate('rt_maps/layout/apikey', {trigger: true});
                    }
                }, this)
            });

        }
        else {
            load_cstm();
        }
    },
    hideAllElements: function () {
        document.getElementById('leads_label').style.display = "none";
        document.getElementById('leads').style.display = "none";
        document.getElementById('accounts_label').style.display = "none";
        document.getElementById('accounts').style.display = "none";
        document.getElementById('contacts_label').style.display = "none";
        document.getElementById('contacts').style.display = "none";

    },
})
