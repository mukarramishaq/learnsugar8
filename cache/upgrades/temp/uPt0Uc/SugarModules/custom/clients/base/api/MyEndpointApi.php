<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * Helper call for addresses
 * */
require_once('custom/include/rt_maps/helper/addresshelper.php');

class MyEndpointApi extends SugarApi {

    public function registerApiRest() {
        return array(
            'getUserSettingData' => array(
                'reqType' => 'GET',
                'path' => array('MyEndpoint', 'getSettingData'),
                'pathVars' => array('', '', 'data'),
                'method' => 'getSettingData',
                'shortHelp' => "Get user settings",
                'longHelp' => '',
            ),
            'salesMapValidateLicense' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'validate', '?'),
                'pathVars' => array('', '', 'key'),
                'method' => 'validate',
                'shortHelp' => 'This method validates SugarOutfitter key',
                'longHelp' => '',
            ),
            'salesMapSaveAPIKey' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'savekey', '?', '?'),
                'pathVars' => array('', '', 'bwkey', 'srkey'),
                'method' => 'savekey',
                'shortHelp' => 'This method save the Google API key',
                'longHelp' => '',
            ),
            'salesMaploadGAPIkey' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'loadGAPIkey'),
                'pathVars' => array('', ''),
                'method' => 'loadGAPIkey',
                'shortHelp' => 'This method fetch the Google API key',
                'longHelp' => '',
            ),
            'salesMapStatistics' => array(
                'reqType' => 'GET',
                'path' => array('salesMapStatistics', 'statisics'),
                'pathVars' => array('', ''),
                'method' => 'statisics',
                'shortHelp' => 'This function count the leads, accounts and contacts records ',
                'longHelp' => '',
            ),
            'validateModuleLicense' => array(
                'reqType' => 'GET',
                'path' => array('validateModuleLicense', 'prefs'),
                'pathVars' => array('', ''),
                'method' => 'validateModule',
                'shortHelp' => 'This method is used for validating license for modules',
                'longHelp' => '',
            ),
            'getUserConfig' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'getUserConfiguration'),
                'pathVars' => array('', ''),
                'method' => 'getUserConfiguration',
                'shortHelp' => 'This method is used for user configuration options',
                'longHelp' => '',
            ),
            'setUserConfig' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'setUserConfig', '?'),
                'pathVars' => array('', '', 'selectedUserIDS'),
                'method' => 'setUserConfig',
                'shortHelp' => 'This method is used for saving user configuration options',
                'longHelp' => '',
            ),
            'salesMapIncreaseLicense' => array(
                'reqType' => 'GET',
                'path' => array('salesMapLicense', 'change', '?'),
                'pathVars' => array('', '', 'key'),
                'method' => 'change',
                'shortHelp' => 'This method boost user count for GSync',
                'longHelp' => '',
            ),
        );
    }

    //boost user count
    public function change($api, $args) {
        if (isset($args) && isset($args['key'])) {
            $_REQUEST['key'] = $args['key'];
        }
        if (isset($args) && isset($args['user_count'])) {
            $_REQUEST['user_count'] = $args['user_count'];
        }
        require_once('custom/include/rt_maps/license/OutfittersLicense.php');

        if (!isset($GLOBALS['currentModule'])) {
            $GLOBALS['currentModule'] = "rt_maps";
        }
        return OutfittersLicense::change();
    }

    //check in db if column exist or not
    function dbFieldExists($table, $column) {
        /* $sql="SELECT count(*) as count FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='".$GLOBALS['sugar_config']['dbconfig']['db_name']."' AND TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$column."'";
          $r=$GLOBALS['db']->query($sql);
          $field=$GLOBALS['db']->fetchByAssoc($r); */

        global $db;
        $cols = $db->get_columns($table);

        //if(isset($field) && isset($field['count']) && $field['count'] > 0){
        if (is_array($cols)) {
            if (isset($cols[$column])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function setUserConfig($api, $args) {
        if (!isset($GLOBALS['currentModule'])) {
            $GLOBALS['currentModule'] = "rt_maps";
        }
        $args['selectedUserIDS'] = json_decode(base64_decode($args['selectedUserIDS']), 1);
        $data = array();
        $data['isRepaired'] = true;
        if (!is_admin($GLOBALS['current_user'])) {
            sugar_die("Unauthorized access to administration.");
        }
        if ($this->dbFieldExists('so_users', 'user_id')) {
            if (!isset($args['selectedUserIDS'])) {
                $args['selectedUserIDS'] = array();
            }
            /*
              $enabled = $args['selectedUserIDS'];
              $enabled = "'".implode("','",$enabled)."'";
              $update_enabled_users_query="UPDATE users SET enable_gsync = CASE WHEN id IN ($enabled) THEN 1 WHEN id NOT IN ($enabled) THEN 0 ELSE enable_gsync=enable_gsync END";
              $GLOBALS['db']->query($update_enabled_users_query);
              $data['enabled_active_users']=$args['selectedUserIDS'];
             */
            require_once('custom/include/rt_maps/license/OutfittersLicense.php');
            $_REQUEST['licensed_users'] = $args['selectedUserIDS'];
            OutfittersLicense::add();
            $data['enabled_active_users'] = $args['selectedUserIDS'];
        } else {
            $data['isRepaired'] = false;
        }
        return array('data' => $data);
    }

    public function getUserConfiguration($api, $args) {
        require_once('custom/include/rt_maps/license/config.php');
        require_once 'include/SugarQuery/SugarQuery.php';
        global $sugar_config;
        $admin = new Administration();
        $admin->retrieveSettings();
        $last_validation = $admin->settings['SugarOutfitters_' . $outfitters_config['shortname']];
        $trimmed_last = trim($last_validation); //to be safe...

        $last_validation = base64_decode($last_validation);
        $last_validation = unserialize($last_validation);
        $data = array();
        $data['isRepaired'] = true;
        $data['isValidated'] = true;
        $enabled_active_users = array();
        $active_users = array();
        if ($this->dbFieldExists('so_users', 'user_id')) {
            //$enabled_active_users = get_user_array(FALSE, 'Active', '', false, '', " AND is_group=0 AND enable_gsync=1");
            //get active users
            $active_users = get_user_array(FALSE, 'Active', '', false, '', " AND is_group=0");
            //get gsync enabled users
            //$sql2 = "select so_users.user_id,users.user_name from users,so_users where so_users.user_id = users.id ";
            $module = "Users";
            $q = new SugarQuery();
            $q->from(BeanFactory::getBean($module), array('team_security' => false));
            $q->joinTable('so_users')->on()->equalsField('id', 'so_users.user_id');
            $q->select(array('user_name', array('so_users.user_id', 'user_id')));
            $result2 = $q->execute();
            //$result2 = $GLOBALS['db']->query($sql2);
            $enabled_active_users = array();
            //while($row2 = $GLOBALS['db']->fetchByAssoc($result2))
            foreach ($result2 as $row2) {
                $enabled_active_users[$row2['user_id']] = $row2['user_name'];
            }
            $data['enabled_active_users'] = $enabled_active_users;
            $data['active_users'] = $active_users;
          } else {
            $data['isRepaired'] = false; //do quick repair and rebuild
        }
        if (isset($last_validation['last_result']['result']['licensed_user_count']) &&
                !empty($last_validation['last_result']['result']['licensed_user_count']) &&
                isset($sugar_config['outfitters_licenses']) &&
                isset($sugar_config['outfitters_licenses'][$outfitters_config['shortname']])) {
            $data['licensed_user_count'] = $last_validation['last_result']['result']['licensed_user_count'];
            $data['license_key'] = $sugar_config['outfitters_licenses'][$outfitters_config['shortname']];
        } else {
            $data['isValidated'] = false;
        }
        $data['select2Onchange'] = true;
        if ($sugar_config['sugar_version']) {
            $version = explode('.', $sugar_config['sugar_version']);
            if ($version[0] >= 7 && $version[1] >= 1 && $version[2] >= 6) {
                $data['select2Onchange'] = false;
            }
        }
        return array('data' => $data);
    }

    public function getSettingData($api, $args) {

        $ad_helper = new AddressHelper();

        $setting_data = array();
        $custs = $ad_helper->get_customers_info();
        $setting_data[] = $custs;

        return $setting_data;
    }

    //validate license key
    public function validate($api, $args) {
        if (!isset($GLOBALS['currentModule'])) {
            $GLOBALS['currentModule'] = "rt_maps";
        }
        if (isset($args) && isset($args['key'])) {
            $_REQUEST['key'] = $args['key'];
        }
        require_once('custom/include/rt_maps/license/OutfittersLicense.php');
        //$result = OutfittersLicense::validate();
        return OutfittersLicense::validate();
    }

    public function validateModule($api, $args) {
        //$response=array();
        //$response = $this->getUserConfig($args);
        if (!isset($GLOBALS['currentModule'])) {
            $GLOBALS['currentModule'] = "rt_maps";
        }
        require_once('custom/include/rt_maps/license/OutfittersLicense.php');
        global $current_user;
        //$result = OutfittersLicense::validate();
        return OutfittersLicense::isValid("rt_maps", $current_user->id);
        //return $response;
    }

    function getUserConfig($args) {
        require('custom/include/rt_maps/license/config.php');
        global $sugar_config;
        $data = array();
        $data['isValidated'] = false;
        if (isset($sugar_config['outfitters_licenses'][$outfitters_config['shortname']]['license_key']) && isset($sugar_config['outfitters_licenses'][$outfitters_config['shortname']]['license']) && $sugar_config['outfitters_licenses'][$outfitters_config['shortname']]['license'] == 'validated') {
            $data['isValidated'] = true;
        }
        return array('data' => $data);
    }

    function loadGAPIkey($api, $args) {
        require_once 'modules/Configurator/Configurator.php';
        $configuratorObj = new Configurator();
        $configuratorObj->loadConfig();
        $keybw = $configuratorObj->config['GOOGLEAPIBWKEY'];
        $keysr = $configuratorObj->config['GOOGLEAPISRKEY'];
        $key = array();
        if (isset($keybw) && !empty($keybw)) {
            $key['apibw_key'] = $keybw;
        }
        if (isset($keysr) && !empty($keysr)) {
            $key['apisr_key'] = $keysr;
        }
        return $key;
    }

    function statisics($api, $args) {
        $query = new SugarQuery();
        $moduleBean = BeanFactory::getBean('Leads');
        $query->from($moduleBean, array('team_security' => false));
        $query->select()->setCountQuery();
        $results = $query->execute();
        $leads = $results[0]['record_count'];
        $query = new SugarQuery();
        $moduleBean = BeanFactory::getBean('Contacts');
        $query->from($moduleBean, array('team_security' => false));
        $query->select()->setCountQuery();
        $results = $query->execute();
        $contacts = $results[0]['record_count'];
        $query = new SugarQuery();
        $moduleBean = BeanFactory::getBean('Accounts');
        $query->from($moduleBean, array('team_security' => false));
        $query->select()->setCountQuery();
        $results = $query->execute();
        $accounts = $results[0]['record_count'];
        return array(
            'Leads' => $leads,
            'Contacts' => $contacts,
            'Accounts' => $accounts,
        );
    }

    function savekey($api, $args) {
        if (!empty($args['bwkey'])|| !empty($args['srkey'])) {
            require_once 'modules/Configurator/Configurator.php';
            $configuratorObj = new Configurator();
            $configuratorObj->loadConfig();
            $configuratorObj->config['GOOGLEAPIBWKEY'] = $args['bwkey'];
            $configuratorObj->config['GOOGLEAPISRKEY'] = $args['srkey'];
            $configuratorObj->saveConfig();
        }
        return true;
    }
}

?>
