<?php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$dictionary['rt_maps'] = array(
    'table' => 'rt_maps',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
	'userid' =>
			  array (
				'name' => 'userid',
				'vname' => 'LBL_USERID',
				'type' => 'id',
				'reportable'=>false,
			  ),	
			'Preferences' =>
			  array (
				'name' => 'Preferences',
				'vname' => 'LBL_PREFERENCES',
				'type' => 'longtext',
			  ),
			'records_per_page' => 
			  array (
				'name' => 'records_per_page',
				'vname' => 'LBL_RECORDS_PER_PAGE',
				'type' => 'int',
				'required' => true,
				'len' => '100',
				'comment' => 'The maximum no of records that map will show upon each page request for each module ',
			), 
			'module_list' => 
			  array (
				'name' => 'module_list',
				'vname' => 'LBL_MODULE_ALL',
				'type' => 'multienum',
				'options' => '',
				'source' => 'non-db',
				'comment' => 'Module drop down',
			), 
			'lead_list' => 
			  array (
				'name' => 'lead_list',
				'vname' => 'LBL_LEAD_LIST',
				'type' => 'multienum',
				'options' => 'maps_lead_list_dom',
				'source' => 'non-db',
				'comment' => 'Lead list',
			), 
			'account_list' => 
			  array (
				'name' => 'account_list',
				'vname' => 'LBL_ACCOUNT_LIST',
				'type' => 'multienum',
				'options' => 'maps_account_list_dom',
				'source' => 'non-db',
				'comment' => 'Account list',
			), 
			'contact_list' => 
			  array (
				'name' => 'contact_list',
				'vname' => 'LBL_CONTACT_LIST',
				'type' => 'multienum',
				'options' => 'maps_contact_list_dom',
				'source' => 'non-db',
				'comment' => 'Contact list',
			), 
			'postal_code' => 
			  array (
				'name' => 'postal_code',
				'vname' => 'LBL_POSTAL_CODE',
				'type' => 'varchar',
				'len' => '255',
				'source' => 'non-db',
				'comment' => 'Postal Code',
			), 
			'radius' => 
			  array (
				'name' => 'radius',
				'vname' => 'LBL_RADIUS',
				'type' => 'int',
				'source' => 'non-db',
				'comment' => 'Radius',
			), 
			'radius_in' => 
			  array (
				'name' => 'radius_in',
				'vname' => 'LBL_RADIUS_IN',
				'type' => 'radioenum',
				'options' => 'maps_radius_in_dom',
				'source' => 'non-db',
				'comment' => 'Radius radio button',
			),
				'license_key' => 
			  array (
				'name' => 'license_key',
				'vname' => 'LBL_LICENSE_KEY',
				'type' => 'varchar',
				'len' => '255',
				'comment' => 'License Key',
			), 
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
);

if (!class_exists('VardefManager')){
    require_once 'include/SugarObjects/VardefManager.php';
}
VardefManager::createVardef('rt_maps','rt_maps', array('basic','team_security','assignable'));
