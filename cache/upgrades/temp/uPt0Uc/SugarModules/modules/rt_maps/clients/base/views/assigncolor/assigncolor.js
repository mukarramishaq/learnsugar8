({
    events: {'click #save_button': 'savekey'},
    className: 'customMainPane',
    usersColor: null,
    initialize: function (options) {
      
        this._super('initiallize', [options]);

    },
    loadData: function (options) {
        var self = this;
        var licenseURL = app.api.buildURL('salesMapColorAssign/loadUserColor', null, null);
        app.api.call('GET', licenseURL, null, {
            success: _.bind(function (result) {
                if (result) {
                    self.usersColor = result;
                    self.render();
                }
            }, this)
        });
    },
    savekey: function () {

        var self = this;
        mapColors = [];
        mapcolor = $('input[type=color]');
        mapcolor.each(function (inp, dom) {
            mapColors[inp] = {'id': dom.id, 'map_color': dom.value};
        });
        var saveURL = app.api.buildURL('salesMapColorAssign/saveColor/', null, null, {ucolr: mapColors});
        //app.api.call('GET', saveURL, null, {
        app.api.call('POST', saveURL, null, {
            success: _.bind(function (result) {
                if (result) {
                    var msg = {autoClose: true, level: 'success'};
                    msg.messages = 'Colors assigned to users has been successfully saved.';
                    app.alert.show('salesmap_success', msg);
                } else {
                    var msg = {autoClose: true, level: 'error'};
                    msg.messages = 'There was an error. try again';
                    app.alert.show('salesmap_error', msg);
                }
            }, this)
        });
    },
})
