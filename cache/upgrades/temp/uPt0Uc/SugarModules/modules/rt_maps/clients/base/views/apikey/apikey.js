({
    events: {'click #save_button': 'savekey'},
    className: 'customMainPane',
    api_key: null,
    update: false, //true if previously license is saved

    initialize: function (options) {
        this._super('initialize', [options]);
    },
    loadData: function (options) {
        var self = this;

        var licenseURL = app.api.buildURL('salesMapLicense/loadGAPIkey', null, null);
        app.api.call('GET', licenseURL, null, {
            success: _.bind(function (result) {
                if (result) {
                    self.apibw_key = result.apibw_key;
                    self.apisr_key = result.apisr_key;
                    self.render();
                }
            }, this)
        });
    },
    savekey: function () {
        var self = this;
        this.apibw_key = $('input[name=apibw_key]').val();
        this.apisr_key = $('input[name=apisr_key]').val();
        if (_.isEmpty(this.apibw_key) || this.apibw_key.length != 39 || _.isEmpty(this.apisr_key) || this.apisr_key.length != 39) {
            var msg = {autoClose: true, level: 'error'};
            msg.messages = 'Enter the valid Google API keys';
            app.alert.show('salesmap_success', msg);
            return true;
        }
        var licenseURL = app.api.buildURL('salesMapLicense/savekey/' + this.apibw_key + '/' + this.apisr_key, null, null);
        app.api.call('GET', licenseURL, null, {
            success: _.bind(function (result) {
                if (result) {
                    var msg = {autoClose: true, level: 'success'};
                    msg.messages = 'Gogle API key has been saved successfully';
                    app.alert.show('salesmap_success', msg);
                    SUGAR.App.router.navigate(parent.SUGAR.App.router.buildRoute('rt_maps'), {trigger: true});
                } else {
                    var msg = {autoClose: true, level: 'error'};
                    msg.messages = 'There was an error. try again';
                    app.alert.show('salesmap_error', msg);
                }
            }, this)
        });
    },
})