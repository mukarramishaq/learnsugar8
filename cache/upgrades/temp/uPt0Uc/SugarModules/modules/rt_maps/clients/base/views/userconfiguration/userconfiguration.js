({
	className:'customMainPane',
	events:{'click #save_button':'handleSave','click #boost_button':'boostUserCount','click #cancel_button':'handleCancel','click .refreshPAGE':'refreshPAGE','click #enable_all_users': 'enable_all_users'},
	title: null,
	isRepaired: null,
	isValidated: null,
	active_users: null,
	enabled_active_users: null,
	licensed_user_count: null,
	showBoostButton: null,
	license_key: null,
	continueURL: null,
	usersObject: null,
	users_mulitiselect: [],
	selected: [],
	select2Onchange: true,
	initialize: function(options){
		if(app.user.get('type')=='admin'){
			this._super('initialize', [options]);
			this.isAdmin=true;
		}else{
			app.router.navigate('#rt_GSync',{trigger: true});
		}
		this.continueURL = "#Administration";
		this.usersObject={active: {},enabled:{},disabled:{}};
		this.title=app.lang.get('LBL_SALESMAP_USERS_CONFIGURATION',this.module);
		this.LBL_SALESMAP_ENABLED_USERS = app.lang.get('LBL_SALESMAP_ENABLED_USERS',this.module);
		this.LBL_ALL_USERS = app.lang.get('LBL_ALL_USERS',this.module);
		this.LBL_LICENSED_USERS = app.lang.get('LBL_LICENSED_USERS',this.module);
		this.LBL_SAVE_BUTTON_LABEL = app.lang.get('LBL_SAVE_BUTTON_LABEL');
		this.LBL_CANCEL_BUTTON_LABEL = app.lang.get('LBL_CANCEL_BUTTON_LABEL');
		this.LBL_BOOST_LABEL = app.lang.get('LBL_BOOST_LABEL',this.module);
	},
	_render:function(){
		var self=this;
		var data=[];
		_.each(this.usersObject.active, function (value, key) {
			var item={};
			item.id=key;
			item.text=value;
			if(self.usersObject.enabled && self.usersObject.enabled[key]){
				item.checked=true;
				self.usersObject.enabled[key]=item;
			}else{
				self.usersObject.disabled[key]=item;
			}
            data.push(item);
        });
		this.users_mulitiselect=data;
		if(this.isNumber(this.licensed_user_count) && this.isNumber(this.enabled_active_users) && this.enabled_active_users > this.licensed_user_count){
			this.showBoostButton=true;
		}else{
			this.showBoostButton=false;
		}
		app.view.View.prototype._render.call(this);
		$( "#disabled, #enabled" ).sortable({
			connectWith: ".connectedSortable",
			stop: function( event, ui ) {
				self.selectionChanged();
				
			}
		}).disableSelection();
		
	},
	selectionChanged: function(){
		this.enabled_active_users=$('#enabled li').length;
		$('#enabled_active_users').html(this.enabled_active_users);
		$('#boost_user_count').val(this.enabled_active_users);
		if(this.isNumber(this.licensed_user_count) && this.isNumber(this.enabled_active_users) && this.enabled_active_users > this.licensed_user_count){
			this.showBoostButton=true;
			$("span[data-name=boost_user_count]").show();
		}else{
			this.showBoostButton=false;
			$("span[data-name=boost_user_count]").hide();
		}
		
	},
    loadData: function (options) {
		this.getUserConfig();
    },
	getUserConfig: function () {
		app.alert.dismissAll();
		app.alert.show('rtGSync_config', {level: 'process', title: 'Processing'});
		/*
		var prefsURL = app.api.buildURL('rt_GSyncConfig/prefs/', null, null, {
			oauth_token: app.api.getOAuthToken(),
			user_id: app.user.get('id'),
			method: 'getUserConfig',
		});*/
		var prefsURL = app.api.buildURL('salesMapLicense/getUserConfiguration/', null, null, {
			oauth_token: app.api.getOAuthToken()
		});
		app.api.call('POST', prefsURL, null, {
			success: _.bind(function (result) {
				this.getUserConfigSuccess(result);
			}, this),

			error: _.bind(function (e) {
				this.configError(e);
			}, this)
		});
	},
	getUserConfigSuccess: function(response){
		app.alert.dismiss('rtGSync_config');
		if(response && response.data){
			if(response.data.isRepaired){
				this.isRepaired=response.data.isRepaired;
				if(response.data.isValidated){
					this.isValidated=response.data.isValidated;
				}else{
					// app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages:'Do validate first'});
				}
			}else{
				// app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages:'Do quick repair and rebuild first'});
			}
			if(response.data.active_users){
				this.active_users=Object.keys(response.data.active_users).length;
				this.usersObject.active=response.data.active_users;
			}
			if(response.data.enabled_active_users){
				this.enabled_active_users=Object.keys(response.data.enabled_active_users).length;
				this.usersObject.enabled=response.data.enabled_active_users;
			}
			if(response.data.licensed_user_count){
				this.licensed_user_count=response.data.licensed_user_count;
			}
			if(response.data.license_key){
				this.license_key=response.data.license_key;
			}
			if(response.data.select2Onchange){
				this.select2Onchange=response.data.select2Onchange;
			}
		}else{
			app.alert.show('salesmap_config', {autoClose: false, level: 'error',messages:'Unable to load'});
		}
		this.isLoaded=true;
		this.render();
	},

	isNumber: function (n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	},
	handleSave: function(){
		app.alert.dismiss('rtGSync_config');
		if(this.showBoostButton){
			$("span[data-name=boost_user_count]").show();
			app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages: 'Not allowed'});
			return;
		}
		app.alert.show('rtGSync_config', {level: 'process', title: 'Saving'});
		var selections=$('#enabled li');
		var selectedUserIDS=[];
		_.each(selections,function(value,key){
			selectedUserIDS.push(value.id);
		});
	/*	var prefsURL = app.api.buildURL('rt_GSyncConfig/prefs/', null, null, {
			oauth_token: app.api.getOAuthToken(),
			selectedUserIDS: selectedUserIDS,
			method: 'setUserConfig',
		}); */
		var prefsURL = app.api.buildURL('salesMapLicense/setUserConfig/'+window.btoa(JSON.stringify(selectedUserIDS)), null, null, {
			oauth_token: app.api.getOAuthToken()
		});
		app.api.call('POST', prefsURL, null, {
			success: _.bind(function (result) {
				this.saveConfigSuccess(result);
			}, this),

			error: _.bind(function (e) {
				this.configError(e);
			}, this)
		});
	},
	configError: function(error){
		var msg = {autoClose: false, level: 'error'};
		if (error && _.isString(error.message)) {
			msg.messages = error.message;
		}
		if (error.status == 412 && !error.request.metadataRetry){
			msg.messages='If this page does not reload automatically, please try to reload manually';
		}else{
			app.alert.show('rtGSync_config', msg);
		}
		if (error && _.isString(error.message)) {
			msg.messages = error.message;
		}
		app.alert.dismiss('rtGSync_config');
		app.logger.error('Failed: ' + error);
		if (typeof error.status != 'undefined')
			{
				if (error.status == 400)
					{
						if (typeof error.responseText != 'undefined')
							{
								var msg = {autoClose: false, level: 'error'};
								msg.messages = error.responseText;
								app.alert.show('salesmap_config', msg);
							}
					}
			}
	},
	saveConfigSuccess: function(response){
		app.alert.dismiss('rtGSync_config');
		var self=this;
		if(response && response.data){
			if(response.data.isRepaired){
				if(response.data.enabled_active_users){
					app.alert.show('rtGSync_config', {autoClose: true, level: 'success', messages: 'Save'});
				}else{
					app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages:'Unable to save'});
				}
			}else{
				app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages:'Do quick repair and rebuild first'});
				this.isRepaired=false;
			}
			// self.render();
		}else{
			app.alert.show('rtGSync_config', {autoClose: false, level: 'error',messages:'Unable to save'});
		}
	},
	boostUserCount: function(){
		this.key=$('input[name=license_key]').val();
		if(!this.key){
			return;
		}
		this.user_count=$('input[name=boost_user_count]').val();
		if(!this.isNumber(this.user_count)){
			alert('Invalid count');
			return;
		}
		if(this.user_count <= this.licensed_user_count){
			alert('Boost count should be greater than licensed user count');
		//	return;
		}
		var licenseURL = app.api.buildURL('salesMapLicense/change/'+this.key, null, null, {
			oauth_token: app.api.getOAuthToken(),
			user_count: this.user_count,
		});
		if(!$("#cancel_button").hasClass('hide')){
			$("#cancel_button").addClass('hide');
		}
		app.alert.show('rtGSync_config', {level: 'process', title: 'Processing'});
		app.api.call('POST', licenseURL, null, {
			success: _.bind(function (result) {
				this.boostUserCountSuccess(result);
			}, this),

			error: _.bind(function (e) {
				this.configError(e);
			}, this)
		});
	},
	boostUserCountSuccess: function(response){
		var msg={};
		app.alert.dismiss('rtGSync_config');
		if(response){
			if(response.data){
				if(response.data.licensed_user_count){
					this.licensed_user_count=response.data.licensed_user_count;
					app.alert.show('salesmap_config', {autoClose: true, level: 'success', messages: 'Boost'});
					$('#licensed_user_count').html(this.licensed_user_count);
					this.showBoostButton=false;
					this.handleSave();
				}else{
					msg= {autoClose: false, level: 'error',messages:'Unexpected data returned from server.'};
					app.alert.show('rtGSync_config', msg);
				}
			}else{
				msg = {autoClose: false, level: 'error',messages:'Unkown error'};
				app.alert.show('rtGSync_config', msg);
			}
		}else{
			msg = {autoClose: false, level: 'error',messages: 'No response received from server.'};
			app.alert.show('rtGSync_config', msg);
		}
	},
	handleCancel: function(){
		app.router.navigate(this.continueURL,{trigger: true});
	},
	refreshPAGE: function(){
		this.getUserConfig();
	},
	enable_all_users: function(){
		var selections=$('#disabled li');
		_.each(selections,function(value,key){
			$('#enabled').append(value);
		});
		if(selections && selections.length){
			this.selectionChanged();
		}
	},
})