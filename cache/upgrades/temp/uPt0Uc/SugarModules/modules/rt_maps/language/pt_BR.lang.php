<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id Equipe',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_DELETED' => 'Eliminado',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DOC_OWNER' => 'Dono do Documento',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_NAME' => 'Nome',
  'LBL_REMOVE' => 'Remover',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap Lista',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'Minha RT SalesMap',
  'LNK_NEW_RECORD' => 'Criar RT SalesMap',
  'LNK_LIST' => 'Vista RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'Novo RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);