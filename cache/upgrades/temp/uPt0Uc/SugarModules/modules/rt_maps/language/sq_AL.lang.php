<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_DELETED' => 'E fshirë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DOC_OWNER' => 'Pronari i Dokumentit',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_NAME' => 'Emri',
  'LBL_REMOVE' => 'Fshijë',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap lista',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'Mia RT SalesMap',
  'LNK_NEW_RECORD' => 'Krijo RT SalesMap',
  'LNK_LIST' => 'Pamje RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'E re RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);