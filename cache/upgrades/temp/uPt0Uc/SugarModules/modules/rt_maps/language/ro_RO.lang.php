<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_DELETED' => 'Sters',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_NAME' => 'Nume',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap Lista',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'Al meu RT SalesMap',
  'LNK_NEW_RECORD' => 'Creeaza RT SalesMap',
  'LNK_LIST' => 'Vizualizare RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'Nou RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);