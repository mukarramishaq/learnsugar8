<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

function pre_install() {

    require_once('ModuleInstall/PackageManager/PackageManager.php');
    $pm = new PackageManager();
    $installeds = $pm->getinstalledPackages();
    foreach ($installeds as $package) {
        if ($package['name'] == 'RT SalesMap') {
            echo "RT SalesMap Package is already installed, first UnInstall it";
            die();
        }
    }


//if package name match not found in module loader and package files exist in sugar
    $expired = false;
//check if 5 minutes have passed since user clicked continue
    if (isset($_COOKIE['preInstallFlag'])) {
        $now = time();
        $minutesPassed = ceil(($now - $_COOKIE['preInstallFlag']) / 60);
        if ($minutesPassed > 5) {
            $expired = true;
        }
    }

    if (!isset($_COOKIE['preInstallFlag']) || $expired == true) {
        if (sugar_is_file("modules/rt_maps/rt_maps_sugar.php") && sugar_is_file("modules/rt_maps/rt_maps.php")) {

//java script function that will store current time stamp in cookie variable
            echo "<script> function continue_again(){
    var stamp = Math.ceil(Date.now()/1000);
    //document.cookie = 'preInstallFlag ='+stamp;
    var response = document.getElementById('continue');
    response.form.submit();
}</script>";

            echo "RT SalesMap Package is not properly UnInstalled, Installing it now can cause conflict with previous files, Click Continue to Install!";
            echo "<br>";
            echo '<input type="button" value = "Continue" onclick="continue_again()"></input>';

            /*
              //getting page url
              $pageURL = 'http';
              if ($_SERVER["HTTPS"] == "on") { $pageURL .= "s"; }
              $pageURL .= "://";
              if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
              {
              $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
              }
              else
              {
              $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
              }
              //echo "The url for this page is ".$pageURL." <br />";
             */
            echo '<form action="index.php?module=Administration&view=module&action=UpgradeWizard_prepare" method="post">';

            echo '<input type="hidden" name="continue" id="continue" >';
            if (isset($_REQUEST["install_file"])) {
                if (strpos($_REQUEST["install_file"], 'upload') === 0) {
                    $install_file = str_replace('upload', 'upload:/', $_REQUEST["install_file"]);
                    $install_file = fileToHash($install_file);
                    echo '<input type="hidden" name="install_file" id="install_file" value ="' . $install_file . '" >';
                }
            }
            echo '<input type="hidden" name="mode" id="mode" value ="' . $_REQUEST['mode'] . '" >';

            echo "</form>";
            $stamp = time();
            setcookie("preInstallFlag", $stamp);
            die();
        }
    } else {
        echo "<script>
    var name = 'preInstallFlag';
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    </script>";
    }
}

?>
