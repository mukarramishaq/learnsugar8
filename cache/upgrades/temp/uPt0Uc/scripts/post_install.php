<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

//At bottom of post_install - redirect to license validation page - CHANGE NAME BELOW - To your module name

function post_install() {
    //install table for user management
    global $db;
    if (!$db->tableExists('so_users')) {

        $fieldDefs = array(
            'id' => array(
                'name' => 'id',
                'vname' => 'LBL_ID',
                'type' => 'id',
                'required' => true,
                'reportable' => true,
            ),
            'deleted' => array(
                'name' => 'deleted',
                'vname' => 'LBL_DELETED',
                'type' => 'bool',
                'default' => '0',
                'reportable' => false,
                'comment' => 'Record deletion indicator',
            ),
            'shortname' => array(
                'name' => 'shortname',
                'vname' => 'LBL_SHORTNAME',
                'type' => 'varchar',
                'len' => 255,
            ),
            'user_id' => array(
                'name' => 'user_id',
                'rname' => 'user_name',
                'module' => 'Users',
                'id_name' => 'user_id',
                'vname' => 'LBL_USER_ID',
                'type' => 'relate',
                'isnull' => 'false',
                'dbType' => 'id',
                'reportable' => true,
                'massupdate' => false,
            ),
        );

        $indices = array(
            'id' => array(
                'name' => 'so_userspk',
                'type' => 'primary',
                'fields' => array(
                    0 => 'id',
                ),
            ),
            'shortname' => array(
                'name' => 'shortname',
                'type' => 'index',
                'fields' => array(
                    0 => 'shortname',
                ),
            ),
        );
        $db->createTableParams('so_users', $fieldDefs, $indices);
    }

    if (createJOB('Calculate Latitude and Longitude', 'function::CalculateLatLong', '*/5::*::*::*::*') === true) {
        $GLOBALS['log']->debug('Calculate Latitude and Longitude job Created');
    }



    // Repair and rebuild
    repair_and_rebuild();
    // redirect to configuration
    redirectToConfg();
}

function createJOB($name, $job, $job_interval) {
    $scheduler = BeanFactory::getBean('Schedulers');
    $scheduler->retrieve_by_string_fields(array('job' => $job, 'deleted' => '0'));
    //if job already created do nothing just return with false
    if (!empty($scheduler->id)) {
        return false;
    }
    $scheduler->name = $name;
    $scheduler->job = $job;
    $scheduler->date_time_start = '2005-01-01 00:00:00';
    $scheduler->job_interval = $job_interval;
    $scheduler->status = 'Active';
    $scheduler->catch_up = '1';
    if ($scheduler->save()) {
        return true;
    }
    return false;
}

function repair_and_rebuild() {
    require_once("modules/Administration/QuickRepairAndRebuild.php");
    $autoexecute = true; //execute the SQL
    $show_output = false; //output to the screen
    $rac = new RepairAndClear();
    $rac->repairAndClearAll(array('clearAll'), array(translate('LBL_ALL_MODULES')), $autoexecute, $show_output);
}

function redirectToConfg() {
  $script = '<script type="text/javascript">
    var pathName = parent.window.location.pathname;
    var newPathName = pathName.replace("index.php","");
    newPathName = newPathName.slice(0, -1);
    parent.window.history.pushState("", "", newPathName + "/#rt_maps/layout/license");
    parent.window.location.reload();
  </script>';
  echo $script;
}
