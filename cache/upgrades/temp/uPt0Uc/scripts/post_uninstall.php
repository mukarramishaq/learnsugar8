<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/* 
    Deletes 'Calculate Latitude and Longitude' Scheduler Job after uninstallation of the plugin
*/
function deleteSchedulerJobs()
{
    //Deleting SalesMap related scheduler jobs
    $job_names = array('Calculate Latitude and Longitude');
    foreach ($job_names as $job_name) {
        $scheduler = BeanFactory::getBean('Schedulers');
        $scheduler->retrieve_by_string_fields(array('name' => $job_name));
        if (!empty($scheduler->id)) {
            $scheduler->mark_deleted($scheduler->id);
            $scheduler->save();
        }
    }
    return true;
}

try {
    deleteSchedulerJobs();
    $GLOBALS['log']->fatal("RT SalesMap uninstalled successfully...");
} catch (Exception $e) {
    $GLOBALS['log']->fatal("Error: " . $e->getMessage());
}
