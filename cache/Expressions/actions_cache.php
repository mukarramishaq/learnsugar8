<?php
// created: 2018-10-17 11:37:49
$actions = array (
  'AssignTo' => 
  array (
    'class' => 'AssignToAction',
    'file' => 'include/Expressions/Actions/AssignToAction.php',
  ),
  'SetPanelVisibility' => 
  array (
    'class' => 'PanelVisibilityAction',
    'file' => 'include/Expressions/Actions/PanelVisibilityAction.php',
  ),
  'SetRequired' => 
  array (
    'class' => 'SetRequiredAction',
    'file' => 'include/Expressions/Actions/SetRequiredAction.php',
  ),
  'ReadOnly' => 
  array (
    'class' => 'ReadOnlyAction',
    'file' => 'include/Expressions/Actions/ReadOnlyAction.php',
  ),
  'Style' => 
  array (
    'class' => 'StyleAction',
    'file' => 'include/Expressions/Actions/StyleAction.php',
  ),
  'SetVisibility' => 
  array (
    'class' => 'VisibilityAction',
    'file' => 'include/Expressions/Actions/VisibilityAction.php',
  ),
  'SetOptions' => 
  array (
    'class' => 'SetOptionsAction',
    'file' => 'include/Expressions/Actions/SetOptionsAction.php',
  ),
  'SetValue' => 
  array (
    'class' => 'SetValueAction',
    'file' => 'include/Expressions/Actions/SetValueAction.php',
  ),
);