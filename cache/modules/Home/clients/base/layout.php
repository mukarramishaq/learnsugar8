<?php
$clientCache['Home']['base']['layout'] = array (
  'record' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'base',
            'name' => 'main-pane',
            'css_class' => 'main-pane home-dashboard row-fluid',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'name' => 'dashboard',
                  'type' => 'dashboard',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'dashboard-headerpane',
                      'loadModule' => 'Dashboards',
                    ),
                    1 => 
                    array (
                      'layout' => 'dashlet-main',
                    ),
                  ),
                  'last_state' => 
                  array (
                    'id' => 'last-visit',
                  ),
                ),
                'loadModule' => 'Dashboards',
              ),
            ),
          ),
        ),
      ),
      'last_state' => 
      array (
        'id' => 'last-visit',
      ),
    ),
  ),
  'dashboard' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Home.DashboardLayout
 * @alias SUGAR.App.view.layouts.HomeDashboardLayout
 * @extends View.Layouts.DashboardLayout
 * @deprecated 7.9.0 Will be removed in 7.11.0. Use
 *   {@link View.Layouts.Dashboards.DashboardLayout} instead.
 */
({
    extendsFrom: \'DashboardLayout\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Layouts.Home.DashboardLayout has been deprecated since 7.9.0.0. \' +
        \'It will be removed in 7.11.0.0. Please use View.Layouts.Dashboards.DashboardLayout instead.\');
        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'about' => 
  array (
    'meta' => 
    array (
      'css_class' => 'row-fluid',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'css_class' => 'main-pane span12',
            'components' => 
            array (
              0 => 
              array (
                'view' => 'about-headerpane',
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'fluid',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'about-copyright',
                        'span' => 12,
                      ),
                    ),
                  ),
                ),
              ),
              2 => 
              array (
                'layout' => 
                array (
                  'type' => 'fluid',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'about-resources',
                        'span' => 6,
                      ),
                    ),
                    1 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'about-source-code',
                        'span' => 6,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'search-dashboard' => 
  array (
    'meta' => 
    array (
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'search-facet',
                    'facet_id' => 'assigned_user_id',
                    'custom_toolbar' => 'no',
                    'label' => 'LBL_FACET_ASSIGNED_TO_ME',
                    'ui_type' => 'single',
                  ),
                  'width' => 12,
                ),
                1 => 
                array (
                  'view' => 
                  array (
                    'type' => 'search-facet',
                    'facet_id' => 'favorite_link',
                    'custom_toolbar' => 'no',
                    'label' => 'LBL_FACET_MY_FAVORITES',
                    'ui_type' => 'single',
                  ),
                  'width' => 12,
                ),
                2 => 
                array (
                  'view' => 
                  array (
                    'type' => 'search-facet',
                    'facet_id' => 'created_by',
                    'custom_toolbar' => 'no',
                    'label' => 'LBL_FACET_CREATED_BY_ME',
                    'ui_type' => 'single',
                  ),
                  'width' => 12,
                ),
                3 => 
                array (
                  'view' => 
                  array (
                    'type' => 'search-facet',
                    'facet_id' => 'modified_user_id',
                    'custom_toolbar' => 'no',
                    'label' => 'LBL_FACET_MODIFIED_BY_ME',
                    'ui_type' => 'single',
                  ),
                  'width' => 12,
                ),
                4 => 
                array (
                  'view' => 
                  array (
                    'type' => 'search-facet',
                    'label' => 'LBL_FACET_MODULES',
                    'facet_id' => 'modules',
                    'ui_type' => 'multi',
                    'custom_toolbar' => 
                    array (
                      'buttons' => 
                      array (
                        0 => 
                        array (
                          'type' => 'dashletaction',
                          'css_class' => 'dashlet-toggle btn btn-invisible minify',
                          'icon' => 'fa-chevron-up',
                          'action' => 'toggleMinify',
                          'tooltip' => 'LBL_DASHLET_TOGGLE',
                        ),
                      ),
                    ),
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 12,
          ),
        ),
      ),
      'name' => 'LBL_FACETS_DASHBOARD_TITLE',
      'css_class' => 'facets-dashboard',
      'drag_and_drop' => false,
    ),
  ),
  'list' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Home.ListLayout
 * @alias SUGAR.App.view.layouts.HomeListLayout
 * @extends View.DashboardLayout
 * @deprecated 7.9.0 Will be removed in 7.11.0. Use
 *   {@link View.Layouts.Home.Record} instead.
 */
({
    extendsFrom: \'DashboardLayout\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Layouts.Home.ListLayout has been deprecated since 7.9.0.0. \' +
        \'It will be removed in 7.11.0.0. Please use View.Layouts.Home.Record instead.\');
        this._super(\'initialize\', [options]);
    }
})

',
    ),
    'meta' => 
    array (
      'type' => 'dashboard',
      'name' => 'dashboard',
      'components' => 
      array (
        0 => 
        array (
          'view' => 'dashboard-headerpane',
        ),
      ),
      'last_state' => 
      array (
        'id' => 'last-visit',
      ),
    ),
  ),
  'record-dashboard' => 
  array (
    'meta' => 
    array (
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'twitter',
                    'label' => 'LBL_DASHLET_RECENT_TWEETS_SUGARCRM_NAME',
                    'twitter' => 'sugarcrm',
                    'limit' => 20,
                  ),
                  'width' => 12,
                ),
              ),
              1 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'dashablelist',
                    'label' => 'TPL_DASHLET_MY_MODULE',
                    'display_columns' => 
                    array (
                      0 => 'full_name',
                      1 => 'account_name',
                      2 => 'phone_work',
                      3 => 'title',
                    ),
                    'limit' => 15,
                  ),
                  'context' => 
                  array (
                    'module' => 'Contacts',
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 4,
          ),
          1 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'sales-pipeline',
                    'label' => 'LBL_DASHLET_PIPLINE_NAME',
                    'visibility' => 'user',
                  ),
                  'width' => 12,
                ),
              ),
              1 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'bubblechart',
                    'label' => 'LBL_DASHLET_TOP10_SALES_OPPORTUNITIES_NAME',
                    'filter_duration' => 'current',
                    'visibility' => 'user',
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 8,
          ),
        ),
      ),
      'name' => 'LBL_HOME_DASHBOARD',
    ),
  ),
  'records' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Home.RecordsLayout
 * @alias SUGAR.App.view.layouts.HomeRecordsLayout
 * @extends View.Layout
 * @deprecated 7.9.0 Will be removed in 7.11.0. Use
 *   {@link View.Layouts.Home.Record} instead.
 */
({
    /**
     * @inheritdoc
     */
    initialize: function(options) {

        app.logger.warn(\'View.Layouts.Home.RecordsLayout has been deprecated since 7.9.0.0. \' +
        \'It will be removed in 7.11.0.0. Please use View.Layouts.Home.Record instead.\');
        this._super(\'initialize\', [options]);
    }
})
',
    ),
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'list',
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'preview',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  '_hash' => '7747906cd966bf4445bcdedfd7416303',
);

