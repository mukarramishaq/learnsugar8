<?php
$clientCache['Home']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Home/create',
        'label' => 'LBL_CREATE_DASHBOARD_MENU',
        'acl_action' => 'edit',
        'acl_module' => 'Home',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#activities',
        'label' => 'LBL_ACTIVITIES',
        'icon' => 'fa-clock-o',
      ),
      2 => 
      array (
        'type' => 'divider',
      ),
      3 => 
      array (
        'route' => '#Dashboards?moduleName=Home',
        'label' => 'LBL_MANAGE_DASHBOARDS',
        'acl_action' => 'read',
        'acl_module' => 'Dashboards',
        'icon' => 'fa-bars',
        'label_module' => 'Dashboards',
      ),
    ),
  ),
  '_hash' => 'f0341b2d84ed3b5a1c63d7b7334514b2',
);

