<?php
$clientCache['Meetings']['base']['dependency'] = array (
  '_hash' => '40cd750bba9870f18aada2478b24840a',
  'dependencies' => 
  array (
    0 => 
    array (
      'name' => 'password_vis',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'type',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetVisibility',
          'params' => 
          array (
            'target' => 'password',
            'value' => 'isInEnum($type,getDD("extapi_meeting_password"))',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    1 => 
    array (
      'name' => 'displayed_url_vis',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'type',
        1 => 'type',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetVisibility',
          'params' => 
          array (
            'target' => 'displayed_url',
            'value' => 'and(isAlpha($type),not(equal($type,"Sugar")))',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    2 => 
    array (
      'name' => 'repeat_selectorDDD',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'repeat_type',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetOptions',
          'params' => 
          array (
            'target' => 'repeat_selector',
            'keys' => 'getListWhere($repeat_type, enum(enum("", enum("None")),enum("Daily", enum("None")),enum("Weekly", enum("None")),enum("Monthly", enum("None","Each","On")),enum("Yearly", enum("None","On"))))',
            'labels' => '"repeat_selector_dom"',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
  ),
);

