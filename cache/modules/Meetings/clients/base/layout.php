<?php
$clientCache['Meetings']['base']['layout'] = array (
  'subpanels' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_NOTES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'notes',
          ),
        ),
      ),
    ),
  ),
  'list-dashboard' => 
  array (
    'meta' => 
    array (
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'dashablelist',
                    'label' => 'LBL_MY_SCHEDULED_MEETINGS',
                    'display_columns' => 
                    array (
                      0 => 'date_start',
                      1 => 'name',
                      2 => 'parent_name',
                    ),
                    'filter_id' => 'my_scheduled_meetings',
                  ),
                  'context' => 
                  array (
                    'module' => 'Meetings',
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 12,
          ),
        ),
      ),
      'name' => 'LBL_MEETINGS_LIST_DASHBOARD',
    ),
  ),
  'record-dashboard' => 
  array (
    'meta' => 
    array (
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'dashablelist',
                    'label' => 'LBL_MY_SCHEDULED_MEETINGS',
                    'display_columns' => 
                    array (
                      0 => 'date_start',
                      1 => 'name',
                      2 => 'parent_name',
                    ),
                    'filter_id' => 'my_scheduled_meetings',
                  ),
                  'context' => 
                  array (
                    'module' => 'Meetings',
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 12,
          ),
        ),
      ),
      'name' => 'LBL_MEETINGS_RECORD_DASHBOARD',
    ),
  ),
  '_hash' => 'df8cd0b43859f604966b8cea50ff9138',
);

