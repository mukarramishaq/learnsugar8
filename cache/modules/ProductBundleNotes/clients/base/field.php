<?php
$clientCache['ProductBundleNotes']['base']['field'] = array (
  'quote-data-actionmenu' => 
  array (
    'templates' => 
    array (
      'list' => '
<div class="btn btn-invisible checkall">
    <input data-check="one" type="checkbox" name="check" {{#if model.selected}}checked{{/if}}>
</div>
{{#if dropdownFields}}
    <a track="click:listactiondropdown" class="btn btn-invisible dropdown-toggle"
       data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
       rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
       role="button" tabindex="{{tabIndex}}" aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}">
        <span class="fa fa-ellipsis-v"></span></a>
    <ul data-menu="dropdown" class="dropdown-menu" role="menu"
        data-row-module="{{../model.module}}" data-row-model-id="{{../model.cid}}"></ul>
{{/if}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundleNotes.QuoteDataActionmenuField
 * @alias SUGAR.App.view.fields.BaseProductBundleNotesQuoteDataActionmenuField
 * @extends View.Fields.Base.ActionmenuField
 */
({
    /**
     * @inheritdoc
     */
    extendsFrom: \'ActionmenuField\',

    /**
     * Skipping ActionmenuField\'s override, just returning this.def.buttons
     *
     * @inheritdoc
     */
    _getChildFieldsMeta: function() {
        return app.utils.deepCopy(this.def.buttons);
    },

    /**
     * Triggers massCollection events to the context.parent
     *
     * @inheritdoc
     */
    toggleSelect: function(checked) {
        var event = !!checked ? \'mass_collection:add\' : \'mass_collection:remove\';
        this.model.selected = !!checked;
        this.context.parent.trigger(event, this.model);
    }
})
',
    ),
  ),
  'textarea' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundleNotes.TextareaField
 * @alias SUGAR.App.view.fields.BaseProductBundleNotesTextareaField
 * @extends View.Fields.Base.BaseTextareaField
 */
({
    extendsFrom: \'BaseTextareaField\',

    /**
     * Having to override because we do want it to go to edit in the list
     * contrary to everywhere else in the app
     *
     * @inheritdoc
     */
    setMode: function(name) {
        // skip textarea\'s setMode and call straight to Field.setMode
        app.view.Field.prototype.setMode.call(this, name);
    }
});
',
    ),
  ),
  'quote-data-editablelistbutton' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundleNotes.EditablelistbuttonField
 * @alias SUGAR.App.view.fields.BaseProductBundleNotesEditablelistbuttonField
 * @extends View.Fields.Base.BaseEditablelistbuttonField
 */
({
    extendsFrom: \'BaseEditablelistbuttonField\',

    /**
     * Overriding EditablelistbuttonField\'s Events with mousedown instead of click
     */
    events: {
        \'mousedown [name=inline-save]\': \'saveClicked\',
        \'mousedown [name=inline-cancel]\': \'cancelClicked\'
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        this._super(\'_render\');

        if (_.isUndefined(this.changed) && this.model.isNew()) {
            // when adding additional items to the list, causing additional renders,
            // this.changed gets set undefined on re-initialize, so we need to make sure
            // if this is an unsaved model and this.changed is undefined, that we set changed true
            this.changed = true;
        }

        if (this.tplName === \'edit\') {
            this.$el.closest(\'.left-column-save-cancel\').addClass(\'higher\');
        } else {
            this.$el.closest(\'.left-column-save-cancel\').removeClass(\'higher\');
        }
    },

    /**
     * Overriding and not calling parent _loadTemplate as those are based off view/actions and we
     * specifically need it based off the modelView set by the parent layout for this row model
     *
     * @inheritdoc
     */
    _loadTemplate: function() {
        this.tplName = this.model.modelView || \'list\';

        if (this.view.action === \'list\' && _.indexOf([\'edit\', \'disabled\'], this.action) < 0) {
            this.template = app.template.empty;
        } else {
            this.template = app.template.getField(this.type, this.tplName, this.module);
        }
    },

    /**
     * @inheritdoc
     */
    cancelEdit: function() {
        if (this.isDisabled()) {
            this.setDisabled(false);
        }
        this.changed = false;
        this.model.revertAttributes();
        this.view.clearValidationErrors();

        // this is the only line I had to change
        this.view.toggleRow(this.model.module, this.model.cid, false);

        // trigger a cancel event across the view layout so listening components
        // know the changes made in this row are being reverted
        if (this.view.layout) {
            this.view.layout.trigger(\'editablelist:\' + this.view.name + \':cancel\', this.model);
        }
    },

    /**
     * Called after the save button is clicked and all the fields have been validated,
     * triggers an event for
     *
     * @inheritdoc
     */
    _save: function() {
        this.view.layout.trigger(\'editablelist:\' + this.view.name + \':saving\', true, this.model.cid);

        if (this.view.model.isNew()) {
            this.view.context.parent.trigger(\'quotes:defaultGroup:save\', _.bind(this._saveRowModel, this));
        } else {
            this._saveRowModel();
        }
    },

    /**
     * Saves the row\'s model
     *
     * @private
     */
    _saveRowModel: function() {
        var self = this;
        var oldModelId = this.model.id || this.model.cid;

        var successCallback = function(model) {
            self.changed = false;
            model.modelView = \'list\';
            if (self.view.layout) {
                self.view.layout.trigger(\'editablelist:\' + self.view.name + \':save\', model, oldModelId);
            }
        };
        var options = {
            success: successCallback,
            error: function(error) {
                if (error.status === 409) {
                    app.utils.resolve409Conflict(error, self.model, function(model, isDatabaseData) {
                        if (model) {
                            if (isDatabaseData) {
                                successCallback(model);
                            } else {
                                self._save();
                            }
                        }
                    });
                }
            },
            complete: function() {
                // remove this model from the list if it has been unlinked
                if (self.model.get(\'_unlinked\')) {
                    self.collection.remove(self.model, {silent: true});
                    self.collection.trigger(\'reset\');
                    self.view.render();
                } else {
                    self.setDisabled(false);
                }
            },
            lastModified: self.model.get(\'date_modified\'),
            //Show alerts for this request
            showAlerts: {
                \'process\': true,
                \'success\': {
                    messages: app.lang.get(\'LBL_RECORD_SAVED\', self.module)
                }
            },
            relate: this.model.link ? true : false
        };

        options = _.extend({}, options, this.getCustomSaveOptions(options));
        this.model.save({}, options);
    },

    /**
     * @inheritdoc
     */
    _validationComplete: function(isValid) {
        if (!isValid) {
            this.setDisabled(false);
            return;
        }
        // also need to make sure the model.changed is empty as well
        if (!this.changed && !this.model.changed) {
            this.cancelEdit();
            return;
        }

        this._save();
    }
});
',
    ),
    'templates' => 
    array (
      'edit' => '
<a href="{{#if fullRoute}}#{{fullRoute}}{{else}}{{#if def.route}}#{{buildRoute context=context model=model action=def.route.action}}{{else}}javascript:void(0);{{/if}}{{/if}}"
    class="btn {{name}}"
    data-placement="bottom"
    {{#if def.tooltip}}
        rel="tooltip"
        data-original-title="{{str def.tooltip module}}"
    {{else}}
        title="{{label}}"
    {{/if}}
    {{#if ariaLabel}}aria-label="{{ariaLabel}}"{{/if}}
    role="button" tabindex="{{tabIndex}}" name="{{name}}">{{#if def.icon}}<i class="fa {{def.icon}}"></i> {{/if}}</a>
',
    ),
  ),
  '_hash' => '4c15655d208d94b30fa81bd676c80038',
);

