<?php
$clientCache['Documents']['base']['layout'] = array (
  'tabbed-layout' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'view' => 'activitystream',
          'label' => 'Activity Stream',
        ),
        1 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Document Revisions',
          'context' => 
          array (
            'link' => 'revisions',
          ),
        ),
        2 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Contracts',
          'context' => 
          array (
            'link' => 'contracts',
          ),
        ),
        3 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Accounts',
          'context' => 
          array (
            'link' => 'accounts',
          ),
        ),
        4 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Contacts',
          'context' => 
          array (
            'link' => 'contacts',
          ),
        ),
        5 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Opportunities',
          'context' => 
          array (
            'link' => 'opportunities',
          ),
        ),
        6 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Cases',
          'context' => 
          array (
            'link' => 'cases',
          ),
        ),
        7 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Bugs',
          'context' => 
          array (
            'link' => 'bugs',
          ),
        ),
        8 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Quotes',
          'context' => 
          array (
            'link' => 'quotes',
          ),
        ),
        9 => 
        array (
          'layout' => 'list-cluster',
          'label' => 'Products',
          'context' => 
          array (
            'link' => 'products',
          ),
        ),
      ),
    ),
  ),
  'records' => 
  array (
    'meta' => 
    array (
      'type' => 'bwc',
      'components' => 
      array (
        0 => 
        array (
          'view' => 'bwc',
        ),
      ),
    ),
  ),
  '_hash' => '00342ccb3238d8601ff0cc6e2ff3ff6b',
);

