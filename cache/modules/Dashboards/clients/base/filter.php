<?php
$clientCache['Dashboards']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'create' => true,
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'quicksearch_split_terms' => false,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_LISTVIEW_FILTER_ALL',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
        1 => 
        array (
          'id' => 'assigned_to_me',
          'name' => 'LBL_ASSIGNED_TO_ME',
          'filter_definition' => 
          array (
            '$owner' => '',
          ),
          'editable' => false,
        ),
        2 => 
        array (
          'id' => 'favorites',
          'name' => 'LBL_FAVORITES',
          'filter_definition' => 
          array (
            '$favorite' => '',
          ),
          'editable' => false,
        ),
        3 => 
        array (
          'id' => 'recently_viewed',
          'name' => 'LBL_RECENTLY_VIEWED',
          'filter_definition' => 
          array (
            '$tracker' => '-7 DAY',
          ),
          'editable' => false,
        ),
        4 => 
        array (
          'id' => 'recently_created',
          'name' => 'LBL_NEW_RECORDS',
          'filter_definition' => 
          array (
            'date_entered' => 
            array (
              '$dateRange' => 'last_7_days',
            ),
          ),
          'editable' => false,
        ),
        5 => 
        array (
          'id' => 'module',
          'name' => 'LBL_BY_MODULE_FILTER',
          'filter_definition' => 
          array (
            0 => 
            array (
              'dashboard_module' => 
              array (
                '$in' => '',
              ),
            ),
          ),
          'editable' => true,
          'is_template' => true,
        ),
        6 => 
        array (
          'id' => 'module_and_layout',
          'name' => 'LBL_BY_MODULE_FILTER',
          'filter_definition' => 
          array (
            0 => 
            array (
              'dashboard_module' => 
              array (
                '$in' => '',
              ),
            ),
            1 => 
            array (
              'view_name' => 
              array (
                '$in' => '',
              ),
            ),
          ),
          'editable' => true,
          'is_template' => true,
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'name' => 
        array (
        ),
        'dashboard_module' => 
        array (
        ),
        'view_name' => 
        array (
        ),
        'assigned_user_name' => 
        array (
        ),
        'default_dashboard' => 
        array (
        ),
        'team_name' => 
        array (
        ),
      ),
    ),
  ),
  '_hash' => '9f6c94031fe3023a589d5740e366b4b1',
);

