<?php
$clientCache['Shippers']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Shippers/create',
        'label' => 'LNK_NEW_SHIPPER',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => '',
      ),
      1 => 
      array (
        'route' => '#TaxRates/create',
        'label' => 'LNK_NEW_TAXRATE',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => '',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => '0036a1680bce26ee618f18a2364c1bb2',
);

