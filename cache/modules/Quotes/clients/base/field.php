<?php
$clientCache['Quotes']['base']['field'] = array (
  'badge' => 
  array (
    'templates' => 
    array (
      'detail' => '
{{#if this.model.id}}
    <span class="label {{this.currentCSSClass}}">{{this.currentLabel}}</span>
{{/if}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.BadgeField
 * @alias SUGAR.App.view.fields.BaseBadgeField
 * @extends View.Fields.Base.BaseField
 */
({
    /**
     * Hash map of the possible labels for the badge
     */
    badgeLabelMap: undefined,

    /**
     * Hash map of the possible CSS Classes for the badge
     */
    cssClassMap: undefined,

    /**
     * The current CSS Class to add to the badge
     */
    currentCSSClass: undefined,

    /**
     * The current Label to use for the badge
     */
    currentLabel: undefined,

    /**
     * The field name to check for the badge
     */
    badgeFieldName: undefined,

    /**
     * The current state of the field
     */
    state: undefined,

    /**
     * @inheritdoc
     *
     * This field doesn\'t support `showNoData`.
     */
    showNoData: false,

    /**
     * @inheritdoc
     *
     * The badge is always a readonly field.
     */
    initialize: function(options) {
        options.def.readonly = true;
        this._initOptionMaps(options);

        this._super(\'initialize\', [options]);

        this._setState();
    },

    /**
     * Sets up any class hashes defined in metadata
     *
     * @param {Object} options The field def options from metadata
     * @private
     */
    _initOptionMaps: function(options) {
        this.cssClassMap = options.def.css_class_map;
        this.badgeLabelMap = options.def.badge_label_map;
    },

    /**
     * Sets the state of the field, field name, label, css classes, etc
     *
     * @private
     */
    _setState: function() {
        this.badgeFieldName = this.def.related_fields && _.first(this.def.related_fields) || this.name;

        var val = this.model.get(this.badgeFieldName);
        switch (this.def.badge_compare.comparison) {
            case \'notEq\':
                this.state = val != this.def.badge_compare.value;
                break;
            case \'eq\':
                this.state = val == this.def.badge_compare.value;
                break;
            case \'notEmpty\':
                this.state = !_.isUndefined(val) && !_.isEmpty(val.toString());
                break;
            case \'empty\':
                this.state = !_.isUndefined(val) && _.isEmpty(val.toString());
                break;
        }

        this.currentLabel = app.lang.get(this.badgeLabelMap[this.state], this.module);
        this.currentCSSClass = this.cssClassMap[this.state];
    },

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        this.model.on(\'change:\' + this.badgeFieldName, function() {
            if (!this.disposed) {
                this._setState();
                this.render();
            }
        }, this);
    }
})
',
    ),
  ),
  'taxrate' => 
  array (
    'templates' => 
    array (
      'detail' => '
{{#if href}}<a class="ellipsis_inline" data-placement="bottom" title="{{value}}" href="{{href}}">{{value}}</a>{{else}}{{value}}{{/if}}
',
      'overwrite-confirmation' => '
<span class="information-underline"
      rel="tooltip"
      data-container="body" data-placement="bottom" data-html="true" data-trigger="click"
      title="{{str \'LBL_OVERWRITE_POPULATED_DATA_FROM\'}}: {{before}} <br> {{str \'LBL_OVERWRITE_POPULATED_DATA_TO\'}}: {{after}}">
    {{field_label}}
</span>
',
      'edit' => '
<input type="hidden" name="{{name}}" class="select2 inherit-width" value="{{formattedIds}}"{{#if def.tabindex}} tabindex="{{def.tabindex}}"{{/if}} data-rname="{{formattedRname}}">
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.TaxrateField
 * @alias SUGAR.App.view.fields.BaseQuotesTaxrateField
 * @extends View.Fields.Base.EnumField
 */
({
    extendsFrom: \'RelateField\',

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        this._super(\'bindDataChange\');

        this.model.on(\'change:taxrate_value\', this._onTaxRateChange, this);
    },

    /**
     * Sets a new "tax" value when the taxrate changes
     *
     * @param {Data.Bean} model The changed model
     * @param {string} taxrateValue The new taxrate value "8.25", "!0", etc
     * @private
     */
    _onTaxRateChange: function(model, taxrateValue) {
        taxrateValue = taxrateValue || \'0\';

        var taxratePercent = app.math.div(taxrateValue, \'100\');
        var newTax = app.math.mul(this.model.get(\'taxable_subtotal\'), taxratePercent);

        this.model.set(\'tax\', newTax);
    },

    /**
     * Extending to add taxrate_value to the id/name values
     *
     * @inheritdoc
     */
    _onSelect2Change: function(e) {
        var plugin = $(e.target).data(\'select2\');
        var id = e.val;
        var value;
        var collection;
        var attributes = {};

        if (_.isUndefined(id)) {
            return;
        }

        value = (id) ? plugin.selection.find(\'span\').text() : $(this).data(\'rname\');
        collection = plugin.context;

        if (collection && !_.isEmpty(id)) {
            // if we have search results use that to set new values
            var model = collection.get(id);
            attributes.id = model.id;
            attributes.value = model.get(\'value\');
            attributes.name = model.get(\'name\');
            _.each(model.attributes, function(value, field) {
                if (app.acl.hasAccessToModel(\'view\', model, field)) {
                    attributes[field] = attributes[field] || model.get(field);
                }
            });
        } else if (e.currentTarget.value && value) {
            // if we have previous values keep them
            attributes.id = value;
            attributes.name = e.currentTarget.value;
            attributes.value = value;
        } else {
            // default to empty
            attributes.id = \'\';
            attributes.name = \'\';
            attributes.value = \'\';
        }

        this.setValue(attributes);
    },

    /**
     * Extending to add taxrate_value to the id/name values
     *
     * @inheritdoc
     */
    setValue: function(models) {
        if (!models) {
            return;
        }
        var updateRelatedFields = true;
        var values = {
            taxrate_id: models.id,
            taxrate_name: models.name,
            taxrate_value: models.value
        };

        if (_.isArray(models)) {
            // Does not make sense to update related fields if we selected
            // multiple models
            updateRelatedFields = false;
        }

        this.model.set(values);

        if (updateRelatedFields) {
            // TODO: move this to SidecarExpressionContext
            // check if link field is currently populated
            if (this.model.get(this.fieldDefs.link)) {
                // unset values of related bean fields in order to make the model load
                // the values corresponding to the currently selected bean
                this.model.unset(this.fieldDefs.link);
            } else {
                // unsetting what is not set won\'t trigger "change" event,
                // we need to trigger it manually in order to notify subscribers
                // that another related bean has been chosen.
                // the actual data will then come asynchronously
                this.model.trigger(\'change:\' + this.fieldDefs.link);
            }
        }
    }
})
',
    ),
  ),
  'currency' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.CurrencyField
 * @alias SUGAR.App.view.fields.BaseQuotesCurrencyField
 * @extends View.Fields.Base.CurrencyField
 */
({
    extendsFrom: \'CurrencyField\',

    /**
     * The field\'s value in Percent
     */
    valuePercent: undefined,

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        this._super(\'bindDataChange\');

        if (this.name === \'deal_tot\' && this.view.name === \'quote-data-grand-totals-header\') {
            this.model.on(\'change:deal_tot_discount_percentage\', function() {
                this._updateDiscountPercent();
            }, this);

            if (this.context.get(\'create\')) {
                // if this is deal_tot and on the create view, update the discount percent
                this._updateDiscountPercent();
            }
        }
    },

    /**
     * Updates `this.valuePercent` for the deal_tot field in the quote-data-grand-totals-header view.
     *
     * @private
     */
    _updateDiscountPercent: function() {
        var percent = this.model.get(\'deal_tot_discount_percentage\');

        if (!_.isUndefined(percent)) {
            //clean up precision
            percent = app.utils.formatNumber(
                percent,
                false,
                app.user.getPreference(\'decimal_precision\'),
                app.user.getPreference(\'number_grouping_separator\'),
                app.user.getPreference(\'decimal_separator\')
            );

            if (app.lang.direction === \'rtl\') {
                this.valuePercent = \'%\' + percent;
            } else {
                this.valuePercent =  percent + \'%\';
            }

            // re-render after update
            this.render();
        }
    }
});
',
    ),
    'templates' => 
    array (
      'quote-data-grand-totals-footer' => '
<div class="quote-footer-item {{css_class}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        {{#if transactionValue}}
            <label class="original">{{transactionValue}}</label><div class="converted">{{value}}</div>
        {{else}}
            {{value}}
        {{/if}}
    </span>
</div>
',
      'quote-data-grand-totals-header' => '
<div class="record-label quote-totals-row-label">{{label}}</div>
<div class="quote-totals-row-value">
    {{#eq name \'deal_tot\'}}
        <span class="percent-discount">{{valuePercent}}</span>
    {{/eq}}
    {{#if transactionValue}}
        <label class="original">{{transactionValue}}</label><div class="converted">{{value}}</div>
    {{else}}
        {{value}}
    {{/if}}
</div>
',
    ),
  ),
  'quote-footer-currency' => 
  array (
    'templates' => 
    array (
      'detail' => '
<div class="quote-footer-item quote-footer-input-item {{css_class}} record-cell"
     data-type="{{type}}" data-name="{{name}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        <div class="currency-field" data-placement="bottom" data-original-title="{{value}}">
            <span class="quote-edit-link-wrapper" data-name="{{name}}" data-wrapper="edit">
                <a class="record-edit-link btn btn-invisible" data-type="{{type}}" data-name="{{name}}">
                    <i class="fa fa-pencil"></i>
                </a>
            </span>
            {{#if transactionValue}}
                <label class="original">{{transactionValue}}</label><div class="converted">{{value}}</div>
            {{else}}
                {{value}}
            {{/if}}
        </div>
    </span>
</div>
',
      'edit' => '
<div class="quote-footer-item quote-footer-input-item {{css_class}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        <div class="currency-field" data-placement="bottom" data-original-title="{{value}}">
            <div class="controls">
                <span class="error-tooltip hide" rel="tooltip" data-container="body">
                    <i class="fa fa-exclamation-circle"></i>
                </span>
            </div>
            <input type="text" value="{{value}}" class="input-small tright" maxlength="26">
            <span sfuuid="{{currencySfId}}" class="hide"></span>
        </div>
    </span>
</div>
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.QuoteFooterCurrency
 * @alias SUGAR.App.view.fields.BaseQuotesQuoteFooterCurrency
 * @extends View.Fields.Base.CurrencyField
 */
({
    extendsFrom: \'CurrencyField\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        var isCreate = options.context.isCreate();
        options.viewName = isCreate ? \'edit\' : \'detail\';

        this._super(\'initialize\', [options]);

        if (!isCreate) {
            // only add this event on record view
            this.events = _.extend({
                \'click .currency-field\': \'_toggleFieldToEdit\'
            }, this.events);
        }

        this.model.addValidationTask(
            \'isNumeric_validator_\' + this.cid,
            _.bind(this._doValidateIsNumeric, this)
        );

        this.action = isCreate ? \'edit\' : \'detail\';

        this.context.trigger(\'quotes:editableFields:add\', this);
    },

    /**
     * Toggles the field to edit if it not in edit
     *
     * @param {jQuery.Event} evt jQuery click event
     * @private
     */
    _toggleFieldToEdit: function(evt) {
        var record;

        if (!this.$el.hasClass(\'edit\')) {
            this.action = \'edit\';
            this.tplName = \'detail\';

            // if this isn\'t already in edit, toggle to edit
            record = this.closestComponent(\'record\');
            if (record) {
                record.context.trigger(\'editable:handleEdit\', evt);
            }
        }
    },

    /**
     * Validation function to check to see if a value is numeric.
     *
     * @param {Array} fields
     * @param {Array} errors
     * @param {Function} callback
     * @private
     */
    _doValidateIsNumeric: function(fields, errors, callback) {
        var value = this.model.get(this.name);
        if (!$.isNumeric(value)) {
            errors[this.name] = app.lang.get(\'ERROR_NUMBER\');
        }
        callback(null, fields, errors);
    },

    /**
      * Extending to remove the custom validation task for this field
      *
      * @inheritdoc
      * @private
      */
    _dispose: function() {
        this.model.removeValidationTask(\'isNumeric_validator_\' + this.cid);
        this._super(\'_dispose\');
    }
});
',
    ),
  ),
  'date' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.DateField
 * @alias SUGAR.App.view.fields.BaseQuotesDateField
 * @extends View.Fields.Base.DateField
 */
({
    extendsFrom: \'DateField\',

    /**
     * @inheritdoc
     */
    _dispose: function() {
        // FIXME: this is a bad "fix" added -- when SC-2395 gets done to upgrade bootstrap we need to remove this
        if (this._hasDatePicker && this.$(this.fieldTag).data(\'datepicker\')) {
            $(window).off(\'resize\', this.$(this.fieldTag).data(\'datepicker\').place);
        }
        this._hasDatePicker = false;

        this._super(\'_dispose\');
    }
})
',
    ),
  ),
  'quote-footer-input' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.QuoteFooterInputField
 * @alias SUGAR.App.view.fields.BaseQuotesQuoteFooterInputField
 * @extends View.Fields.Base.Field
 */
({
    /**
     * The value dollar amount
     */
    value_amount: undefined,

    /**
     * The value percent amount
     */
    value_percent: undefined,

    /**
     * @inheritdoc
     */
    format: function(value) {
        if (!value) {
            this.value_amount = app.currency.formatAmountLocale(\'0\');
            this.value_percent = \'0%\';
        }
    }
})
',
    ),
    'templates' => 
    array (
      'detail' => '
<div class="quote-footer-item quote-footer-input-item {{css_class}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        <input type="text" id="{{name}}-percent" name="{{name}}" value="{{value_percent}}" />
        <input type="text" id="{{name}}-amount" name="{{name}}" value="{{value_amount}}" />
    </span>
</div>
',
    ),
  ),
  'copy' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.CopyField
 * @alias SUGAR.App.view.fields.BaseQuotesCopyField
 * @extends View.Fields.Base.CopyField
 */
({
    extendsFrom: \'CopyField\',

    /**
     * If this field is on a view that is converting from a "Ship To" Subpanel
     */
    isConvertingFromShipping: undefined,

    /**
     * If this is a Quote Record Copy
     */
    isCopy: undefined,

    /**
     * Is this the first time the Copy field has run
     */
    firstRun: undefined,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        this.firstRun = true;
        this.isCopy = this.context.get(\'copy\') || false;
        this.isConvertingFromShipping = this.view.isConvertFromShippingOrBilling === \'shipping\';
    },

    /**
     * Extending to set Shipping Account Name field editable after copy
     *
     * @inheritdoc
     */
    sync: function(enable) {
        var shippingAcctNameField;
        var isChecked = this._isChecked();

        // do not sync field mappings if this is a quote record copy
        if (this.isCopy && this.firstRun) {
            enable = false;
            this.firstRun = false;
        }

        this._super(\'sync\', [enable]);

        // if this is coming from a Ship To subpanel and the Copy Billing to Shipping box
        // is not checked then re-enable the Shipping Account Name field so it can be canceled
        if (!isChecked) {
            shippingAcctNameField = this.getField(\'shipping_account_name\');
            if (shippingAcctNameField) {
                shippingAcctNameField.setDisabled(false);
            }
        }
    },

    /**
     * Extending to add the model value condition in pre-rendered versions of the field
     *
     * @inheritdoc
     */
    toggle: function() {
        this.sync(this._isChecked());
    },

    /**
     * Pulling this out to a function that can be checked from multiple places if the field
     * is checked or if the field does not exist yet (pre-render) then use the model value
     *
     * @return {boolean} True if the field is checked or false if not
     * @private
     */
    _isChecked: function() {
        return this.$fieldTag ? this.$fieldTag.is(\':checked\') : this.model.get(this.name);
    },

    /**
     * Extending to check if we need to add sync events or not
     *
     * @inheritdoc
     */
    syncCopy: function(enable) {
        if ((!this.isConvertingFromShipping && !_.isUndefined(this._isChecked())) ||
            (this.isConvertingFromShipping && this._isChecked())) {
            // if this view is not coming from a Ship To convert subpanel,
            // or if it IS but the user specifically checked the Copy Billing to Shipping checkbox
            this._super(\'syncCopy\', [enable]);
        } else {
            // set _inSync to be false so that sync() will work properly
            this._inSync = false;

            if (!enable) {
                // remove sync events from the model
                this.model.off(null, this.copyChanged, this);
                return;
            }
        }
    }
})
',
    ),
  ),
  'convert-to-opportunity' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

/**
 * @class View.Fields.Base.Quotes.ConvertToOpportunity
 * @alias SUGAR.App.view.fields.BaseQuotesConvertToOpportunity
 * @extends View.Fields.Base.RowactionField
 */
({
    extendsFrom: \'RowactionField\',

    /**
     * @inheritdoc
     *
     * @param {Object} options
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);
        this.type = \'rowaction\';

        this.context.on(\'button:convert_to_opportunity:click\', this._onCreateOppFromQuoteClicked, this);
    },

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        this.model.on(\'sync\', this._toggleDisable, this);
        this.model.on(\'change:opportunity_id\', this._toggleDisable, this);
    },

    /**
     * Handler for when "Create Opp from Quote" is clicked
     * @private
     */
    _onCreateOppFromQuoteClicked: function() {
        var id = this.model.get(\'id\');
        var url = app.api.buildURL(\'Quotes/\' + id + \'/opportunity\');

        app.alert.show(\'convert_to_opp\', {
            level: \'info\',
            title: app.lang.get(\'LBL_QUOTE_TO_OPPORTUNITY_STATUS\'),
            messages: [\'\']
        });

        app.api.call(
            \'create\',
            url,
            null,
            {
                success: this._onCreateOppFromQuoteCallback,
                error: this._onCreateOppFromQuoteError
            });
    },

    /**
     * Success callback for Create Opp From Quote
     * @param data Data from the server
     * @private
     */
    _onCreateOppFromQuoteCallback: function(data) {
        var id = data.record.id;
        var url = \'Opportunities/\' + id;
        app.alert.dismiss(\'convert_to_opp\');
        app.router.navigate(url, {trigger: true});
    },

    /**
     * Error callback for Create Opp From Quote
     * @param data
     * @private
     */
    _onCreateOppFromQuoteError: function(data) {
        app.alert.dismiss(\'convert_to_opp\');
        app.alert.show(\'error_convert\', {
            level: \'error\',
            title: app.lang.get(\'LBL_ERROR\'),
            messages: [data.message]
        });
    },

    /**
     * Reusable method for the event actions
     *
     * @private
     */
    _toggleDisable: function() {
        var opportunityId = this.model.get(\'opportunity_id\');
        this.setDisabled(!(_.isUndefined(opportunityId) || _.isEmpty(opportunityId)));
    }
});
',
    ),
  ),
  'datetimecombo' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.DatetimecomboField
 * @alias SUGAR.App.view.fields.BaseQuotesDatetimecomboField
 * @extends View.Fields.Base.DatetimecomboField
 */
({
    extendsFrom: \'DatetimecomboField\',

    /**
     * @inheritdoc
     */
    _dispose: function() {
        // FIXME: this is a bad "fix" added -- when SC-2395 gets done to upgrade bootstrap we need to remove this
        if (this._hasTimePicker) {
            this.$(this.secondaryFieldTag).timepicker(\'remove\');
        }

        if (this._hasDatePicker && this.$(this.fieldTag).data(\'datepicker\')) {
            $(window).off(\'resize\', this.$(this.fieldTag).data(\'datepicker\').place);
        }

        this._hasTimePicker = false;
        this._hasDatePicker = false;

        this._super(\'_dispose\');
    }
})
',
    ),
  ),
  'currency-type-dropdown' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.CurrencyTypeDropdownField
 * @alias SUGAR.App.view.fields.BaseQuotesCurrencyTypeDropdownField
 * @extends View.Fields.Base.EnumField
 */
({
    extendsFrom: \'EnumField\',

    /**
     * Holds the compiled currencies templates with symbol/iso by currencyID key
     * @type {Object}
     */
    currenciesTpls: undefined,

    /**
     * The currency ID field name to use on the model when changing currency ID
     * Defaults to \'currency_id\' if no currency_field exists in metadata
     * @type {string}
     */
    currencyIdFieldName: undefined,

    /**
     * The base rate field name to use on the model
     * Defaults to \'base_rate\' if no base_rate_field exists in metadata
     * @type {string}
     */
    baseRateFieldName: undefined,

    /**
     * The last known record currency id
     * @type {string}
     */
    _lastCurrencyId: undefined,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        // get the currencies and run them through the template
        this.currenciesTpls = app.currency.getCurrenciesSelector(Handlebars.compile(\'{{symbol}} ({{iso4217}})\'));

        // Type should be enum to use the enum templates
        options.def.type = \'enum\';
        // update options defs the currencies templates
        options.def.options = options.def.options || this.currenciesTpls;

        // get the default field names from metadata
        this.currencyIdFieldName = options.def.currency_field || \'currency_id\';
        this.baseRateFieldName = options.def.base_rate_field || \'base_rate\';

        this._super(\'initialize\', [options]);

        // check to make sure this is a new model or currency_id has not been set, and the model is not a copy
        // so we don\'t overwrite the models previously entered values
        if ((this.model.isNew() && !this.model.isCopy())) {
            var currencyFieldValue = app.user.getPreference(\'currency_id\');
            var baseRateFieldValue = app.metadata.getCurrency(currencyFieldValue).conversion_rate;

            // set the currency_id to the user\'s preferred currency
            this.model.set(this.currencyIdFieldName, currencyFieldValue);

            // set the base_rate to the preferred currency conversion_rate
            this.model.set(this.baseRateFieldName, baseRateFieldValue);

            // if this.name is not the same as the currency ID field, also set this.name on the model
            if (this.name !== this.currencyIdFieldName) {
                this.model.set(this.name, currencyFieldValue);
            }

            // Modules such as `Forecasts` uses models that aren\'t `Data.Bean`
            if (_.isFunction(this.model.setDefault)) {
                var defaults = {};
                defaults[this.currencyIdFieldName] = currencyFieldValue;
                defaults[this.baseRateFieldName] = baseRateFieldValue;
                this.model.setDefault(defaults);
            }
        }

        // track the last currency id to convert the value on change
        this._lastCurrencyId = this.model.get(this.currencyIdFieldName);
    }
})
',
    ),
  ),
  'quote-data-actionmenu' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.QuoteDataActionmenuField
 * @alias SUGAR.App.view.fields.BaseQuotesQuoteDataActionmenuField
 * @extends View.Fields.Base.BaseActionmenuField
 */
({
    /**
     * @inheritdoc
     */
    extendsFrom: \'BaseActionmenuField\',

    /**
     * Skipping ActionmenuField\'s override, just returning this.def.buttons
     *
     * @inheritdoc
     */
    _getChildFieldsMeta: function() {
        return app.utils.deepCopy(this.def.buttons);
    }
})
',
    ),
    'templates' => 
    array (
      'list' => '
<div class="btn btn-invisible checkall"><input data-check="all" type="checkbox" name="check"></div>
{{#if dropdownFields}}
    <a track="click:listactiondropdown" class="btn btn-invisible dropdown-toggle"
       data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
       rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
       role="button" tabindex="{{tabIndex}}" aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}">
        <span class="fa fa-ellipsis-v"></span></a>
    <ul data-menu="dropdown" class="dropdown-menu" role="menu"></ul>
{{/if}}
',
    ),
  ),
  'quote-data-actiondropdown' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Quotes.QuoteDataActiondropdownField
 * @alias SUGAR.App.view.fields.BaseQuotesQuoteDataActiondropdownField
 * @extends View.Fields.Base.BaseActiondropdownField
 */
({
    /**
     * @inheritdoc
     */
    extendsFrom: \'BaseActiondropdownField\',

    /**
     * @inheritdoc
     */
    className: \'quote-data-actiondropdown\'
})
',
    ),
    'templates' => 
    array (
      'list-header' => '
<a track="click:listactiondropdown" class="btn btn-invisible dropdown-toggle"
   data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
   rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
   role="button" tabindex="{{tabIndex}}" aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}">
    <span class="fa fa-plus"></span></a>
<ul data-menu="dropdown" class="dropdown-menu" role="menu"></ul>
',
    ),
  ),
  '_hash' => '26ede385627bdeeb47cf1eb35e088704',
);

