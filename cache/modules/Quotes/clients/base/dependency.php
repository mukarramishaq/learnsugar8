<?php
$clientCache['Quotes']['base']['dependency'] = array (
  '_hash' => '40cd750bba9870f18aada2478b24840a',
  'dependencies' => 
  array (
    0 => 
    array (
      'name' => 'subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
        0 => 'subtotal',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'subtotal',
            'value' => 'rollupCurrencySum($product_bundles, "subtotal")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    1 => 
    array (
      'name' => 'subtotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'subtotal',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'subtotal_usdollar',
            'value' => 'ifElse(isNumeric($subtotal), currencyDivide($subtotal,$base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    2 => 
    array (
      'name' => 'shipping',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'shipping',
        1 => 'shipping',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'shipping',
            'value' => 'ifElse(equal($shipping,""),"0.00",$shipping)',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    3 => 
    array (
      'name' => 'shipping_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'shipping',
        1 => 'shipping',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'shipping_usdollar',
            'value' => 'ifElse(isNumeric($shipping), currencyDivide($shipping, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    4 => 
    array (
      'name' => 'deal_tot',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
        0 => 'deal_tot',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'deal_tot',
            'value' => 'rollupCurrencySum($product_bundles, "deal_tot")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    5 => 
    array (
      'name' => 'deal_tot_discount_percentage',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal_usdollar',
        1 => 'deal_tot_usdollar',
        2 => 'subtotal_usdollar',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'deal_tot_discount_percentage',
            'value' => 'ifElse(not(equal($subtotal_usdollar, 0)), mul(divide($deal_tot_usdollar, $subtotal_usdollar),100), 0)',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    6 => 
    array (
      'name' => 'deal_tot_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'deal_tot',
        1 => 'deal_tot',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'deal_tot_usdollar',
            'value' => 'ifElse(isNumeric($deal_tot), currencyDivide($deal_tot, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    7 => 
    array (
      'name' => 'new_sub',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
        0 => 'new_sub',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'new_sub',
            'value' => 'rollupCurrencySum($product_bundles, "new_sub")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    8 => 
    array (
      'name' => 'new_sub_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
        1 => 'new_sub',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'new_sub_usdollar',
            'value' => 'ifElse(isNumeric($new_sub), currencyDivide($new_sub, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    9 => 
    array (
      'name' => 'taxable_subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
        0 => 'taxable_subtotal',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'taxable_subtotal',
            'value' => 'rollupCurrencySum($product_bundles, "taxable_subtotal")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    10 => 
    array (
      'name' => 'tax',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'taxable_subtotal',
        1 => 'taxrate_value',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'tax',
            'value' => 'currencyMultiply($taxable_subtotal, currencyDivide($taxrate_value, "100"))',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    11 => 
    array (
      'name' => 'tax_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'tax',
        1 => 'tax',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'tax_usdollar',
            'value' => 'ifElse(isNumeric($tax), currencyDivide($tax, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    12 => 
    array (
      'name' => 'total',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
        1 => 'tax',
        2 => 'tax',
        3 => 'shipping',
        4 => 'shipping',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'total',
            'value' => 'currencyAdd(                rollupCurrencySum($product_bundles, "new_sub"),                ifElse(isNumeric($tax), $tax, "0"),                ifElse(isNumeric($shipping), $shipping, "0")            )',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    13 => 
    array (
      'name' => 'total_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'total',
        1 => 'total',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'total_usdollar',
            'value' => 'ifElse(isNumeric($total), currencyDivide($total, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    14 => 
    array (
      'name' => 'readOnlysubtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'subtotal',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    15 => 
    array (
      'name' => 'readOnlysubtotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'subtotal',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'subtotal_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    16 => 
    array (
      'name' => 'readOnlyshipping_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'shipping',
        1 => 'shipping',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'shipping_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    17 => 
    array (
      'name' => 'readOnlydeal_tot',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'deal_tot',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    18 => 
    array (
      'name' => 'readOnlydeal_tot_discount_percentage',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal_usdollar',
        1 => 'deal_tot_usdollar',
        2 => 'subtotal_usdollar',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'deal_tot_discount_percentage',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    19 => 
    array (
      'name' => 'readOnlydeal_tot_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'deal_tot',
        1 => 'deal_tot',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'deal_tot_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    20 => 
    array (
      'name' => 'readOnlynew_sub',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'new_sub',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    21 => 
    array (
      'name' => 'readOnlynew_sub_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
        1 => 'new_sub',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'new_sub_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    22 => 
    array (
      'name' => 'readOnlytaxable_subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'taxable_subtotal',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    23 => 
    array (
      'name' => 'readOnlytax',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'taxable_subtotal',
        1 => 'taxrate_value',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'tax',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    24 => 
    array (
      'name' => 'readOnlytax_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'tax',
        1 => 'tax',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'tax_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    25 => 
    array (
      'name' => 'readOnlytotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'product_bundles',
        1 => 'tax',
        2 => 'tax',
        3 => 'shipping',
        4 => 'shipping',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'total',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    26 => 
    array (
      'name' => 'readOnlytotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'total',
        1 => 'total',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'total_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
  ),
);

