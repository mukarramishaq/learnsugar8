<?php
$clientCache['Quotes']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Quotes/create',
        'label' => 'LNK_NEW_QUOTE',
        'acl_action' => 'create',
        'acl_module' => 'Quotes',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#Quotes',
        'label' => 'LNK_QUOTE_LIST',
        'acl_action' => 'list',
        'acl_module' => 'Quotes',
        'icon' => 'fa-bars',
      ),
      2 => 
      array (
        'route' => '#Reports?filterModule=Quotes',
        'label' => 'LNK_QUOTE_REPORTS',
        'acl_action' => 'list',
        'acl_module' => 'Reports',
        'icon' => 'fa-bar-chart-o',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => '863369dc1fa4b5ad52d044b6c91f727e',
);

