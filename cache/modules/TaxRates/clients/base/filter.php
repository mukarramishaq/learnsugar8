<?php
$clientCache['TaxRates']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'create' => false,
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_MODULE_NAME',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
        1 => 
        array (
          'id' => 'active_taxrates',
          'name' => 'LBL_FILTER_ACTIVE',
          'filter_definition' => 
          array (
            'status' => 
            array (
              '$in' => 
              array (
                0 => 'Active',
              ),
            ),
          ),
          'editable' => false,
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'name' => 
        array (
        ),
      ),
    ),
  ),
  '_hash' => '4835f93f472a30b6d798a468b769c6ab',
);

