<?php
$clientCache['TaxRates']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Shippers',
        'label' => 'LNK_NEW_SHIPPER',
        'acl_action' => 'admin',
        'acl_module' => 'Products',
        'icon' => 'fa-bars',
      ),
      1 => 
      array (
        'route' => '#TaxRates/create',
        'label' => 'LNK_NEW_TAXRATE',
        'acl_action' => 'admin',
        'acl_module' => 'Products',
        'icon' => 'fa-bars',
      ),
      2 => 
      array (
        'route' => '#bwc/index.php?module=Import&action=Step1&import_module=TaxRates&return_module=TaxRates&return_action=index',
        'label' => 'LNK_IMPORT_TAXRATES',
        'acl_action' => 'admin',
        'acl_module' => 'Products',
        'icon' => 'fa-arrow-circle-o-up',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => '2328792c464b12236defac6fe6ef206d',
);

