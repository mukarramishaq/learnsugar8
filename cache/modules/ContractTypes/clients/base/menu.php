<?php
$clientCache['ContractTypes']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#ContractTypes/create',
        'label' => 'LNK_NEW_CONTRACTTYPE',
        'acl_action' => 'create',
        'acl_module' => 'ContractTypes',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#ContractTypes',
        'label' => 'LNK_CONTRACTTYPE_LIST',
        'acl_action' => 'list',
        'acl_module' => 'ContractTypes',
        'icon' => 'fa-bars',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => '1e87e4fe3257ecba5fcfd1a9b40f2aa2',
);

