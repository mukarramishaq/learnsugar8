<?php
$clientCache['ContractTypes']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'create' => false,
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_MODULE_NAME',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'name' => 
        array (
        ),
        'tag' => 
        array (
        ),
        'assigned_user_name' => 
        array (
        ),
        '$owner' => 
        array (
          'predefined_filter' => true,
          'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => 
        array (
          'predefined_filter' => true,
          'vname' => 'LBL_FAVORITES_FILTER',
        ),
      ),
    ),
  ),
  '_hash' => '7483ad5c75bb090141ef0a22ecd50c3b',
);

