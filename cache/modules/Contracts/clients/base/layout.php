<?php
$clientCache['Contracts']['base']['layout'] = array (
  'subpanels' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_DOCUMENTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'contracts_documents',
          ),
        ),
        1 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_NOTES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'notes',
          ),
        ),
        2 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CONTACTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'contacts',
          ),
        ),
        3 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_QUOTES_SUBPANEL_TITLE',
          'override_paneltop_view' => 'panel-top-for-contracts',
          'override_subpanel_list_view' => 'subpanel-for-contracts',
          'context' => 
          array (
            'link' => 'quotes',
            'ignore_role' => 0,
          ),
        ),
      ),
    ),
  ),
  '_hash' => 'd131638123770c78593a613d3ec715d2',
);

