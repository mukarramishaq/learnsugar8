<?php
$clientCache['Reports']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'create' => true,
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'quicksearch_split_terms' => false,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_LISTVIEW_FILTER_ALL',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
        1 => 
        array (
          'id' => 'assigned_to_me',
          'name' => 'LBL_ASSIGNED_TO_ME',
          'filter_definition' => 
          array (
            '$owner' => '',
          ),
          'editable' => false,
        ),
        2 => 
        array (
          'id' => 'favorites',
          'name' => 'LBL_FAVORITES',
          'filter_definition' => 
          array (
            '$favorite' => '',
          ),
          'editable' => false,
        ),
        3 => 
        array (
          'id' => 'recently_viewed',
          'name' => 'LBL_RECENTLY_VIEWED',
          'filter_definition' => 
          array (
            '$tracker' => '-7 DAY',
          ),
          'editable' => false,
        ),
        4 => 
        array (
          'id' => 'recently_created',
          'name' => 'LBL_NEW_RECORDS',
          'filter_definition' => 
          array (
            'date_entered' => 
            array (
              '$dateRange' => 'last_7_days',
            ),
          ),
          'editable' => false,
        ),
        5 => 
        array (
          'id' => 'recently_modified',
          'name' => 'LBL_RECENTLY_MODIFIED',
          'filter_definition' => 
          array (
            'date_modified' => 
            array (
              '$dateRange' => 'last_7_days',
            ),
          ),
          'editable' => false,
        ),
      ),
    ),
  ),
  'reports' => 
  array (
    'meta' => 
    array (
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'by_module',
          'name' => 'LBL_FILTER_BY_MODULE',
          'filter_definition' => 
          array (
            0 => 
            array (
              'module' => 
              array (
                '$in' => 
                array (
                ),
              ),
            ),
          ),
          'editable' => true,
          'is_template' => true,
        ),
        1 => 
        array (
          'id' => 'with_charts',
          'name' => 'LBL_FILTER_WITH_CHARTS',
          'filter_definition' => 
          array (
            0 => 
            array (
              'chart_type' => 
              array (
                '$not_equals' => 'none',
              ),
            ),
          ),
          'editable' => false,
          'is_template' => true,
        ),
      ),
    ),
  ),
  'operators' => 
  array (
    'meta' => 
    array (
      'enum' => 
      array (
        '$in' => 'LBL_OPERATOR_CONTAINS',
        '$not_in' => 'LBL_OPERATOR_NOT_CONTAINS',
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'module' => 
        array (
        ),
        'name' => 
        array (
        ),
        'report_type' => 
        array (
        ),
        'assigned_user_name' => 
        array (
        ),
        'team_name' => 
        array (
        ),
        'tag' => 
        array (
        ),
        'date_entered' => 
        array (
        ),
        'date_modified' => 
        array (
        ),
        'last_run_date' => 
        array (
        ),
      ),
    ),
  ),
  '_hash' => 'c5abde947509c74b094cb1f3e76cf0b3',
);

