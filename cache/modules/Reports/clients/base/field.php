<?php
$clientCache['Reports']['base']['field'] = array (
  'next-run' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * This field is to open a pop up scheduler when clicking on Schedule Report
 *
 * @class View.Fields.Base.Reports.NextRunField
 * @alias SUGAR.App.view.fields.BaseReportsNextRunField
 * @extends View.Fields.Base.DatetimecomboField
 */
({

    extendsFrom: \'DatetimecomboField\',

    events: {
        \'click a\': \'openScheduler\'
    },

    /**
     * Opens the old BWC schedule report popup
     */
    openScheduler: function() {

        // Temp fix for when session cookie isn\'t set
        // FIXME this will be removed when we move scheduler to sidecar
        var openScheduler = function() {
            window.open(
                \'index.php?module=Reports&action=add_schedule&to_pdf=true&id=\' + this.model.id,
                \'test\',
                \'width=400,height=250,resizable=1,scrollbars=1\'
            );
        };
        app.bwc.login(null, _.bind(openScheduler, this));
    }
})
',
    ),
    'templates' => 
    array (
      'list' => '
{{#if ellipsis}}
    <div class="ellipsis_inline" data-placement="bottom"{{#if dir}} dir="{{dir}}"{{/if}} title="{{#if value}}{{value}}{{else}}{{str "LBL_NONE"}}{{/if}}" >
{{/if}}
{{#if href}}<a href="javascript:void(0);">{{#if value}}{{value}}{{else}}{{str "LBL_NONE"}}{{/if}}</a>{{/if~}}
{{#if ellipsis}}
    </div>
{{/if~}}
',
    ),
  ),
  'drillthrough-collection-count' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * DrillthroughCollectionCountField is a field for Reports to set total in drillthrough drawer headerpane.
 *
 * @class View.Fields.Base.Reports.DrillthroughCollectionCountField
 * @alias SUGAR.App.view.fields.BaseReportsDrillthroughCollectionCountField
 * @extends View.Fields.Base.CollectionCountField
 */
({
    extendsFrom: \'CollectionCountField\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);
        this.type = \'collection-count\';
    },

    /**
     * @inheritdoc
     *
     * Calls ReportsApi to get collection count.
     */
    fetchCount: function() {
        if (_.isNull(this.collection.total)) {
            app.alert.show(\'fetch_count\', {
                level: \'process\',
                title: app.lang.get(\'LBL_LOADING\'),
                autoClose: false
            });
        }
        var filterDef = this.context.get(\'filterDef\');
        var useSavedFilters = this.context.get(\'useSavedFilters\') || false;
        var params = {group_filters: filterDef, use_saved_filters: useSavedFilters};
        var reportId = this.context.get(\'reportId\');
        var url = app.api.buildURL(\'Reports\', \'record_count\', {id: reportId}, params);
        app.api.call(\'read\', url, null, {
            success: _.bind(function(data) {
                this.collection.total = parseInt(data.record_count, 10);
                if (!this.disposed) {
                    this.updateCount();
                }
            }, this),
            complete: function() {
                app.alert.dismiss(\'fetch_count\');
            }
        });
    },
})
',
    ),
  ),
  'drillthrough-labels' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Reports.DrillthroughLabelsField
 * @alias SUGAR.App.view.fields.BaseReportsDrillthroughLabelsField
 * @extends View.Fields.Base.BaseField
 */
({

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        this.context.on(\'refresh:drill:labels\', this.render, this);
    },

    /**
     * @override We want to grab the data from the context, not the model
     */
    format: function(value) {
        var params = this.context.get(\'dashConfig\');
        var reportDef = this.context.get(\'reportData\');
        var chartModule = this.context.get(\'chartModule\');
        var filterDef = this.context.get(\'filterDef\');
        var filterFields = _.flatten(_.map(filterDef, function(filter) {
            return _.keys(filter);
        }));
        var groupDefs = _.filter(reportDef.group_defs, function(groupDef) {
            var groupField = groupDef.table_key + \':\' + groupDef.name;
            return _.contains(filterFields, groupField);
        });
        if (groupDefs.length > 0) {
            var group = SUGAR.charts.getFieldDef(groupDefs[0], reportDef);
            var module = group.custom_module || group.module || chartModule;
            this.groupName = app.lang.get(group.vname, module) + \': \';
            this.groupValue = params.groupLabel;
        }
        if (groupDefs.length > 1) {
            var series = SUGAR.charts.getFieldDef(groupDefs[1], reportDef);
            var module = series.custom_module || series.module || chartModule;
            this.seriesName = app.lang.get(series.vname, module) + \': \';
            this.seriesValue = params.seriesLabel;
        }

        // returns nothing
        return value;
    }
})
',
    ),
    'templates' => 
    array (
      'detail' => '
<div {{#if seriesValue}}class="series-adjust"{{/if}}>
    {{#if groupValue}}
        <div class="ellipsis_inline">
            <span class="record-label">{{groupName}}</span>
            <span>{{groupValue}}</span>
        </div>
    {{/if}}
    {{#if seriesValue}}
        <div class="ellipsis_inline">
            <span class="record-label">{{seriesName}}</span>
            <span>{{seriesValue}}</span>
        </div>
    {{/if}}
</div>
',
    ),
  ),
  'chart-type' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Reports.ChartTypeField
 * @alias SUGAR.App.view.fields.BaseReportsChartTypeField
 * @extends View.Fields.Base.BaseField
 */
({

    extendsFrom: \'BaseField\',

    /**
     * The mapping for each of the chart types
     */
    mapping: {
        none: \'LBL_NO_CHART\',
        hBarF: \'LBL_HORIZ_BAR\',
        vBarF: \'LBL_VERT_BAR\',
        pieF: \'LBL_PIE\',
        funnelF: \'LBL_FUNNEL\',
        lineF: \'LBL_LINE\',
    },

    /**
     * Gets the correct mapping for the DB value
     *
     * @param {string} value The value from the server
     * @return {string} The mapped and translated value
     */
    format: function(value) {
        return app.lang.get(this.mapping[value], this.module);
    }
})
',
    ),
  ),
  '_hash' => '4bfc9d6d2451e914aa4c322c7ed9d97c',
);

