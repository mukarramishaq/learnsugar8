<?php
$clientCache['Reports']['base']['layout'] = array (
  'drillthrough-pane' => 
  array (
    'meta' => 
    array (
      'name' => 'drillthrough-pane',
      'css_class' => 'dashboard drillthrough-pane',
      'components' => 
      array (
        0 => 
        array (
          'view' => 
          array (
            'name' => 'drillthrough-pane-headerpane',
            'template' => 'headerpane',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'title',
                'type' => 'text',
              ),
            ),
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'button',
                'icon' => 'fa-refresh',
                'css_class' => 'btn',
                'tooltip' => 'LBL_REFRESH_LIST_AND_CHART',
                'events' => 
                array (
                  'click' => 'click:refresh_list_chart',
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Reports.DrillthroughPaneLayout
 * @alias SUGAR.App.view.layouts.ReportsDrillthroughPaneLayout
 * @extends View.Layout
 */
({
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        // configuration from clicked dashlet
        var config = this.context.get(\'dashConfig\');

        var metadata = {
                component: \'saved-reports-chart\',
                name: \'saved-reports-chart\',
                type: \'saved-reports-chart\',
                label: config.label || app.lang.get(\'LBL_DASHLET_SAVED_REPORTS_CHART\', \'Reports\'),
                description: \'LBL_DASHLET_SAVED_REPORTS_CHART_DESC\',
                // module: this.context.get(\'module\'), // this breaks Dashlet plugin at context.parent
                module: null,
                config: [],
                preview: []
            };

        var field = {
                type: \'chart\',
                name: \'chart\',
                label: \'LBL_CHART\',
                view: \'detail\',
                module: metadata.module
            };

        var component = {
                name: metadata.component,
                type: metadata.type,
                preview: true,
                context: this.context,
                module: metadata.module,
                custom_toolbar: \'no\',
                chart: field
            };

        component.view = _.extend({module: metadata.module}, metadata.preview, component);

        this.initComponents([{
            layout: {
                type: \'dashlet\',
                css_class: \'dashlets\',
                config: false,
                preview: false,
                label: metadata.label,
                module: metadata.module,
                context: this.context,
                components: [
                    component
                ]
            }
        }], this.context);
        this.on(\'click:refresh_list_chart\', this.refreshListChart, this);
    },

    /**
     * @inheritdoc
     */
    render: function() {
        var config = this.context.get(\'dashConfig\');
        // Set the title of the side pane
        this.model.setDefault(\'title\', config.label);
        this._super(\'render\');

        var dashlet = this.getComponent(\'dashlet\').getComponent(\'saved-reports-chart\');
        var config = this.context.get(\'dashConfig\');
        var chartData = this.context.get(\'chartData\');
        var reportData = this.context.get(\'reportData\');
        var chartLabels = {groupLabel: config.groupLabel, seriesLabel: config.seriesLabel};
        this.context.set(\'chartLabels\', chartLabels);
        var title = dashlet.$(\'.dashlet-title\');

        // This will allow scrolling when drilling thru from Report detail view
        // but will respect the dashlet setting when drilling thru from SRC
        config.allowScroll = true;

        dashlet.settings.set(config);
        dashlet.reportData.set(\'rawChartParams\', config);
        dashlet.reportData.set(\'rawReportData\', reportData);
        // set reportData\'s rawChartData to the chartData from the source chart
        // this will trigger chart.js\' change:rawChartData and the chart will update
        dashlet.reportData.set(\'rawChartData\', chartData);

        return this;
    },

    /**
     * Refresh list and chart
     */
    refreshListChart: function() {
        var drawer = this.closestComponent(\'drawer\').getComponent(\'drillthrough-drawer\');
        drawer.updateList();
        var dashlet = this.getComponent(\'dashlet\').getComponent(\'saved-reports-chart\');
        dashlet.loadData();
    }
})
',
    ),
  ),
  'drillthrough-drawer' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Reports.DrillthroughDrawerLayout
 * @alias SUGAR.App.view.layouts.BaseReportsDrillthroughDrawerLayout
 * @extends View.Layout
 */
({
    plugins: [\'ShortcutSession\'],

    shortcuts: [
        \'Sidebar:Toggle\',
        \'List:Headerpane:Create\',
        \'List:Select:Down\',
        \'List:Select:Up\',
        \'List:Scroll:Left\',
        \'List:Scroll:Right\',
        \'List:Select:Open\',
        \'List:Inline:Edit\',
        \'List:Delete\',
        \'List:Inline:Cancel\',
        \'List:Inline:Save\',
        \'List:Favorite\',
        \'List:Follow\',
        \'List:Preview\',
        \'List:Select\',
        \'SelectAll:Checkbox\',
        \'SelectAll:Dropdown\',
        \'Filter:Search\',
        \'Filter:Create\',
        \'Filter:Edit\',
        \'Filter:Show\'
    ],

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        /**
         * Cache for enum and enum like values
         */
        this.enums = {};
    },

    /**
     * Override the default loadData method to allow for manually constructing
     * context for each component in layout. We are loading data from the
     * ReportAPI in public method updateList. We need to first get any enum data
     * so that we can translate to english
     *
     * @override
     */
    loadData: function() {
        var enumsToFetch = this.context.get(\'enumsToFetch\');
        // Make requests for any enums here so they can happen while the drawer is still rendering
        if (!_.isEmpty(enumsToFetch) && _.isEmpty(this.enums)) {
            this._loadEnumOptions(enumsToFetch);
        } else {
            this.updateList();
        }
    },
    /**
     * Make a request for each enum like field so we can reverse lookup values later
     *
     * @param enumsToFetch
     * @private
     */
    _loadEnumOptions: function(enumsToFetch) {
        var reportDef = this.context.get(\'reportData\');
        var count = enumsToFetch.length;

        var enumSuccess = function(key, data) {
            count--;

            // cache the values inverted to help with reverse lookup
            this.enums[key] = _.invert(data);

            // I love that I have to simulate Promise.all but anyways, once
            // we have all our enum data, then make the record list request
            if (count === 0) {
                this.updateList();
            }
        };
        _.each(enumsToFetch, function(field) {
            var module = reportDef.full_table_list[field.table_key].module;
            var key = field.table_key + \':\' + field.name;
            app.api.enumOptions(module, field.name, {
                success: _.bind(enumSuccess, this, key)
            });
        }, this);
    },

    /**
     * Fetch report related records based on drawer context as defined in
     * saved-reports-chart dashlet or Report detail view with context containing
     * a filter definition based on a chart click event. This method will also
     * render the list component in layout after data is fetched.
     */
    updateList: function() {
        var chartModule = this.context.get(\'chartModule\');
        var reportId = this.context.get(\'reportId\');
        var reportDef = this.context.get(\'reportData\');
        var params = this.context.get(\'dashConfig\');

        // At this point, we should have finished all translations and requests for translations so
        // we can finally build the filter in english
        var filterDef = SUGAR.charts.buildFilter(reportDef, params, this.enums);
        this.context.set(\'filterDef\', filterDef);
        var useSavedFilters = this.context.get(\'useSavedFilters\') || false;

        var endpoint = function(method, model, options, callbacks) {
            var params = _.extend(options.params || {},
                {view: \'list\', group_filters: filterDef, use_saved_filters: useSavedFilters});
            var url = app.api.buildURL(\'Reports\', \'records\', {id: reportId}, params);
            return app.api.call(\'read\', url, null, callbacks);
        };
        var callbacks = {
            success: _.bind(function(data) {
                if (this.disposed) {
                    return;
                }
                this.context.trigger(\'refresh:count\');
                this.context.trigger(\'refresh:drill:labels\');
            }, this),
            error: function(o) {
                app.alert.show(\'listfromreport_loading\', {
                    level: \'error\',
                    messages: app.lang.get(\'ERROR_RETRIEVING_DRILLTHRU_DATA\', \'Reports\')
                });
            },
            complete: function(data) {
                app.alert.dismiss(\'listfromreport_loading\');
            }
        };
        var collection = this.context.get(\'collection\');
        collection.module = chartModule;
        collection.model = app.data.getBeanClass(chartModule);
        collection.setOption(\'endpoint\', endpoint);
        collection.setOption(\'fields\', this.context.get(\'fields\'));
        collection.fetch(callbacks);
        var massCollection = this.context.get(\'mass_collection\');
        if (massCollection) {
            massCollection.setOption(\'endpoint\', endpoint);
        }
    }
})
',
    ),
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'drillthrough-headerpane',
                    ),
                    1 => 
                    array (
                      'layout' => 'drillthrough-list',
                      'xmeta' => 
                      array (
                        'components' => 
                        array (
                          0 => 
                          array (
                            'view' => 'massupdate',
                          ),
                          1 => 
                          array (
                            'view' => 'massaddtolist',
                          ),
                          2 => 
                          array (
                            'view' => 'recordlist',
                            'primary' => true,
                            'xmeta' => 
                            array (
                              'favorite' => true,
                            ),
                          ),
                          3 => 
                          array (
                            'view' => 'list-bottom',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'dashboard-pane',
                  'css_class' => 'dashboard-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'drillthrough-pane',
                    ),
                  ),
                ),
              ),
              2 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'preview',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'drillthrough-list' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Reports.DrillthroughListLayout
 * @alias SUGAR.App.view.layouts.BaseReportsDrillthroughListLayout
 * @extends View.Views.Base.ListLayout
 */
({
    extendsFrom: \'ListLayout\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        // from this level down we should use target module instead of Reports
        // model needs to be a target module bean so some list level operations can work
        // for different module
        // for example, the function _filterMeta relies on the correct model
        var chartModule = options.context.get(\'chartModule\');
        options.context.set(\'model\', app.data.createBean(chartModule));
        options.context.set(\'collection\', app.data.createBeanCollection(chartModule));

        options.module = chartModule;
        options = this._removeFieldSorting(options);
        this._super(\'initialize\', [options]);
    },

    /**
     * Set the sortable property to false for all fields
     * We don\'t want to sort in the drill-through drawer
     *
     * @param {Object} options Backbone view options
     * @return {Object} options with the fields\' sortable property set to false
     * @private
     */
    _removeFieldSorting: function(options) {
        var listMeta = app.metadata.getView(options.module, \'list\');
        var fields = _.first(listMeta.panels).fields;
        var unsortableFields = _.each(fields, function(field) {
            field.sortable = false;
        });

        var panels = [{fields: unsortableFields}];
        options.meta.components[2].xmeta.panels = panels;
        return options;
    }
})
',
    ),
  ),
  '_hash' => 'e8a267da238f2859273301d44677ffce',
);

