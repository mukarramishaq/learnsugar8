<?php
$clientCache['Manufacturers']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#ProductTemplates/create',
        'label' => 'LNK_NEW_PRODUCT',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#ProductTemplates',
        'label' => 'LNK_PRODUCT_LIST',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      2 => 
      array (
        'route' => '#Manufacturers',
        'label' => 'LNK_NEW_MANUFACTURER',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      3 => 
      array (
        'route' => '#ProductCategories',
        'label' => 'LNK_NEW_PRODUCT_CATEGORY',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      4 => 
      array (
        'route' => '#ProductTypes',
        'label' => 'LNK_NEW_PRODUCT_TYPE',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      5 => 
      array (
        'route' => '#bwc/index.php?module=Import&action=Step1&import_module=Manufacturers&return_module=Manufacturers&return_action=index',
        'label' => 'LNK_IMPORT_MANUFACTURERS',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-arrow-circle-o-up',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => 'f3b169160c856dbbf1706b996dc70513',
);

