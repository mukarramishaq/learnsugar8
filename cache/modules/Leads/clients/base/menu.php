<?php
$clientCache['Leads']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Leads/create',
        'label' => 'LNK_NEW_LEAD',
        'acl_action' => 'create',
        'acl_module' => 'Leads',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#Leads/vcard-import',
        'label' => 'LNK_IMPORT_VCARD',
        'acl_action' => 'create',
        'acl_module' => 'Leads',
        'icon' => 'fa-plus',
      ),
      2 => 
      array (
        'route' => '#Leads',
        'label' => 'LNK_LEAD_LIST',
        'acl_action' => 'list',
        'acl_module' => 'Leads',
        'icon' => 'fa-bars',
      ),
      3 => 
      array (
        'route' => '#Reports?filterModule=Leads',
        'label' => 'LNK_LEAD_REPORTS',
        'acl_action' => 'list',
        'acl_module' => 'Reports',
        'icon' => 'fa-bar-chart-o',
      ),
      4 => 
      array (
        'route' => '#bwc/index.php?module=Import&action=Step1&import_module=Leads&return_module=Leads&return_action=index',
        'label' => 'LNK_IMPORT_LEADS',
        'acl_action' => 'import',
        'acl_module' => 'Leads',
        'icon' => 'fa-arrow-circle-o-up',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_LEAD',
      'visible' => true,
      'order' => 3,
      'icon' => 'fa-plus',
      'related' => 
      array (
        0 => 
        array (
          'module' => 'Accounts',
          'link' => 'leads',
        ),
        1 => 
        array (
          'module' => 'Contacts',
          'link' => 'leads',
        ),
        2 => 
        array (
          'module' => 'Opportunities',
          'link' => 'leads',
        ),
      ),
    ),
  ),
  '_hash' => '16f9b568d0e120aa10fef9d1c7a5f81c',
);

