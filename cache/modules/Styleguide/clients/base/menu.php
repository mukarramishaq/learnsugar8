<?php
$clientCache['Styleguide']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Styleguide/docs/index',
        'label' => 'Core Elements',
        'acl_action' => 'list',
        'acl_module' => 'Accounts',
        'icon' => 'fa-book',
      ),
      1 => 
      array (
        'route' => '#Styleguide/fields/index',
        'label' => 'Example Sugar7 Fields',
        'acl_action' => 'list',
        'acl_module' => 'Accounts',
        'icon' => 'fa-list-alt',
      ),
      2 => 
      array (
        'route' => '#Styleguide/views/index',
        'label' => 'Example Sugar7 Views',
        'acl_action' => 'list',
        'acl_module' => 'Accounts',
        'icon' => 'fa-bars',
      ),
      3 => 
      array (
        'route' => '#Styleguide/layout/records',
        'label' => 'Default Module List Layout',
        'acl_action' => 'list',
        'acl_module' => 'Accounts',
        'icon' => 'fa-columns',
      ),
      4 => 
      array (
        'route' => '#Styleguide/create',
        'label' => 'Default Record Create Layout',
        'acl_action' => 'list',
        'acl_module' => 'Accounts',
        'icon' => 'fa-plus',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => 'eb793c749a1ade047c665758de54902b',
);

