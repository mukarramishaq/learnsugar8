<?php
$clientCache['Styleguide']['base']['layout'] = array (
  'views' => 
  array (
    'meta' => 
    array (
      'css_class' => 'styleguide',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'base',
            'name' => 'sidebar',
            'css_class' => 'row-fluid',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span12',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'sg-headerpane',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    plugins: [\'Prettify\'],
    extendsFrom: \'StyleguideDocsLayout\'
})
',
    ),
  ),
  'styleguide' => 
  array (
    'meta' => 
    array (
      'css_class' => 'styleguide',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'base',
            'css_class' => 'row-fluid',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'css_class' => 'main-pane span12',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'sg-headerpane',
                    ),
                    1 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'styleguide',
                        'css_class' => 'container-fluid',
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      'metadata' => 
      array (
        'chapters' => 
        array (
          'home' => 
          array (
            'title' => 'Styleguide',
            'description' => 'A guide to styling the Sugar7 User Interface',
            'index' => false,
          ),
          'docs' => 
          array (
            'title' => 'Core UI Elements',
            'description' => 'Simple and flexible HTML, CSS, and Javascript for popular user interface components and interactions.',
            'index' => true,
            'sections' => 
            array (
              'base' => 
              array (
                'title' => 'Base CSS',
                'description' => 'Basic HTML elements styled and enhanced with extensible classes for a fresh, consistent look and feel.',
                'index' => true,
                'pages' => 
                array (
                  'typography' => 
                  array (
                    'title' => 'Typography',
                    'description' => 'Headings, paragraphs, lists, and other inline type elements.',
                  ),
                  'grid' => 
                  array (
                    'title' => 'Grid system',
                    'description' => 'A responsive 12-column grid fluid-width layout.',
                  ),
                  'icons' => 
                  array (
                    'title' => 'Icons',
                    'description' => 'Font Awesome icon library for scalable font based icons and glyphs.',
                  ),
                  'mixins' => 
                  array (
                    'title' => 'Mixins',
                    'description' => 'Include or generate snippets of CSS with parameters.',
                  ),
                  'responsive' => 
                  array (
                    'title' => 'Responsive design',
                    'description' => 'Media queries for various devices and resolutions.',
                  ),
                  'variables' => 
                  array (
                    'title' => 'Variables',
                    'description' => 'LESS variables, HTML values, and usage guidelines.',
                  ),
                  'labels' => 
                  array (
                    'title' => 'Labels',
                    'description' => 'Label and text annotations.',
                  ),
                  'edit' => 
                  array (
                    'title' => 'Edit Documentation',
                    'description' => 'Instructions for updating Styleguide documentation.',
                  ),
                  'theme' => 
                  array (
                    'title' => 'Custom Theme Variables',
                    'description' => 'Instructions for modifying theme colors.',
                  ),
                ),
              ),
              'forms' => 
              array (
                'title' => 'Form Elements',
                'description' => 'Basic form elements and layouts for a consistent editing experience.',
                'index' => true,
                'pages' => 
                array (
                  'fields' => 
                  array (
                    'title' => 'Sugar7 fields',
                    'url' => '#Styleguide/fields/index',
                    'description' => 'Basic fields that support detail, record, and edit modes with error addons.',
                  ),
                  'buttons' => 
                  array (
                    'title' => 'Buttons',
                    'description' => 'Standard css only button styles.',
                  ),
                  'layouts' => 
                  array (
                    'title' => 'Form layouts',
                    'description' => 'Customized layouts of field components.',
                  ),
                  'file' => 
                  array (
                    'title' => 'File uploader',
                    'description' => 'Stylized file upload widget.',
                  ),
                  'datetime' => 
                  array (
                    'title' => 'Date-time picker',
                    'description' => 'Lightweight date/time picker.',
                  ),
                  'select2' => 
                  array (
                    'title' => 'Select2',
                    'description' => 'jQuery plugin replacement for select boxes. It supports searching, remote data sets, and infinite scrolling of results.',
                  ),
                  'jstree' => 
                  array (
                    'title' => 'jsTree',
                    'description' => 'jQuery plugin cross browser tree component.',
                  ),
                  'range' => 
                  array (
                    'title' => 'Range Slider',
                    'description' => 'jQuery plugin range picker.',
                  ),
                  'switch' => 
                  array (
                    'title' => 'Switch',
                    'description' => 'jQuerty plugin turns check boxes into toggle switch.',
                  ),
                ),
              ),
              'components' => 
              array (
                'title' => 'Components',
                'description' => 'Dozens of reusable components are built in to provide navigation, alerts, popovers, and much more.',
                'index' => true,
                'pages' => 
                array (
                  'alerts' => 
                  array (
                    'title' => 'Alerts',
                    'description' => 'Styles for success, warning, and error messages.',
                  ),
                  'collapse' => 
                  array (
                    'title' => 'Collapse',
                    'description' => 'Get base styles and flexible support for collapsible components like accordions and navigation.',
                  ),
                  'dropdowns' => 
                  array (
                    'title' => 'Dropdowns',
                    'description' => 'Add dropdown menus to nearly anything with this simple plugin. Features full dropdown menu support on in the navbar, tabs, and pills.',
                  ),
                  'popovers' => 
                  array (
                    'title' => 'Popovers',
                    'description' => 'Add small overlays of content, like those on the iPad, to any element for housing secondary information.',
                  ),
                  'progress' => 
                  array (
                    'title' => 'Progress bars',
                    'description' => 'For loading, redirecting, or action status.',
                  ),
                  'tooltips' => 
                  array (
                    'title' => 'Tooltips',
                    'description' => 'A new take on the jQuery Tipsy plugin, Tooltips don\'t rely on images, uses CSS3 for animations, and data-attributes for local title storage.',
                  ),
                  'keybindings' => 
                  array (
                    'title' => 'Key bindings',
                    'description' => 'Interacting with UI components using the keyboard.',
                  ),
                ),
              ),
              'layouts' => 
              array (
                'title' => 'Layouts & Views',
                'description' => 'Modals, navbars, and other layout widgets.',
                'index' => true,
                'pages' => 
                array (
                  'list' => 
                  array (
                    'title' => 'List Tables',
                    'description' => 'For, you guessed it, tabular data.',
                  ),
                  'record' => 
                  array (
                    'title' => 'Record Views',
                    'url' => '#Styleguide/create',
                    'description' => 'Detail, edit and create views for records.',
                  ),
                  'drawer' => 
                  array (
                    'title' => 'Drawers',
                    'description' => 'Drawer is a form of a modal that pushes main content down and expands from the top taking 100% of the screen width.',
                  ),
                  'navbar' => 
                  array (
                    'title' => 'Navbar',
                    'description' => 'Top level navigation layout.',
                  ),
                  'tabs' => 
                  array (
                    'title' => 'Tab Navigation',
                    'description' => 'Highly customizable list-based navigation.',
                  ),
                ),
              ),
              'dashboards' => 
              array (
                'title' => 'Dashboards',
                'description' => 'Documentation and guidelines for dashboards within the app.',
                'index' => true,
                'pages' => 
                array (
                  'home' => 
                  array (
                    'title' => 'Home Module Dashboard',
                    'description' => 'A grid layout for arranging dashlets.',
                  ),
                  'intel' => 
                  array (
                    'title' => 'Intelligence Pane Dashboard',
                    'description' => 'Special features of the content related right hand side dashboard.',
                  ),
                  'dashlets' => 
                  array (
                    'title' => 'Dashlets',
                    'description' => 'Patterns, styles, and elements for creating dashlets.',
                  ),
                ),
              ),
              'charts' => 
              array (
                'title' => 'Sucrose Charts',
                'description' => 'Standard and custom charts in Sugar7 are developed using D3 and the Sucrose Charts library. For configuration details see the <a href=\'http://sucrose.io\'>sucrose.io</a> website.',
                'index' => true,
                'pages' => 
                array (
                  'types' => 
                  array (
                    'title' => 'Chart Types',
                    'description' => 'Currently supported Sucrose chart types.',
                  ),
                  'colors' => 
                  array (
                    'title' => 'Chart Colors',
                    'description' => 'Flexible methods for assigning color maps and fill methods to Sucrose charts.',
                  ),
                ),
              ),
            ),
          ),
          'fields' => 
          array (
            'title' => 'Example Sugar7 Fields',
            'description' => 'Basic fields that support detail, record, and edit modes with error addons.',
            'index' => false,
          ),
          'views' => 
          array (
            'title' => 'Example Sugar7 Views',
            'description' => 'Basic views are the building blocks of a layout.',
            'index' => true,
            'sections' => 
            array (
              'list' => 
              array (
                'title' => 'List Views',
                'description' => 'List views for simple and complex data tables.',
                'index' => true,
                'pages' => 
                array (
                  'basic' => 
                  array (
                    'title' => 'Basic List',
                    'description' => 'Simple table layouts with striping.',
                  ),
                ),
              ),
              'dashlet' => 
              array (
                'title' => 'Dashlet Views',
                'description' => 'Component views combined to form a dashlet.',
                'index' => true,
                'pages' => 
                array (
                  'toolbar' => 
                  array (
                    'title' => 'Toolbar',
                    'description' => 'Dashlet header bar for interacting with dashlet.',
                  ),
                ),
              ),
            ),
          ),
        ),
        'template_values' => 
        array (
          'last_updated' => '2015-12-01T22:47:00+00:00',
          'version' => '7.8.0',
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    initialize: function(options) {
        var request = {
                page_data: {},
                keys: [],
                chapter_details: {},
                section_details: {},
                page_details: {},
                parent_link: \'\',
                view: \'index\'
            };
        var chapterName;
        var contentName;
        var chapter;
        var section;
        var page;

        chapterName = options.context.get(\'chapter_name\');
        contentName = options.context.get(\'content_name\');

        // load up the styleguide css if not already loaded
        //TODO: cleanup styleguide.css and add to main file
        if ($(\'head #styleguide_css\').length === 0) {
            $(\'<link>\')
                .attr({
                    rel: \'stylesheet\',
                    href: \'styleguide/assets/css/styleguide.css\',
                    id: \'styleguide_css\'
                })
                .appendTo(\'head\');
        }

        document.title = $(\'<span/>\').html(\'Styleguide &#187; SugarCRM\').text();

        // request.page_data = this.meta.metadata.page_data;
        request.page_data = app.metadata.getLayout(options.module, \'styleguide\').metadata.chapters;

        request.keys = [chapterName];
        if (!_.isUndefined(contentName) && !_.isEmpty(contentName)) {
            Array.prototype.push.apply(request.keys, contentName.split(\'-\'));
        }

        chapter = request.page_data[request.keys[0]];
        request.chapter_details = {
            title: chapter.title,
            description: chapter.description
        };
        if (chapter.index && request.keys.length > 1 && request.keys[1] !== \'index\') {
            section = chapter.sections[request.keys[1]];
            request.section_details = {
                title: section.title,
                description: section.description
            };
            if (section.index && request.keys.length > 2 && request.keys[2] !== \'index\') {
                page = section.pages[request.keys[2]];
                request.page_details = {
                    title: page.title,
                    description: page.description,
                    url: page.url
                };
                request.view = contentName;
                request.parent_link = \'-\' + request.keys[0][request.keys[1]];
                window.prettyPrint && prettyPrint();
            } else {
                request.page_details = request.section_details;
            }
        } else {
            request.page_details = request.chapter_details;
        }

        request.page_details.css_class = \'container-fluid\';

        options.context.set(\'request\', request);

        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'fields' => 
  array (
    'meta' => 
    array (
      'css_class' => 'styleguide',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'base',
            'css_class' => 'row-fluid',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'css_class' => 'main-pane span12',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'sg-headerpane',
                    ),
                    1 => 
                    array (
                      'view' => 'fields-index',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    plugins: [\'Prettify\'],
    extendsFrom: \'StyleguideStyleguideLayout\'
})
',
    ),
  ),
  'records' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'list-headerpane',
                    ),
                    1 => 
                    array (
                      'layout' => 
                      array (
                        'type' => 'filterpanel',
                        'last_state' => 
                        array (
                          'id' => 'list-filterpanel',
                          'defaults' => 
                          array (
                            'toggle-view' => 'list',
                          ),
                        ),
                        'availableToggles' => 
                        array (
                          0 => 
                          array (
                            'name' => 'list',
                            'icon' => 'fa-table',
                            'label' => 'LBL_LISTVIEW',
                          ),
                          1 => 
                          array (
                            'name' => 'activitystream',
                            'icon' => 'fa-clock-o',
                            'label' => 'LBL_ACTIVITY_STREAM',
                          ),
                        ),
                        'components' => 
                        array (
                          0 => 
                          array (
                            'layout' => 'filter',
                            'loadModule' => 'Filters',
                          ),
                          1 => 
                          array (
                            'view' => 'filter-rows',
                          ),
                          2 => 
                          array (
                            'view' => 'filter-actions',
                          ),
                          3 => 
                          array (
                            'layout' => 'activitystream',
                            'context' => 
                            array (
                              'module' => 'Activities',
                            ),
                          ),
                          4 => 
                          array (
                            'layout' => 'list',
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'dashboard-pane',
                  'css_class' => 'dashboard-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'dashboard',
                      'context' => 
                      array (
                        'forceNew' => true,
                        'module' => 'Home',
                      ),
                      'loadModule' => 'Dashboards',
                    ),
                  ),
                ),
              ),
              2 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'preview',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'docs' => 
  array (
    'meta' => 
    array (
      'css_class' => 'styleguide',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'css_class' => 'main-content',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'styleguide main-pane span12',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'sg-headerpane',
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'dashlet-preview',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'templates' => 
    array (
      'docs' => '
{{component this \'main-pane\'}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    plugins: [\'Prettify\'],
    extendsFrom: \'StyleguideStyleguideLayout\',

    /**
     * @inheritdoc
     */
    initComponents: function(components, context, module) {
        var def;
        var main;
        var content;
        var request = this.context.get(\'request\');

        this._super(\'initComponents\', [components, context, module]);

        def = {
            view: {
                type: request.keys[0] + \'-\' + request.view,
                name: request.keys[0] + \'-\' + request.view,
                meta: request.page_details
            }
        };

        main = this.getComponent(\'sidebar\').getComponent(\'main-pane\');
        content = this.createComponentFromDef(def, this.context, this.module);
        main.addComponent(content);
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        var defaultLayout = this.getComponent(\'sidebar\');
        if (defaultLayout) {
            defaultLayout.trigger(\'sidebar:toggle\', false);
        }

        this._super(\'_render\');
    }
})
',
    ),
  ),
  '_hash' => '8fa19d8ee3bb211b7939bb9c62d88f3a',
);

