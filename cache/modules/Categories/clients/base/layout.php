<?php
$clientCache['Categories']['base']['layout'] = array (
  'nested-set-list' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.NestedSetListLayout
 * @alias SUGAR.App.view.layouts.BaseNestedSetListLayout
 * @extends View.Layout
 */
({
    plugins: [\'ShortcutSession\'],

    shortcuts: [
        \'Sidebar:Toggle\'
    ],

    /**
     * @inheritdoc
     */
    loadData: function(options) {
        var fields = _.union(this.getFieldNames(), (this.context.get(\'fields\') || []));
        this.context.set(\'fields\', fields);
        this._super(\'loadData\', [options]);
    }
})
',
    ),
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'nested-set-headerpane',
                    ),
                    1 => 
                    array (
                      'view' => 'tree',
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'preview',
                    ),
                  ),
                ),
              ),
              2 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'side-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'selection-sidebar',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  '_hash' => '26ff38ff0a73aea7e3e2bea116b71a6f',
);

