<?php
$clientCache['Emails']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Emails/compose',
        'label' => 'LBL_COMPOSE_MODULE_NAME_SINGULAR',
        'acl_action' => 'create',
        'acl_module' => 'Emails',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#Emails/create',
        'label' => 'LBL_CREATE_ARCHIVED_EMAIL',
        'acl_action' => 'create',
        'acl_module' => 'Emails',
        'icon' => 'fa-plus',
      ),
      2 => 
      array (
        'route' => '#Emails',
        'label' => 'LNK_EMAIL_LIST',
        'acl_action' => 'list',
        'acl_module' => 'Emails',
        'icon' => 'fa-bars',
      ),
      3 => 
      array (
        'route' => '#bwc/index.php?module=EmailTemplates&action=EditView&return_module=EmailTemplates&return_action=DetailView',
        'label' => 'LNK_NEW_EMAIL_TEMPLATE',
        'acl_action' => 'create',
        'acl_module' => 'EmailTemplates',
        'icon' => 'fa-plus',
      ),
      4 => 
      array (
        'route' => '#bwc/index.php?module=EmailTemplates&action=index',
        'label' => 'LNK_EMAIL_TEMPLATE_LIST',
        'acl_action' => 'list',
        'acl_module' => 'EmailTemplates',
        'icon' => 'fa-bars',
      ),
      5 => 
      array (
        'route' => '#UserSignatures/create',
        'label' => 'LNK_NEW_EMAIL_SIGNATURE',
        'acl_action' => 'create',
        'acl_module' => 'Emails',
        'icon' => 'fa-plus',
      ),
      6 => 
      array (
        'route' => '#UserSignatures',
        'label' => 'LNK_EMAIL_SIGNATURE_LIST',
        'acl_action' => 'create',
        'acl_module' => 'Emails',
        'icon' => 'fa-bars',
      ),
      7 => 
      array (
        'route' => '#OutboundEmail',
        'label' => 'LNK_EMAIL_SETTINGS_LIST',
        'acl_action' => 'list',
        'acl_module' => 'OutboundEmail',
        'icon' => 'fa-cog',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'compose',
      'label' => 'LBL_COMPOSE_MODULE_NAME_SINGULAR',
      'visible' => true,
      'order' => 5,
      'icon' => 'fa-plus',
    ),
  ),
  '_hash' => '26e5fbe98a5a95f10d900009ab0140df',
);

