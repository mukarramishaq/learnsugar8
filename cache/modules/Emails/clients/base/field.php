<?php
$clientCache['Emails']['base']['field'] = array (
  'sender' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.SenderField
 * @alias SUGAR.App.view.fields.BaseEmailsSenderField
 * @extends View.Fields.Base.BaseField
 * @deprecated Use {@link View.Fields.Base.Emails.OutboundEmailField} instead.
 */
({
    fieldTag: \'input.select2\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Fields.Base.Emails.SenderField is deprecated. Use \' +
            \'View.Fields.Base.Emails.OutboundEmailField instead.\');

        this._super(\'initialize\', [options]);
        this.endpoint = this.def.endpoint;
    },

    _render: function() {
        var result = app.view.Field.prototype._render.call(this);

        if (this.tplName === \'edit\') {
            var action = (this.endpoint.action) ? this.endpoint.action : null,
                attributes = (this.endpoint.attributes) ? this.endpoint.attributes : null,
                params = (this.endpoint.params) ? this.endpoint.params : null,
                myURL = app.api.buildURL(this.endpoint.module, action, attributes, params);

            app.api.call(\'GET\', myURL, null, {
                success: _.bind(this.populateValues, this),
                error: function(error) {
                    // display error if not a metadata refresh
                    if (error.status !== 412) {
                        app.alert.show(\'server-error\', {
                            level: \'error\',
                            messages: \'ERR_GENERIC_SERVER_ERROR\'
                        });
                    }
                    app.error.handleHttpError(error);
                }
            });
        }

        return result;
    },

    populateValues: function(results) {
        var self = this,
            defaultResult,
            defaultValue = {};

        if (this.disposed === true) {
            return; //if field is already disposed, bail out
        }

        if (!_.isEmpty(results)) {
            defaultResult = _.find(results, function(result) {
                return result.default;
            });

            defaultValue = (defaultResult) ? defaultResult : results[0];

            if (!this.model.has(this.name)) {
                this.model.set(this.name, defaultValue.id);
                this.model.setDefault(this.name, defaultValue.id);
            }
        }

        var format = function(item) {
            return item.display;
        };

        this.$(this.fieldTag).select2({
            data:{ results: results, text: \'display\' },
            formatSelection: format,
            formatResult: format,
            width: \'100%\',
            placeholder: app.lang.get(\'LBL_SELECT_FROM_SENDER\', this.module),
            initSelection: function(el, callback) {
                if (!_.isEmpty(defaultValue)) {
                      callback(defaultValue);
                }
            }
        }).on("change", function(e) {
            if (self.model.get(self.name) !== e.val) {
                self.model.set(self.name, e.val, {silent: true});
            }
        });
    },

    /**
     * @inheritdoc
     *
     * We need this empty so it won\'t affect refresh the select2 plugin
     */
    bindDomChange: function() {
    }
})
',
    ),
    'templates' => 
    array (
      'edit' => '
<input type="hidden" name="{{name}}" class="select2 inherit-width" value="{{value}}" data-id="{{fieldValue this.model this.def.id_name}}">
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
      'detail' => '
{{value}}
',
    ),
  ),
  'recipients-fieldset' => 
  array (
    'templates' => 
    array (
      'recipient-options' => '
<div class="compose-recipient-options">
    <div class="btn-group">
        <button type="button" class="btn" data-toggle-field="cc_collection"><span class="text">{{str "LBL_CC_BUTTON" this.module}}</span></button>
        <button type="button" class="btn" data-toggle-field="bcc_collection"><span class="text">{{str "LBL_BCC_BUTTON" this.module}}</span></button>
    </div>
</div>
',
      'edit' => '
{{#each fields}}
<div class="fieldset-group">
    <div class="fieldset-label" data-name="{{name}}">{{str label ../module}}</div>
    <div class="fieldset-field" data-type="{{type}}" data-name="{{name}}">{{placeholder}}</div>
</div>
{{/each}}
',
      'detail' => '

<div class="fieldset-field detail">
    <span class="scroll">{{value}}</span>
</div>
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Recipients field group for handling expand to edit
 *
 * @class View.Fields.Base.Emails.RecipientsFieldsetField
 * @alias SUGAR.App.view.fields.BaseEmailsRecipientsFieldsetField
 * @extends View.Fields.Base.FieldsetField
 */
({
    extendsFrom: \'FieldsetField\',

    _addressBookState: \'closed\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.events = _.extend({}, this.events, {
            \'click [data-toggle-field]\': \'_handleToggleButtonClick\',
            \'click .fieldset-field\': \'_focus\'
        });
        this._super(\'initialize\', [options]);

        this.listenTo(this.view, \'address-book-state\', function(state) {
            this._addressBookState = state;
        });
        this.listenTo(this.view, \'tinymce:focus\', this._blur);
        $(document).on(\'click.email-recipients\', _.bind(this._blur, this));
    },

    /**
     * Adds the CC and BCC toggle buttons to the From field and sets the
     * visibility of those fields. Switches the field to edit mode when there
     * are no recipients and the user is creating an email.
     *
     * @inheritdoc
     */
    _render: function() {
        var cc = this.model.get(\'cc_collection\');
        var bcc = this.model.get(\'bcc_collection\');

        this._super(\'_render\');

        this._addToggleButtons(\'outbound_email_id\');
        this._toggleFieldVisibility(\'cc_collection\', !!cc.length);
        this._toggleFieldVisibility(\'bcc_collection\', !!bcc.length);
    },

    /**
     * @inheritdoc
     * @example
     * // Only the To field has recipients.
     * a@b.com, b@c.com
     * @example
     * // All fields have recipients.
     * a@b.com; CC: c@d.com; BCC: e@f.com
     * @example
     * // CC does not have recipients.
     * a@b.com; BCC: e@f.com
     * @example
     * Only the CC field has recipients.
     * CC: c@d.com
     */
    format: function(value) {
        return _.chain(this.fields)
            // The from field is not used for calculating the value.
            .where({type: \'email-recipients\'})
            // Construct each field\'s string from it\'s formatted value.
            .reduce(function(fields, field) {
                var models = field.getFormattedValue();
                var str = _.map(models, function(model) {
                    var name = model.get(\'parent_name\') || \'\';
                    var email = model.get(\'email_address\') || \'\';

                    // The name was erased, so let\'s use the label.
                    if (_.isEmpty(name) && model.isNameErased()) {
                        name = app.lang.get(\'LBL_VALUE_ERASED\', model.module);
                    }

                    if (!_.isEmpty(name)) {
                        return name;
                    }

                    // The email was erased, so let\'s use the label.
                    if (_.isEmpty(email) && model.isEmailErased()) {
                        email = app.lang.get(\'LBL_VALUE_ERASED\', model.module);
                    }

                    return email;
                }).join(\', \');

                if (!_.isEmpty(str)) {
                    fields[field.name] = str;
                }

                return fields;
            }, {})
            // Add the label for each field\'s string.
            .map(function(field, fieldName) {
                var label = \'\';

                if (fieldName === \'cc_collection\') {
                    label = app.lang.get(\'LBL_CC\', this.module) + \': \';
                } else if (fieldName === \'bcc_collection\') {
                    label = app.lang.get(\'LBL_BCC\', this.module) + \': \';
                }

                return label + field;
            }, this)
            .value()
            // Separate each field\'s string by a semi-colon.
            .join(\'; \');
    },

    /**
     * Cannot switch to detail mode if in create mode and there are no
     * recipients. The mode is set to edit.
     *
     * @inheritdoc
     */
    setMode: function(name) {
        var to = this.model.get(\'to_collection\');
        var cc = this.model.get(\'cc_collection\');
        var bcc = this.model.get(\'bcc_collection\');
        var hasRecipients = to.length > 0 || cc.length > 0 || bcc.length > 0;

        if (this.view.createMode && name === \'detail\' && !hasRecipients) {
            name = \'edit\';
        }

        this._super(\'setMode\', [name]);
    },

    /**
     * Switches the field to edit mode when the user clicks on the fieldset.
     *
     * @param {Event} [event]
     * @public
     */
    _focus: function(event) {
        if (this.disposed) {
            return;
        }

        // Stop the event from triggering _blur to be called.
        if (event) {
            event.stopPropagation();
        }

        if (this.action !== \'edit\') {
            this.setMode(\'edit\');
        }
    },

    /**
     * Switches the field to detail mode when the user clicks outside the
     * fieldset.
     *
     * @param {Event} [event]
     * @public
     */
    _blur: function(event) {
        if (this.disposed) {
            return;
        }

        // Don\'t change modes if the address book is open.
        if (this._addressBookState === \'open\') {
            return;
        }

        if (this.action !== \'detail\') {
            this.setMode(\'detail\');
        }
    },

    /**
     * Add CC and BCC toggle buttons to the field.
     *
     * @param {string} fieldName The name of the field where the buttons are
     * added.
     * @private
     */
    _addToggleButtons: function(fieldName) {
        var field = this.view.getField(fieldName);
        var $field;
        var template;
        var html;

        if (!field) {
            return;
        }

        $field = field.$el.closest(\'.fieldset-field\');

        if ($field.length > 0) {
            template = app.template.getField(this.type, \'recipient-options\', this.module);
            html = template({module: this.module});
            $(html).appendTo($field);
        }
    },

    /**
     * Toggle the visibility of the field associated with the button that was
     * clicked.
     *
     * @param {Event} event
     * @private
     */
    _handleToggleButtonClick: function(event) {
        var $toggleButton = $(event.currentTarget);
        var fieldName = $toggleButton.data(\'toggle-field\');

        this._toggleFieldVisibility(fieldName);
    },

    /**
     * Toggles the visibility of the field and the toggle state of its
     * associated button.
     *
     * @param {string} fieldName The name of the field to toggle.
     * @param {boolean} [show] True when the button should be inactive and the
     * field should be shown. The toggle is flipped when undefined.
     * @private
     */
    _toggleFieldVisibility: function(fieldName, show) {
        var toggleButtonSelector = \'[data-toggle-field="\' + fieldName + \'"]\';
        var $toggleButton = this.$(toggleButtonSelector);
        var field = this.view.getField(fieldName);

        // if explicit active state not set, toggle to opposite
        if (_.isUndefined(show)) {
            show = !$toggleButton.hasClass(\'active\');
        }

        $toggleButton.toggleClass(\'active\', show);

        if (field) {
            field.$el.closest(\'.fieldset-group\').toggleClass(\'hide\', !show);
        }

        this.view.trigger(\'email-recipients:toggled\');
    },

    /**
     * @inheritdoc
     */
    _dispose: function() {
        $(document).off(\'click.email-recipients\');
        this._super(\'_dispose\');
    }
})
',
    ),
  ),
  'quickcreate' => 
  array (
    'templates' => 
    array (
      'quickcreate' => '
<a data-action="email" href="javascript:void(0);" track="click:quickCreate-Emails" tabindex="-1">{{#if def.icon}}<i class="fa {{def.icon}}"></i>{{/if}}{{label}}</a>
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.QuickcreateField
 * @alias SUGAR.App.view.fields.BaseEmailsQuickcreateField
 * @extends View.Fields.Base.QuickcreateField
 */
({
    extendsFrom: \'QuickcreateField\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.plugins = _.union(this.plugins || [], [\'EmailClientLaunch\']);
        this._super(\'initialize\', [options]);

        if (this.context && this.context.has(\'model\')) {
            // call updateEmailLinks if the user changes something on the context model
            // so if user changes the email address we make sure we\'ve got the latest
            // email address in the quick Compose Email link
            this.context.get(\'model\').on(\'change\', this.updateEmailLinks, this);
        }

        app.routing.before(\'route\', this._beforeRouteChanged, this);
        app.router.on(\'route\', this._routeChanged, this);
    },

    /**
     * Before we navigate to a different page, we need to remove the
     * change event listener we added on the context model
     *
     * @protected
     */
    _beforeRouteChanged: function() {
        if (this.context && this.context.has(\'model\')) {
            // route is about to change, need to remove previous
            // listeners before model gets changed
            this.context.get(\'model\').off(\'change\', null, this);
        }
    },

    /**
     * After the route has changed, we need to re-add the model listener
     * on the new context model. This also calls updateEmailLinks to blank
     * out any existing email on the current quickcreate link; e.g. re-set the
     * quick Compose Email link back to "mailto:"
     *
     * @protected
     */
    _routeChanged: function() {
        if (this.context && this.context.has(\'model\')) {
            // route has changed, most likely a new model, need to add new listeners
            this.context.get(\'model\').on(\'change\', this.updateEmailLinks, this);
        }
        this.updateEmailLinks();
    },

    /**
     * Used by EmailClientLaunch as a hook point to retrieve email options that are specific to a view/field
     * In this case we are using it to retrieve the parent model to make this email compose launching
     * context aware - prepopulating the to address with the given model and the parent relate field
     *
     * @return {Object}
     * @private
     */
    _retrieveEmailOptionsFromLink: function() {
        var context = this.context.parent || this.context,
            parentModel = context.get(\'model\'),
            emailOptions = {};

        if (parentModel && parentModel.id) {
            // set parent model as option to be passed to compose for To address & relate
            // if parentModel does not have email, it will be ignored as a To recipient
            // if parentModel\'s module is not an available module to relate, it will also be ignored
            emailOptions = {
                to: [{bean: parentModel}],
                related: parentModel
            };
        }

        return emailOptions;
    },

    /**
     * @inheritdoc
     */
    _dispose: function() {
        // remove context model change listeners if they exist
        this._beforeRouteChanged();
        app.routing.offBefore(\'route\', this.beforeRouteChanged, this);
        app.router.off(\'route\', this.routeChanged, this);

        this._super(\'_dispose\');
    }
})
',
    ),
  ),
  'email-attachments' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.EmailAttachmentsField
 * @alias SUGAR.App.view.fields.BaseEmailsEmailAttachmentsField
 * @extends View.Fields.Base.EmailAttachmentsField
 */
({
    extendsFrom: \'BaseEmailAttachmentsField\',

    /**
     * @inheritdoc
     *
     * Adds a listener for the `email_attachments:template` event, which is
     * triggered on the view to add attachments. The handler will fetch the
     * attachments from a template, so that they can be copied to the email.
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);
        this.listenTo(this.view, \'email_attachments:template\', this._fetchTemplateAttachments);
    },

    /**
     * Retrieves all of an email template\'s attachments so they can be added to
     * the email.
     *
     * @param {Data.Bean} template The email template whose attachments are to
     * be added.
     * @private
     */
    _fetchTemplateAttachments: function(template) {
        var def;
        var notes = app.data.createBeanCollection(\'Notes\');
        var request;

        if (this.disposed === true) {
            return;
        }

        def = [{
            //FIXME: email_type should be EmailTemplates
            email_id: {
                \'$equals\': template.get(\'id\')
            }
        }];
        request = notes.fetch({
            filter: {
                filter: def
            },
            success: _.bind(this._handleTemplateAttachmentsFetchSuccess, this),
            complete: _.bind(function(request) {
                if (request && request.uid) {
                    delete this._requests[request.uid];
                }
            }, this)
        });

        // This request is not associated with a placeholder because
        // placeholders aren\'t used when handling templates.
        if (request && request.uid) {
            this._requests[request.uid] = request;
        }
    },

    /**
     * Handles a successful response from the API for retrieving an email
     * template\'s attachments.
     *
     * The relevant data is taken from each record and added as an attachment.
     * Before adding the new attachments, all existing attachments that came
     * from another email template are removed.
     *
     * @param {Data.BeanCollection} notes The collection of attachments from
     * the template.
     * @private
     */
    _handleTemplateAttachmentsFetchSuccess: function(notes) {
        var attachments;
        var existingTemplateAttachments;
        var newTemplateAttachments;

        if (this.disposed === true) {
            return;
        }

        // Remove all existing attachments that came from an email template.
        attachments = this.model.get(this.name);
        existingTemplateAttachments = attachments.where({file_source: \'EmailTemplates\'});
        attachments.remove(existingTemplateAttachments);

        // Add the attachments from the new email template.
        newTemplateAttachments = notes.map(function(model) {
            return {
                _link: \'attachments\',
                upload_id: model.get(\'id\'),
                name: model.get(\'filename\') || model.get(\'name\'),
                filename: model.get(\'filename\') || model.get(\'name\'),
                file_mime_type: model.get(\'file_mime_type\'),
                file_size: model.get(\'file_size\'),
                file_ext: model.get(\'file_ext\'),
                file_source: \'EmailTemplates\'
            };
        });
        attachments.add(newTemplateAttachments, {merge: true});
    }
})
',
    ),
  ),
  'htmleditable_tinymce' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.Htmleditable_tinymceField
 * @alias SUGAR.App.view.fields.BaseEmailsHtmleditable_tinymceField
 * @extends View.Fields.Base.Htmleditable_tinymceField
 */
({
    extendsFrom: \'Htmleditable_tinymceField\',

    /**
     * Force the field to display the correct view even if there is no data to
     * show.
     *
     * @property {boolean}
     */
    showNoData: false,

    /**
     * Constant for inserting content above the existing email body.
     *
     * @property {string}
     */
    ABOVE_CONTENT: \'above\',

    /**
     * Constant for inserting content below the existing email body.
     *
     * @property {string}
     */
    BELOW_CONTENT: \'below\',

    /**
     * Constant for inserting content into the email body at the current cursor
     * location.
     *
     * @property {string}
     */
    CURSOR_LOCATION: \'cursor\',

    /**
     * The tinyMCE button object for the signature dropdown.
     *
     * @private
     * @property {Object|null}
     */
    _signatureBtn: null,

    /**
     * The number of signatures found from the API response.
     *
     * @private
     * @property {number}
     */
    _numSignatures: 0,

    /**
     * Track the editor focus/blur state.
     *
     * @private
     * @property {boolean}
     */
    _editorFocused: false,

    /**
     * @inheritdoc
     *
     * Stores the user\'s default signature on the context using the attribute
     * name `current_signature`. This attribute is updated anytime a new
     * signature is selected.
     *
     * Stores the initial signature location for inserting the default
     * signature. If the context already has `signature_location` attribute,
     * then that value is used. Otherwise, this attribute is defaulted to
     * insert the signature below any content. This attribute is updated
     * anytime a signature is inserted in a different location.
     *
     * The default signature is inserted in the initial location if the email
     * is new. The signature is not inserted if the email is an existing draft
     * that is being edited. If the initial location is the cursor, then the
     * signature is inserted after the editor is fully loaded and the cursor
     * has been placed.
     *
     * For new replies, the cursor is placed above the reply content, once the
     * editor has been loaded.
     */
    initialize: function(options) {
        var signature;
        var location;
        // We insert an empty <p> node in the tinyMCE editor and use that to
        // focus the cursor to the bottom of the tinyMCE editor. This is
        // because if the last element in the editor has content
        // (i.e. <p>Sincercely, John Doe</p>) and we select that element, the
        // cursor would be placed at the beginning of the content
        // (in the example, the cursor would be before the "S" in Sincerely).
        var emptyNode;

        this._super(\'initialize\', [options]);

        // Get the default signature and store it on the context.
        signature = app.user.getPreference(\'signature_default\');

        if (!(signature instanceof app.Bean)) {
            signature = app.data.createBean(\'UserSignatures\', signature);
        }

        this.context.set(\'current_signature\', signature);

        // Determine the initial signature location for inserting the default.
        location = this.context.get(\'signature_location\');

        if (_.isEmpty(location)) {
            // Default the location.
            location = this.BELOW_CONTENT;
            this.context.set(\'signature_location\', location);
        }

        // Don\'t do the following if updating an existing draft.
        if (this.model.isNew()) {
            // Insert the default signature.
            if (location === this.CURSOR_LOCATION) {
                // Need to wait for the editor before inserting.
                this.listenToOnce(this.context, \'tinymce:oninit\', function() {
                    this._insertSignature(signature, location);
                });
            } else {
                this._insertSignature(signature, location);
            }

            // Focus the editor and place the cursor at the desired location.
            if (!_.isEmpty(this.context.get(\'cursor_location\'))) {
                this.listenToOnce(this.context, \'tinymce:oninit\', function() {
                    if (this._htmleditor) {
                        this._htmleditor.focus();

                        // Move the cursor to the bottom of the editor by
                        // inserting an empty node and selecting it.
                        if (this.context.get(\'cursor_location\') == this.BELOW_CONTENT) {
                            emptyNode = this._insertNodeInEditor();

                            if (emptyNode) {
                                this._htmleditor.selection.setCursorLocation(emptyNode);
                                this._htmleditor.selection.collapse(true);
                            }
                        }
                    }
                });
            }
        }
    },

    /**
     * Suppress calling the sidecar _render method in detail view
     *
     * @inheritdoc
     */
    _render: function() {
        if (this._isEditView()) {
            this._super(\'_render\');
            this.$el.toggleClass(\'detail\', false).toggleClass(\'edit\', true);
        } else {
            this.destroyTinyMCEEditor();
            this._renderView();
            this.$el.toggleClass(\'detail\', true).toggleClass(\'edit\', false);
        }
        return this;
    },

    /**
     * Replicate the sidecar render logic for detail view except for
     * manually appending an iframe instead of invoking the template
     *
     * @inheritdoc
     */
    _renderView: function() {
        var self = this;
        var iFrame;

        // sets this.tplName and this.action
        this._loadTemplate();

        if (this.model instanceof Backbone.Model) {
            this.value = this.getFormattedValue();
        }

        this.dir = _.result(this, \'direction\');

        if (app.lang.direction === this.dir) {
            delete this.dir;
        }

        this.unbindDom();

        // begin custom rendering
        if (this.$el.find(\'iframe\').length === 0) {
            iFrame = $(\'<iframe>\', {
                src: \'\',
                class: \'htmleditable\' + (this.def.span ? \' span\' + this.def.span : \'\'),
                frameborder: 0,
                name: this.name
            });
            // Perform it on load for Firefox.
            iFrame.appendTo(this.$el).on(\'load\', function() {
                self._setIframeBaseTarget(iFrame, \'_blank\');
            });
            this._setIframeBaseTarget(iFrame, \'_blank\');
        }

        this.setViewContent(this.value);
        // end custom rendering

        if (this.def && this.def.css_class) {
            this.getFieldElement().addClass(this.def.css_class);
        }

        this.$(this.fieldTag).attr(\'dir\', this.dir);
        this.bindDomChange();
    },

    /**
     * @inheritdoc
     *
     * Resize the field\'s container based on the height of the iframe content
     * for preview.
     */
    setViewContent: function(value) {
        var field;
        // Pad this to the final height due to the iframe margins/padding
        var padding = 25;
        var contentHeight = 0;

        this._super(\'setViewContent\', [value]);

        // Only set this field height if it is in the preview pane
        if (this.tplName !== \'preview\') {
            return;
        }

        contentHeight = this._getContentHeight() + padding;

        // Only resize the editor when the content is fully loaded
        if (contentHeight > padding) {
            // Set the maximum height to 400px
            if (contentHeight > 400) {
                contentHeight = 400;
            }

            field = this._getHtmlEditableField();
            field.css(\'height\', contentHeight);
        }
    },

    /**
     * Get the content height of the field\'s iframe.
     *
     * @private
     * @return {number} Returns 0 if the iframe isn\'t found.
     */
    _getContentHeight: function() {
        var editable = this._getHtmlEditableField();

        if (this._iframeHasBody(editable)) {
            return editable.contents().find(\'body\')[0].offsetHeight;
        }

        return 0;
    },

    /**
     * Set iframe base target value
     *
     * @param {jQuery} iFrame The iframe element that the target will be added to.
     * @param {string} targetValue e.g. _self, _blank, _parent, _top or frameName
     * @private
     */
    _setIframeBaseTarget: function(iFrame, targetValue) {
        var target = $(\'<base>\', {
            target: targetValue
        });

        target.appendTo(iFrame.contents().find(\'head\'));
    },

    /**
     * @inheritdoc
     *
     * Adds buttons for uploading a local file and selecting a Sugar Document
     * to attach to the email.
     *
     * Adds a button for selecting and inserting a signature at the cursor.
     *
     * Adds a button for selecting and applying a template.
     *
     * @fires email_attachments:file on the view when the user elects to attach
     * a local file.
     */
    addCustomButtons: function(editor) {
        var self = this;
        var attachmentButtons = [];

        // Attachments can only be added if the user has permission to create
        // Notes records. Only add the attachment button(s) if the user is
        // allowed.
        if (app.acl.hasAccess(\'create\', \'Notes\')) {
            attachmentButtons.push({
                text: app.lang.get(\'LBL_ATTACH_FROM_LOCAL\', this.module),
                onclick: _.bind(function(event) {
                    // Track click on the file attachment button.
                    app.analytics.trackEvent(\'click\', \'tinymce_email_attachment_file_button\', event);
                    this.view.trigger(\'email_attachments:file\');
                }, this)
            });

            // The user can only select a document to attach if he/she has
            // permission to view Documents records in the selection list.
            // Don\'t add the Documents button if the user can\'t view and select
            // documents.
            if (app.acl.hasAccess(\'view\', \'Documents\')) {
                attachmentButtons.push({
                    text: app.lang.get(\'LBL_ATTACH_SUGAR_DOC\', this.module),
                    onclick: _.bind(function(event) {
                        // Track click on the document attachment button.
                        app.analytics.trackEvent(\'click\', \'tinymce_email_attachment_doc_button\', event);
                        this._selectDocument();
                    }, this)
                });
            }

            editor.addButton(\'sugarattachment\', {
                type: \'menubutton\',
                tooltip: app.lang.get(\'LBL_ATTACHMENT\', this.module),
                icon: \'paperclip\',
                onclick: function(event) {
                    // Track click on the attachment button.
                    app.analytics.trackEvent(\'click\', \'tinymce_email_attachment_button\', event);
                },
                menu: attachmentButtons
            });
        }

        editor.addButton(\'sugarsignature\', {
            type: \'menubutton\',
            tooltip: app.lang.get(\'LBL_SIGNATURE\', this.module),
            icon: \'pencil\',
            // disable the signature button until they have been loaded
            disabled: true,
            onPostRender: function() {
                self._signatureBtn = this;
                // load the users signatures
                self._getSignatures();
            },
            onclick: function(event) {
                // Track click on the signature button.
                app.analytics.trackEvent(\'click\', \'tinymce_email_signature_button\', event);
            },
            // menu is populated from the _getSignatures() response
            menu: []
        });

        if (app.acl.hasAccess(\'view\', \'EmailTemplates\')) {
            editor.addButton(\'sugartemplate\', {
                tooltip: app.lang.get(\'LBL_TEMPLATE\', this.module),
                icon: \'file-o\',
                onclick: _.bind(function(event) {
                    // Track click on the template button.
                    app.analytics.trackEvent(\'click\', \'tinymce_email_template_button\', event);
                    this._selectEmailTemplate();
                }, this)
            });
        }

        // Enable the signature button when the editor is focused and the user
        // has signatures that can be inserted.
        editor.on(\'focus\', _.bind(function(e) {
            this._editorFocused = true;
            this.view.trigger(\'tinymce:focus\');
            // the user has at least 1 signature
            if (this._numSignatures > 0) {
                // enable the signature button
                this._signatureBtn.disabled(false);
            }
        }, this));

        // Disable the signature button when the editor is blurred and the user
        // has signatures. Signatures are inserted at the cursor location. If
        // the button is not disabled when the editor is unfocused, then issues
        // would arise with the user clicking a signature to insert at the
        // cursor without a cursor being present.
        editor.on(\'blur\', _.bind(function(e) {
            this._editorFocused = false;
            this.view.trigger(\'tinymce:blur\');
            // the user has at least 1 signature
            if (this._numSignatures > 0) {
                // disable the signature button
                this._signatureBtn.disabled(true);
            }
        }, this));
    },

    /**
     * Inserts the content into the TinyMCE editor at the specified location.
     *
     * @private
     * @param {string} content
     * @param {string} [location="cursor"] Whether to insert the new content
     *   above existing content, below existing content, or at the cursor
     *   location. Defaults to being inserted at the cursor position.
     * @return {string} The updated content.
     */
    _insertInEditor: function(content, location) {
        var emailBody = this.model.get(this.name) || \'\';

        if (_.isEmpty(content)) {
            return emailBody;
        }

        // Default to the cursor location.
        location = location || this.CURSOR_LOCATION;

        // Add empty divs so user can place the cursor on the line before or
        // after.
        content = \'<div></div>\' + content + \'<div></div>\';

        if (location === this.CURSOR_LOCATION) {
            if (_.isNull(this._htmleditor)) {
                // Unable to insert content at the cursor without an editor.
                return emailBody;
            }

            this._htmleditor.execCommand(\'mceInsertContent\', false, content);

            // Get the HTML content from the editor.
            emailBody = this._htmleditor.getContent();
        } else if (location === this.BELOW_CONTENT) {
            emailBody += content;
        } else if (location === this.ABOVE_CONTENT) {
            emailBody = content + emailBody;
        }

        // Update the model with the new content.
        this.model.set(this.name, emailBody);

        return emailBody;
    },

    /**
     * Inserts a unique element into the TinyMCE editor to the end of the
     * <body>.
     *
     * @private
     * @return {HTMLElement|boolean} The inserted element or false if an
     * element can\'t be inserted.
     */
    _insertNodeInEditor: function() {
        var body;
        var uniqueId;

        if (this._htmleditor) {
            body = this._htmleditor.getBody();
            uniqueId = this._htmleditor.dom.uniqueId();
            $(\'<p id="\' + uniqueId + \'"><br /></p>\').appendTo(body);

            return this._htmleditor.dom.select(\'p#\' + uniqueId)[0];
        }

        // There is no editor to insert the element into.
        return false;
    },

    /**
     * Fetches the signatures for the current user.
     *
     * @private
     */
    _getSignatures: function() {
        var signatures = app.data.createBeanCollection(\'UserSignatures\');

        signatures.filterDef = [{
            user_id: {$equals: app.user.get(\'id\')}
        }];
        signatures.fetch({
            max_num: -1, // Get as many as we can.
            success: _.bind(this._getSignaturesSuccess, this),
            error: function() {
                app.alert.show(\'server-error\', {
                    level: \'error\',
                    messages: \'ERR_GENERIC_SERVER_ERROR\'
                });
            }
        });
    },

    /**
     * Add each signature as buttons under the signature button.
     *
     * @private
     * @param {Data.BeanCollection} signatures
     */
    _getSignaturesSuccess: function(signatures) {
        if (this.disposed === true) {
            return;
        }

        if (!_.isUndefined(signatures) && !_.isUndefined(signatures.models)) {
            signatures = signatures.models;
        } else {
            app.alert.show(\'server-error\', {
                level: \'error\',
                messages: \'ERR_GENERIC_SERVER_ERROR\'
            });

            return;
        }

        if (!_.isNull(this._signatureBtn)) {
            // write the signature names to the control dropdown
            _.each(signatures, _.bind(function(signature) {
                this._signatureBtn.settings.menu.push({
                    text: signature.get(\'name\'),
                    onclick: _.bind(function(event) {
                        // Track click on a signature.
                        app.analytics.trackEvent(\'click\', \'email_signature\', event);
                        this._insertSignature(signature, this.CURSOR_LOCATION);
                    }, this)
                });
            }, this));

            // Set the number of signatures the user has
            this._numSignatures = signatures.length;

            // If the editor is focused before the signatures are returned, enable the signature button
            if (this._editorFocused) {
                this._signatureBtn.disabled(false);
            }
        }
    },

    /**
     * Inserts the signature into the editor.
     *
     * @private
     * @param {Data.Bean} signature
     * @param {string} [location="cursor"] Whether to insert the new content
     * above existing content, below existing content, or at the cursor
     * location. Defaults to being inserted at the cursor position.
     */
    _insertSignature: function(signature, location) {
        var htmlBodyObj;
        var emailBody;
        var signatureHtml;
        var decodedSignature;
        var signatureContent;

        function decodeBrackets(str) {
            str = str.replace(/&lt;/gi, \'<\');
            str = str.replace(/&gt;/gi, \'>\');

            return str;
        }

        if (this.disposed === true) {
            return;
        }

        if (!(signature instanceof app.Bean)) {
            return;
        }

        signatureHtml = signature.get(\'signature_html\');

        if (_.isEmpty(signatureHtml)) {
            return;
        }

        decodedSignature = decodeBrackets(signatureHtml);
        signatureContent = \'<div class="signature keep">\' + decodedSignature + \'</div>\';

        emailBody = this._insertInEditor(signatureContent, location);
        htmlBodyObj = $(\'<div>\' + emailBody + \'</div>\');

        // Mark each signature to either keep or remove.
        $(\'div.signature\', htmlBodyObj).each(function() {
            if (!$(this).hasClass(\'keep\')) {
                // Mark for removal.
                $(this).addClass(\'remove\');
            } else {
                // If the parent is also a signature, move the node out of the
                // parent so it isn\'t removed.
                if ($(this).parent().hasClass(\'signature\')) {
                    // Move the signature outside of the nested signature.
                    $(this).parent().before(this);
                }

                // Remove the "keep" class so if another signature is added it
                // will remove this one.
                $(this).removeClass(\'keep\');
            }
        });

        // After each signature is marked, perform the removal.
        htmlBodyObj.find(\'div.signature.remove\').remove();

        emailBody = htmlBodyObj.html();
        this.model.set(this.name, emailBody);

        this.context.set(\'current_signature\', signature);
        this.context.set(\'signature_location\', location || this.CURSOR_LOCATION);
    },

    /**
     * Allows the user to select a template to apply.
     *
     * @private
     */
    _selectEmailTemplate: function() {
        var def = {
            layout: \'selection-list\',
            context: {
                module: \'EmailTemplates\',
                fields: [
                    \'subject\',
                    \'body\',
                    \'body_html\',
                    \'text_only\'
                ]
            }
        };

        app.drawer.open(def, _.bind(this._onEmailTemplateDrawerClose, this));
    },

    /**
     * Verifies that the user has access to the email template before applying
     * it.
     *
     * @private
     * @param {Data.Bean} model
     */
    _onEmailTemplateDrawerClose: function(model) {
        var emailTemplate;

        if (this.disposed === true) {
            return;
        }

        // This is an edge case where user has List but not View permission.
        // Search & Select will return only id and name if View permission is
        // not permitted for this record. Display appropriate error.
        if (model && _.isUndefined(model.subject)) {
            app.alert.show(\'no_access_error\', {
                level: \'error\',
                messages: app.lang.get(\'ERR_NO_ACCESS\', this.module, {name: model.value})
            });
        } else if (model) {
            // `value` is not a real attribute.
            emailTemplate = app.data.createBean(\'EmailTemplates\', _.omit(model, \'value\'));
            this._confirmTemplate(emailTemplate);
        }
    },

    /**
     * Confirms that the user wishes to replace all content in the editor. The
     * template is applied if there is no existing content or if the user
     * confirms "yes".
     *
     * @private
     * @param {Data.Bean} template
     */
    _confirmTemplate: function(template) {
        var subject = this.model.get(\'name\') || \'\';
        var text = this.model.get(\'description\') || \'\';
        var html = this.model.get(this.name) || \'\';
        var fullContent = subject + text + html;

        if (_.isEmpty(fullContent)) {
            this._applyTemplate(template);
        } else {
            app.alert.show(\'delete_confirmation\', {
                level: \'confirmation\',
                messages: app.lang.get(\'LBL_EMAILTEMPLATE_MESSAGE_SHOW_MSG\', this.module),
                onConfirm: _.bind(function(event) {
                    // Track click on confirmation button.
                    app.analytics.trackEvent(\'click\', \'email_template_confirm\', event);
                    this._applyTemplate(template);
                }, this),
                onCancel: function(event) {
                    // Track click on cancel button.
                    app.analytics.trackEvent(\'click\', \'email_template_cancel\', event);
                }
            });
        }
    },

    /**
     * Inserts the template into the editor.
     *
     * The template\'s subject does not overwrite the existing subject if:
     *
     * 1. The email is a forward or reply.
     * 2. The template does not have a subject.
     *
     * @private
     * @fires email_attachments:template on the view with the selected template
     * as a parameter. {@link View.Fields.Base.Emails.EmailAttachmentsField}
     * adds the template\'s attachments to the email.
     * @param {Data.Bean} template
     */
    _applyTemplate: function(template) {
        var body;
        var replyContent;
        var forwardContent;
        var subject;
        var signature = this.context.get(\'current_signature\');

        /**
         * Check the email body and pull out any forward/reply content from a
         * draft email.
         *
         * @param {string} body The full content to search.
         * @return {string} The forward/reply content.
         */
        function getForwardReplyContent(body, id) {
            var content = \'\';
            var $content;

            if (body) {
                $content = $(\'<div>\' + body + \'</div>\').find(\'div#\' + id);

                if ($content.length > 0) {
                    content = $content[0].outerHTML;
                }
            }

            return content;
        }

        if (this.disposed === true) {
            return;
        }

        // Track applying an email template.
        app.analytics.trackEvent(\'email_template\', \'apply\', template);

        replyContent = getForwardReplyContent(this.model.get(this.name), \'replycontent\');
        forwardContent = getForwardReplyContent(this.model.get(this.name), \'forwardcontent\');
        subject = template.get(\'subject\');

        // Only use the subject if it\'s not a forward or reply.
        if (subject && !(replyContent || forwardContent)) {
            this.model.set(\'name\', subject);
        }

        //TODO: May need to move over replaces special characters.
        body = template.get(\'text_only\') ? template.get(\'body\') : template.get(\'body_html\');
        this.model.set(this.name, body);

        this.view.trigger(\'email_attachments:template\', template);

        // The HTML signature is used even when the template is text-only.
        if (signature) {
            this._insertSignature(signature, this.BELOW_CONTENT);
        }

        // Append the reply content to the end of the email.
        if (replyContent) {
            this._insertInEditor(replyContent, this.BELOW_CONTENT);
        }

        // Append the forward content to the end of the email.
        if (forwardContent) {
            this._insertInEditor(forwardContent, this.BELOW_CONTENT);
        }
    },

    /**
     * Allows the user to select a document to attach.
     *
     * @private
     * @fires email_attachments:document on the view with the selected document
     * as a parameter. {@link View.Fields.Base.EmailAttachmentsField} attaches
     * the document to the email.
     */
    _selectDocument: function() {
        var def = {
            layout: \'selection-list\',
            context: {
                module: \'Documents\'
            }
        };

        app.drawer.open(def, _.bind(function(model) {
            var document;

            if (model) {
                // `value` is not a real attribute.
                document = app.data.createBean(\'Documents\', _.omit(model, \'value\'));
                this.view.trigger(\'email_attachments:document\', document);
            }
        }, this));
    }
})
',
    ),
    'templates' => 
    array (
      'detail' => '
{{!-- the detail field view is being constructed in the controller --}}
{{!-- created ticket MAR-4775 to investigate why empty template still
results in the iframe being reset in Firefox only --}}
',
    ),
  ),
  'forward-action' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Forward action.
 *
 * This allows a user to "forward" an existing email.
 *
 * @class View.Fields.Base.Emails.ForwardActionField
 * @alias SUGAR.App.view.fields.EmailsBaseForwardActionField
 * @extends View.Fields.Base.EmailactionField
 */
({
    extendsFrom: \'EmailactionField\',

    /**
     * Template for forward header.
     *
     * @protected
     */
    _tplHeaderHtml: null,

    /**
     * The name of the template for forward header.
     *
     * @protected
     */
    _tplHeaderHtmlName: \'forward-header-html\',

    /**
     * The prefix to apply to the subject.
     *
     * @protected
     */
    _subjectPrefix: \'LBL_FW\',

    /**
     * The element ID to use to identify the forward content.
     *
     * The ID is added to the div wrapper around the content for later
     * identifying the portion of the email body which is the forward content
     * (e.g., when inserting templates into an email, but maintaining the
     * forward content).
     *
     * @protected
     */
    _contentId: \'forwardcontent\',

    /**
     * @inheritdoc
     *
     * The forward content is built ahead of the button click to support the
     * option of doing a mailto link which needs to be built and set in the DOM
     * at render time.
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        // Use field templates from emailaction.
        this.type = \'emailaction\';

        this.addEmailOptions({
            // If there is a default signature in email compose, it should be
            // placed above the forward content in the email body.
            signature_location: \'above\',
            // Focus the editor and place the cursor at the beginning of all
            // content.
            cursor_location: \'above\',
            // Prevent prepopulating the email with case data.
            skip_prepopulate_with_case: true
        });
    },

    /**
     * Returns the subject to use in the email.
     *
     * Any instances of "Re: ", "FW: ", and "FWD: " (case-insensitive) found at
     * the beginning of the subject are removed prior to applying the prefix.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the subject.
     * @return {undefined|string}
     */
    emailOptionSubject: function(model) {
        var pattern = /^((?:re|fw|fwd): *)*/i;
        var subject = model.get(\'name\') || \'\';

        return app.lang.get(this._subjectPrefix, model.module) + \': \' + subject.replace(pattern, \'\');
    },

    /**
     * Returns the plain-text body to use in the email.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the body.
     * @return {undefined|string}
     */
    emailOptionDescription: function(model) {
        var headerParams;
        var header;
        var body;
        var description;

        if (!this.useSugarEmailClient()) {
            headerParams = this._getHeaderParams(model);
            header = this._getHeader(headerParams);
            body = model.get(\'description\') || \'\';
            description = \'\\n\' + header + \'\\n\' + body;
        }

        return description;
    },

    /**
     * Returns the HTML body to use in the email.
     *
     * Ensure the result is a defined string and strip any signature wrapper
     * tags to ensure it doesn\'t get stripped if we insert a signature above
     * the forward content. Also strip any reply content class if this is a
     * forward to a previous reply. And strip any forward content class if this
     * is a forward to a previous forward.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when constructing the body.
     * @return {undefined|string}
     */
    emailOptionDescriptionHtml: function(model) {
        var tplHeaderHtml = this._getHeaderHtmlTemplate();
        var headerParams = this._getHeaderParams(model);
        var headerHtml = tplHeaderHtml(headerParams);
        var body = model.get(\'description_html\') || \'\';

        body = body.replace(\'<div class="signature">\', \'<div>\');
        body = body.replace(\'<div id="replycontent">\', \'<div>\');
        body = body.replace(\'<div id="forwardcontent">\', \'<div>\');

        return \'<div></div><div id="\' + this._contentId + \'">\' + headerHtml + body + \'</div>\';
    },

    /**
     * Returns the attachments to use in the email.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when building the attachments.
     * @return {undefined|Array}
     */
    emailOptionAttachments: function(model) {
        return model.get(\'attachments_collection\').map(function(attachment) {
            var filename = attachment.get(\'filename\') || attachment.get(\'name\');

            return {
                _link: \'attachments\',
                upload_id: attachment.get(\'upload_id\') || attachment.get(\'id\'),
                name: filename,
                filename: filename,
                file_mime_type: attachment.get(\'file_mime_type\'),
                file_size: attachment.get(\'file_size\'),
                file_ext: attachment.get(\'file_ext\')
            };
        });
    },

    /**
     * Returns the bean to use as the email\'s related record.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model This model\'s parent is used as the email\'s
     * related record.
     * @return {undefined|Data.Bean}
     */
    emailOptionRelated: function(model) {
        var parent;

        if (model.get(\'parent\') && model.get(\'parent\').type && model.get(\'parent\').id) {
            // We omit type because it is actually the module name and should
            // not be treated as an attribute.
            parent = app.data.createBean(model.get(\'parent\').type, _.omit(model.get(\'parent\'), \'type\'));
        } else if (model.get(\'parent_type\') && model.get(\'parent_id\')) {
            parent = app.data.createBean(model.get(\'parent_type\'), {
                id: model.get(\'parent_id\'),
                name: model.get(\'parent_name\')
            });
        }

        return parent;
    },

    /**
     * Returns the teamset array to seed the email\'s teams.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model This model\'s teams is used as the email\'s
     * teams.
     * @return {undefined|Array}
     */
    emailOptionTeams: function(model) {
        return model.get(\'team_name\');
    },

    /**
     * Build the header for text only emails.
     *
     * @param {Object} params
     * @param {string} params.from
     * @param {string} [params.date] Date original email was sent
     * @param {string} params.to
     * @param {string} [params.cc]
     * @param {string} params.name The subject of the original email.
     * @return {string}
     * @private
     */
    _getHeader: function(params) {
        var header = \'-----\\n\' + app.lang.get(\'LBL_FROM\', params.module) + \': \' + (params.from || \'\') + \'\\n\';
        var date;

        if (params.date) {
            date = app.date(params.date).formatUser();
            header += app.lang.get(\'LBL_DATE\', params.module) + \': \' + date + \'\\n\';
        }

        header += app.lang.get(\'LBL_TO_ADDRS\', params.module) + \': \' + (params.to || \'\') + \'\\n\';

        if (params.cc) {
            header += app.lang.get(\'LBL_CC\', params.module) + \': \' + params.cc + \'\\n\';
        }

        header += app.lang.get(\'LBL_SUBJECT\', params.module) + \': \' + (params.name || \'\') + \'\\n\';

        return header;
    },

    /**
     * Returns the template for producing the header HTML for the top of the
     * forward content.
     *
     * @return {Function}
     * @private
     */
    _getHeaderHtmlTemplate: function() {
        // Use `this.def.type` because `this.type` was changed to `emailaction`
        // during initialization.
        this._tplHeaderHtml = this._tplHeaderHtml ||
            app.template.getField(this.def.type, this._tplHeaderHtmlName, this.module);
        return this._tplHeaderHtml;
    },

    /**
     * Get the data required by the header template.
     *
     * @param {Data.Bean} model The params come from this model\'s attributes.
     * EmailClientLaunch plugin should dictate the model based on the context.
     * @return {Object}
     * @protected
     */
    _getHeaderParams: function(model) {
        return {
            module: model.module,
            from: this._formatEmailList(model.get(\'from_collection\')),
            date: model.get(\'date_sent\'),
            to: this._formatEmailList(model.get(\'to_collection\')),
            cc: this._formatEmailList(model.get(\'cc_collection\')),
            name: model.get(\'name\')
        };
    },

    /**
     * Given a list of people, format a text only list for use in a forward
     * header.
     *
     * @param {Data.BeanCollection} collection A list of models
     * @protected
     */
    _formatEmailList: function(collection) {
        return collection.map(function(model) {
            return model.toHeaderString();
        }).join(\', \');
    }
})
',
    ),
    'templates' => 
    array (
      'forward-header-html' => '
<hr>
<p>
    <strong>{{str \'LBL_FROM\' module}}:</strong> {{from}}<br/>
    {{#if date}}<strong>{{str \'LBL_DATE\' module}}:</strong> {{formatDate date}}<br/>{{/if}}
    <strong>{{str \'LBL_TO_ADDRS\' module}}:</strong> {{to}}<br/>
    {{#if cc}}<strong>{{str \'LBL_CC\' module}}:</strong> {{cc}}<br/>{{/if}}
    <strong>{{str \'LBL_SUBJECT\' module}}:</strong> {{name}}<br/>
</p>
',
    ),
  ),
  'outbound-email' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
 /**
 * @class View.Fields.Base.Emails.OutboundEmailField
 * @alias SUGAR.App.view.fields.BaseEmailsOutboundEmailField
 * @extends View.Fields.Base.EnumField
 */
({
    extendsFrom: \'BaseEnumField\',

    /**
     * Sets the field type to `enum` so that the `BaseEnumField` templates are
     * loaded. This is necessary when extending a field and using a
     * different name without any custom templates.
     *
     * Adds help text (LBL_OUTBOUND_EMAIL_ID_HELP) for admins.
     *
     * @inheritdoc
     */
    initialize: function(options) {
        if (app.user.get(\'type\') === \'admin\') {
            options.def.help = \'LBL_OUTBOUND_EMAIL_ID_HELP\';
        }

        this._super(\'initialize\', [options]);
        this.type = \'enum\';
    },

    /**
     * @inheritdoc
     *
     * Only add the help tooltip if the help text is being hidden.
     */
    decorateHelper: function() {
        if (this.def.hideHelp) {
            this._super(\'decorateHelper\');
        }
    },

    /**
     * @inheritdoc
     *
     * Dismisses any alerts with the key `email-client-status`.
     */
    _dispose: function() {
        app.alert.dismiss(\'email-client-status\');
        this._super(\'_dispose\');
    },

    /**
     * Shows a warning to the user when a not_authorized error is returned.
     *
     * @inheritdoc
     * @fires email_not_configured Triggered on the view to allow the view to
     * decide what should be done beyond warning the user. The error is passed
     * to listeners.
     */
    loadEnumOptions: function(fetch, callback, error) {
        var oError = error;

        error = _.bind(function(e) {
            if (e.code === \'not_authorized\') {
                // Mark the error as having been handled so that it doesn\'t get
                // handled again.
                e.handled = true;
                app.alert.show(\'email-client-status\', {
                    level: \'warning\',
                    messages: app.lang.get(e.message, this.module),
                    autoClose: false,
                    onLinkClick: function() {
                        app.alert.dismiss(\'email-client-status\');
                    }
                });
                this.view.trigger(\'email_not_configured\', e);
            }

            if (oError) {
                oError(e);
            }
        }, this);

        this._super(\'loadEnumOptions\', [fetch, callback, error]);
    }
})
',
    ),
  ),
  'from' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.FromField
 * @alias SUGAR.App.view.fields.BaseEmailsFromField
 * @extends View.Fields.Base.BaseField
 */
({
    /**
     * @inheritdoc
     *
     * This field doesn\'t support `showNoData`.
     */
    showNoData: false,

    /**
     * The selector for accessing the Select2 field when in edit mode. The
     * Select2 field is where the sender is displayed.
     *
     * @property {string}
     */
    fieldTag: \'input.select2\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.plugins = _.union(this.plugins || [], [\'EmailParticipants\', \'ListEditable\']);
        this._super(\'initialize\', [options]);

        // Specify the error label for when the sender\'s email address is
        // invalid.
        app.error.errorName2Keys[this.type] = app.lang.get(\'ERR_INVALID_SENDER\', this.module);
    },

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        if (this.model) {
            // Avoids a full re-rendering when editing. The current value of
            // the field is formatted and passed directly to Select2 when in
            // edit mode.
            this.listenTo(this.model, \'change:\' + this.name, _.bind(function() {
                var $el = this.$(this.fieldTag);

                if (_.isEmpty($el.data(\'select2\'))) {
                    this.render();
                } else {
                    $el.select2(\'data\', this.getFormattedValue());
                }
            }, this));
        }
    },

    /**
     * @inheritdoc
     */
    bindDomChange: function() {
        var $el = this.$(this.fieldTag);

        $el.on(\'select2-selecting\', _.bind(function(event) {
            if (this.disposed) {
                event.preventDefault();
            }
        }, this));

        $el.on(\'change\', _.bind(function(event) {
            var collection;

            if (this.model && !this.disposed) {
                collection = this.model.get(this.name);

                if (!_.isEmpty(event.added)) {
                    // Replace the current model in the collection, as there
                    // can only be one.
                    collection.remove(collection.models);
                    collection.add(event.added);
                }

                if (!_.isEmpty(event.removed)) {
                    collection.remove(event.removed);
                }
            }
        }, this));
    },

    /**
     * @inheritdoc
     *
     * Destroys the Select2 element.
     */
    unbindDom: function() {
        this.$(this.fieldTag).select2(\'destroy\');
        this._super(\'unbindDom\');
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        var $el;
        var options;

        this._super(\'_render\');

        $el = this.$(this.fieldTag);

        if ($el.length > 0) {
            options = this.getSelect2Options();
            options = _.extend(options, {
                allowClear: !this.def.required,
                multiple: false,

                /**
                 * Constructs a representation for a selected sender to be
                 * displayed in the field.
                 *
                 * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
                 *
                 * @param {Data.Bean} sender
                 * @return {string}
                 * @private
                 */
                formatSelection: _.bind(function(sender) {
                    var template = app.template.getField(this.type, \'select2-selection\', this.module);

                    return sender ? template({value: sender.toHeaderString({quote_name: true})}) : \'\';
                }, this),

                /**
                 * Constructs a representation for the sender to be displayed
                 * in the dropdown options after a query.
                 *
                 * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
                 *
                 * @param {Data.Bean} sender
                 * @return {string}
                 */
                formatResult: _.bind(function(sender) {
                    var template = app.template.getField(this.type, \'select2-result\', this.module);

                    return template({
                        value: sender.toHeaderString({quote_name: true}),
                        module: sender.get(\'parent_type\')
                    });
                }, this),

                /**
                 * Don\'t escape a choice\'s markup since we built the HTML.
                 *
                 * See [Select2 Documentation](https://select2.github.io/select2/#documentation).
                 *
                 * @param {string} markup
                 * @return {string}
                 */
                escapeMarkup: function(markup) {
                    return markup;
                }
            });
            $el.select2(options).select2(\'val\', []);

            if (this.isDisabled()) {
                $el.select2(\'disable\');
            }
        }
    },

    /**
     * @inheritdoc
     * @return {Data.Bean}
     */
    format: function(value) {
        // Reset the tooltip.
        this.tooltip = \'\';

        if (value instanceof app.BeanCollection) {
            value = value.first();

            if (value) {
                value = this.prepareModel(value);
            }

            if (value) {
                this.tooltip = value.toHeaderString();
            }
        }

        return value;
    }
})
',
    ),
    'templates' => 
    array (
      'edit' => '
<input type="hidden" name="{{name}}" class="select2{{#if def.css_class}} {{def.css_class}}{{/if}}"/>
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
      'select2-selection' => '
{{value}}
',
      'detail' => '
<div class="ellipsis_inline" rel="tooltip" data-placement="bottom" title="{{tooltip}}">
    {{#if value.nameIsErased}}
        <span class="label">
            {{#if value.href}}<a href="{{value.href}}">{{/if}}{{str \'LBL_VALUE_ERASED\' module}}{{#if value.href}}</a>{{/if}}
        </span>
    {{else}}
        {{#if value.attributes.parent_name}}
            {{#if value.href}}<a href="{{value.href}}">{{/if}}{{value.attributes.parent_name}}{{#if value.href}}</a>{{/if}}
        {{else}}
            {{#if value.emailIsErased}}
                <span class="label">{{str \'LBL_VALUE_ERASED\' module}}</span>
            {{else}}
                {{value.attributes.email_address}}
            {{/if}}
        {{/if}}
    {{/if}}
</div>
',
      'list-edit' => '
<div class="ellipsis_inline" rel="tooltip" data-placement="bottom" title="{{tooltip}}">
    {{#if value.nameIsErased}}
        <span class="label">{{str \'LBL_VALUE_ERASED\' module}}</span>
    {{else}}
        {{#if value.attributes.parent_name}}
            {{value.attributes.parent_name}}
        {{else}}
            {{#if value.emailIsErased}}
                <span class="label">{{str \'LBL_VALUE_ERASED\' module}}</span>
            {{else}}
                {{value.attributes.email_address}}
            {{/if}}
        {{/if}}
    {{/if}}
</div>
',
      'list' => '
<div class="ellipsis_inline" rel="tooltip" data-placement="bottom" title="{{tooltip}}">
    {{#if value.nameIsErased}}
        <span class="label">{{str \'LBL_VALUE_ERASED\' module}}</span>
    {{else}}
        {{#if value.attributes.parent_name}}
            {{value.attributes.parent_name}}
        {{else}}
            {{#if value.emailIsErased}}
                <span class="label">{{str \'LBL_VALUE_ERASED\' module}}</span>
            {{else}}
                {{value.attributes.email_address}}
            {{/if}}
        {{/if}}
    {{/if}}
</div>
',
      'select2-result' => '
{{#if module}}<span class="label label-module label-module-mini label-{{module}}">
    {{moduleIconLabel module}}
</span>{{/if}}
{{value}}
',
    ),
  ),
  'name' => 
  array (
    'templates' => 
    array (
      'list' => '
{{#if value}}
    {{#if model.attributes.total_attachments}}
        <span class="email-has-attachments">
            <i class="fa fa-paperclip" aria-hidden="true"></i>
        </span>
    {{/if}}
    {{#if ../ellipsis}}
        <div class="ellipsis_inline" data-placement="bottom" title="{{value}}">
    {{/if}}
    {{#if href}}<a href="{{#if def.events}}javascript:void(0);{{else}}{{href}}{{/if}}">{{value}}</a>{{else}}{{value}}{{/if}}
    {{#if ../ellipsis}}
        </div>
    {{/if}}
{{/if}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
 /**
 * @class View.Fields.Base.Emails.NameField
 * @alias SUGAR.App.view.fields.BaseEmailsNameField
 * @extends View.Fields.Base.NameField
 */
({
    extendsFrom: \'BaseNameField\',

    /**
     * @inheritdoc
     *
     * Returns "(no subject)" when the email has no subject and not in edit
     * mode. This allows for the subject to be a link in a list view.
     */
    format: function(value) {
        if (_.isEmpty(value) && this.action !== \'edit\') {
            return app.lang.get(\'LBL_NO_SUBJECT\', this.module);
        }

        return value;
    },

    /**
     * Build email record route depending on whether or not the email is a
     * draft and whether the user has the Sugar Email Client option enabled.
     *
     * @return {string}
     */
    buildHref: function() {
        var action = this.def.route && this.def.route.action ? this.def.route.action : null;
        var module = this.model.module || this.context.get(\'module\');

        if (this.model.get(\'state\') === \'Draft\' &&
            app.acl.hasAccessToModel(\'edit\', this.model) &&
            this._useSugarEmailClient() &&
            !action
        ) {
            action = \'compose\';
        }

        return \'#\' + app.router.buildRoute(module, this.model.get(\'id\'), action);
    },

    /**
     * Determine if the user is configured to use the Sugar Email Client for
     * editing existing draft emails.
     *
     * @return {boolean}
     * @private
     */
    _useSugarEmailClient: function() {
        var emailClientPreference = app.user.getPreference(\'email_client_preference\');

        return (
            emailClientPreference &&
            emailClientPreference.type === \'sugar\' &&
            app.acl.hasAccess(\'edit\', \'Emails\')
        );
    }
})
',
    ),
  ),
  'attachment-button' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Attachment button is a label that is styled like a button and will trigger a
 * given file input field.
 *
 * @class View.Fields.Base.Emails.AttachmentButtonField
 * @alias SUGAR.App.view.fields.BaseEmailsAttachmentButtonField
 * @extends View.Fields.Base.ButtonField
 * @deprecated Use {@link View.Fields.Base.Emails.EmailAttachmentsField}
 * instead.
 */
({
    extendsFrom: \'ButtonField\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Fields.Base.Emails.AttachmentButtonField is deprecated. Use \' +
            \'View.Fields.Base.Emails.EmailAttachmentsField instead.\');

        this._super(\'initialize\',[options]);
        this.fileInputId = this.context.get(\'attachment_field_email_attachment\');
    }
})
',
    ),
    'templates' => 
    array (
      'detail' => '
{{!--
This is a <label> because IE does not allow you to upload an attachment that was
chosen programatically as a result of a .click() call from a button, but is
OK with clicking on a label for that file input element.
--}}
<label class="btn" for="{{fileInputId}}">{{#if def.icon}}<i class="fa {{def.icon}}"></i>{{/if}}{{str label}}</label>
',
    ),
  ),
  'email-recipients' => 
  array (
    'templates' => 
    array (
      'edit' => '
<div class="controls btn-fit controls-one">
    <input type="hidden" name="{{name}}" class="select2{{#if def.css_class}} {{def.css_class}}{{/if}}{{#if def.placeholder}} placeholder="{{str def.placeholder this.model.module}}"{{/if}}"/>
    <button class="btn address-book" data-name="{{name}}"><i class="fa {{#if def.icon}}{{def.icon}}{{else}}fa-book{{/if}}"></i></button>
    {{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
</div>
',
      'select2-selection' => '
<span data-id="{{cid}}" {{#if email_address}}rel="tooltip" data-title="{{email_address}}" {{/if}}data-invalid="{{invalid}}" data-optout="{{opt_out}}" data-name-is-erased="{{name_is_erased}}" data-email-is-erased="{{email_is_erased}}">{{name}}</span>
',
      'detail' => '
{{#each value}}
    {{#if nameIsErased}}
        <span
            {{#if emailIsErased}}rel="tooltip" data-placement="top" title="{{str \'LBL_VALUE_ERASED\' ../module}}"{{/if}}
            {{#if attributes.email_address}}rel="tooltip" data-placement="top" title="{{attributes.email_address}}"{{/if}}
        >
            <span class="label">
                {{#if href}}<a href="{{href}}">{{/if}}{{str \'LBL_VALUE_ERASED\' ../module}}{{#if href}}</a>{{/if}}
            </span>
            {{#unless @last}}, {{/unless}}
        </span>
    {{else}}
        {{#if attributes.parent_name}}
            <span
                {{#if emailIsErased}}rel="tooltip" data-placement="top" title="{{str \'LBL_VALUE_ERASED\' ../module}}"{{/if}}
                {{#if attributes.email_address}}rel="tooltip" data-placement="top" title="{{attributes.email_address}}"{{/if}}
            >
                {{#if href}}<a href="{{href}}">{{/if}}{{attributes.parent_name}}{{#if href}}</a>{{/if}}
                {{#unless @last}}, {{/unless}}
            </span>
        {{else}}
            <span>
                {{#if emailIsErased}}
                    <span class="label">{{str \'LBL_VALUE_ERASED\' ../module}}</span>
                {{else}}
                    {{attributes.email_address}}
                {{/if}}
                {{#unless @last}}, {{/unless}}
            </span>
        {{/if}}
    {{/if}}
{{/each}}
',
      'list-edit' => '
{{#if ellipsis}}
    <div class="ellipsis_inline" data-placement="bottom"{{#if dir}} dir="{{dir}}"{{/if}} title="{{tooltip}}">
{{/if}}
{{#each value}}
    {{#if nameIsErased}}
            <span class="label">
            {{#if href}}<a href="{{href}}">{{/if}}{{str \'LBL_VALUE_ERASED\' ../module}}{{#if href}}</a>{{/if}}
        </span>
    {{else}}
        {{#if attributes.parent_name}}
            {{#if href}}<a href="{{href}}">{{/if}}{{attributes.parent_name}}{{#if href}}</a>{{/if}}
        {{else}}
            {{#if emailIsErased}}
                    <span class="label">{{str \'LBL_VALUE_ERASED\' ../module}}</span>
            {{else}}
                {{attributes.email_address}}
            {{/if}}
        {{/if}}
    {{/if}}
    {{#unless @last}}, {{/unless}}
{{/each}}
{{#if ellipsis}}
    </div>
{{/if}}
',
      'list' => '
{{#if ellipsis}}
    <div class="ellipsis_inline" data-placement="bottom"{{#if dir}} dir="{{dir}}"{{/if}} title="{{tooltip}}">
{{/if}}
{{#each value}}
    {{#if nameIsErased}}
        <span class="label">
            {{#if href}}<a href="{{href}}">{{/if}}{{str \'LBL_VALUE_ERASED\' ../module}}{{#if href}}</a>{{/if}}
        </span>
    {{else}}
        {{#if attributes.parent_name}}
            {{#if href}}<a href="{{href}}">{{/if}}{{attributes.parent_name}}{{#if href}}</a>{{/if}}
        {{else}}
            {{#if emailIsErased}}
                <span class="label">{{str \'LBL_VALUE_ERASED\' ../module}}</span>
            {{else}}
                {{attributes.email_address}}
            {{/if}}
        {{/if}}
    {{/if}}
    {{#unless @last}}, {{/unless}}
{{/each}}
{{#if ellipsis}}
    </div>
{{/if}}
',
      'select2-result' => '
{{#if module}}<span class="label label-module label-module-mini label-{{module}}">
    {{moduleIconLabel module}}
</span>{{/if}}
{{value}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.EmailRecipientsField
 * @alias SUGAR.App.view.fields.BaseEmailsEmailRecipientsField
 * @extends View.Fields.Base.BaseField
 */
({
    /**
     * @inheritdoc
     *
     * This field doesn\'t support `showNoData`.
     */
    showNoData: false,

    /**
     * The selector for accessing the Select2 field when in edit mode. The
     * Select2 field is where the recipients are displayed.
     *
     * @property {string}
     */
    fieldTag: \'input.select2\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        var plugins = [
            \'CollectionFieldLoadAll\',
            \'EmailParticipants\',
            \'DragdropSelect2\',
            \'ListEditable\'
        ];

        this.plugins = _.union(this.plugins || [], plugins);
        this.events = _.extend({}, this.events, {
            \'click .btn\': \'_showAddressBook\'
        });
        this._super(\'initialize\', [options]);

        // Specify the error label for when any recipient\'s email address is
        // invalid.
        app.error.errorName2Keys[this.type] = app.lang.get(\'ERR_INVALID_RECIPIENTS\', this.module);
    },

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        if (this.model) {
            // Avoids a full re-rendering when editing. The current value of
            // the field is formatted and passed directly to Select2 when in
            // edit mode.
            this.listenTo(this.model, \'change:\' + this.name, _.bind(function() {
                var $el = this.$(this.fieldTag);

                if (_.isEmpty($el.data(\'select2\'))) {
                    this.render();
                } else {
                    $el.select2(\'data\', this.getFormattedValue());
                    this._decorateRecipients();
                    this._enableDragDrop();
                }
            }, this));
        }
    },

    /**
     * @inheritdoc
     */
    bindDomChange: function() {
        var $el = this.$(this.fieldTag);

        $el.on(\'select2-selecting\', _.bind(function(event) {
            // Don\'t add the choice if it duplicates an existing recipient.
            var duplicate = this.model.get(this.name).find(function(model) {
                if (event.choice.get(\'parent_id\')) {
                    return event.choice.get(\'parent_type\') === model.get(\'parent_type\') &&
                        event.choice.get(\'parent_id\') === model.get(\'parent_id\');
                }

                return event.choice.get(\'email_address_id\') === model.get(\'email_address_id\') ||
                    event.choice.get(\'email_address\') === model.get(\'email_address\');
            });

            if (this.disposed || duplicate) {
                event.preventDefault();
            }
        }, this));

        $el.on(\'change\', _.bind(function(event) {
            var collection;

            if (this.model && !this.disposed) {
                collection = this.model.get(this.name);

                if (!_.isEmpty(event.added)) {
                    collection.add(event.added);
                }

                if (!_.isEmpty(event.removed)) {
                    collection.remove(event.removed);
                }
            }
        }, this));
    },

    /**
     * @inheritdoc
     *
     * Destroys the Select2 element.
     */
    unbindDom: function() {
        this.$(this.fieldTag).select2(\'destroy\');
        this._super(\'unbindDom\');
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        var $el;
        var options;

        this._super(\'_render\');

        $el = this.$(this.fieldTag);

        if ($el.length > 0) {
            options = this.getSelect2Options();
            options = _.extend(options, {
                allowClear: true,
                multiple: true,
                containerCssClass: \'select2-choices-pills-close\',

                /**
                 * Constructs a representation for a selected recipient to be
                 * displayed in the field.
                 *
                 * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
                 *
                 * @param {Data.Bean} recipient
                 * @return {string}
                 * @private
                 */
                formatSelection: _.bind(function(recipient) {
                    var template = app.template.getField(this.type, \'select2-selection\', this.module);
                    var name = recipient.get(\'parent_name\') || \'\';
                    var email = recipient.get(\'email_address\') || \'\';

                    // The name was erased, so let\'s use the label.
                    if (_.isEmpty(name) && recipient.nameIsErased) {
                        name = app.lang.get(\'LBL_VALUE_ERASED\', recipient.module);
                    }

                    // The email was erased, so let\'s use the label.
                    if (_.isEmpty(email) && recipient.emailIsErased) {
                        email = app.lang.get(\'LBL_VALUE_ERASED\', recipient.module);
                    }

                    return template({
                        cid: recipient.cid,
                        name: name || email,
                        email_address: email,
                        invalid: recipient.invalid,
                        opt_out: !!recipient.get(\'opt_out\'),
                        name_is_erased: recipient.nameIsErased,
                        email_is_erased: recipient.emailIsErased
                    });
                }, this),

                /**
                 * Constructs a representation for the recipient to be
                 * displayed in the dropdown options after a query.
                 *
                 * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
                 *
                 * @param {Data.Bean} recipient
                 * @return {string}
                 */
                formatResult: _.bind(function(recipient) {
                    var template = app.template.getField(this.type, \'select2-result\', this.module);

                    return template({
                        value: recipient.toHeaderString({quote_name: true}),
                        module: recipient.get(\'parent_type\')
                    });
                }, this),

                /**
                 * Don\'t escape a choice\'s markup since we built the HTML.
                 *
                 * See [Select2 Documentation](https://select2.github.io/select2/#documentation).
                 *
                 * @param {string} markup
                 * @return {string}
                 */
                escapeMarkup: function(markup) {
                    return markup;
                }
            });
            $el.select2(options).select2(\'val\', []);

            if (this.isDisabled()) {
                $el.select2(\'disable\');
            }

            this._decorateRecipients();
            this._enableDragDrop();
        }
    },

    /**
     * @inheritdoc
     * @return {Array}
     */
    format: function(value) {
        // Reset the tooltip.
        this.tooltip = \'\';

        if (value instanceof app.BeanCollection) {
            value = value.map(this.prepareModel, this);

            // Must wrap the callback in a function or else the collection\'s
            // index will be passed, causing the second parameter of
            // EmailParticipantsPlugin#formatForHeader to unintentionally
            // receive a value.
            this.tooltip = _.map(value, function(model) {
                return model.toHeaderString();
            }, this).join(\', \');
        }

        return value;
    },

    /**
     * Decorates recipients that need it.
     *
     * @private
     */
    _decorateRecipients: function() {
        this._decorateOptedOutRecipients();
        this._decorateInvalidRecipients();
    },

    /**
     * Decorate any invalid recipients.
     *
     * @private
     */
    _decorateInvalidRecipients: function() {
        var self = this;
        var $invalidRecipients = this.$(\'.select2-search-choice [data-invalid="true"]\');

        $invalidRecipients.each(function() {
            var $choice = $(this).closest(\'.select2-search-choice\');
            $choice.addClass(\'select2-choice-danger\');

            // Don\'t change the tooltip if the email address has been erased.
            if (!$(this).data(\'email-is-erased\')) {
                $(this).attr(\'data-title\', app.lang.get(\'ERR_INVALID_EMAIL_ADDRESS\', self.module));
            }
        });
    },

    /**
     * Decorate any opted out email addresses.
     *
     * Email addresses that are opted out and invalid are not decorated by this
     * method. This preserves the invalid recipient decoration, since users
     * will need that decoration to correct their email before saving or
     * sending.
     *
     * @private
     */
    _decorateOptedOutRecipients: function() {
        var self = this;
        var $optedOutRecipients = this.$(\'.select2-search-choice [data-optout="true"]:not([data-invalid="true"])\');

        $optedOutRecipients.each(function() {
            var $choice = $(this).closest(\'.select2-search-choice\');
            $choice.addClass(\'select2-choice-optout\');
            $(this).attr(\'data-title\', app.lang.get(\'LBL_EMAIL_ADDRESS_OPTED_OUT\', self.module));
        });
    },

    /**
     * Enable the user to drag and drop recipients between recipient fields.
     *
     * @private
     */
    _enableDragDrop: function() {
        var $el = this.$(this.fieldTag);

        if (!this.def.readonly) {
            this.setDragDropPluginEvents($el);
        }
    },

    /**
     * When in edit mode, the field includes an icon button for opening an
     * address book. Clicking the button will trigger an event to open the
     * address book, which calls this method does. The selected recipients are
     * added to this field upon closing the address book.
     *
     * @private
     */
    _showAddressBook: function() {
        app.drawer.open(
            {
                layout: \'compose-addressbook\',
                context: {
                    module: \'Emails\',
                    mixed: true
                }
            },
            _.bind(function(recipients) {
                if (recipients && recipients.length > 0) {
                    // Set the correct link for the field where these
                    // recipients are being added.
                    var eps = recipients.map(function(recipient) {
                        recipient.set(\'_link\', this.getLinkName());

                        return recipient;
                    }, this);

                    this.model.get(this.name).add(eps);
                }

                this.view.trigger(\'address-book-state\', \'closed\');
            }, this)
        );

        this.view.trigger(\'address-book-state\', \'open\');
    },

    /**
     * Moves the recipients to the target collection.
     *
     * @param {Data.BeanCollection} source The collection from which the
     *   recipients are removed.
     * @param {Data.BeanCollection} target The collection to which the
     *   items are added.
     * @param {Array} draggedItems The recipients that are to be removed from
     *   the source collection.
     * @param {Array} droppedItems The recipients that are to be added to
     *   the target collection.
     */
    dropDraggedItems: function(source, target, draggedItems, droppedItems) {
        source.remove(draggedItems);

        _.each(droppedItems, function(item) {
            // The `id` must be unset because we\'re effectively creating
            // a brand new model to be linked.
            item.unset(\'id\');
            item.set(\'_link\', this.getLinkName());
        }, this);

        target.add(droppedItems);
    }
})
',
    ),
  ),
  'emailaction-paneltop' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.EmailactionPaneltopField
 * @alias SUGAR.App.view.fields.BaseEmailsEmailactionPaneltopField
 * @extends View.Fields.Base.EmailactionField
 */
({
    extendsFrom: \'EmailactionField\',

    /**
     * @inheritdoc
     * Set type to emailaction to get the template
     */
    initialize: function(options) {
        this._super("initialize", [options]);
        this.type = \'emailaction\';
        this.on(\'emailclient:close\', this.handleEmailClientClose, this);
    },

    /**
     * When email compose is done, refresh the data in the Emails subpanel
     */
    handleEmailClientClose: function() {
        var context = this.context.parent || this.context;
        var links = app.utils.getLinksBetweenModules(context.get(\'module\'), this.module);

        _.each(links, function(link) {
            context.trigger(\'panel-top:refresh\', link.name);
        });
    },

    /**
     * No additional options are needed from the element in order to launch the
     * email client.
     *
     * @param {jQuery} [$link] The element from which to get options.
     * @return {Object}
     * @private
     * @deprecated Use
     * View.Fields.Base.Emails.EmailactionPaneltopField#emailOptionTo and
     * View.Fields.Base.Emails.EmailactionPaneltopField#emailOptionRelated
     * instead.
     */
    _retrieveEmailOptionsFromLink: function($link) {
        app.logger.warn(\'View.Fields.Base.Emails.EmailactionPaneltopField#_retrieveEmailOptionsFromLink is \' +
            \'deprecated. Use View.Fields.Base.Emails.EmailactionPaneltopField#emailOptionTo and \' +
            \'View.Fields.Base.Emails.EmailactionPaneltopField#emailOptionRelated instead.\');
        return {};
    },

    /**
     * Returns the recipients to use in the To field of the email. If
     * `this.def.set_recipient_to_parent` is true, then the model is added to
     * the email\'s To field.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when identifying the recipients.
     * @return {undefined|Array}
     */
    emailOptionTo: function(model) {
        if (this.def.set_recipient_to_parent) {
            return [{
                bean: model
            }];
        }
    },

    /**
     * Returns the bean to use as the email\'s related record. If
     * `this.def.set_related_to_parent` is true, then the model is used.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model This model\'s parent is used as the email\'s
     * related record.
     * @return {undefined|Data.Bean}
     */
    emailOptionRelated: function(model) {
        if (this.def.set_related_to_parent) {
            return model;
        }
    }
})
',
    ),
  ),
  'compose-actionbar' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Actionbar for the email compose view
 *
 * @class View.Fields.Base.Emails.ComposeActionbarField
 * @alias SUGAR.App.view.fields.BaseEmailsComposeActionbarField
 * @extends View.Fields.Base.FieldsetField
 *
 * @deprecated Use {@link View.Fields.Base.Emails.Htmleditable_tinymceField}
 * instead to add buttons for email composition.
 */
({
    extendsFrom: \'FieldsetField\',

    events: {
        \'click a:not(.dropdown-toggle)\': \'handleButtonClick\'
    },

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Fields.Base.Emails.SenderField is deprecated. Use \' +
            \'View.Fields.Base.Emails.Htmleditable_tinymceField instead.\');

        this._super(\'initialize\', [options]);
        this.type = \'fieldset\';
    },

    /**
     * Fire an event when any of the buttons on the actionbar are clicked
     * Events could be set via the data-event attribute or an event is built using the button name
     *
     * @param evt
     */
    handleButtonClick: function(evt) {
        var triggerName, buttonName,
            $currentTarget = $(evt.currentTarget);
        if ($currentTarget.data(\'event\')) {
            triggerName = $currentTarget.data(\'event\');
        } else {
            buttonName = $currentTarget.attr(\'name\') || \'button\';
            triggerName = \'actionbar:\' + buttonName + \':clicked\';
        }
        this.view.context.trigger(triggerName);
    }
})
',
    ),
  ),
  'recipients' => 
  array (
    'templates' => 
    array (
      'edit' => '
<input type="hidden" name="{{name}}" class="select2{{#if def.css_class}} {{def.css_class}}{{/if}}"/>
<a class="btn" href="javascript:void(0);" data-name="{{name}}"><i class="fa {{#if def.icon}}{{def.icon}}{{else}}fa-book{{/if}}"></i></a>
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
      'select2-selection' => '
<span rel="tooltip" data-id="{{id}}" data-title="{{email}}" data-invalid="{{invalid}}">{{name}}</span>
',
      'detail' => '
<input type="hidden" name="{{name}}" readonly="readonly" class="select2{{#if def.css_class}} {{def.css_class}}{{/if}}"/>
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Emails.RecipientsField
 * @alias SUGAR.App.view.fields.BaseEmailsRecipientsField
 * @extends View.Fields.Base.BaseField
 * @deprecated Use {@link View.Fields.Base.Emails.EmailRecipientsField}
 * instead.
 */
({
    /**
     * @inheritdoc
     *
     * This field doesn\'t support `showNoData`.
     */
    showNoData: false,

    events: {
        \'click .btn\': \'_showAddressBook\'
    },

    fieldTag: \'input.select2\',

    plugins: [\'DragdropSelect2\'],

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Fields.Base.Emails.RecipientsField is deprecated. Use \' +
            \'View.Fields.Base.Emails.EmailRecipientsField instead.\');

        this._super(\'initialize\', [options]);
        // initialize the value to an empty collection
        this.model.setDefault(this.name, new Backbone.Collection);
    },

    /**
     * Sets up event handlers for syncing between the model and the recipients field.
     *
     * See {@link #format} for the acceptable formats for recipients.
     */
    bindDataChange: function() {
        /**
         * Sets up event handlers that allow external forces to manipulate the contents of the collection, while
         * maintaining the requirement for storing formatted recipients.
         */
        var bindCollectionChange = _.bind(function() {
            var value = this.model.get(this.name);
            if (value instanceof Backbone.Collection) {
                // on "add" we want to force the collection to be reset to guarantee that all models in the collection
                // have been properly formatted for use in this field
                value.on(\'add\', function(models, collection) {
                    // Backbone destroys the models currently in the collection on reset, so we must clone the
                    // collection in order to add the same models again
                    collection.reset(collection.clone().models);
                }, this);
                // on "remove" the requisite models have already been removed, so we only need to bother updating the
                // value in the DOM
                value.on(\'remove\', function(models, collection) {
                    // format the recipients and put them in the DOM
                    this._updateTheDom(this.format(this.model.get(this.name)));
                }, this);
                // on "reset" we want to replace all models in the collection with their formatted versions
                value.on(\'reset\', function(collection) {
                    var recipients = this.format(collection.models);
                    // do this silently so we don\'t trigger another reset event and end up in an infinite loop
                    collection.reset(recipients, {silent: true});
                    // put the newly formatted recipients in the DOM
                    this._updateTheDom(recipients);
                }, this);
            }
        }, this);

        // set up collection event handlers for the initial collection (initialized during this.initialize)
        bindCollectionChange();

        // handle the value on the model being changed to something other than the initial collection
        this.model.on(\'change:\' + this.name, function(model, recipients) {
            var value = this.model.get(this.name);
            if (!(value instanceof Backbone.Collection)) {
                // whoa! someone changed the value to be something other than a collection
                // stick that new value inside a collection and reset the value, so we\'re always dealing with a
                // collection... another change event will be triggered, so we\'ll end up in the else block right after
                // this
                this.model.set(this.name, new Backbone.Collection(value));
            } else {
                // phew! the value is a collection
                // but it\'s not the initial collection, so we\'ll have to set up collection event handlers for this
                // instance
                bindCollectionChange();
                // you never know what data someone sticks on the field, so we better reset the values in the collection
                // so that the recipients become formatted as we expect
                value.reset(recipients.clone().models);
            }
        }, this);
    },

    /**
     * Sets the value of the Select2 element, decorates any invalid recipients,
     * and rebuilds the tooltips for all recipients.
     *
     * @param {Array} recipients the return value for {@link #format}.
     */
    _updateTheDom: function(recipients) {
        // put the formatted recipients in the DOM
        this.getFieldElement().select2(\'data\', recipients);
        this._decorateInvalidRecipients();
        if (!this.def.readonly) {
            this.setDragDropPluginEvents(this.getFieldElement());
        }
    },

    /**
     * Remove events from the field value if it is a collection
     */
    unbindData: function() {
        var value = this.model.get(this.name);
        if (value instanceof Backbone.Collection) {
            value.off(null, null, this);
        }

        this._super(\'unbindData\');
    },

    /**
     * Render field with select2 widget
     *
     * @private
     */
    _render: function() {
        var $controlsEl;
        var $recipientsField;

        if (this.$el) {
            $controlsEl = this.$el.closest(\'.controls\');
            if ($controlsEl.length) {
                $controlsEl.addClass(\'controls-one btn-fit\');
            }
        }
        this._super(\'_render\');

        $recipientsField = this.getFieldElement();

        if ($recipientsField.length > 0) {
            $recipientsField.select2({
                allowClear: true,
                multiple: true,
                width: \'off\',
                containerCssClass: \'select2-choices-pills-close\',
                containerCss: {\'width\': \'100%\'},
                minimumInputLength: 1,
                query: _.bind(function(query) {
                    this.loadOptions(query);
                }, this),
                createSearchChoice: _.bind(this.createOption, this),
                formatSelection: _.bind(this.formatSelection, this),
                formatResult: _.bind(this.formatResult, this),
                formatSearching: _.bind(this.formatSearching, this),
                formatInputTooShort: _.bind(this.formatInputTooShort, this),
                selectOnBlur: true
            });

            if (!!this.def.disabled) {
                $recipientsField.select2(\'disable\');
            }

            if (!this.def.readonly) {
                this.setDragDropPluginEvents(this.getFieldElement());
            }
        }
    },

    /**
     * Fetches additional recipients from the server.
     *
     * See [Select2 Documentation of `query` parameter](http://ivaynberg.github.io/select2/#doc-query).
     *
     * @param {Object} query Possible attributes can be found in select2\'s
     *   documentation.
     */
    loadOptions: _.debounce(function(query) {
        var self = this,
            data = {
                results: [],
                // only show one page of results
                // if more results are needed, then the address book should be used
                more: false
            },
            options = {},
            callbacks = {},
            url;

        // add the search term to the URL params
        options.q = query.term;
        // the first 10 results should be enough
        // if more results are needed, then the address book should be used
        options.max_num = 10;
        // build the URL for fetching recipients that match the search term
        url = app.api.buildURL(\'Mail\', \'recipients/find\', null, options);
        // create the callbacks
        callbacks.success = function(result) {
            // the api returns objects formatted such that sidecar can convert them to beans
            // we need the records to be in a standard object format (@see RecipientsField::format) and the records
            // need to be converted into beans before we can format them
            var records = app.data.createMixedBeanCollection(result.records);
            // format and add the recipients that were found via the select2 callback
            data.results = self.format(records);
        };
        callbacks.error = function() {
            // don\'t add any recipients via the select2 callback
            data.results = [];
        };
        callbacks.complete = function() {
            // execute the select2 callback to add any new recipients
            query.callback(data);
        };
        app.api.call(\'read\', url, null, callbacks);
    }, 300),

    /**
     * Create additional select2 options when loadOptions returns no matches for the search term.
     *
     * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
     *
     * @param {String} term
     * @param {Array} data The options in the select2 drop-down after the query callback has been executed.
     * @return {Object}
     */
    createOption: function(term, data) {
        if (data.length === 0) {
            return {id: term, email: term};
        }
    },

    /**
     * Formats a recipient object for displaying selected recipients.
     *
     * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
     *
     * @param {Object} recipient
     * @return {String}
     */
    formatSelection: function(recipient) {
        var value = recipient.name || recipient.email,
            template = app.template.getField(this.type, \'select2-selection\', this.module);
        if (template) {
            return template({
                id: recipient.id,
                name: value,
                email: recipient.email,
                invalid: recipient._invalid
            });
        }
        return value;
    },

    /**
     * Formats a recipient object for displaying items in the recipient options list.
     *
     * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
     *
     * @param {Object} recipient
     * @return {String}
     */
    formatResult: function(recipient) {
        var format,
            email = Handlebars.Utils.escapeExpression(recipient.email);

        if (recipient.name) {
            format = \'"\' + Handlebars.Utils.escapeExpression(recipient.name) + \'" &lt;\' + email + \'&gt;\';
        } else {
            format = email;
        }

        return format;
    },

    /**
     * Returns the localized message indicating that a search is in progress
     *
     * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
     *
     * @return {string}
     */
    formatSearching: function() {
        return app.lang.get(\'LBL_LOADING\', this.module);
    },

    /**
     * Suppresses the message indicating the number of characters remaining before a search will trigger
     *
     * See [Select2 Documentation](http://ivaynberg.github.io/select2/#documentation).
     *
     * @param {string} term Search string entered by user.
     * @param {number} min Minimum required term length.
     * @return {string}
     */
    formatInputTooShort: function(term, min) {
        return \'\';
    },

    /**
     * Formats a set of recipients into an array of objects that select2 understands.
     *
     * See {@link #_formatRecipient} for the acceptable/expected attributes to
     * be found on each recipient.
     *
     * @param {Mixed} data A Backbone collection, a single Backbone model or standard JavaScript object, or an array of
     *   Backbone models or standard JavaScript objects.
     * @return {Array}
     */
    format: function(data) {
        var formattedRecipients = [];
        // the lowest common denominator of potential inputs is an array of objects
        // force the parameter to be an array of either objects or Backbone models so that we\'re always dealing with
        // one data-structure type
        if (data instanceof Backbone.Collection) {
            // get the raw array of models
            data = data.models;
        } else if (data instanceof Backbone.Model || (_.isObject(data) && !_.isArray(data))) {
            // wrap the single model in an array so the code below behaves the same whether it\'s a model or a collection
            data = [data];
        }
        if (_.isArray(data)) {
            _.each(data, function(recipient) {
                var formattedRecipient;
                if (!(recipient instanceof Backbone.Model)) {
                    // force the object to be a Backbone.Model to allow for certain assumptions to be made
                    // there is no harm in this because the recipient will not be added to the return value if no email
                    // address is found on the model
                    recipient = new Backbone.Model(recipient);
                }
                formattedRecipient = this._formatRecipient(recipient);
                // only add the recipient if there is an email address
                if (!_.isEmpty(formattedRecipient.email)) {
                    formattedRecipients.push(formattedRecipient);
                }
            }, this);
        }
        return formattedRecipients;
    },

    /**
     * Determine whether or not the recipient pills should be locked.
     * @return {boolean}
     */
    recipientsLocked: function() {
        return this.def.readonly || false;
    },

    /**
     * Synchronize the recipient field value with the model and setup tooltips for email pills.
     */
    bindDomChange: function() {
        var self = this;
        this.getFieldElement()
            .on(\'change\', function(event) {
                var value = $(this).select2(\'data\');
                if (event.removed) {
                    value = _.filter(value, function(d) {
                        return d.id !== event.removed.id;
                    });
                }
                self.model.get(self.name).reset(value);
            })
            .on(\'select2-selecting\', _.bind(this._handleEventOnSelected, this));
    },

    /**
     * Event handler for the Select2 "select2-selecting" event.
     *
     * @param {Event} event
     * @return {boolean}
     * @private
     */
    _handleEventOnSelected: function(event) {
        // only allow the user to select an option if it is determined to be a valid email address
        // returning true will select the option; false will prevent the option from being selected
        var isValidChoice = false;

        // since this event is fired twice, we only want to perform validation on the first event
        // event.object is not available on the second event
        if (event.object) {
            // the id and email address will not match when the email address came from the database and
            // we are assuming that email addresses stored in the database have already been validated
            if (event.object.id == event.object.email) {
                // this option must be a new email address that the application does not recognize
                // so mark it as valid and kick off an async validation
                isValidChoice = true;
                this._validateEmailAddress(event.object);
            } else {
                // the application should recognize the email address, so no need to validate it again
                // just assume it\'s a valid choice and we\'ll deal with the consequences later (server-side)
                isValidChoice = true;
            }
        }

        return isValidChoice;
    },

    /**
     * Destroy all select2 and tooltip plugins
     */
    unbindDom: function() {
        this.getFieldElement().select2(\'destroy\');
        this._super(\'unbindDom\');
    },

    /**
     * When in edit mode, the field includes an icon button for opening an address book. Clicking the button will
     * trigger an event to open the address book, which calls this method to do the dirty work. The selected recipients
     * are added to this field upon closing the address book.
     *
     * @private
     */
    _showAddressBook: function() {
        /**
         * Callback to add recipients, from a closing drawer, to the target Recipients field.
         * @param {undefined|Backbone.Collection} recipients
         */
        var addRecipients = _.bind(function(recipients) {
            if (recipients && recipients.length > 0) {
                this.model.get(this.name).add(recipients.models);
            }
        }, this);
        app.drawer.open(
            {
                layout: \'compose-addressbook\',
                context: {
                    module: \'Emails\',
                    mixed: true
                }
            },
            function(recipients) {
                addRecipients(recipients);
            }
        );
    },

    /**
     * update ul.select2-choices data attribute which prevents underrun of pills by
     * using a css definition for :before {content:\'\'} set to float right
     *
     * @param {string} content
     */
    setContentBefore: function(content) {
        this.$(\'.select2-choices\').attr(\'data-content-before\', content);
    },

    /**
     * Gets the recipients DOM field
     *
     * @return {Object} DOM Element
     */
    getFieldElement: function() {
        return this.$(this.fieldTag);
    },

    /**
     * Format a recipient from a Backbone.Model to a standard JavaScript object with id, module, email, and name
     * attributes. Only id and email are required for the recipient to be considered valid
     * {@link #format}.
     *
     * All attributes are optional. However, if the email attribute is not present, then a primary email address should
     * exist on the bean. Without an email address that can be resolved, the recipient is considered to be invalid. The
     * bean attribute must be a Backbone.Model and it is likely to be a Bean. Data found in the bean is considered to be
     * secondary to the attributes found on its parent model. The bean is a mechanism for collecting additional
     * information about the recipient that may not have been explicitly set when the recipient was passed in.
     * @param {Backbone.Model} recipient
     * @return {Object}
     * @private
     */
    _formatRecipient: function(recipient) {
        var formattedRecipient = {};
        if (recipient instanceof Backbone.Model) {
            var bean = recipient.get(\'bean\');
            // if there is a bean attribute, then more data can be extracted about the recipient to fill in any holes if
            // attributes are missing amongst the primary attributes
            // so follow the trail using recursion
            if (bean) {
                formattedRecipient = this._formatRecipient(bean);
            }
            // prioritize any values found on recipient over those already extracted from bean
            formattedRecipient = {
                id: recipient.get(\'id\') || formattedRecipient.id || recipient.get(\'email\'),
                module: recipient.get(\'module\') || recipient.module || recipient.get(\'_module\') || formattedRecipient.module,
                email: recipient.get(\'email\') || formattedRecipient.email,
                locked: this.recipientsLocked(),
                name: recipient.get(\'name\') || recipient.get(\'full_name\') || formattedRecipient.name,
                _invalid: recipient.get(\'_invalid\')
            };
            // don\'t bother with the recipient unless an id is present
            if (!_.isEmpty(formattedRecipient.id)) {
                // extract the primary email address for the recipient
                if (_.isArray(formattedRecipient.email)) {
                    var primaryEmailAddress = _.findWhere(formattedRecipient.email, {primary_address: true});

                    if (!_.isUndefined(primaryEmailAddress) && !_.isEmpty(primaryEmailAddress.email_address)) {
                        formattedRecipient.email = primaryEmailAddress.email_address;
                    }
                }
                // drop any values that are empty or non-compliant
                _.each(formattedRecipient, function(val, key) {
                    if ((_.isEmpty(formattedRecipient[key]) || !_.isString(formattedRecipient[key])) && !_.isBoolean(formattedRecipient[key])) {
                        delete formattedRecipient[key];
                    }
                });
            } else {
                // drop all values if an id isn\'t present
                formattedRecipient = {};
            }
        }
        return formattedRecipient;
    },

    /**
     * Validates an email address on the server asynchronously.
     *
     * Marks the recipient as invalid if it is not a valid email address.
     *
     * @param {Object} recipient
     * @param {string} recipient.id
     * @param {string} recipient.email
     * @private
     */
    _validateEmailAddress: function(recipient) {
        var callbacks = {};
        var url = app.api.buildURL(\'Mail\', \'address/validate\');

        callbacks.success = _.bind(function(result) {
            if (!result[recipient.email] && !this.disposed) {
                this._markRecipientInvalid(recipient.id);
            }
        }, this);
        callbacks.error = _.bind(function() {
            if (!this.disposed) {
                this._markRecipientInvalid(recipient.id);
            }
        }, this);

        app.api.call(\'create\', url, [recipient.email], callbacks);
    },

    /**
     * Mark the given recipient as invalid in the collection and update select2.
     *
     * @param {string} recipientId
     * @private
     */
    _markRecipientInvalid: function(recipientId) {
        var recipients = this.model.get(this.name);
        var recipient = recipients.get(recipientId);
        recipient.set(\'_invalid\', true);
        this._updateTheDom(this.format(recipients));
    },

    /**
     * Decorate any invalid recipients in this field.
     * @private
     */
    _decorateInvalidRecipients: function() {
        var self = this;
        var $invalidRecipients = this.$(\'.select2-search-choice [data-invalid="true"]\');
        $invalidRecipients.each(function() {
            var $choice = $(this).closest(\'.select2-search-choice\');
            $choice.addClass(\'select2-choice-danger\');
            $(this).attr(\'data-title\', app.lang.get(\'ERR_INVALID_EMAIL_ADDRESS\', self.module));
        });
    }
})
',
    ),
  ),
  'reply-action' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Reply action.
 *
 * This allows a user to "reply" to an existing email.
 *
 * @class View.Fields.Base.Emails.ReplyActionField
 * @alias SUGAR.App.view.fields.EmailsBaseReplyActionField
 * @extends View.Fields.Base.Emails.ForwardActionField
 */
({
    extendsFrom: \'EmailsForwardActionField\',

    /**
     * The name of the template for the reply header.
     *
     * @inheritdoc
     */
    _tplHeaderHtmlName: \'reply-header-html\',

    /**
     * @inheritdoc
     */
    _subjectPrefix: \'LBL_RE\',

    /**
     * The element ID to use to identify the reply content.
     *
     * @inheritdoc
     */
    _contentId: \'replycontent\',

    /**
     * @inheritdoc
     *
     * Updates the reply_to_id email option anytime the model\'s id attribute
     * changes.
     */
    bindDataChange: function() {
        var context = this.context.parent || this.context;
        var model = context.get(\'model\');

        this._super(\'bindDataChange\');

        if (model) {
            // Set the reply_to_id email option if the ID already exists.
            this.addEmailOptions({reply_to_id: model.get(\'id\')});

            // Update the reply_to_id email option anytime the ID changes. This
            // might occur if the ID was discovered later. It is an edge-case.
            this.listenTo(model, \'change:id\', function() {
                this.addEmailOptions({reply_to_id: model.get(\'id\')});
            });
        }
    },

    /**
     * Returns the recipients to use in the To field of the email. The sender
     * from the original email is included.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when identifying the recipients.
     * @return {undefined|Array}
     */
    emailOptionTo: function(model) {
        var originalTo;
        var originalSender = model.get(\'from_collection\');
        var to = this._createRecipients(originalSender);

        if (this.def.reply_all) {
            app.logger.warn(\'The reply_all option is deprecated. Use View.Fields.Base.Emails.ReplyAllActionField \' +
                \'instead.\');
            originalTo = model.get(\'to_collection\');
            to = _.union(to, this._createRecipients(originalTo));
        }

        return to;
    },

    /**
     * Returns the recipients to use in the CC field of the email. The
     * `reply_all` option is deprecated. Use
     * View.Fields.Base.Emails.ReplyAllActionField instead.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when identifying the recipients.
     * @return {undefined|Array}
     */
    emailOptionCc: function(model) {
        var originalCc;
        var cc;

        if (this.def.reply_all) {
            app.logger.warn(\'The reply_all option is deprecated. Use View.Fields.Base.Emails.ReplyAllActionField \' +
                \'instead.\');
            originalCc = model.get(\'cc_collection\');
            cc = this._createRecipients(originalCc);
        }

        return cc;
    },

    /**
     * Attachments are not carried over to replies.
     *
     * @inheritdoc
     */
    emailOptionAttachments: function(model) {
    },

    /**
     * Sets up the email options for the EmailClientLaunch plugin to use -
     * passing to the email compose drawer or building up the mailto link.
     *
     * @protected
     * @deprecated The EmailClientLaunch plugin handles email options.
     */
    _updateEmailOptions: function() {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_updateEmailOptions is deprecated. \' +
            \'The EmailClientLaunch plugin handles email options.\');
    },

    /**
     * Build the reply recipients based on the original email\'s from, to, and cc
     *
     * @param {boolean} all Whether this is reply to all (true) or just a standard
     *   reply (false).
     * @return {Object} To and Cc values for the reply email.
     * @return {Array} return.to The to values for the reply email.
     * @return {Array} return.cc The cc values for the reply email.
     * @protected
     * @deprecated Use
     * View.Fields.Base.Emails.ReplyActionField#emailOptionTo and
     * View.Fields.Base.Emails.ReplyActionField#emailOptionCc instead.
     */
    _getReplyRecipients: function(all) {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_getReplyRecipients is deprecated. Use \' +
            \'View.Fields.Base.Emails.ReplyActionField#emailOptionTo and \' +
            \'View.Fields.Base.Emails.ReplyActionField#emailOptionCc instead.\');

        if (all) {
            app.logger.warn(\'The reply_all option is deprecated. Use View.Fields.Base.Emails.ReplyAllActionField \' +
                \'instead.\');
        }

        return {
            to: this.emailOptionTo(this.model) || [],
            cc: this.emailOptionCc(this.model) || []
        };
    },

    /**
     * Given the original subject, generate a reply subject.
     *
     * @param {string} subject
     * @protected
     * @deprecated Use
     * View.Fields.Base.Emails.ReplyActionField#emailOptionSubject instead.
     */
    _getReplySubject: function(subject) {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_getReplySubject is deprecated. Use \' +
            \'View.Fields.Base.Emails.ReplyActionField#emailOptionSubject instead.\');

        return this.emailOptionSubject(this.model);
    },

    /**
     * Get the data required by the header template.
     *
     * @return {Object}
     * @protected
     * @deprecated Use
     * View.Fields.Base.Emails.ReplyActionField#_getHeaderParams instead.
     */
    _getReplyHeaderParams: function() {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_getReplyHeaderParams is deprecated. Use \' +
            \'View.Fields.Base.Emails.ReplyActionField#_getHeaderParams instead.\');

        return this._getHeaderParams(this.model);
    },

    /**
     * Build the reply header for text only emails.
     *
     * @param {Object} params
     * @param {string} params.from
     * @param {string} [params.date] Date original email was sent
     * @param {string} params.to
     * @param {string} [params.cc]
     * @param {string} params.name The subject of the original email.
     * @return {string}
     * @private
     * @deprecated Use
     * View.Fields.Base.Emails.ReplyActionField#_getHeader instead.
     */
    _getReplyHeader: function(params) {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_getReplyHeader is deprecated. Use \' +
            \'View.Fields.Base.Emails.ReplyActionField#_getHeader instead.\');

        return this._getHeader(params);
    },

    /**
     * Create an array of email recipients from the collection, which can be
     * used as recipients to pass to the new email.
     *
     * @param {Data.BeanCollection} collection
     * @return {Array}
     * @private
     */
    _createRecipients: function(collection) {
        return collection.map(function(recipient) {
            var data = {
                email: app.data.createBean(\'EmailAddresses\', recipient.get(\'email_addresses\'))
            };
            var parent;

            if (recipient.hasParent()) {
                parent = recipient.getParent();

                if (parent) {
                    data.bean = parent;
                }
            }

            return data;
        });
    },

    /**
     * Retrieve the plain text version of the reply body.
     *
     * @param {Data.Bean} model The body should come from this model\'s
     * attributes. EmailClientLaunch plugin should dictate the model based on
     * the context.
     * @return {string} The reply body
     * @private
     */
    _getReplyBody: function(model) {
        // Falls back to the `this.model` for backward compatibility.
        model = model || this.model;

        return model.get(\'description\') || \'\';
    },

    /**
     * Retrieve the HTML version of the email body.
     *
     * Ensure the result is a defined string and strip any signature wrapper
     * tags to ensure it doesn\'t get stripped if we insert a signature above
     * the forward content. Also strip any reply content class if this is a
     * forward to a previous reply. And strip any forward content class if this
     * is a forward to a previous forward.
     *
     * @return {string}
     * @protected
     * @deprecated Use
     * View.Fields.Base.Emails.ReplyActionField#emailOptionDescriptionHtml
     * instead.
     */
    _getReplyBodyHtml: function() {
        app.logger.warn(\'View.Fields.Base.Emails.ReplyActionField#_getReplyBodyHtml is deprecated. Use \' +
            \'View.Fields.Base.Emails.ReplyActionField#emailOptionDescriptionHtml instead.\');

        return this.emailOptionDescriptionHtml(this.model);
    }
})
',
    ),
    'templates' => 
    array (
      'reply-header-html' => '
<hr>
<p>
    <strong>{{str \'LBL_FROM\' module}}:</strong> {{from}}<br/>
    {{#if date}}<strong>{{str \'LBL_DATE\' module}}:</strong> {{formatDate date}}<br/>{{/if}}
    <strong>{{str \'LBL_TO_ADDRS\' module}}:</strong> {{to}}<br/>
    {{#if cc}}<strong>{{str \'LBL_CC\' module}}:</strong> {{cc}}<br/>{{/if}}
    <strong>{{str \'LBL_SUBJECT\' module}}:</strong> {{name}}<br/>
</p>
',
    ),
  ),
  'reply-all-action' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Reply all action.
 *
 * This allows a user to "reply all" to an existing email.
 *
 * @class View.Fields.Base.Emails.ReplyAllActionField
 * @alias SUGAR.App.view.fields.EmailsBaseReplyAllActionField
 * @extends View.Fields.Base.Emails.ReplyActionField
 */
({
    extendsFrom: \'EmailsReplyActionField\',

    /**
     * Returns the recipients to use in the To field of the email. The sender
     * and the recipients in the To field from the original email are included.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when identifying the recipients.
     * @return {undefined|Array}
     */
    emailOptionTo: function(model) {
        var originalTo = model.get(\'to_collection\');
        var to = this._super(\'emailOptionTo\', [model]) || [];

        to = _.union(to, this._createRecipients(originalTo));

        return to;
    },

    /**
     * Returns the recipients to use in the CC field of the email. These
     * recipients are the same ones who appeared in the original email\'s CC
     * field.
     *
     * @see EmailClientLaunch plugin.
     * @param {Data.Bean} model Use this model when identifying the recipients.
     * @return {undefined|Array}
     */
    emailOptionCc: function(model) {
        var originalCc = model.get(\'cc_collection\');
        var cc = this._createRecipients(originalCc);

        return cc;
    },

    /**
     * Returns the template from View.Fields.Base.Emails.ReplyActionField.
     *
     * @inheritdoc
     */
    _getHeaderHtmlTemplate: function() {
        this._tplHeaderHtml = this._tplHeaderHtml ||
            app.template.getField(\'reply-action\', this._tplHeaderHtmlName, \'Emails\');

        return this._tplHeaderHtml;
    }
})
',
    ),
  ),
  '_hash' => '97209d65a80c933fba808c36747636f9',
);

