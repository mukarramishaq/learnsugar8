<?php
$clientCache['Emails']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'create' => true,
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_LISTVIEW_FILTER_ALL',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
        1 => 
        array (
          'id' => 'assigned_to_me',
          'name' => 'LBL_ASSIGNED_TO_ME',
          'filter_definition' => 
          array (
            '$owner' => '',
          ),
          'editable' => false,
        ),
        2 => 
        array (
          'id' => 'my_sent',
          'name' => 'LBL_FILTER_MY_SENT',
          'filter_definition' => 
          array (
            0 => 
            array (
              '$from' => 
              array (
                0 => 
                array (
                  'parent_type' => 'Users',
                  'parent_id' => '$current_user_id',
                ),
              ),
            ),
            1 => 
            array (
              'state' => 
              array (
                '$in' => 
                array (
                  0 => 'Archived',
                ),
              ),
            ),
          ),
          'editable' => false,
        ),
        3 => 
        array (
          'id' => 'my_received',
          'name' => 'LBL_FILTER_MY_RECEIVED',
          'filter_definition' => 
          array (
            0 => 
            array (
              '$or' => 
              array (
                0 => 
                array (
                  '$to' => 
                  array (
                    0 => 
                    array (
                      'parent_type' => 'Users',
                      'parent_id' => '$current_user_id',
                    ),
                  ),
                ),
                1 => 
                array (
                  '$cc' => 
                  array (
                    0 => 
                    array (
                      'parent_type' => 'Users',
                      'parent_id' => '$current_user_id',
                    ),
                  ),
                ),
                2 => 
                array (
                  '$bcc' => 
                  array (
                    0 => 
                    array (
                      'parent_type' => 'Users',
                      'parent_id' => '$current_user_id',
                    ),
                  ),
                ),
              ),
            ),
            1 => 
            array (
              'state' => 
              array (
                '$in' => 
                array (
                  0 => 'Archived',
                ),
              ),
            ),
          ),
          'editable' => false,
        ),
        4 => 
        array (
          'id' => 'my_drafts',
          'name' => 'LBL_FILTER_MY_DRAFTS',
          'filter_definition' => 
          array (
            0 => 
            array (
              '$owner' => '',
            ),
            1 => 
            array (
              'state' => 
              array (
                '$in' => 
                array (
                  0 => 'Draft',
                ),
              ),
            ),
          ),
          'editable' => false,
        ),
        5 => 
        array (
          'id' => 'favorites',
          'name' => 'LBL_FAVORITES',
          'filter_definition' => 
          array (
            '$favorite' => '',
          ),
          'editable' => false,
        ),
        6 => 
        array (
          'id' => 'recently_viewed',
          'name' => 'LBL_RECENTLY_VIEWED',
          'filter_definition' => 
          array (
            '$tracker' => '-7 DAY',
          ),
          'editable' => false,
        ),
        7 => 
        array (
          'id' => 'recently_created',
          'name' => 'LBL_NEW_RECORDS',
          'filter_definition' => 
          array (
            'date_entered' => 
            array (
              '$dateRange' => 'last_7_days',
            ),
          ),
          'editable' => false,
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'name' => 
        array (
        ),
        'state' => 
        array (
          'vname' => 'LBL_LIST_STATUS',
        ),
        'date_sent' => 
        array (
          'vname' => 'LBL_LIST_DATE_COLUMN',
        ),
        'assigned_user_name' => 
        array (
        ),
        'parent_name' => 
        array (
        ),
        'tag' => 
        array (
        ),
        'mailbox_name' => 
        array (
        ),
        'total_attachments' => 
        array (
        ),
        '$owner' => 
        array (
          'predefined_filter' => true,
          'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => 
        array (
          'predefined_filter' => true,
          'vname' => 'LBL_FAVORITES_FILTER',
        ),
      ),
    ),
  ),
  '_hash' => 'c128e60b4a3c413e8cbca905f4e09d26',
);

