<?php
$clientCache['Emails']['base']['layout'] = array (
  'create' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.CreateLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsCreateLayout
 * @extends View.Layouts.Base.CreateLayout
 */
({
    extendsFrom: \'CreateLayout\',

    /**
     * @inheritdoc
     *
     * Enables the DragdropSelect2:SelectAll shortcut for views that implement
     * it.
     */
    initialize: function(options) {
        this.shortcuts = _.union(this.shortcuts || [], [\'DragdropSelect2:SelectAll\']);
        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'compose' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.ComposeLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsComposeLayout
 * @extends View.Layouts.Base.Emails.CreateLayout
 * @deprecated Use {@link View.Layouts.Base.Emails.ComposeEmailLayout} instead.
 */
({
    extendsFrom: \'EmailsCreateLayout\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Layouts.Base.Emails.ComposeLayout is deprecated. \' +
            \'Use View.Layouts.Base.Emails.ComposeEmailLayout instead.\');

        this._super(\'initialize\', [options]);
    }
})
',
    ),
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'compose-email',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'compose-email' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'compose-email',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.ComposeEmailLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsComposeEmailLayout
 * @extends View.Layouts.Base.Emails.CreateLayout
 */
({
    extendsFrom: \'EmailsCreateLayout\',

    /**
     * @inheritdoc
     *
     * Enables the Compose:Send shortcut for views that implement it.
     */
    initialize: function(options) {
        this.shortcuts = _.union(this.shortcuts || [], [\'Compose:Send\']);
        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'compose-addressbook' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'compose-addressbook-headerpane',
                    ),
                    1 => 
                    array (
                      'view' => 'compose-addressbook-recipientscontainer',
                    ),
                    2 => 
                    array (
                      'view' => 'compose-addressbook-filter',
                    ),
                    3 => 
                    array (
                      'layout' => 'compose-addressbook-list',
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'preview-pane',
                  'css_class' => 'preview-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'layout' => 'preview',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.ComposeAddressbookLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsComposeAddressbookLayout
 * @extends View.Layout
 */
({
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.view.Layout.prototype.initialize.call(this, options);
        this.collection.sync = this.sync;
        this.collection.allowed_modules = [\'Accounts\', \'Contacts\', \'Leads\', \'Prospects\', \'Users\'];
        this.context.on(\'compose:addressbook:search\', this.search, this);
    },
    /**
     * Calls the custom Mail API endpoint to search for email addresses.
     *
     * @param {string} method
     * @param {Data.Bean} model
     * @param {Object} options
     */
    sync: function(method, model, options) {
        var callbacks;
        var url;
        var success;

        options = options || {};

        // only fetch from the approved modules
        if (_.isEmpty(options.module_list)) {
            options.module_list = [\'all\'];
        } else {
            options.module_list = _.intersection(this.allowed_modules, options.module_list);
        }

        // this is a hack to make pagination work while trying to minimize the affect on existing configurations
        // there is a bug that needs to be fixed before the correct approach (config.maxQueryResult vs. options.limit)
        // can be determined
        app.config.maxQueryResult = app.config.maxQueryResult || 20;
        options.limit = options.limit || app.config.maxQueryResult;

        // Is there already a success callback?
        if (options.success) {
            success = options.success;
        }

        // Map the response so that the email field data is packaged as an
        // array of objects. The email field component expects the data to be
        // in that format.
        options.success = function(data) {
            if (_.isArray(data)) {
                data = _.map(data, function(row) {
                    row.email = [{
                        email_address: row.email,
                        email_address_id: row.email_address_id,
                        opt_out: row.opt_out,
                        // The email address must be seen as the primary email
                        // address to be shown in a list view.
                        primary_address: true
                    }];

                    // Remove the properties that are now stored in the nested
                    // email array.
                    delete row.opt_out;
                    delete row.email_address_id;

                    return row;
                });
            }

            // Call the original success callback.
            if (success) {
                success(data);
            }
        };

        options = app.data.parseOptionsForSync(method, model, options);
        options.params.erased_fields = true;

        callbacks = app.data.getSyncCallbacks(method, model, options);
        this.trigger(\'data:sync:start\', method, model, options);

        url = app.api.buildURL(\'Mail\', \'recipients/find\', null, options.params);
        app.api.call(\'read\', url, null, callbacks);
    },
    /**
     * Adds the set of modules and term that should be used to search for recipients.
     *
     * @param {Array} modules
     * @param {String} term
     */
    search: function(modules, term) {
        // reset offset to 0 on a search. make sure that it resets and does not update.
        this.collection.fetch({query: term, module_list: modules, offset: 0, update: false});
    }
})
',
    ),
  ),
  'subpanels' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_ACCOUNTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'accounts',
          ),
        ),
        1 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_LEADS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'leads',
          ),
        ),
        2 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CONTACTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'contacts',
          ),
        ),
        3 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_OPPORTUNITY_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'opportunities',
          ),
        ),
        4 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CASES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'cases',
          ),
        ),
        5 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_NOTES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'notes',
          ),
        ),
        6 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_TASKS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'tasks',
          ),
        ),
        7 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_BUGS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'bugs',
          ),
        ),
        8 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_QUOTES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'quotes',
          ),
        ),
      ),
    ),
  ),
  'archive-email' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'archive-email',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.ArchiveEmailLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsArchiveEmailLayout
 * @extends View.Layouts.Base.Emails.CreateLayout
 * @deprecated Use {@link View.Layouts.Base.Emails.CreateLayout} instead.
 */
({
    extendsFrom: \'EmailsCreateLayout\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Layouts.Base.Emails.ArchiveEmailLayout is deprecated. \' +
            \'Use View.Layouts.Base.Emails.CreateLayout instead.\');

        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'compose-documents' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.ComposeDocumentsLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsComposeDocumentsLayout
 * @extends View.Layout
 * @deprecated Use {@link View.Layouts.Base.SelectionListLayout} instead.
 */
({
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        app.logger.warn(\'View.Layouts.Base.Emails.ComposeDocumentsLayout is deprecated.\');

        this._super(\'initialize\', [options]);
    }
})
',
    ),
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'selection-list',
          'context' => 
          array (
            'module' => 'Documents',
          ),
        ),
      ),
    ),
  ),
  'records' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.Emails.RecordsLayout
 * @alias SUGAR.App.view.layouts.BaseEmailsRecordsLayout
 * @extends View.Layouts.Base.RecordsLayout
 */
({
    extendsFrom: \'RecordsLayout\',

    /**
     * @inheritdoc
     *
     * Remove shortcuts that do not apply to Emails module list view
     */
    initialize: function(options) {
        this.shortcuts = _.without(
            this.shortcuts,
            \'List:Favorite\',
            \'List:Follow\'
        );

        this._super(\'initialize\', [options]);
    }
})
',
    ),
  ),
  'compose-addressbook-list' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'view' => 'compose-addressbook-list',
        ),
        1 => 
        array (
          'view' => 'compose-addressbook-list-bottom',
        ),
      ),
    ),
  ),
  '_hash' => '085d3c62fe2b1de0d09a6ec94dfdce8e',
);

