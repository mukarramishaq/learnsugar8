<?php
$clientCache['Currencies']['base']['dependency'] = array (
  '_hash' => '40cd750bba9870f18aada2478b24840a',
  'dependencies' => 
  array (
    0 => 
    array (
      'name' => 'name',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'iso4217',
        1 => 'name',
        2 => 'iso4217',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'name',
            'value' => 'ifElse(equal(getDropdownValue("iso_currency_name", $iso4217), ""), $name, getDropdownValue("iso_currency_name", $iso4217))',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    1 => 
    array (
      'name' => 'symbol',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'id',
        1 => 'iso4217',
        2 => 'symbol',
        3 => 'iso4217',
        4 => 'symbol',
        5 => 'iso4217',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'symbol',
            'value' => 'ifElse(or(equal($id, -99), equal($iso4217, "")), $symbol, ifElse(equal(getDropdownValue("iso_currency_symbol", $iso4217), ""), $symbol, getDropdownValue("iso_currency_symbol", $iso4217)))',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
  ),
);

