<?php
$clientCache['DataPrivacy']['base']['dependency'] = array (
  '_hash' => '40cd750bba9870f18aada2478b24840a',
  'dependencies' => 
  array (
    0 => 
    array (
      'name' => 'business_purposeDDD',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'type',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetOptions',
          'params' => 
          array (
            'target' => 'business_purpose',
            'keys' => 'getListWhere($type, enum(enum("", enum("")),enum("Request for Data Privacy Policy", enum("")),enum("Send Personal Information being processed", enum("")),enum("Rectify Information", enum("")),enum("Request to Erase Information", enum("")),enum("Export Information", enum("")),enum("Restrict Processing", enum("")),enum("Object to Processing", enum("")),enum("Consent to Process", enum("Business Communications","Marketing Communications by company","Marketing Communications by partners")),enum("Withdraw Consent", enum("Business Communications","Marketing Communications by company","Marketing Communications by partners")),enum("Other", enum(""))))',
            'labels' => '"dataprivacy_business_purpose_dom"',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
  ),
);

