<?php
$clientCache['DataPrivacy']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#DataPrivacy/create',
        'label' => 'LNK_NEW_DATAPRIVACY',
        'acl_action' => 'create',
        'acl_module' => 'DataPrivacy',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#DataPrivacy',
        'label' => 'LNK_DATAPRIVACY_LIST',
        'acl_action' => 'list',
        'acl_module' => 'DataPrivacy',
        'icon' => 'fa-bars',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => 'a7fb83ac3dcb93be78f1197253f7d1a9',
);

