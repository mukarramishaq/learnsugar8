<?php
$clientCache['DataPrivacy']['base']['layout'] = array (
  'record' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
 /**
 * @class View.Layouts.Base.DataPrivacy.RecordLayout
 * @alias SUGAR.App.view.layouts.BaseDataPrivacyRecordLayout
 * @extends View.Layouts.Base.RecordLayout
 */
({
    extendsFrom: \'RecordLayout\',

    /**
     * @inheritdoc
     *
     * Adds handler for invoking Mark for Erasure view
     */
    initialize: function(options) {
        this._super(\'initialize\', arguments);
        this.listenTo(this.context, \'mark-erasure:click\', this.showMarkForEraseDrawer);
    },

    /**
     * Open a drawer to mark fields on the given model for erasure.
     *
     * @param {Data.Bean} modelForErase Model to mark fields on.
     */
    showMarkForEraseDrawer: function(modelForErase) {
        var context = this.context.getChildContext({
            name: \'Pii\',
            model: app.data.createBean(\'Pii\'),
            modelForErase: modelForErase,
            fetch: false
        });

        app.drawer.open({
            layout: \'mark-for-erasure\',
            context: context
        });
    }
})
',
    ),
  ),
  'mark-for-erasure' => 
  array (
    'meta' => 
    array (
      'css_class' => 'row-fluid',
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'base',
            'name' => 'main-pane',
            'css_class' => 'main-pane span12',
            'components' => 
            array (
              0 => 
              array (
                'view' => 'mark-for-erasure-headerpane',
              ),
              1 => 
              array (
                'view' => 'filtered-search',
              ),
              2 => 
              array (
                'view' => 'mark-for-erasure',
              ),
            ),
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
 /**
 * @class View.Layouts.Base.MarkForErasureLayout
 * @alias SUGAR.App.view.layouts.MarkForErasureLayout
 * @extends View.Layouts.Base.DefaultLayout
 */
({
    extendsFrom: \'DefaultLayout\',

    plugins: [\'ShortcutSession\'],

    shortcuts: [\'MarkForErasureHeaderPanel:Close\']
})
',
    ),
  ),
  'subpanels' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_LEADS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'leads',
          ),
        ),
        1 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CONTACTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'contacts',
          ),
        ),
        2 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_PROSPECTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'prospects',
          ),
        ),
        3 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_ACCOUNTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'accounts',
          ),
        ),
      ),
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Layouts.Base.DataPrivacy.SubpanelsLayout
 * @alias SUGAR.App.view.layouts.DataPrivacySubpanelsLayout
 * @extends View.Layout.Base.SubpanelsLayout
 */
({
    /**
     * @inheritdoc
     * inject the Mark for Erase action link to all subpanels
     */
    initComponents: function(component, def) {
        this._super(\'initComponents\', arguments);

        // Add the erase action to all subpanel rowactions
        _.each(this._components, function(comp) {
            if (!comp.getComponent) {
                return;
            }
            var viewName = \'subpanel-list\';
            if (comp.meta && comp.meta.components) {
                _.find(comp.meta.components, function(def) {
                    var name = \'\';
                    var prefix = \'subpanel-for\';
                    if (def.view) {
                        name = _.isObject(def.view) ? def.view.name || def.view.type : def.view;
                    }

                    if (name === \'subpanel-list\' || _.isString(name) && name.substr(0, prefix.length) === prefix) {
                        viewName = name;
                        return true;
                    }

                    return false;
                });
            }
            var subView = comp.getComponent(viewName);
            if (subView && subView.meta && subView.meta.rowactions && subView.meta.rowactions.actions) {
                subView.meta.rowactions.actions.push({
                    \'type\': \'dataprivacyerase\',
                    \'icon\': \'fa-eye\',
                    \'name\': \'dataprivacy-erase\',
                    \'label\': \'LBL_DATAPRIVACY_MARKFORERASE\'
                });
            }
        });
    }
})
',
    ),
  ),
  '_hash' => '3042f28780e0d1fe596d51313d1f1067',
);

