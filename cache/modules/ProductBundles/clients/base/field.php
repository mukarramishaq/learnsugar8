<?php
$clientCache['ProductBundles']['base']['field'] = array (
  'quote-group-title' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundles.QuoteGroupTitleField
 * @alias SUGAR.App.view.fields.BaseProductBundlesQuoteGroupTitleField
 * @extends View.Fields.Base.Field
 */
({
    /**
     * Any additional CSS classes that need to be applied to the field
     */
    css_class: undefined,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.css_class = options.def.css_class || \'\';
        this._super(\'initialize\', [options]);
    }
})
',
    ),
    'templates' => 
    array (
      'edit' => '
    <input type="text" name="{{name}}" value="{{value}}" class="inherit-width">
',
      'detail' => '
{{value}}
',
    ),
  ),
  'quote-data-actiondropdown' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundles.QuoteDataActiondropdownField
 * @alias SUGAR.App.view.fields.BaseProductBundlesQuoteDataActiondropdownField
 * @extends View.Fields.Base.ActiondropdownField
 */
({
    /**
     * @inheritdoc
     */
    extendsFrom: \'BaseActiondropdownField\',

    /**
     * @inheritdoc
     */
    className: \'quote-data-actiondropdown\',

    /**
     * Skipping ActionmenuField\'s override, just returning this.def.buttons
     *
     * @inheritdoc
     */
    _getChildFieldsMeta: function() {
        return app.utils.deepCopy(this.def.buttons);
    },

    /**
     * Overriding for quote-data-group-header in create view to display a specific template
     *
     * @inheritdoc
     */
    _loadTemplate: function() {
        this._super(\'_loadTemplate\');

        if (this.view.name === \'quote-data-group-header\' && this.view.isCreateView) {
            this.template = app.template.getField(\'quote-data-actiondropdown\', \'list\', this.model.module);
        }
    }
})
',
    ),
    'templates' => 
    array (
      'list' => '
<a track="click:listactiondropdown" class="btn btn-invisible dropdown-toggle {{name}}-toggle"
   data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
   rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
   role="button" tabindex="{{tabIndex}}" aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}">
    <span class="fa {{def.icon}}"></span></a>
<ul data-menu="dropdown" class="dropdown-menu" role="menu"></ul>
',
    ),
  ),
  'quote-data-editablelistbutton' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundles.EditablelistbuttonField
 * @alias SUGAR.App.view.fields.BaseProductBundlesEditablelistbuttonField
 * @extends View.Fields.Base.BaseEditablelistbuttonField
 */
({
    extendsFrom: \'BaseEditablelistbuttonField\',

    /**
     * @inheritdoc
     */
    _render: function() {
        var syncedName;

        if (this.name === \'inline-save\') {
            syncedName = this.model.getSynced(\'name\');
            if (this.model.get(\'name\') !== syncedName) {
                this.changed = true;
            }
        }

        this._super(\'_render\');

        if (this.tplName === \'edit\') {
            this.$el.closest(\'.left-column-save-cancel\').addClass(\'higher\');
        } else {
            this.$el.closest(\'.left-column-save-cancel\').removeClass(\'higher\');
        }
    },

    /**
     * Overriding and not calling parent _loadTemplate as those are based off view/actions and we
     * specifically need it based off the modelView set by the parent layout for this row model
     *
     * @inheritdoc
     */
    _loadTemplate: function() {
        this.tplName = this.model.modelView || \'list\';

        if (this.view.action === \'list\' && _.indexOf([\'edit\', \'disabled\'], this.action) < 0) {
            this.template = app.template.empty;
        } else {
            this.template = app.template.getField(this.type, this.tplName, this.module);
        }
    },

    /**
     * Overriding cancelEdit so we can update the group name if this is coming from
     * the quote data group header
     *
     * @inheritdoc
     */
    cancelEdit: function() {
        var modelModule = this.model.module;
        var modelId = this.model.cid;
        var syncedAttribs = this.model.getSynced();
        if (this.isDisabled()) {
            this.setDisabled(false);
        }

        this.changed = false;

        if (this.view.name === \'quote-data-group-header\') {
            // for cancel on group-header, revertAttributes doesn\'t reset the model
            if (this.model.get(\'name\') !== syncedAttribs.name) {
                if (_.isUndefined(syncedAttribs.name)) {
                    // if name was undefined, unset name
                    this.model.unset(\'name\');
                } else {
                    // if name was defined or \'\', set back to that
                    this.model.set(\'name\', syncedAttribs.name);
                }
            }
        } else {
            this.model.revertAttributes();
        }

        this.view.clearValidationErrors();

        this.view.toggleRow(modelModule, modelId, false);

        // trigger a cancel event across the view layout so listening components
        // know the changes made in this row are being reverted
        if (this.view.layout) {
            this.view.layout.trigger(\'editablelist:\' + this.view.name + \':cancel\', this.model);
        }
    },

    /**
     * Overriding cancelClicked to trigger an event if this is a
     * create view or the group was just saved
     *
     * @inheritdoc
     */
    cancelClicked: function() {
        var syncedAttribs = this.model.getSynced();
        var itemsInGroup = this.model.get(\'product_bundle_items\');

        if (itemsInGroup) {
            itemsInGroup = itemsInGroup.length;
        }

        if (this.view.isCreateView || (syncedAttribs._justSaved && itemsInGroup === 0)) {
            this.view.layout.trigger(\'editablelist:\' + this.view.name + \':create:cancel\', this.model);
        } else {
            this.cancelEdit();
        }
    },

    /**
     * Called after the save button is clicked and all the fields have been validated,
     * triggers an event for
     *
     * @inheritdoc
     */
    _save: function() {
        this.view.layout.trigger(\'editablelist:\' + this.view.name + \':saving\', true);
        this._saveRowModel();
    },

    /**
     * Saves the row\'s model
     *
     * @private
     */
    _saveRowModel: function() {
        var self = this;
        var oldModelId = this.model.cid;
        var quoteModel = this.context.get(\'parentModel\');
        var successCallback = function(data, request) {
            self.changed = false;
            self.model.modelView = \'list\';

            if (!_.isEmpty(data.related_record)) {
                self.model.setSyncedAttributes(data.related_record);
                self.model.set(data.related_record);
            }

            if (self.view.layout) {
                self.view.layout.trigger(\'editablelist:\' + self.view.name + \':save\', self.model, oldModelId);
            }
        };
        var options = {
            success: successCallback,
            error: function(error) {
                if (error.status === 409) {
                    app.utils.resolve409Conflict(error, self.model, function(model, isDatabaseData) {
                        if (model) {
                            if (isDatabaseData) {
                                successCallback(model);
                            } else {
                                self._save();
                            }
                        }
                    });
                }
            },
            complete: function() {
                // remove this model from the list if it has been unlinked
                if (self.model.get(\'_unlinked\')) {
                    self.collection.remove(self.model, {silent: true});
                    self.collection.trigger(\'reset\');
                    self.view.render();
                } else {
                    self.setDisabled(false);
                }
            },
            lastModified: self.model.get(\'date_modified\'),
            //Show alerts for this request
            showAlerts: {
                \'process\': true,
                \'success\': {
                    messages: app.lang.get(\'LBL_RECORD_SAVED\', self.module)
                }
            },
            relate: this.model.link ? true : false
        };

        options = _.extend({}, options, this.getCustomSaveOptions(options));

        app.api.relationships(\'update\', \'Quotes\', {
            id: quoteModel.get(\'id\'),
            link: \'product_bundles\',
            relatedId: this.model.get(\'id\'),
            related: {
                name: this.model.get(\'name\')
            }
        }, null, options);
    }
});
',
    ),
    'templates' => 
    array (
      'edit' => '
<a href="{{#if fullRoute}}#{{fullRoute}}{{else}}{{#if def.route}}#{{buildRoute context=context model=model action=def.route.action}}{{else}}javascript:void(0);{{/if}}{{/if}}"
    class="btn {{name}}"
    data-placement="bottom"
    {{#if def.tooltip}}
        rel="tooltip"
        data-original-title="{{str def.tooltip module}}"
    {{else}}
        title="{{label}}"
    {{/if}}
    {{#if ariaLabel}}aria-label="{{ariaLabel}}"{{/if}}
    role="button" tabindex="{{tabIndex}}" name="{{name}}">{{#if def.icon}}<i class="fa {{def.icon}}"></i> {{/if}}</a>
',
    ),
  ),
  'quote-footer-input' => 
  array (
    'templates' => 
    array (
      'detail' => '
<div class="quote-footer-item quote-footer-input-item {{css_class}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        <input type="text" id="{{name}}-percent" name="{{name}}" value="{{value_percent}}" />
        <input type="text" id="{{name}}-amount" name="{{name}}" value="{{value_amount}}" />
    </span>
</div>
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.ProductBundles.QuoteFooterInputField
 * @alias SUGAR.App.view.fields.BaseProductBundlesQuoteFooterInputField
 * @extends View.Fields.Base.Field
 */
({
    /**
     * The value dollar amount
     */
    value_amount: undefined,

    /**
     * The value percent amount
     */
    value_percent: undefined,

    /**
     * @inheritdoc
     */
    format: function(value) {
        if (!value) {
            this.value_amount = app.currency.formatAmountLocale(\'0\');
            this.value_percent = \'0%\';
        }
    }
})
',
    ),
  ),
  'currency' => 
  array (
    'templates' => 
    array (
      'quote-data-group-footer' => '
<div class="quote-footer-item {{css_class}}">
    <span class="quote-footer-label">{{label}}</span>
    <span class="quote-footer-value">
        {{#if transactionValue}}
            <label class="original">{{transactionValue}}</label><div class="converted">{{value}}</div>
        {{else}}
            {{value}}
        {{/if}}
    </span>
</div>
',
    ),
  ),
  '_hash' => 'ae26edab2fe2c1927ae5e501b1555464',
);

