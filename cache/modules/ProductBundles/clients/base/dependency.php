<?php
$clientCache['ProductBundles']['base']['dependency'] = array (
  '_hash' => '40cd750bba9870f18aada2478b24840a',
  'dependencies' => 
  array (
    0 => 
    array (
      'name' => 'tax_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'tax',
        1 => 'tax',
        2 => 'tax',
        3 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'tax_usdollar',
            'value' => 'ifElse(and(isNumeric($tax), not(equal($tax, 0))), currencyDivide($tax, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    1 => 
    array (
      'name' => 'total',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'total',
            'value' => 'currencyAdd(                $new_sub,                "0"            )',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    2 => 
    array (
      'name' => 'total_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'total',
        1 => 'total',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'total_usdollar',
            'value' => 'ifElse(isNumeric($total), currencyDivide($total, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    3 => 
    array (
      'name' => 'subtotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'subtotal',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'subtotal_usdollar',
            'value' => 'ifElse(isNumeric($subtotal), currencyDivide($subtotal, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    4 => 
    array (
      'name' => 'shipping_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'shipping',
        1 => 'shipping',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'shipping_usdollar',
            'value' => 'ifElse(isNumeric($shipping), currencyDivide($shipping, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    5 => 
    array (
      'name' => 'deal_tot',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
        0 => 'deal_calc',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'deal_tot',
            'value' => 'rollupCurrencySum($products, "deal_calc")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    6 => 
    array (
      'name' => 'deal_tot_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'deal_tot',
        1 => 'deal_tot',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'deal_tot_usdollar',
            'value' => 'ifElse(isNumeric($deal_tot), currencyDivide($deal_tot, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    7 => 
    array (
      'name' => 'new_sub',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'deal_tot',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'new_sub',
            'value' => 'currencySubtract($subtotal, $deal_tot)',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    8 => 
    array (
      'name' => 'new_sub_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
        1 => 'new_sub',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => false,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'new_sub_usdollar',
            'value' => 'ifElse(isNumeric($new_sub), currencyDivide($new_sub, $base_rate), "")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    9 => 
    array (
      'name' => 'subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
        0 => 'subtotal',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'subtotal',
            'value' => 'rollupCurrencySum($products, "subtotal")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    10 => 
    array (
      'name' => 'taxable_subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
        0 => 'total_amount',
        1 => 'tax_class',
      ),
      'onload' => false,
      'isRelated' => true,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'SetValue',
          'params' => 
          array (
            'target' => 'taxable_subtotal',
            'value' => 'rollupConditionalSum($products, "total_amount", "tax_class", "Taxable")',
            'errorValue' => NULL,
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    11 => 
    array (
      'name' => 'readOnlytax_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'tax',
        1 => 'tax',
        2 => 'tax',
        3 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'tax_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    12 => 
    array (
      'name' => 'readOnlytotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'total',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    13 => 
    array (
      'name' => 'readOnlytotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'total',
        1 => 'total',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'total_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    14 => 
    array (
      'name' => 'readOnlysubtotal_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'subtotal',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'subtotal_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    15 => 
    array (
      'name' => 'readOnlyshipping_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'shipping',
        1 => 'shipping',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'shipping_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    16 => 
    array (
      'name' => 'readOnlydeal_tot',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'deal_tot',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    17 => 
    array (
      'name' => 'readOnlydeal_tot_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'deal_tot',
        1 => 'deal_tot',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'deal_tot_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    18 => 
    array (
      'name' => 'readOnlynew_sub',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'subtotal',
        1 => 'deal_tot',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'new_sub',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    19 => 
    array (
      'name' => 'readOnlynew_sub_usdollar',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'new_sub',
        1 => 'new_sub',
        2 => 'base_rate',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'new_sub_usdollar',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    20 => 
    array (
      'name' => 'readOnlysubtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'subtotal',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
    21 => 
    array (
      'name' => 'readOnlytaxable_subtotal',
      'hooks' => 
      array (
        0 => 'all',
      ),
      'trigger' => 'true',
      'triggerFields' => 
      array (
        0 => 'products',
      ),
      'relatedFields' => 
      array (
      ),
      'onload' => true,
      'isRelated' => false,
      'actions' => 
      array (
        0 => 
        array (
          'action' => 'ReadOnly',
          'params' => 
          array (
            'target' => 'taxable_subtotal',
            'value' => 'true',
          ),
        ),
      ),
      'notActions' => 
      array (
      ),
    ),
  ),
);

