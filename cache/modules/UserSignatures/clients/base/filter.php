<?php
$clientCache['UserSignatures']['base']['filter'] = array (
  'basic' => 
  array (
    'meta' => 
    array (
      'quicksearch_field' => 
      array (
        0 => 'name',
      ),
      'quicksearch_priority' => 1,
      'quicksearch_split_terms' => false,
      'filters' => 
      array (
        0 => 
        array (
          'id' => 'all_records',
          'name' => 'LBL_LISTVIEW_FILTER_ALL',
          'filter_definition' => 
          array (
          ),
          'editable' => false,
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'meta' => 
    array (
      'default_filter' => 'all_records',
      'fields' => 
      array (
        'name' => 
        array (
        ),
        'date_modified' => 
        array (
        ),
      ),
    ),
  ),
  '_hash' => '81cbdb44125a358879627544b24c2fd9',
);

