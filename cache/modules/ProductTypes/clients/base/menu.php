<?php
$clientCache['ProductTypes']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#ProductTypes/create',
        'label' => 'LNK_NEW_PRODUCT_TYPE',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#ProductTypes',
        'label' => 'LNK_VIEW_PRODUCT_TYPES',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      2 => 
      array (
        'route' => '#ProductTemplates/create',
        'label' => 'LNK_NEW_PRODUCT',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-plus',
      ),
      3 => 
      array (
        'route' => '#ProductTemplates',
        'label' => 'LNK_PRODUCT_LIST',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      4 => 
      array (
        'route' => '#Manufacturers',
        'label' => 'LNK_NEW_MANUFACTURER',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      5 => 
      array (
        'route' => '#ProductCategories',
        'label' => 'LNK_NEW_PRODUCT_CATEGORY',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-list',
      ),
      6 => 
      array (
        'route' => '#bwc/index.php?module=Import&action=Step1&import_module=ProductTypes&return_module=ProductTypes&return_action=index',
        'label' => 'LNK_IMPORT_PRODUCT_TYPES',
        'acl_action' => '',
        'acl_module' => '',
        'icon' => 'fa-arrow-circle-o-up',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_RECORD',
      'visible' => false,
      'icon' => 'fa-plus',
      'order' => 1,
    ),
  ),
  '_hash' => '23c325b09c0e8429c7103bd876a86548',
);

