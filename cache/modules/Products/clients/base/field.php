<?php
$clientCache['Products']['base']['field'] = array (
  'discount-select' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.DiscountSelectField
 * @alias SUGAR.App.view.fields.BaseProductsDiscountSelectField
 * @extends View.Fields.Base.ActiondropdownField
 */
({
    extendsFrom: \'BaseActiondropdownField\',

    /**
     * The current currency object
     */
    currentCurrency: undefined,

    /**
     * The current symbol to use in place of the caret dropdown icon
     */
    currentDropdownSymbol: undefined,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);
        this.events = _.extend({}, this.events, {
            \'shown.bs.dropdown\': \'toggleDropdown\',
            \'hidden.bs.dropdown\': \'toggleDropdown\'
        });
        this.updateCurrencyStrings();
    },

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        this._super(\'bindDataChange\');

        this.context.on(\'button:discount_select_change:click\', this.onDiscountChanged, this);
        this.context.on(\'record:cancel:clicked\', this.onRecordCancel, this);

        this.model.on(\'change:currency_id\', this.updateCurrencyStrings, this);
    },

    /**
     * Handles setting the field back to synced values when the record is canceled
     */
    onRecordCancel: function() {
        var changedAttributes = this.model.changedAttributes(this.model.getSynced());
        this.model.set(changedAttributes, {
            revert: true
        });

        this.updateDropdownSymbol();
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        var $dropdown;

        this._super(\'_render\');

        $dropdown = this.$(\'.fa\');
        $dropdown.removeClass(this.caretIcon);
        $dropdown.text(this.currentDropdownSymbol);
    },

    /**
     * Called when a user clicks the Amount or Percent dropdown buttons
     *
     * @param {Data.Bean} model The model of the row that was changed
     * @param {View.Field} field The field that triggered the event
     * @param {Event} evt The click event
     */
    onDiscountChanged: function(model, field, evt) {
        var isPercent = false;
        if (this.model === model) {
            // only update for the row the event was triggered in
            if (field.name === \'select_discount_percent_button\') {
                isPercent = true;
            }
            this.model.set(this.name, isPercent);

            this.view.context.trigger(\'editable:record:toggleEdit\');

            this.updateDropdownSymbol();
        }
    },

    /**
     * Updates the dropdown icon symbol
     */
    updateDropdownSymbol: function() {
        var val = this.model.get(this.name);

        if (_.isUndefined(val) || val === false) {
            this.currentDropdownSymbol = this.currentCurrency.symbol;
        } else {
            this.currentDropdownSymbol = \'%\';
        }

        this.render();
    },

    /**
     * Gets the current row model\'s currency_id and updates the labels for the buttons
     */
    updateCurrencyStrings: function() {
        var btn;
        var currentCurrencyLabel;

        if (this.model.has(\'currency_id\')) {
            this.currentCurrency = app.metadata.getCurrency(this.model.get(\'currency_id\'));
            currentCurrencyLabel = this.currentCurrency.symbol + \' \' + this.currentCurrency.name;

            if (app.lang.direction !== \'ltr\') {
                currentCurrencyLabel = this.currentCurrency.name + \' \' + this.currentCurrency.symbol;
            }

            btn = _.find(this.def.buttons, function(button) {
                return button.name === \'select_discount_amount_button\';
            });

            // update the button field def label to the current row currency
            btn.label = currentCurrencyLabel;

            btn = _.find(this.dropdownFields, function(button) {
                return button.name === \'select_discount_amount_button\';
            });

            if (btn) {
                // if the button has already been rendered into dropdownFields
                // update the actual button field label, not just the defs
                btn.label = currentCurrencyLabel;
            }

            // make sure the dropdown symbol is updated
            this.updateDropdownSymbol();
        }
    },

    /**
     * Sets a button accessibility class \'aria-expanded\' to true or false
     * depending on if the dropdown menu is open or closed.
     *
     * @private
     */
    toggleDropdown: function(evt) {
        var state = evt.type ? evt.type !== \'hidden\' : this.$el.hasClass(\'open\');
        var flexView = this.closestComponent(\'quote-data-list-groups\');
        var $fieldSet = this.$el.parents(\'.quote-discount-percent\');
        var $tableCell = $fieldSet.parent();
        var scrollOffset = $fieldSet.offset().left;
        var scrollWidth = $tableCell.width();

        // on record view flexView doesn\'t exist
        if (flexView) {
            flexView.trigger(\'list:scrollLock\', state);
        }
        $fieldSet.css(\'left\', state ? scrollOffset : \'auto\');
        $fieldSet.css(\'width\', state ? scrollWidth : \'100%\');

        this._toggleAria();
    }
})
',
    ),
    'templates' => 
    array (
      'dropdown' => '
{{#each dropdownFields}}
    {{#eq type \'divider\'}}
        <li class="divider"></li>
    {{else}}
        <li>{{placeholder}}</li>
    {{/eq}}
{{/each}}
',
      'list' => '
',
      'detail' => '
{{#if def.buttons}}
    {{#unless def.no_default_action}}
        {{defaultActionBtn.placeholder}}
    {{/unless}}
    {{#if dropdownFields}}
        <a track="click:actiondropdown"
           class="btn dropdown-toggle{{#if def.primary}} btn-primary{{/if}}"
           data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
           rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
           role="button" tabindex="{{tabIndex}}" aria-haspopup="true" aria-expanded="false"
           aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}"><span class="fa {{caretIcon}}"></span></a>
        <ul data-menu="dropdown" class="dropdown-menu" role="menu"></ul>
    {{/if}}
{{/if}}
',
    ),
  ),
  'line-num' => 
  array (
    'templates' => 
    array (
      'list' => '
{{#if value}}
<div class="ellipsis_inline" data-placement="bottom"{{#if dir}} dir="{{dir}}"{{/if}} title="{{value}}">
    {{value}}
</div>
{{/if}}
',
      'edit' => '
{{#if value}}
<div class="ellipsis_inline" data-placement="bottom"{{#if dir}} dir="{{dir}}"{{/if}} title="{{value}}">
    {{value}}
</div>
{{/if}}
',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.LineNumField
 * @alias SUGAR.App.view.fields.BaseProductsLineNumField
 * @extends View.Fields.Base.IntField
 */
({
    extendsFrom: \'IntField\'
})
',
    ),
  ),
  'quote-data-editablelistbutton' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.EditablelistbuttonField
 * @alias SUGAR.App.view.fields.BaseProductsEditablelistbuttonField
 * @extends View.Fields.Base.BaseEditablelistbuttonField
 */
({
    extendsFrom: \'BaseEditablelistbuttonField\',

    /**
     * Overriding EditablelistbuttonField\'s Events with mousedown instead of click
     */
    events: {
        \'mousedown [name=inline-save]\': \'saveClicked\',
        \'mousedown [name=inline-cancel]\': \'cancelClicked\'
    },

    /**
     * @inheritdoc
     */
    _render: function() {
        this._super(\'_render\');

        if (_.isUndefined(this.changed) && this.model.isNew()) {
            // when adding additional items to the list, causing additional renders,
            // this.changed gets set undefined on re-initialize, so we need to make sure
            // if this is an unsaved model and this.changed is undefined, that we set changed true
            this.changed = true;
        }

        if (this.tplName === \'edit\') {
            this.$el.closest(\'.left-column-save-cancel\').addClass(\'higher\');
        } else {
            this.$el.closest(\'.left-column-save-cancel\').removeClass(\'higher\');
        }
    },

    /**
     * Overriding and not calling parent _loadTemplate as those are based off view/actions and we
     * specifically need it based off the modelView set by the parent layout for this row model
     *
     * @inheritdoc
     */
    _loadTemplate: function() {
        this.tplName = this.model.modelView || \'list\';

        if (this.view.action === \'list\' && _.indexOf([\'edit\', \'disabled\'], this.action) < 0) {
            this.template = app.template.empty;
        } else {
            this.template = app.template.getField(this.type, this.tplName, this.module);
        }
    },

    /**
     * @inheritdoc
     */
    cancelEdit: function() {
        if (this.isDisabled()) {
            this.setDisabled(false);
        }
        this.changed = false;
        this.model.revertAttributes();
        this.view.clearValidationErrors();

        // this is the only line I had to change
        this.view.toggleRow(this.model.module, this.model.cid, false);

        // trigger a cancel event across the view layout so listening components
        // know the changes made in this row are being reverted
        if (this.view.layout) {
            this.view.layout.trigger(\'editablelist:\' + this.view.name + \':cancel\', this.model);
        }
    },

    /**
     * @inheritdoc
     */
    saveClicked: function(evt) {
        // If name exists but product_template_name is empty,
        // copy name to product_template_name so the field validates
        if (!_.isEmpty(this.model.get(\'name\')) && _.isEmpty(this.model.get(\'product_template_name\'))) {
            this.model.set(\'product_template_name\', this.model.get(\'name\'), {silent: true});
        }

        this._super(\'saveClicked\', [evt]);
    },

    /**
     * Called after the save button is clicked and all the fields have been validated,
     * triggers an event for
     *
     * @inheritdoc
     */
    _save: function() {
        this.view.layout.trigger(\'editablelist:\' + this.view.name + \':saving\', true, this.model.cid);

        if (this.view.model.isNew()) {
            this.view.context.parent.trigger(\'quotes:defaultGroup:save\', _.bind(this._saveRowModel, this));
        } else {
            this._saveRowModel();
        }
    },

    /**
     * Saves the row\'s model
     *
     * @private
     */
    _saveRowModel: function() {
        var self = this;
        var oldModelId = this.model.id || this.model.cid;

        var successCallback = function(model) {
            self.changed = false;
            self.model.modelView = \'list\';
            if (self.view.layout) {
                self.view.layout.trigger(\'editablelist:\' + self.view.name + \':save\', self.model, oldModelId);
                // trigger event for QuotesLineNumHelper plugin to re-number the lines
                self.view.layout.trigger(\'quotes:line_nums:reset\');
            }

            if (model.collection._resavePositions) {
                delete model.collection._resavePositions;
                var bulkSaveRequests = [];
                var bulkUrl;
                var bulkRequest;
                var linkName;
                var itemModelId;
                var collectionId = model.link.bean.id;

                _.each(model.collection.models, function(mdl) {
                    itemModelId = mdl.id;
                    linkName = mdl.module === \'Products\' ? \'products\' : \'product_bundle_notes\';
                    bulkUrl = app.api.buildURL(\'ProductBundles/\' + collectionId + \'/link/\' +
                        linkName + \'/\' + itemModelId);
                    bulkRequest = {
                        url: bulkUrl.substr(4),
                        method: \'PUT\',
                        data: {
                            position: mdl.get(\'position\')
                        }
                    };

                    bulkSaveRequests.push(bulkRequest);
                }, this);

                app.api.call(\'create\', app.api.buildURL(null, \'bulk\'), {
                    requests: bulkSaveRequests
                });
            }
        };
        var options = {
            success: successCallback,
            error: function(error) {
                if (error.status === 409) {
                    app.utils.resolve409Conflict(error, self.model, function(model, isDatabaseData) {
                        if (model) {
                            if (isDatabaseData) {
                                successCallback(model);
                            } else {
                                self._save();
                            }
                        }
                    });
                }
            },
            complete: function() {
                // remove this model from the list if it has been unlinked
                if (self.model.get(\'_unlinked\')) {
                    self.collection.remove(self.model, {silent: true});
                    self.collection.trigger(\'reset\');
                    self.view.render();
                } else {
                    self.setDisabled(false);
                }
            },
            lastModified: self.model.get(\'date_modified\'),
            //Show alerts for this request
            showAlerts: {
                \'process\': true,
                \'success\': {
                    messages: app.lang.get(\'LBL_RECORD_SAVED\', self.module)
                }
            },
            relate: this.model.link ? true : false
        };

        options = _.extend({}, options, this.getCustomSaveOptions(options));
        this.model.save({}, options);
    },

    /**
     * @inheritdoc
     */
    _validationComplete: function(isValid) {
        if (!isValid) {
            this.setDisabled(false);
            return;
        }
        // also need to make sure the model.changed is empty as well
        if (!this.changed && !this.model.changed) {
            this.cancelEdit();
            return;
        }

        this._save();
    }
});
',
    ),
    'templates' => 
    array (
      'edit' => '
<a href="{{#if fullRoute}}#{{fullRoute}}{{else}}{{#if def.route}}#{{buildRoute context=context model=model action=def.route.action}}{{else}}javascript:void(0);{{/if}}{{/if}}"
    class="btn {{name}}"
    data-placement="bottom"
    {{#if def.tooltip}}
        rel="tooltip"
        data-original-title="{{str def.tooltip module}}"
    {{else}}
        title="{{label}}"
    {{/if}}
    {{#if ariaLabel}}aria-label="{{ariaLabel}}"{{/if}}
    role="button" tabindex="{{tabIndex}}" name="{{name}}">{{#if def.icon}}<i class="fa {{def.icon}}"></i> {{/if}}</a>
',
    ),
  ),
  'quote-data-relate' => 
  array (
    'templates' => 
    array (
      'overwrite-confirmation' => '
<span class="information-underline"
      rel="tooltip"
      data-container="body" data-placement="bottom" data-html="true" data-trigger="click"
      title="{{str \'LBL_OVERWRITE_POPULATED_DATA_FROM\'}}: {{before}} <br> {{str \'LBL_OVERWRITE_POPULATED_DATA_TO\'}}: {{after}}">
    {{field_label}}
</span>
',
      'list' => '
<div class="ellipsis_inline" data-placement="bottom" data-container="body"
     title="{{#unless value}}{{#if def.placeholder}}{{str def.placeholder this.model.module}}{{/if}}{{/unless}}{{value}}">
    {{# if href}}
        <a href="{{href}}">{{value}}</a>
    {{else}}
        {{value}}
    {{/if}}
</div>
',
      'detail' => '
{{#if href}}<a class="ellipsis_inline" data-placement="bottom" title="{{value}}" href="{{href}}">{{value}}</a>{{else}}{{value}}{{/if}}
',
      'edit' => '
<input type="hidden" name="{{name}}" class="select2 inherit-width" value="{{formattedIds}}"{{#if def.tabindex}} tabindex="{{def.tabindex}}"{{/if}} data-rname="{{formattedRname}}">
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
      'pill' => '
<div class="ellipsis_inline" title="{{text}}">{{text}}</div>
',
      'noaccess' => '
<span class="label">{{str "LBL_NO_FIELD_ACCESS" this.module}}</span>
',
      'options' => '
{{#each selectOptions}}
    <option value="{{fieldValue this "id"}}" {{#eq @index 0}}selected="selected"{{/eq}}>{{fieldValue this "name"}}</option>
{{/each}}
{{#eq selectOptions.length 0}}
    <option data-empty="true">{{str "MSG_LIST_VIEW_NO_RESULTS_BASIC"}}</option>
{{/eq}}
<option data-searchmore="true" value="{{str "LBL_SEARCH_FOR_MORE"}}" class="more">{{str "LBL_SEARCH_FOR_MORE"}}</option>',
    ),
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.QuoteDataRelateField
 * @alias SUGAR.App.view.fields.BaseProductsQuoteDataRelateField
 * @extends View.Fields.Base.BaseRelateField
 */
({
    extendsFrom: \'BaseRelateField\',

    /**
     * The temporary "(New QLI}" string to add if users type in their own product name
     * @type {string}
     */
    createNewLabel: undefined,

    /**
     * The temporary ID to user for newly created QLI names
     * @type {string}
     */
    newQLIId: undefined,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.createNewLabel = app.lang.get(\'LBL_CREATE_NEW_QLI_IN_DROPDOWN\', \'Products\');
        this.newQLIId = \'newQLIId\';

        this._super(\'initialize\', [options]);
    },

    /**
     * Overriding because getSearchModule needs to return Products for this metadata
     *
     * @inheritdoc
     */
    _getPopulateMetadata: function() {
        return app.metadata.getModule(\'Products\');
    },

    /**
     * Overridden select2 change handler for the custom case of being able to add new unlinked Products
     * @param evt
     * @private
     */
    _onSelect2Change: function(evt) {
        var $select2 = $(evt.target).data(\'select2\');
        var id = evt.val;
        var value = id ? $select2.selection.find(\'span\').text() : $(evt.target).data(\'rname\');
        var collection = $select2.context;
        var model;
        var attributes = {
            id: \'\',
            value: \'\'
        };

        if (value && value.indexOf(this.createNewLabel)) {
            // if value had new QLI label, remove it
            value = value.replace(this.createNewLabel, \'\');
        }

        value = value ? value.trim() : value;

        // default to given id/value or empty strings, cleans up logic significantly
        attributes.id = id || \'\';
        attributes.value = value || \'\';

        if (collection && id) {
            // if we have search results use that to set new values
            model = collection.get(id);
            if (model) {
                attributes.id = model.id;
                attributes.value = model.get(\'name\');
                _.each(model.attributes, function(value, field) {
                    if (app.acl.hasAccessToModel(\'view\', model, field)) {
                        attributes[field] = attributes[field] || model.get(field);
                    }
                });
            }
        } else if (evt.currentTarget.value && value) {
            // if we have previous values keep them
            attributes.id = value;
            attributes.value = evt.currentTarget.value;
        }

        // set the attribute values
        this.setValue(attributes);

        if (id === this.newQLIId) {
            // if this is a new QLI
            this.model.set({
                product_template_id: \'\',
                product_template_name: value,
                name: value
            });
            // update the select2 label
            this.$(this.fieldTag).select2(\'val\', value);
        }

        return;
    },

    /**
     * Extending to add the custom createSearchChoice option
     *
     * @inheritdoc
     */
    _getSelect2Options: function() {
        return _.extend(this._super(\'_getSelect2Options\'), {
            createSearchChoice: _.bind(this._createSearchChoice, this)
        });
    },

    /**
     * Extending to also check models\' product_template_name/name and product_template_id/id
     *
     * @inheritdoc
     */
    format: function(value) {
        var idList;
        value = value || this.model.get(this.name) || this.model.get(\'name\');

        this._super(\'format\', [value]);

        // If value is not set (new row item) then the select2 will show the ID and we dont want that
        if (value) {
            idList = this.model.get(this.def.id_name) || this.model.get(\'id\');
            if (_.isArray(value)) {
                this.formattedIds = idList.join(this._separator);
            } else {
                this.formattedIds = idList;
            }

            if (_.isEmpty(this.formattedIds)) {
                this.formattedIds = value;
            }
        }

        return value;
    },

    /**
     * Overriding if there\'s no product_template_id or name, use the Products module and record ID
     *
     * @inheritdoc
     */
    _buildRoute: function() {
        this.buildRoute(this.model.module, this.model.get(\'id\'));
    },

    /**
     * Overriding as should default to the model\'s ID then if empty go to the link id
     *
     * @inheritdoc
     */
    _getRelateId: function() {
        return this.model.get(this.def.id_name) || this.model.get(\'id\') ;
    },

    /**
     * Add a new search choice for the user\'s text
     *
     * @param {string} term The text the user is searching for
     * @return {{id: (*|string), text: *}}
     * @private
     */
    _createSearchChoice: function(term) {
        return {
            id: this.newQLIId,
            text: term + this.createNewLabel
        };
    }
});
',
    ),
  ),
  'discount' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.DiscountField
 * @alias SUGAR.App.view.fields.BaseProductsDiscountField
 * @extends View.Fields.Base.CurrencyField
 */
({
    extendsFrom: \'CurrencyField\',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super(\'initialize\', [options]);

        var validationTaskName = \'isNumeric_validator_\' + this.cid;

        // removing the validation task if it exists already for this field
        this.model.removeValidationTask(validationTaskName);
        this.model.addValidationTask(validationTaskName, _.bind(this._validateAsNumber, this));
    },

    /**
     * Overriding to add the custom validation handler to the dom change event
     *
     * @inheritdoc
     */
    bindDomChange: function() {
        if (!(this.model instanceof Backbone.Model)) {
            return;
        }

        var $el = this.$(this.fieldTag);
        if ($el.length) {
            $el.on(\'change\', _.bind(function(evt) {
                var val = evt.currentTarget.value;

                this.clearErrorDecoration();
                this.model.set(this.name, this.unformat(val));
                this.model.doValidate(this.name, _.bind(this._validationComplete, this));
            }, this));
        }
    },

    /**
     * Callback for after validation runs.
     * @param {bool} isValid flag determining if the validation is correct
     * @private
     */
    _validationComplete: function(isValid) {
        if (isValid) {
            app.alert.dismiss(\'invalid-data\');
        }
    },

    /**
     * @inheritdoc
     *
     * Listen for the discount_select field to change, when it does, re-render the field
     */
    bindDataChange: function() {
        this._super(\'bindDataChange\');

        // if discount select changes, we need to re-render this field
        this.model.on(\'change:discount_select\', this.render, this);
    },

    /**
     * @inheritdoc
     *
     * Special handling of the templates, if we are displaying it as a percent, then use the _super call,
     * otherwise get the templates from the currency field.
     */
    _loadTemplate: function() {
        if (this.model.get(\'discount_select\') == true) {
            this._super(\'_loadTemplate\');
        } else {
            this.template = app.template.getField(\'currency\', this.action || this.view.action, this.module) ||
                app.template.empty;
            this.tplName = this.action || this.view.action;
        }
    },

    /**
     * @inheritdoc
     *
     * Special handling for the format, if we are in a percent, use the decimal field to handle the percent, otherwise
     * use the format according to the currency field
     */
    format: function(value) {
        if (this.model.get(\'discount_select\') == true) {
            return app.utils.formatNumberLocale(value);
        } else {
            return this._super(\'format\', [value]);
        }
    },

    /**
     * @inheritdoc
     *
     * Special handling for the unformat, if we are in a percent, use the decimal field to handle the percent,
     * otherwise use the format according to the currency field
     */
    unformat: function(value) {
        if (this.model.get(\'discount_select\') == true) {
            var unformattedValue = app.utils.unformatNumberStringLocale(value, true);
            // if unformat failed, return original value
            return _.isFinite(unformattedValue) ? unformattedValue : value;
        } else {
            return this._super(\'unformat\', [value]);
        }
    },

    /**
     * Validate the discount field as a number - do not allow letters
     *
     * @param {Object} fields The list of field names and their definitions.
     * @param {Object} errors The list of field names and their errors.
     * @param {Function} callback Async.js waterfall callback.
     * @private
     */
     _validateAsNumber: function(fields, errors, callback) {
        var value = this.model.get(this.name);

        if (!_.isFinite(value)) {
            errors[this.name] = {\'number\': value};
        }

        callback(null, fields, errors);
    },

    /**
     * Extending to remove the custom validation task for this field
     *
     * @inheritdoc
     * @private
     */
    _dispose: function() {
        var validationTaskName = \'isNumeric_validator_\' + this.cid;
        this.model.removeValidationTask(validationTaskName);

        this._super(\'_dispose\');
    }
})
',
    ),
    'templates' => 
    array (
      'list' => '
<div class="ellipsis_inline" data-placement="bottom" data-original-title="{{value}}%">
    <div class="pull-right">{{value}}%</div>
</div>
',
      'detail' => '
<div class="ellipsis_inline" data-placement="bottom" data-original-title="{{value}}%">
    {{value}}%
</div>
',
      'edit' => '
<input type="text" name="{{name}}" value="{{value}}"{{#if def.len}} maxlength="{{def.len}}"{{/if}}{{#if def.placeholder}} placeholder="{{str def.placeholder this.model.module}}"{{/if}} class="inherit-width">
{{#unless hideHelp}}{{#if def.help}}<p class="help-block">{{str def.help module}}</p>{{/if}}{{/unless}}
',
    ),
  ),
  'quote-data-actionmenu' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * @class View.Fields.Base.Products.QuoteDataActionmenuField
 * @alias SUGAR.App.view.fields.BaseProductsQuoteDataActionmenuField
 * @extends View.Fields.Base.ActionmenuField
 */
({
    /**
     * @inheritdoc
     */
    extendsFrom: \'BaseActionmenuField\',

    /**
     * Skipping ActionmenuField\'s override, just returning this.def.buttons
     *
     * @inheritdoc
     */
    _getChildFieldsMeta: function() {
        return app.utils.deepCopy(this.def.buttons);
    },

    /**
     * Triggers massCollection events to the context.parent
     *
     * @inheritdoc
     */
    toggleSelect: function(checked) {
        var event = !!checked ? \'mass_collection:add\' : \'mass_collection:remove\';
        this.model.selected = !!checked;
        this.context.parent.trigger(event, this.model);
    }
})
',
    ),
    'templates' => 
    array (
      'list' => '
<div class="btn btn-invisible checkall">
    <input data-check="one" type="checkbox" name="check" {{#if model.selected}}checked{{/if}}>
</div>
{{#if dropdownFields}}
    <a track="click:listactiondropdown" class="btn btn-invisible dropdown-toggle"
       data-toggle="dropdown" href="javascript:void(0);" data-placement="bottom"
       rel="tooltip" title="{{str \'LBL_LISTVIEW_ACTIONS\'}}"
       role="button" tabindex="{{tabIndex}}" aria-label="{{str \'LBL_LISTVIEW_ACTIONS\'}}">
        <span class="fa fa-ellipsis-v"></span></a>
    <ul data-menu="dropdown" class="dropdown-menu" role="menu"
        data-row-module="{{../model.module}}" data-row-model-id="{{../model.cid}}"></ul>
{{/if}}
',
    ),
  ),
  '_hash' => '9625e669ca4cf6f54a29fac2dea80fd0',
);

