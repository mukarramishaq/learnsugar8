<?php
$clientCache['Contacts']['base']['menu'] = array (
  'header' => 
  array (
    'meta' => 
    array (
      0 => 
      array (
        'route' => '#Contacts/create',
        'label' => 'LNK_NEW_CONTACT',
        'acl_action' => 'create',
        'acl_module' => 'Contacts',
        'icon' => 'fa-plus',
      ),
      1 => 
      array (
        'route' => '#Contacts/vcard-import',
        'label' => 'LNK_IMPORT_VCARD',
        'acl_action' => 'create',
        'acl_module' => 'Contacts',
        'icon' => 'fa-plus',
      ),
      2 => 
      array (
        'route' => '#Contacts',
        'label' => 'LNK_CONTACT_LIST',
        'acl_action' => 'list',
        'acl_module' => 'Contacts',
        'icon' => 'fa-bars',
      ),
      3 => 
      array (
        'route' => '#Reports?filterModule=Contacts',
        'label' => 'LNK_CONTACT_REPORTS',
        'acl_action' => 'list',
        'acl_module' => 'Reports',
        'icon' => 'fa-bar-chart-o',
      ),
      4 => 
      array (
        'route' => '#bwc/index.php?module=Import&action=Step1&import_module=Contacts&return_module=Contacts&return_action=index',
        'label' => 'LNK_IMPORT_CONTACTS',
        'acl_action' => 'import',
        'acl_module' => 'Contacts',
        'icon' => 'fa-arrow-circle-o-up',
      ),
    ),
  ),
  'quickcreate' => 
  array (
    'meta' => 
    array (
      'layout' => 'create',
      'label' => 'LNK_NEW_CONTACT',
      'visible' => true,
      'order' => 1,
      'icon' => 'fa-plus',
      'related' => 
      array (
        0 => 
        array (
          'module' => 'Accounts',
          'link' => 'contacts',
        ),
        1 => 
        array (
          'module' => 'Opportunities',
          'link' => 'contacts',
        ),
      ),
    ),
  ),
  '_hash' => 'b1049af0e18d37fd30ec8387e47ed7b8',
);

