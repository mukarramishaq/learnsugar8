<?php
$clientCache['Opportunities']['base']['layout'] = array (
  'subpanels' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CALLS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'calls',
          ),
        ),
        1 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_MEETINGS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'meetings',
          ),
        ),
        2 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_TASKS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'tasks',
          ),
        ),
        3 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_NOTES_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'notes',
          ),
        ),
        4 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_QUOTE_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'quotes',
          ),
        ),
        5 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_PRODUCTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'products',
          ),
        ),
        6 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_INVITEE',
          'override_subpanel_list_view' => 'subpanel-for-opportunities',
          'context' => 
          array (
            'link' => 'contacts',
          ),
        ),
        7 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_LEADS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'leads',
          ),
        ),
        8 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_DOCUMENTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'documents',
          ),
        ),
        9 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_CONTRACTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'contracts',
          ),
        ),
        10 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
          'override_subpanel_list_view' => 'subpanel-for-opportunities-archived-emails',
          'context' => 
          array (
            'link' => 'archived_emails',
          ),
        ),
        11 => 
        array (
          'layout' => 'subpanel',
          'label' => 'LBL_PROJECTS_SUBPANEL_TITLE',
          'context' => 
          array (
            'link' => 'project',
          ),
        ),
      ),
    ),
  ),
  'config-drawer-content' => 
  array (
    'templates' => 
    array (
      'help' => '
<div class="help-dashlet-content">
    <div class="help-body">
        {{nl2br body}}
    </div>

    <div class="help-more">
        {{{more_help}}}
    </div>
</div>
',
    ),
  ),
  'create-preview' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'view' => 'product-catalog',
          'context' => 
          array (
            'module' => 'Quotes',
          ),
        ),
      ),
    ),
  ),
  'config-drawer' => 
  array (
    'meta' => 
    array (
      'components' => 
      array (
        0 => 
        array (
          'layout' => 
          array (
            'type' => 'default',
            'name' => 'sidebar',
            'components' => 
            array (
              0 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'main-pane',
                  'css_class' => 'main-pane span8',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'config-header-buttons',
                    ),
                    1 => 
                    array (
                      'layout' => 'config-drawer-content',
                    ),
                  ),
                ),
              ),
              1 => 
              array (
                'layout' => 
                array (
                  'type' => 'base',
                  'name' => 'side-pane',
                  'components' => 
                  array (
                    0 => 
                    array (
                      'view' => 'config-drawer-howto',
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'list-dashboard' => 
  array (
    'meta' => 
    array (
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                0 => 
                array (
                  'view' => 
                  array (
                    'type' => 'sales-pipeline',
                    'label' => 'LBL_DASHLET_PIPLINE_NAME',
                    'visibility' => 'user',
                  ),
                  'width' => 12,
                ),
              ),
            ),
            'width' => 12,
          ),
        ),
      ),
      'name' => 'LBL_OPPORTUNITIES_LIST_DASHBOARD',
    ),
  ),
  'record-dashboard' => 
  array (
    'meta' => 
    array (
      'name' => 'LBL_OPPORTUNITIES_RECORD_DASHBOARD',
      'metadata' => 
      array (
        'components' => 
        array (
          0 => 
          array (
            'rows' => 
            array (
              0 => 
              array (
                'rows' => 
                array (
                  0 => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'forecast-pareto',
                        'label' => 'LBL_DASHLET_FORECASTS_CHART_NAME',
                      ),
                      'context' => 
                      array (
                        'module' => 'Forecasts',
                      ),
                      'width' => 12,
                    ),
                  ),
                  1 => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'planned-activities',
                        'label' => 'LBL_PLANNED_ACTIVITIES_DASHLET',
                      ),
                      'width' => 12,
                    ),
                  ),
                  2 => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'name' => 'active-tasks',
                        'label' => 'LBL_ACTIVE_TASKS_DASHLET',
                      ),
                      'width' => 12,
                    ),
                  ),
                  3 => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'history',
                        'label' => 'LBL_HISTORY_DASHLET',
                      ),
                      'width' => 12,
                    ),
                  ),
                  4 => 
                  array (
                    0 => 
                    array (
                      'view' => 
                      array (
                        'type' => 'attachments',
                        'label' => 'LBL_DASHLET_ATTACHMENTS_NAME',
                        'limit' => '5',
                        'auto_refresh' => '0',
                      ),
                      'context' => 
                      array (
                        'module' => 'Notes',
                        'link' => 'notes',
                      ),
                      'width' => 12,
                    ),
                  ),
                ),
                'width' => 12,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  '_hash' => '4f5747935e5506dbbf10b68128deff2d',
);

