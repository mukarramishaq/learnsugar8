<?php
$clientCache['Opportunities']['base']['field'] = array (
  'rowaction' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    extendsFrom: "RowactionField",
    
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.plugins = _.clone(this.plugins) || [];
        this.plugins.push(\'DisableDelete\');
        this._super("initialize", [options]);
    }
})
',
    ),
  ),
  'radioenum' => 
  array (
    'templates' => 
    array (
      'edit' => '
{{#eachOptions items}}
    <p class="radioenum-inline">
        <label>
            <input type="radio" name="{{../name}}" value="{{key}}"
                {{#if def.tabindex}} tabindex="{{def.tabindex}}"{{/if}}{{#eq key ../value}}checked{{/eq}}>
            {{value}}
        </label>
    </p>
{{/eachOptions}}
{{#unless hideHelp}}
    {{#if def.help}}
        <p class="help-block">{{str def.help module}}</p>
    {{/if}}
{{/unless}}
',
    ),
  ),
  'rowactions' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/*
 * @class View.Fields.Base.Opportunities.RowactionsField
 * @alias SUGAR.App.view.fields.BaseOpportunitiesRowactionsField
 * @extends View.Fields.Base.RowactionsField
 */
({
    extendsFrom: \'RowactionsField\',

    /**
     * Enable or disable caret depending on if there are any enabled actions in the dropdown list
     *
     * @inheritdoc
     * @private
     */
    _updateCaret: function() {
        // Left empty on purpose, the menu should always show
    }
})
',
    ),
  ),
  'actiondropdown' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
/**
 * Create a dropdown button that contains multiple
 * {@link View.Fields.Base.RowactionField} fields.
 *
 * @class View.Fields.Base.Opportunities.ActiondropdownField
 * @alias SUGAR.App.view.fields.BaseOpportunitiesActiondropdownField
 * @extends View.Fields.Base.ActiondropdownField
 */
({
    extendsFrom: \'ActiondropdownField\',

    /**
     * Enable or disable caret depending on if there are any enabled actions in the dropdown list
     *
     * @inheritdoc
     * @private
     */
    _updateCaret: function() {
        // Left empty on purpose, the menu should always show
    }
})
',
    ),
  ),
  'editablelistbutton' => 
  array (
    'controller' => 
    array (
      'base' => '/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    extendsFrom: \'EditablelistbuttonField\',
    /**
     * extend save options
     * @param {Object} options save options.
     * @return {Object} modified success param.
     */
    getCustomSaveOptions: function(options) {
        // make copy of original function we are extending
        var origSuccess = options.success;
        // return extended success function with added alert
        return {
            success: _.bind(function() {
                if (_.isFunction(origSuccess)) {
                    origSuccess.apply(this, arguments);
                }

                if(this.context.parent) {
                    var oppsCfg = app.metadata.getModule(\'Opportunities\', \'config\'),
                        reloadLinks = [\'opportunities\'];
                    if (oppsCfg && oppsCfg.opps_view_by == \'RevenueLineItems\') {
                        reloadLinks.push(\'revenuelineitems\');
                    }

                    this.context.parent.set(\'skipFetch\', false);

                    // reload opportunities subpanel
                    this.context.parent.trigger(\'subpanel:reload\', {links: reloadLinks});
                }
            }, this)
        };
    }
});
',
    ),
  ),
  '_hash' => '1eed3f061b8cfcd034712c4d98e762b8',
);

