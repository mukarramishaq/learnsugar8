<?php
$clientCache['EmbeddedFiles']['base']['field'] = array (
  'file' => 
  array (
    'templates' => 
    array (
      'detail' => '
{{#each value}}
    {{#eq mimeType \'image\'}}
        <a class="ellipsis_inline" data-placement="bottom" title="{{name}}" href="{{url}}"
           data-url="{{url}}" target="_blank">{{name}}</a>
    {{/eq}}
    {{#notEq mimeType \'image\'}}
        <a class="ellipsis_inline" data-placement="bottom" title="{{name}}" href="{{url}}"
           data-url="{{url}}" data-action="download">{{name}}</a>
    {{/notEq}}
{{/each}}
',
      'list' => '
{{#if value}}
    {{#if value.[0]}}
        {{#with value.[0]}}
            {{#notEq mimeType \'image\'}}
                <a class="ellipsis_inline" data-placement="bottom" title="{{name}}"
                   href="{{url}}" data-url="{{url}}" data-action="download">{{name}}</a>
            {{/notEq}}
            {{#eq mimeType \'image\'}}
                <a class="ellipsis_inline" data-placement="bottom" title="{{name}}"
                   href="{{url}}" target="_blank">{{name}}</a>
            {{/eq}}
        {{/with}}
    {{/if}}
{{/if}}
',
    ),
  ),
  '_hash' => '6de3b4d7d4c008bd87a45713f7059e27',
);

