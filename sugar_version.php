<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */



$sugar_version      = '8.0.1';
$sugar_db_version   = '8.0.1';
$sugar_flavor       = 'PRO';
$sugar_build        = '239';
$sugar_timestamp    = '2018-06-29 11:01pm';

?>
