({
	events:{'click #validate_button':'validateLicense','click #continue_button':'continuePage'},
	className: 'customMainPane',
	license_key:null,
	update: false, //true if previously license is saved
	continueURL: null,
	user_count: null,
	licensed_user_count: null,

	initialize: function(options){
		
			this._super('initialize', [options]);
	},

    loadData: function (options) {
    	var self = this;
    		this.continueURL="#rt_maps/layout/userconfiguration";
        //populate your data
		this.LBL_STEPS_TO_LOCATE_KEY_TITLE=app.lang.get('LBL_STEPS_TO_LOCATE_KEY_TITLE',this.module);
		this.LBL_STEPS_TO_LOCATE_KEY1=app.lang.get('LBL_STEPS_TO_LOCATE_KEY1',this.module);
		this.LBL_STEPS_TO_LOCATE_KEY2=app.lang.get('LBL_STEPS_TO_LOCATE_KEY2',this.module);
		this.LBL_STEPS_TO_LOCATE_KEY3=app.lang.get('LBL_STEPS_TO_LOCATE_KEY3',this.module);
		this.LBL_STEPS_TO_LOCATE_KEY4=app.lang.get('LBL_STEPS_TO_LOCATE_KEY4',this.module);
		this.LBL_STEPS_TO_LOCATE_KEY5=app.lang.get('LBL_STEPS_TO_LOCATE_KEY5',this.module);
		this.LBL_LICENSE_KEY=app.lang.get('LBL_LICENSE_KEY',this.module);
		this.LBL_VALIDATE_LABEL=app.lang.get('LBL_VALIDATE_LABEL',this.module);
		this.LBL_CONTINUE_LABEL=app.lang.get('LBL_CONTINUE_LABEL',this.module);
		this.LBL_BOOST_LABEL=app.lang.get('LBL_BOOST_LABEL',this.module);
		this.LBL_CURRENT_USERS=app.lang.get('LBL_CURRENT_USERS',this.module);
		this.LBL_LICENSED_USERS=app.lang.get('LBL_LICENSED_USERS',this.module);
		this.LBL_SALESMAP_ENABLED_USERS=app.lang.get('LBL_SALESMAP_ENABLED_USERS',this.module);
		this.title = app.lang.get('LBL_SALESMAP_LICENSE_CONFIGURATION',this.module);
		
		//check if previously call settings are saved with id=1, before rendering the view
	var previous = SUGAR.App.data.createBean("rt_maps",{id:"1"});
	previous.fetch({
			success:function(){
			
			self.update = true; //call settings found previously, now they need to be updated
			self.license_key = previous["attributes"]["license_key"];
			
			self.render();
			app.alert.dismissAll();
			},
			error:function(){
			self.update = false; // no call settings found
			self.render();
			app.alert.dismissAll();
			}});
    },

	validateLicense: function () {
		
			var self = this;
			//var salesmap_key = '6822a0f4daea20bb3d8b9e7f86d8ad25';
			//var user_key = $('input[name=license_key]').val();
			this.license_key=$('input[name=license_key]').val();
			if(!this.license_key){
				return;
			}
			var licenseURL = app.api.buildURL('salesMapLicense/validate/'+this.license_key, null, null, {
			oauth_token: app.api.getOAuthToken()
		});
		app.alert.show('salesMap_config', {level: 'process', title: 'Validating'});
		app.api.call('GET', licenseURL, null, {
			success: _.bind(function (result) {
				this.validateLicenseSuccess(result);
			}, this),

			error: _.bind(function (e) {
				this.LicenseError(e);
			}, this)
		});
		//save License Key for the first time	
				if (self.update == false)
				{
				var rt_maps = SUGAR.App.data.createBean("rt_maps");
				rt_maps.isNew = function ()
				{
				return true;
				};

				rt_maps.save({id:"1",name:"Sales Map",license_key:this.license_key},{
					success:function(){
					//window.location.reload(true);
					},error:function(){
		
						SUGAR.App.alert.show("server-error", {
										level: 'error',
										messages: 'ERR_GENERIC_SERVER_ERROR',
										autoClose: true
									});
					}
					});
				}

				//needs to update license key
				else if (self.update == true)
				{
					var rt_maps_prev = SUGAR.App.data.createBean("rt_maps",{id:"1"});
					rt_maps_prev.save({id:"1",license_key:this.license_key},{
					success:function(){
					},error:function(){
		
						SUGAR.App.alert.show("server-error", {
										level: 'error',
										messages: 'ERR_GENERIC_SERVER_ERROR',
										autoClose: true
									});
					}
					});
				}
			/*
			$.ajax('https://www.sugaroutfitters.com/api/v1/key/validate', {
					type: 'GET',
					dataType: 'jsonp',
					crossDomain: true,
					data: { format: 'jsonp',public_key: salesmap_key,key: user_key},
					timeout: 5000 //work around for jsonp not returning errors
				
				}).success(function(response) 
				{
					 msg = {autoClose: false, level: 'success',messages:'Validated Successfully'};
					app.alert.show('salesMap_config', msg);
					
				}).error(function() 
				{
					
					 msg = {autoClose: false, level: 'error',messages:'Bad License Key!'};
					app.alert.show('salesMap_config', msg);
				});	

				//save License Key for the first time	
				if (self.update == false)
				{
				var rt_maps = SUGAR.App.data.createBean("rt_maps");
				rt_maps.isNew = function ()
				{
				return true;
				};

				rt_maps.save({id:"1",name:"Sales Map",license_key:user_key},{
					success:function(){
					window.location.reload(true);
					},error:function(){
		
						SUGAR.App.alert.show("server-error", {
										level: 'error',
										messages: 'ERR_GENERIC_SERVER_ERROR',
										autoClose: true
									});
					}
					});
				}

				//needs to update license key
				else if (self.update == true)
				{
					var rt_maps_prev = SUGAR.App.data.createBean("rt_maps",{id:"1"});
					rt_maps_prev.save({id:"1",license_key:user_key},{
					success:function(){
					},error:function(){
		
						SUGAR.App.alert.show("server-error", {
										level: 'error',
										messages: 'ERR_GENERIC_SERVER_ERROR',
										autoClose: true
									});
					}
					});
				}
				*/
	},
	validateLicenseSuccess: function(response){
		var msg={};
		app.alert.dismiss('salesMap_config');
		if(response){
			if(response.data){
				if(response.data.validated && response.data.validated==true ){
					//validation true
					if(response.data.validated_users && response.data.validated_users==true ){
						this.user_count=response.data.user_count;
						this.licensed_user_count=response.data.licensed_user_count;
						app.alert.show('salesMap_config', {autoClose: true, level: 'success', messages: 'Validation'});
					}else{
						this.user_count=response.data.user_count;
						this.licensed_user_count=response.data.licensed_user_count;
						app.alert.show("salesMap_config", {
							level: "warning",
							messages: 'Boost user count',
							autoClose: false
						});
						//$("#boost_button").removeClass('hide');
					}
					$("#validate_button").addClass('hide');
					$("#continue_button").removeClass('hide');
					$("#salesmap_users").html(this.LBL_SALESMAP_ENABLED_USERS +': '+this.user_count);
					$("#licensed_users").html(this.LBL_LICENSED_USERS +': '+this.licensed_user_count);
					$("#users_count").removeClass('hide');
				}else{
					msg= {autoClose: false, level: 'error',messages:response.data};
					app.alert.show('salesMap_config', msg);
				}
			}else{
				msg = {autoClose: false, level: 'error',messages:'Unkown error'};
				app.alert.show('salesMap_config', msg);
			}
		}else{
			msg = {autoClose: false, level: 'error',messages: 'No response received from server.'};
			app.alert.show('salesMap_config', msg);
		}
	},
	continuePage: function(){
		app.router.navigate(this.continueURL,{trigger: true});
	},
	LicenseError: function(error){
		var msg = {autoClose: false, level: 'error'};
		if (error && _.isString(error.message)) {
			msg.messages = error.message;
		}
		if (error.status == 412 && !error.request.metadataRetry){
			msg.messages='If this page does not reload automatically, please try to reload manually';
		}
		app.alert.dismissAll();
		app.alert.show('salesMap_config', msg);
		app.logger.error('Failed: ' + error);
			if (typeof error.status != 'undefined')
			{
				if (error.status == 400)
					{
						if (typeof error.responseText != 'undefined')
							{
								var msg = {autoClose: false, level: 'error'};
								msg.messages = error.responseText;
								app.alert.show('salesmap_config', msg);
							}
					}
			}
	},

	
})
