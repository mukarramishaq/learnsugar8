<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID:',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Kasutaja',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev:',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja Id',
  'LBL_MODIFIED_NAME' => 'Muutja nime järgi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja Id',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Looja',
  'LBL_MODIFIED_USER' => 'Muutja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap Loend',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'Minu RT SalesMap',
  'LNK_NEW_RECORD' => 'Loo RT SalesMap',
  'LNK_LIST' => 'Vaade RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevused',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'Uus RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);