<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team ID',
  'LBL_ASSIGNED_TO_ID' => 'Toegewezen aan ID',
  'LBL_ASSIGNED_TO_NAME' => 'Toegewezen aan',
  'LBL_CREATED' => 'Gemaakt door',
  'LBL_CREATED_ID' => 'Gemaakt door ID',
  'LBL_CREATED_USER' => 'Gemaakt door Gebruiker',
  'LBL_DATE_ENTERED' => 'Datum ingevoerd',
  'LBL_DATE_MODIFIED' => 'Datum gewijzigd',
  'LBL_DELETED' => 'Verwijderd',
  'LBL_DESCRIPTION' => 'Beschrijving',
  'LBL_DOC_OWNER' => 'Eigenaar van document',
  'LBL_EDIT_BUTTON' => 'Wijzig',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'Naam',
  'LBL_MODIFIED' => 'Gewijzigd door',
  'LBL_MODIFIED_ID' => 'Gewijzigd door ID',
  'LBL_MODIFIED_NAME' => 'Gewijzigd door Naam',
  'LBL_MODIFIED_USER' => 'Gewijzigd door Gebruiker',
  'LBL_NAME' => 'Naam',
  'LBL_REMOVE' => 'Verwijder',
  'LBL_USER_FAVORITES' => 'Gebruikers die favoriet gemaakt hebben',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap List',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'My RT SalesMap',
  'LNK_NEW_RECORD' => 'Create RT SalesMap',
  'LNK_LIST' => 'View RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Search RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'New RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);