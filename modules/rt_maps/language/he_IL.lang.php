<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'צוותים',
  'LBL_TEAMS' => 'צוותים',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'משתמש שהוקצה Id',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_DELETED' => 'נמחק',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DOC_OWNER' => 'בעלים של המסמך',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_ID' => 'ID',
  'LBL_LIST_NAME' => 'שם',
  'LBL_MODIFIED' => 'שונה על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_NAME' => 'שם',
  'LBL_REMOVE' => 'הסר',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_LIST_FORM_TITLE' => 'RT SalesMap List',
  'LBL_MODULE_NAME' => 'RT SalesMap',
  'LBL_MODULE_TITLE' => 'RT SalesMap',
  'LBL_MODULE_NAME_SINGULAR' => 'RT SalesMap',
  'LBL_HOMEPAGE_TITLE' => 'My RT SalesMap',
  'LNK_NEW_RECORD' => 'Create RT SalesMap',
  'LNK_LIST' => 'View RT SalesMap',
  'LNK_IMPORT_RT_MAPS' => 'Import RT SalesMap',
  'LBL_SEARCH_FORM_TITLE' => 'Search RT SalesMap',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_RT_MAPS_SUBPANEL_TITLE' => 'RT SalesMap',
  'LBL_NEW_FORM_TITLE' => 'New RT SalesMap',
  'LNK_IMPORT_VCARD' => 'Import RT SalesMap vCard',
  'LBL_IMPORT' => 'Import RT SalesMap',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new RT SalesMap record by importing a vCard from your file system.',
);