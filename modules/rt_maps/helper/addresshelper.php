<?php

/**
 * Purpsoe of this class to minimize the code of googlemaps file
 * to handle data return by mysql tables
 */
class AddressHelper {

    /**
     * purpose of this function to 
     * retrieve the address and other 
     * information to collect the data for schedular jobs

      @parameters
      $table
     */
    public function getScheduleResultSet($module) {
        global $db;

        //$query = "SELECT * FROM ".$table." WHERE deleted=0 AND (maps_lat IS NULL OR TRIM(maps_lat) ='')  AND (maps_long IS NULL OR TRIM(maps_long) ='')";
        $q = new SugarQuery();
        $q->from(BeanFactory::getBean($module), array('team_security' => false));
        $q->select(array('*'));
        $q->where()->queryOr()->isNull("maps_lat")->equals('maps_lat', '');
        $q->where()->queryOr()->isNull("maps_long")->equals('maps_long', '');
        $res = $q->limit(800);
        $res = $q->execute();
        return $res;
    }

    public function getRecordsResultSet($table, $limit) {
        global $db;
        if ($db->dbType == 'mysql' || strtolower($db->dbType) == 'mysql') {
            if ($table == 'accounts') {
                $query = "SELECT * FROM " . $table . " WHERE deleted=0 AND (non_geo_coded_address != md5(CONCAT(billing_address_street, ',', billing_address_city, ',', billing_address_state, ',', billing_address_postalcode, ',', billing_address_country)) OR non_geo_coded_address IS NULL) LIMIT $limit";
            } else {
                $query = "SELECT * FROM " . $table . " WHERE deleted=0 AND (non_geo_coded_address != md5(CONCAT(primary_address_street, ',', primary_address_city, ',', primary_address_state, ',', primary_address_postalcode, ',', primary_address_country)) OR non_geo_coded_address IS NULL)  LIMIT $limit";
            }
        } else if ($db->dbType == 'mssql' || strtolower($db->dbType) == 'mssql') {
            if ($table == 'accounts') {
                $query = "SELECT TOP " . $limit . " * FROM " . $table . " WHERE deleted=0 AND (CONCAT('Ox',non_geo_coded_address) != HASHBYTES('MD5', CONCAT(billing_address_street, ',', billing_address_city, ',', billing_address_state, ',', billing_address_postalcode, ',', billing_address_country)) OR non_geo_coded_address IS NULL)";
            } else {
                $query = "SELECT TOP " . $limit . " * FROM " . $table . " WHERE deleted=0 AND (CONCAT('Ox',non_geo_coded_address) != HASHBYTES('MD5', CONCAT(primary_address_street, ',', primary_address_city, ',', primary_address_state, ',', primary_address_postalcode, ',', primary_address_country)) OR non_geo_coded_address IS NULL)";
            }
        }
        $res = $db->query($query);

        return $res;
    }

    /* niki start */

    /**
     * 	This will process the string and remove known special characters and extra spaces present in string
     * 	and replace with empty string 
     * 	@paeram : $str => input string
     * 	@return val : $processed_string => filtered string
     */
    public function process_address_value($str) {
        $strippable_chars = array(',,', '\n', '\t', '\r', '$', '&', '#', '@', '(', ')', '*', '%', '!', '^', '~', '<', '>', '?', '{', '}', '[', ']', '/', '|', '=', "'", '"', '""', "''");
        //$str = ln2br($str);

        $str = html_entity_decode($str);
        $processed_string = str_replace($strippable_chars, ' ', $str);

        while (strstr($processed_string, '  ') != false) {
            $processed_string = str_replace('  ', ' ', $processed_string); // removing double or more spaces 
        }

        $processed_string = trim($processed_string); // trim spaces etc from both sides
        $processed_string = preg_replace("/[\n\r]/", "", $processed_string);

        return $processed_string;
    }

    /**
     * 	This will return the total customers info(geo-coded and non-geo-coded)
     */
    function get_customers_info() {
        $customers = ''; //  this will contain the total info of the total customers
        $modules_cust = array('Accounts', 'Leads', 'Contacts');
        $modules_cust_lower = array('accounts', 'leads', 'contacts');
        for ($i = 0; $i < count($modules_cust); $i++) {
            $cust_info = $this->get_cust_info($modules_cust[$i]);

            $customers['total_' . $modules_cust_lower[$i]] = $cust_info['total_cust'];
            $customers['total_' . $modules_cust_lower[$i] . '_non_geo'] = $cust_info['total_non_geo'];
        }

        return $customers;
    }

    /**
     * 	This will return single customer info
     */
    function get_cust_info($customer) {
        $q = new SugarQuery();
        $q->from(BeanFactory::getBean($customer), array('team_security' => false));
        $customertolower = strtolower($customer);
        $q->select->fieldRaw("COUNT(" . $customertolower . ".id)", "total_cust");
        $total_cust = $q->execute();

        global $db;
        /* calculating total customers */
        //$sql_total = "SELECT COUNT(id) as total_cust FROM ".$customer." WHERE deleted=0";
        //$sql_res_total = $db->query($sql_total);
        //$total_cust = $db->fetchByAssoc($sql_res_total);			
        $customers['total_cust'] = $total_cust[0]['total_cust'];

        /* calculating total non-geo coded customers */
        //$where = " (".$customer.".maps_lat IS NULL OR TRIM(".$customer.".maps_lat) ='')  AND (".$customer.".maps_long IS NULL OR TRIM(".$customer.".maps_long) ='') AND ".$customer.".deleted=0";
        //$sql = "SELECT COUNT(id) as total_non_geo FROM ".$customer." WHERE ".$where;

        $q = new SugarQuery();
        $q->from(BeanFactory::getBean($customer), array('team_security' => false));
        $q->select->fieldRaw("COUNT(" . $customertolower . ".id)", "total_non_geo");
        //$q->select(array('*'));
        $q->where()->queryOr()->isNull("maps_lat")->equals('maps_lat', '');
        $q->where()->queryOr()->isNull("maps_long")->equals('maps_long', '');
        $total = $q->execute();

        //$sql_res = $db->query($sql);
        //$total = $db->fetchByAssoc($sql_res);
        $customers['total_non_geo'] = $total[0]['total_non_geo'];
        return $customers;
    }

}

?>
