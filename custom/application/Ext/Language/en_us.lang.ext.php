<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/application/Ext/Language/en_us.RTMaps.php

/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['rt_maps'] = 'RT SalesMap';
$app_list_strings['moduleListSingular']['rt_maps'] = 'RT SalesMap';

$app_list_strings ['maps_module_list_dom'] = array (
  ''=>'',
  'All'=>'All',
  'Leads' => 'Leads',
  'Accounts' => 'Accounts',
  'Contacts' => 'Contacts',);

?>
