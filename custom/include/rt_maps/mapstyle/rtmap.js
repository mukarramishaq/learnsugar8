
$('document').ready(function () {

    $("#scroller").click(function () {
        $("#result-panel").hide();
        $("#scroller").hide();
    });

    $("#loadmap").click();
});

var mod_list = new Array();
var modulelist = new Array();
var module_latlng;
var full_address;
var beaches = [];
var r = 0;
var t = 0;
var map;
var markerchoice = "";
var pagination_info = new Array();

var selValue_lead = new Array();
var selValue_contact = new Array();
var selValue_account = new Array();
var lead_status = new Array();
var contact_status = new Array();
var account_status = new Array();

//Options and their color
var account_option_color = new Array();
var account_color_values = new Array();
var lead_option_color = new Array();
var lead_color_values = new Array();
var contact_option_color = new Array();
var contact_color_values = new Array();

// Lat,long, postal address, radius and radius unit value
lat_center = "";
lng_center = "";
var postal = "";
var radius = "";
var radius_unit_val = "";
var is_grouped_query = false; // this will check whether the request came from double click event listner or from direction finder
var source_add = ''; //  this will contaitn the source address against event triggering of the double click of mouse
var destination_add = ''; //  this will contaitn the source address against event triggering of the double click of mouse
special_chars = new Array('at;', 'lt;', 'quot;', '039;', 'quot;039;', 'gt;', ';', ':', '/', '_', '  ', ',,'); // this contains the special characters to be replaced by space in address field
var totalRecords = null; //it will contain the total number of records returned from api call
var contacts = null; //it will contain all contacts returned using sugar filter api
var leads = null;// it will contain all leads returned sugar filter api
var accounts = null;// it will contain all accounts returned sugar filter api
var allRecords = new Array();//it will contain all records found after filtering

//Fixes issue 61 - Rehman Ahmad - Start
var directionsDisplay = new Array();
var directionsDisplayCount = -1;
//Fixes issue 61 - Rehman Ahmad - End

function load_cstm() {

    google.maps.visualRefresh = true; // this is to add visual Refresh feature in maps

    geocoder = new google.maps.Geocoder();
    setSelectedValuesCustom();

    // For loading alert
    $(".alert").css("display", "block");
    $(".containerDiv").css("opacity", "0.35");

    $("#loadmap").css("display", "none");
    $("#clickMap").css("display", "none");

    showmodulemarker();
    google.maps.event.addListener(map, 'center_changed', function () {
        checkBounds();
    });
}
function setSelectedValuesCustom() {
    selValue_lead = new Array();
    selValue_contact = new Array();
    selValue_account = new Array();
    var elements = $('[data-filter=row]');
    var leadCounter = 0;
    var contactCounter = 0;
    var accountCounter = 0;
    for (var i = 0; i < elements.length; i++) {
//var counter=0;

        if ($(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val') == 'lead_list') {
            if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$in') {
                var values = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                for (var j = 0; j < values.length; j++) {
                    selValue_lead[leadCounter] = values[j];
                    leadCounter++;
                }
            }
            else if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$not_in') {
                var field = $(elements[i]).find('[data-filter=field] input[type=hidden]').select2('val');
                var module = $(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val');
                var relatedModule = null;
                if (module == "account_list")
                {
                    relatedModule = "Accounts";
                }
                else if (module == "contact_list")
                {
                    relatedModule = "Contacts";
                }
                else if (module == "lead_list")
                {
                    relatedModule = "Leads";
                }
                var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
                var valueOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
                var notvalues = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                var enabledValue = Array();
                var enabledCount = 0;
                jQuery.each(valueOptions, function (category, items) {
                    var found = false;
                    for (var j = 0; j < notvalues.length; j++) {
                        if (category == notvalues[j])
                            found = true;
                    }
                    if (found == false && category != "") {
                        enabledValue[enabledCount] = category;
                        enabledCount++;
                    }
                });
                for (var j = 0; j < enabledValue.length; j++) {
                    selValue_lead[leadCounter] = enabledValue[j];
                    leadCounter++;
                }
                ;
            }

        }
        if ($(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val') == 'contact_list') {
            if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$in') {
                var values = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                for (var j = 0; j < values.length; j++) {
                    selValue_contact[contactCounter] = values[j];
                    contactCounter++;
                }
            }
            else if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$not_in') {
                var field = $(elements[i]).find('[data-filter=field] input[type=hidden]').select2('val');
                var module = $(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val');
                var relatedModule = null;
                if (module == "account_list")
                {
                    relatedModule = "Accounts";
                }
                else if (module == "contact_list")
                {
                    relatedModule = "Contacts";
                }
                else if (module == "lead_list")
                {
                    relatedModule = "Leads";
                }
                var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
                var valueOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
                var notvalues = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                var enabledValue = Array();
                var enabledCount = 0;
                jQuery.each(valueOptions, function (category, items) {
                    var found = false;
                    for (var j = 0; j < notvalues.length; j++) {
                        if (category == notvalues[j])
                            found = true;
                    }
                    if (found == false && category != "") {
                        enabledValue[enabledCount] = category;
                        enabledCount++;
                    }
                });
                for (var j = 0; j < enabledValue.length; j++) {
                    selValue_contact[contactCounter] = enabledValue[j];
                    contactCounter++;
                }
            }

        }
        if ($(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val') == 'account_list') {
            if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$in') {
                var values = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                for (var j = 0; j < values.length; j++) {
                    selValue_account[accountCounter] = values[j];
                    accountCounter++;
                }
            }
            else if ($(elements[i]).find('[data-filter=type] input[type=hidden]').select2('val') == '$not_in') {
                var field = $(elements[i]).find('[data-filter=field] input[type=hidden]').select2('val');
                var module = $(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val');
                var relatedModule = null;
                if (module == "account_list")
                {
                    relatedModule = "Accounts";
                }
                else if (module == "contact_list")
                {
                    relatedModule = "Contacts";
                }
                else if (module == "lead_list")
                {
                    relatedModule = "Leads";
                }
                var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
                var valueOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
                var notvalues = $(elements[i]).find('[data-filter=value] input[type=hidden]').select2('val');
                var enabledValue = Array();
                var enabledCount = 0;
                jQuery.each(valueOptions, function (category, items) {
                    var found = false;
                    for (var j = 0; j < notvalues.length; j++) {
                        if (category == notvalues[j])
                            found = true;
                    }
                    if (found == false && category != "") {
                        enabledValue[enabledCount] = category;
                        enabledCount++;
                    }
                });
                for (var j = 0; j < enabledValue.length; j++) {
                    selValue_account[accountCounter] = enabledValue[j];
                    accountCounter++;
                }
            }

        }


    }
}
function getModulesData(pageNo) {
    window.module_latlng = "";
    contacts = null; //it will contain all contacts returned using sugar filter api
    leads = null;// it will contain all leads returned sugar filter api
    accounts = null;// it will contain all accounts returned sugar filter api
    allRecords = new Array();
    filterStat = {'Contacts': new Array(), 'Accounts': new Array(), 'Leads': new Array()};
    r = 0;

    mod_list_count = 0;
    if (selValue_lead.length > 0) {
        mod_list[mod_list_count] = 'leads';
        mod_list_count++;
    }
    else {
        mod_list[mod_list_count] = '';
        mod_list_count++;
    }
    if (selValue_contact.length > 0) {
        mod_list[mod_list_count] = 'contacts';
        mod_list_count++;
    }
    else {
        mod_list[mod_list_count] = '';
        mod_list_count++;
    }
    if (selValue_account.length > 0) {
        mod_list[mod_list_count] = 'accounts';
        mod_list_count++;
    }
    else {
        mod_list[mod_list_count] = '';
        mod_list_count++;
    }

//New code for getting filtered records using sugar filter api
    self1.buildFilterDefCustom(true);

    //if accoutns filter is not defined, we dont need to fetch any records so in filterDef define date_entered null so that no
    //record is fetched
    var arr3 = [];
    arr3[0] = null;
    arr3[1] = "";
    if (self1.accountsFilterDef.length == 0)
    {
        self1.accountsFilterDef = [{date_entered: null}];
    }
    else
    {
        self1.accountsFilterDef.push({maps_lat: {"$not_in": arr3}})
    }
    //if leads filter is not defined, we dont need to fetch any records so in filterDef define date_entered null so that no
    //record is fetched
    if (self1.leadsFilterDef.length == 0)
    {
        self1.leadsFilterDef = [{date_entered: null}];

    }
    else
    {
        self1.leadsFilterDef.push({maps_lat: {"$not_in": arr3}})
    }
//if contacts filter is not defined, we dont need to fetch any records so in filterDef define date_entered null so that no
    //record is fetched
    if (self1.contactsFilterDef.length == 0)
    {
        self1.contactsFilterDef = [{date_entered: null}];
    }
    else
    {
        self1.contactsFilterDef.push({maps_lat: {"$not_in": arr3}})
    }

    var count = 0;
    //fetch accounts , then leads, then contacts
    var accountsCollection = SUGAR.App.data.createBeanCollection('Accounts');
    console.log('accountsFilterDef');
    console.log(self1.accountsFilterDef);
    accountsCollection.filterDef = self1.accountsFilterDef;
    accountsCollection.fetch({
        relate: false,
        limit: 1001,
        //limit: -1,
        fields: ['id', 'maps_lat', 'maps_long', 'name', 'billing_address_street', 'billing_address_city', 'billing_address_state', 'billing_address_postalcode', 'billing_address_country'],
        success: function (data) {
            accounts = data;
        },
        complete: function ()
        {
            //fetch all leads
            var leadsCollection = SUGAR.App.data.createBeanCollection('Leads');
            leadsCollection.filterDef = self1.leadsFilterDef;
            leadsCollection.fetch({
                relate: false,
                fields: ['id', 'maps_lat', 'maps_long', 'name', 'primary_address_street', 'primary_address_city', 'primary_address_state', 'primary_address_postalcode', 'primary_address_country'],
                limit: 1001,
                //limit: -1,
                success: function (data) {
                    leads = data;
                },
                complete: function () {
                    //fetch all contacts
                    var contactsCollection = SUGAR.App.data.createBeanCollection('Contacts');
                    contactsCollection.filterDef = self1.contactsFilterDef;
                    contactsCollection.fetch({
                        relate: false,
                        limit: 1001,
                        //limit: -1,
                        fields: ['id', 'maps_lat', 'maps_long', 'name', 'primary_address_street', 'primary_address_city', 'primary_address_state', 'primary_address_postalcode', 'primary_address_country'],
                        success: function (data) {
                            contacts = data;
                            if (!_.isNull(contacts)) {
                                filterStat.Contacts = new Array();
                                for (var i = 0; i < contacts["models"].length; i++)
                                {

                                    if (contacts["models"][i]["attributes"]["maps_lat"] != null && contacts["models"][i]["attributes"]["maps_lat"] != "" && contacts["models"][i]["attributes"]["maps_long"] != null && contacts["models"][i]["attributes"]["maps_long"] != "")
                                    {
                                        allRecords[count] = contacts["models"][i]["attributes"];
                                        filterStat.Contacts.push(i);
                                        count++;
                                    }
                                }
                            }
                            if (!_.isNull(leads)) {
                                filterStat.Leads = new Array();
                                for (var i = 0; i < leads["models"].length; i++)
                                {
                                    if (leads["models"][i]["attributes"]["maps_lat"] != null && leads["models"][i]["attributes"]["maps_lat"] != "" && leads["models"][i]["attributes"]["maps_long"] != null && leads["models"][i]["attributes"]["maps_long"] != "")
                                    {
                                        allRecords[count] = leads["models"][i]["attributes"];
                                        filterStat.Leads.push(i);
                                        count++;
                                    }
                                }
                            }
                            if (!_.isNull(accounts)) {
                                filterStat.Accounts = new Array();
                                console.log("length: " + accounts["models"].length);
                                for (var i = 0; i < accounts["models"].length; i++)
                                {
                                    if (accounts["models"][i]["attributes"]["maps_lat"] != null && accounts["models"][i]["attributes"]["maps_lat"] != "" && accounts["models"][i]["attributes"]["maps_long"] != null && accounts["models"][i]["attributes"]["maps_long"] != "")
                                    {
                                          allRecords[count] = accounts["models"][i]["attributes"];
                                          filterStat.Accounts.push(i);
                                      count++;
                                    }
                                }
                            }

                            if (allRecords.length > 1000)
                            {
                              //Fixes 40 - Start
                              // console.log("Records Length: " + allRecords.length);
                              //   allRecords = new Array();
                              //   $('.load').show();
                                //alert("Your records are more than 8, please refine your search");
                             //Fixes 40 - End
                            }
                            else
                            {
                                $('.load').hide();
                            }

                            updateValuesCustom();
                            setTimeout(function () {
                                showStatistics(leads, contacts, accounts);
                            }, 3000);
                        }});
                }
            });
        }
    });
}

function updateValuesCustom() {

    var selected_module = "All";
    markerchoice = selected_module.toLowerCase();

    // For showing hiding the accounts, leads and contact drop downs
    if (selected_module == "All") {
        initializeCustom();
    }
    // For loading alert
    $(".alert").css("display", "none");
    $(".containerDiv").css("opacity", "1");

}

/**
 * Initialize function that actually starts the google map
 */
function initializeCustom() {
    var geocoder = new google.maps.Geocoder();

    //provide the services to display the directions
    directionsService = new google.maps.DirectionsService();


    pointer_index = 0;
    l = 0;
    mapCntr = new google.maps.LatLng(52.5233, 13.4127);
    if (typeof allRecords[0] != "undefined") {
        mapCntr = new google.maps.LatLng(allRecords[0].maps_lat, allRecords[0].maps_long);
    }
    var mapOptions = {
        zoom: 5,
        center: mapCntr,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

    setMarkersCustom(map, beaches);


    if (lat_center != "" && lng_center != "") {
        var image_postalcode = new google.maps.MarkerImage('http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-pushpin.png', new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));
        var shape = {
            coord: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat_center, lng_center),
            map: map,
            icon: image_postalcode,
            shape: shape,
        });

        if (radius_unit_val == "M") {
            var circle_radius = parseFloat(radius) * 1609;
        }
        else {
            var circle_radius = parseFloat(radius) * 1000;
        }
        var circle = new google.maps.Circle({
            map: map,
            radius: circle_radius, //radius in meters
            strokeColor: "#00AAFF",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#00AAFF",
            fillOpacity: 0.35,
            center: new google.maps.LatLng(lat_center, lng_center),
            zIndex: 99999,
        });

        map.fitBounds(circle.getBounds());
    }
}

function setMarkersCustom(map, locations) {

    ml = 0;
    var markerslatlng = new Array();

    // Add markers to the map
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.
    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    //https://developers.google.com/chart/image/docs/gallery/dynamic_icons
    var image = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=+L|ff0000|',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(37, 32),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            new google.maps.Point(0, 32));
    var image2 = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|A|56A5EC|',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(37, 32),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            new google.maps.Point(0, 32));
    // var shadow = new google.maps.MarkerImage('images/beachflag_shadow.png',
    var shadow_c = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_shadow',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(37, 32),
            new google.maps.Point(0, 0),
            new google.maps.Point(0, 32));


    var shadow_l = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_shadow', new google.maps.Size(40, 37), new google.maps.Point(0, 0), new google.maps.Point(1, 36));
    var shadow_a = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_xpin_letter_withshadow', new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));

    // Shapes define the clickable region of the icon.
    // The type defines an HTML &lt;area&gt; element 'poly' which
    // traces out a polygon as a series of X,Y points. The final
    // coordinate closes the poly by connecting to the first
    // coordinate.


    var latlngbounds = new google.maps.LatLngBounds();
    var shape = {
        coord: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
    };

    var infowindow = new google.maps.InfoWindow(), marker, i;

    for (var j = 0; j < allRecords.length; j++)
    {
        showModmarkersCustom(allRecords[j], j, map, shape, shadow_a, infowindow, allRecords[j]["_module"]);
        if (allRecords[j]['maps_lat'] != '' && allRecords[j]['maps_long'] != '') {
            var recordLatLng = new google.maps.LatLng(allRecords[j]['maps_lat'], allRecords[j]['maps_long']);
        }
    }
    google.maps.event.addListener(map, 'center_changed', function () {
        checkBounds();
    });
}

/**
 * Show Module marker on the basis of type module type
 * a generic function
 */
function showModmarkersCustom(record, j, map, shape, shadow, infowindow, module) {

    /** Prepare map array to reduce the code **/
    var keyMap = new Array();

    keyMap['Accounts'] = new Array();
    keyMap['Accounts']['marker_url'] = "http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|A|";
    keyMap['Accounts']['condition'] = 0;
    keyMap['Accounts']['color_values'] = "FF0000";

    keyMap['Leads'] = new Array();
    keyMap['Leads']['marker_url'] = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=+L|";
    keyMap['Leads']['condition'] = 1;
    keyMap['Leads']['color_values'] = "0080FF";



    keyMap['Contacts'] = new Array();
    keyMap['Contacts']['marker_url'] = "http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_sright|C|";
    keyMap['Contacts']['condition'] = 2;
    keyMap['Contacts']['color_values'] = "00FF37";

    if (postal != "") {
        var inlat = document.getElementById('lat_id').value;
        var inlng = document.getElementById('lng_id').value;

        boundry_distance = distance(inlat, inlng, record["maps_lat"], record["maps_long"], radius_unit_val);

        var myLatLng = new google.maps.LatLng(record["maps_lat"], record["maps_long"]);

        module_name = toTitleCase(record["_module"]);

        var cval = keyMap[record["_module"]]['color_values'];
        var path = keyMap[record["_module"]]['marker_url'] + cval + '|';

        //  var image = new google.maps.MarkerImage(path,new google.maps.Size(37, 32),new google.maps.Point(0,0),new google.maps.Point(0, 32));
        if (record["_module"] == "Accounts")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(18.5, 32));
        }
        else if (record["_module"] == "Contacts")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));
        }
        else if (record["_module"] == "Leads")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(9.25, 32));
        }
        var img = image;

        /*if(myLatLng != "") {
         markerslatlng[ml] = myLatLng;
         ml++;
         }*/

        if (record["_module"] == "Accounts")
        {
            record["billing_address_street"] = (record["billing_address_street"] == null || record["billing_address_street"] == '') ? '' : record["billing_address_street"] + ', ';
            record["billing_address_city"] = (record["billing_address_city"] == null || record["billing_address_city"] == '') ? '' : record["billing_address_city"] + ', ';
            record["billing_address_postalcode"] = (record["billing_address_postalcode"] == null || record["billing_address_postalcode"] == '') ? '' : record["billing_address_postalcode"] + ', ';
            record["billing_address_state"] = (record["billing_address_state"] == null || record["billing_address_state"] == '') ? '' : record["billing_address_state"] + ', ';

            // var cust_addd = process_address(record["billing_address_city"]+","+record["billing_address_country"]+","+record["billing_address_postalcode"]+","+record["billing_address_state"]+","+record["billing_address_street"]);
            var cust_addd = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

        }

        else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
        {
            record["primary_address_street"] = (record["primary_address_street"] == null || record["primary_address_street"] == '') ? '' : record["primary_address_street"] + ', ';
            record["primary_address_city"] = (record["primary_address_city"] == null || record["primary_address_city"] == '') ? '' : record["primary_address_city"] + ', ';
            record["primary_address_postalcode"] = (record["primary_address_postalcode"] == null || record["primary_address_postalcode"] == '') ? '' : record["primary_address_postalcode"] + ', ';
            record["primary_address_state"] = (record["primary_address_state"] == null || record["primary_address_state"] == '') ? '' : record["primary_address_state"] + ', ';

            // var cust_addd = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
            var cust_addd = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);
        }
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            shadow: shadow,
            icon: img,
            shape: shape,
            title: cust_addd,
        });


        //Fixes 53 - Start
          marker.setZIndex(999);
        //Fixes 53 - End

        google.maps.event.addListener(marker, 'dblclick', (function (marker, j) {
            return function () {

                //var origin_dbl = process_address(locations[i][0]);
                if (record["_module"] == "Accounts")
                {
                    //var origin_dbl = process_address(record["billing_address_city"]+","+record["billing_address_country"]+","+record["billing_address_postalcode"]+","+record["billing_address_state"]+","+record["billing_address_street"]);
                    var origin_dbl = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

                }

                else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
                {
                    //   var origin_dbl = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
                    var origin_dbl = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);

                }

                if (isEmpty(source_add))
                {
                    source_add = origin_dbl;
                }
                else
                {
                    is_grouped_query = true; // it means double click event occurs second time
                    destination_add = origin_dbl;

                    calcRoute(source_add, destination_add);
                }
            }
        })(marker, j));


        google.maps.event.addListener(marker, 'click', (function (marker, j) {
            return function () {
                module_name = toTitleCase(record["_module"]);

                // var origin = process_address(locations[i][0]);
                if (record["_module"] == "Accounts")
                {
                    //   var origin = process_address(record["billing_address_city"]+","+record["billing_address_country"]+","+record["billing_address_postalcode"]+","+record["billing_address_state"]+","+record["billing_address_street"]);
                    var origin = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

                }

                else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
                {
                    // var origin = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
                    var origin = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);

                }
                var get_directions = "onclick=\"javascript:get_directions('" + origin + "')\"";

                var content_settings = "<div style='width: auto; height: 100px;'><p><a href='#" + module_name + "/" + record["id"] + "' target='_blank'>" + record["name"] + "</a>&nbsp;&nbsp;&nbsp;<span title='Get Routes' " + get_directions + " style='color:#0B578F;cursor:pointer;'><u>Directions</u></span><br>" + origin + "</p></div>";

                infowindow.setContent(content_settings);

                infowindow.open(map, marker);
            }
        })(marker, j));

        google.maps.event.addListener(map, 'center_changed', function () {
            checkBounds();
        });

    } else { // if postal code is empty
        var myLatLng = new google.maps.LatLng(record["maps_lat"], record["maps_long"]);
        var cval = keyMap[record["_module"]]['color_values'];
        var path = keyMap[record["_module"]]['marker_url'] + cval + '|';

        //var image = new google.maps.MarkerImage(path,new google.maps.Size(37, 32),new google.maps.Point(0,0),new google.maps.Point(0, 32));

        if (record["_module"] == "Accounts")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(18.5, 32));
        }
        else if (record["_module"] == "Contacts")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));
        }
        else if (record["_module"] == "Leads")
        {
            var image = new google.maps.MarkerImage(path, new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(9.25, 32));
        }
        var img = image;

        /*if(myLatLng!="") {
         markerslatlng[ml] = myLatLng;
         ml++;
         }*/

        if (record["_module"] == "Accounts")
        {
            record["billing_address_street"] = (record["billing_address_street"] == null || record["billing_address_street"] == '') ? '' : record["billing_address_street"] + ', ';
            record["billing_address_city"] = (record["billing_address_city"] == null || record["billing_address_city"] == '') ? '' : record["billing_address_city"] + ', ';
            record["billing_address_postalcode"] = (record["billing_address_postalcode"] == null || record["billing_address_postalcode"] == '') ? '' : record["billing_address_postalcode"] + ', ';
            record["billing_address_state"] = (record["billing_address_state"] == null || record["billing_address_state"] == '') ? '' : record["billing_address_state"] + ', ';

            //var cust_addd = process_address(record["billing_address_city"]+','+record["billing_address_country"]+','+record["billing_address_postalcode"]+","+record["billing_address_state"]+","+record["billing_address_street"]);
            var cust_addd = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

        }

        else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
        {
            record["primary_address_street"] = (record["primary_address_street"] == null || record["primary_address_street"] == '') ? '' : record["primary_address_street"] + ', ';
            record["primary_address_city"] = (record["primary_address_city"] == null || record["primary_address_city"] == '') ? '' : record["primary_address_city"] + ', ';
            record["primary_address_postalcode"] = (record["primary_address_postalcode"] == null || record["primary_address_postalcode"] == '') ? '' : record["primary_address_postalcode"] + ', ';
            record["primary_address_state"] = (record["primary_address_state"] == null || record["primary_address_state"] == '') ? '' : record["primary_address_state"] + ', ';

            // var cust_addd = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
            var cust_addd = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);

        }
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            shadow: shadow,
            icon: img,
            shape: shape,
            title: cust_addd,
        });


        google.maps.event.addListener(marker, 'dblclick', (function (marker, j) {
            return function () {
                //var origin_dbl = process_address(locations[i][0]);
                if (record["_module"] == "Accounts")
                {
                    // var origin_dbl = process_address(record["billing_address_city"]+","+record["billing_address_country"]+","+record["billing_address_postalcode"]+","+record["billing_address_state"]+","+record["billing_address_street"]);
                    var origin_dbl = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

                }

                else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
                {
                    //  var origin_dbl = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
                    var origin_dbl = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);

                }

                if (isEmpty(source_add))
                {
                    source_add = origin_dbl;
                }
                else
                {
                    is_grouped_query = true; // it means double click event occurs second time
                    destination_add = origin_dbl;

                    calcRoute(source_add, destination_add);
                }
            }
        })(marker, j));


        google.maps.event.addListener(marker, 'click', (function (marker, j) {
            return function () {
                module_name = toTitleCase(record["_module"]);

                // var origin = process_address(locations[i][0]);
                if (record["_module"] == "Accounts")
                {

                    //  var origin = process_address(record["billing_address_city"]+","+record["billing_address_country"]+","+record["billing_address_postalcode"]+","+record["billing_address_state"]);
                    var origin = process_address(record["billing_address_street"] + record["billing_address_city"] + record["billing_address_postalcode"] + record["billing_address_state"] + record["billing_address_country"]);

                }

                else if (record["_module"] == "Leads" || record["_module"] == "Contacts")
                {
                    //  var origin = process_address(record["primary_address_city"]+","+record["primary_address_country"]+","+record["primary_address_postalcode"]+","+record["primary_address_state"]+","+record["primary_address_street"]);
                    var origin = process_address(record["primary_address_street"] + record["primary_address_city"] + record["primary_address_postalcode"] + record["primary_address_state"] + record["primary_address_country"]);

                }
                var get_directions = "onclick=\"javascript:get_directions('" + origin + "')\"";

                var content_settings = "<div style='width: auto; height: 100px;'><p><a href='#" + module_name + "/" + record["id"] + "' target='_blank'>" + record["name"] + "</a>&nbsp;&nbsp;&nbsp;<span title='Get Routes' " + get_directions + " style='color:#0B578F;cursor:pointer;'><u>Directions</u></span><br>" + origin + "</p></div>";

                infowindow.setContent(content_settings);

                infowindow.open(map, marker);
            }
        })(marker, j));

        google.maps.event.addListener(map, 'center_changed', function () {
            checkBounds();
        });

    }

}

/**
 * Initialize function that actually starts the google map
 */
function initialize() {
    var geocoder = new google.maps.Geocoder();

    //provide the services to display the directions
    directionsService = new google.maps.DirectionsService();

    pointer_index = 0;
    l = 0;

    if (typeof module_latlng[0] != "undefined") {
        for (var j = 0; j < module_latlng[0].length; j++) {
            for (var k = 0; k < module_latlng[0][j].length; k++) {
                var n = module_latlng[0][j][k].split(",");
                var lat = parseFloat(n[0]);
                var lng = parseFloat(n[1]);
                pointer_index = j;
                var address = module_latlng[1][j][k];
                beaches[r] = new Array();
                beaches[r][0] = module_latlng[1][j][k];// address
                beaches[r][1] = lat; //latitude
                beaches[r][2] = lng; //longitude
                beaches[r][3] = pointer_index;
                beaches[r][4] = n[2]; //name
                beaches[r][5] = n[3]; //db dropdownvalue
                beaches[r][6] = n[4]; // option value
                beaches[r][7] = n[5]; //id
                beaches[r][8] = n[6]; // country
                r++;
            }
        }
    }

    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(52.5233, 13.4127),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

    setMarkers(map, beaches);


    if (lat_center != "" && lng_center != "") {
        var image_postalcode = new google.maps.MarkerImage('http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-pushpin.png', new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));
        var shape = {
            coord: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat_center, lng_center),
            map: map,
            icon: image_postalcode,
            shape: shape,
        });

        if (radius_unit_val == "M") {
            var circle_radius = parseFloat(radius) * 1609;
        }
        else {
            var circle_radius = parseFloat(radius) * 1000;
        }
        var circle = new google.maps.Circle({
            map: map,
            radius: circle_radius, //radius in meters
            strokeColor: "#00AAFF",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#00AAFF",
            fillOpacity: 0.35,
            center: new google.maps.LatLng(lat_center, lng_center),
            zIndex: 99999,
        });

        map.fitBounds(circle.getBounds());
    }


    google.maps.event.addListener(map, 'center_changed', function () {
        checkBounds();
    });
}

function setMarkers(map, locations) {

    ml = 0;
    var markerslatlng = new Array();

    // Add markers to the map
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.
    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    //https://developers.google.com/chart/image/docs/gallery/dynamic_icons
    var image = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=+L|ff0000|',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(37, 32),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            new google.maps.Point(0, 32));
    var image2 = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|A|56A5EC|',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(37, 32),
            // The origin for this image is 0,0.
            new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at 0,32.
            new google.maps.Point(0, 32));
    // var shadow = new google.maps.MarkerImage('images/beachflag_shadow.png',
    var shadow_c = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_shadow',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(37, 32),
            new google.maps.Point(0, 0),
            new google.maps.Point(0, 32));


    var shadow_l = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_shadow', new google.maps.Size(40, 37), new google.maps.Point(0, 0), new google.maps.Point(1, 36));
    var shadow_a = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_xpin_letter_withshadow', new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));

    // Shapes define the clickable region of the icon.
    // The type defines an HTML &lt;area&gt; element 'poly' which
    // traces out a polygon as a series of X,Y points. The final
    // coordinate closes the poly by connecting to the first
    // coordinate.


    var latlngbounds = new google.maps.LatLngBounds();
    var shape = {
        coord: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
    };

    var infowindow = new google.maps.InfoWindow(), marker, i;

    for (i = 0; i < locations.length; i++) {
        var beach = locations[i];

        if (markerchoice == "all")
        {
            // if selected module  value is All and no selected result is queried
            /* if(isEmpty(document.getElementById('contactsid').value) &&
             isEmpty(document.getElementById('leadsid').value)  &&
             isEmpty(document.getElementById('accountsid').value))  */
            if (true)
            {
                if (selValue_account.length != 0) {
                    for (var k = 0; k < selValue_account.length; k++) {
                        if (beach[5] == selValue_account[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_a, infowindow, "accounts");
                        }
                    }
                }

                if (selValue_lead.length != 0) {
                    for (var k = 0; k < selValue_lead.length; k++) {
                        if (beach[5] == selValue_lead[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_l, infowindow, "leads");
                        }
                    }
                }
                if (selValue_contact.length != 0) {
                    for (var k = 0; k < selValue_contact.length; k++) {
                        if (beach[5] == selValue_contact[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_c, infowindow, "contacts");
                        }
                    }
                }
            }
            else
            {
                /* filter out the selected values from the drop downs and maps*/
                if (selValue_account.length != 0 && document.getElementById('accountsid').value != '') {
                    for (var k = 0; k < selValue_account.length; k++) {
                        if (beach[5] == selValue_account[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_a, infowindow, "accounts");
                        }
                    }
                }
                if (selValue_lead.length != 0 && document.getElementById('leadsid').value != '') {
                    for (var k = 0; k < selValue_lead.length; k++) {
                        if (beach[5] == selValue_lead[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_l, infowindow, "leads");
                        }
                    }
                }
                if (selValue_contact.length != 0 && document.getElementById('contactsid').value != '') {
                    for (var k = 0; k < selValue_contact.length; k++) {
                        if (beach[5] == selValue_contact[k]) {
                            showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_c, infowindow, "contacts");
                        }
                    }
                }

            }

        } else if (markerchoice == "leads") {
            if (selValue_lead.length != 0) {
                /* if dropdown value is selected */
                for (var k = 0; k < selValue_lead.length; k++) {
                    /*check status match*/
                    if (beach[5] == selValue_lead[k]) {
                        showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_l, infowindow, "leads");
                    }
                }
            }
        } else if (markerchoice == "accounts") {
            if (selValue_account.length != 0) {
                for (var k = 0; k < selValue_account.length; k++) {
                    if (beach[5] == selValue_account[k]) {
                        showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_a, infowindow, "accounts");
                    }
                }
            }
        } else if (markerchoice == "contacts") {
            if (selValue_contact.length != 0) {
                for (var k = 0; k < selValue_contact.length; k++) {
                    if (beach[5] == selValue_contact[k]) {
                        showModmarkers(beach, markerslatlng, locations, i, map, shape, shadow_c, infowindow, "contacts");
                    }
                }
            }
        }
    }

    if (markerslatlng.length != 0) {

        var uniques = removeDuplicateElement(markerslatlng);

        for (var f = 0; f < markerslatlng.length; f++) {
            latlngbounds.extend(markerslatlng[f]);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        /*
         *  Here we set the Zoom level of the Map to our requirements
         */
        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setCenter(new google.maps.LatLng(41.850033, -87.6500523)); // setting default center to United States
            map.setZoom(4);
            google.maps.event.removeListener(listener);
        });
    }

}
//get options for selected module selected field
function getFieldsDom(obj)
{
    var elements = $('[data-filter=row]');
    for (var i = 0; i < elements.length; i++) {

        var field = $(elements[i]).find('[data-filter=field] input[type=hidden]').select2('val');
        var module = $(elements[i]).find('[data-filter=module] input[type=hidden]').select2('val');
        var relatedModule = null;
        if (module == "account_list")
        {
            relatedModule = "Accounts";
            var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
            var fieldOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
            obj[2][0] = fieldOptions;

        }
        else if (module == "lead_list")
        {
            relatedModule = "Leads";
            var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
            var fieldOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
            obj[2][1] = fieldOptions;
        }
        else if (module == "contact_list")
        {
            relatedModule = "Contacts";
            var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
            var fieldOptions = SUGAR.App.lang.getAppListStrings(fieldList[field].options);
            obj[2][2] = fieldOptions;
        }
    }
    return obj;
}

function showmodulemarker(obj) {
    //setSelectedValuesCustom();
    var current = -1;
    if (obj) {
        var tbl = document.getElementById("paginator_div");
        while (tbl.hasChildNodes()) {
            tbl.removeChild(tbl.childNodes[0])
        }
        var value = obj.value;
        var page = value.split("=");
        current = page[1];
    }

    getModulesData(current);

}

function refreshmap() {
    var row = $('[data-filter=row]');
    for (var i = 0; i < row.length; i++) {
        var modulevalue = $(row[i]).find('[data-filter=module] input[type=hidden]').select2('val');
        var fieldvalue = $(row[i]).find('[data-filter=field] input[type=hidden]').select2('val');
        var typevalue = $(row[i]).find('[data-filter=type] input[type=hidden]').select2('val');
        if (modulevalue == "" || fieldvalue == "" || typevalue == "") {
          //alert("Please complete your search criteria before proceeding");
            SUGAR.App.alert.show('incomplete_search', {
                level: 'error',
                messages: SUGAR.App.lang.getModString('LBL_ALERT_COMPLETE_SEARCH' , 'rt_maps'),
                autoClose: false,
            });
          return;
        }
    }
    beaches = new Array();
    /* Here needs to set default values to source and destination addresses */
    destination_add = '';
    source_add = '';
    document.getElementById("directions-panel").innerHTML = '';
    document.getElementById("scroller").style.display = 'none';

    radius = document.getElementById('radius_id').value;
    if (isEmpty(radius))
    {
        getcenterlatlngCustom();
    } else {
        if (isNumber(radius))
            getcenterlatlngCustom();
        else
            //showDialogue("Please enter a valid radius.");
            //alert("Please enter a valid radius.");

            SUGAR.App.alert.show('invalid_radius', {
                level: 'error',
                messages: SUGAR.App.lang.getModString('LBL_ALERT_VALID_RADIUS' , 'rt_maps'),
                autoClose: false,
            });
    }

    $("#loadmap").click();

    //  if(typeof google !== 'undefined'){
    //load_cstm();
    //  // Check the radius is null or not
    // if not null it should be a valid number
    // If not a valid number alert for the user to enter a valid number.
    //var module_id = document.getElementById("moduleid").value;
    //var module_id = "Contacts";

    //  }
}

/**
 * Get the lat long on the entered address and radius
 **/
function getcenterlatlngCustom() {
    lat_center = "";
    lng_center = "";
    var geocoder = new google.maps.Geocoder();
    radius_unit_val = "";
    postal = "";
    radius = "";
    postal = document.getElementById('postal_code_id').value;
    radius = document.getElementById('radius_id').value;
    var radius_unit = document.getElementsByName('radio');

    if (postal != "") {
        if (radius == "") {
            radius = 100;
            document.getElementById('radius_id').value = 100;
            for (var y = 0; y < radius_unit.length; y++) {
                if (radius_unit[y].value == 'K') {
                    radius_unit[y].checked = true;
                }
            }
            radius_unit_val = 'K';
        } else {
            for (var y = 0; y < radius_unit.length; y++) {
                if (radius_unit[y].checked) {
                    radius_unit_val = radius_unit[y].value;
                }
            }
        }

        var address1 = postal;
        geocoder.geocode({'address': address1}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat_center = results[0].geometry.location.lat();
                lng_center = results[0].geometry.location.lng();
                document.getElementById('lat_id').value = results[0].geometry.location.lat();
                document.getElementById('lng_id').value = results[0].geometry.location.lng();
                t++;
            }
            else {
                // showDialogue("Postal address is invalid. Please enter a valid postal address.");
                //alert("Postal address is invalid. Please enter a valid postal address.");
                SUGAR.App.alert.show('invalid_postal_address', {
                    level: 'error',
                    messages: SUGAR.App.lang.getModString('LBL_ALERT_INVALID_POSTAL_ADDRESS' , 'rt_maps'),
                    autoClose: false,
                });
            }
        });
    }
}

// Check the entered string is a valid number
function isNumber(n) {
    return (parseFloat(n) == n);
}

/**
 *  this will redirect the control to find out the directions and route calculation
 *  first it will show search panel to search route upon directions click
 */
function get_directions(destination)
{
    $("#progressbar").show();
    getLocation_geo(); // This will check whether the geo-location is supported in current browser

    document.getElementById("destination").value = destination;
    $("#scroller").show();
    $("#search-panel").show();
    $("#directions-panel").hide();
    $("#result-panel").show();
    $("#destination").focus();
}

/**
 *  This will try to find out whether the geo location is supported in current browser
 *  If supported then it will return the location's info, latitude,longitude
 */
function getLocation_geo()
{
    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
        return true;
    }
    else
    {
      //alert("Geolocation is not supported by this browser.");
      SUGAR.App.alert.show('geolocation_unsupported', {
          level: 'error',
          messages: SUGAR.App.lang.getModString('LBL_ALERT_GEOLOCATION_NOT_SUPPORTED' , 'rt_maps'),
          autoClose: false,
      });
      $("#progressbar").hide();
        return false;
    }
}

function showPosition(position)
{
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    get_current_loc_address(lat, lon);
}

/**
 * This will return the GeoLocation based upon lat and long
 */
function get_current_loc_address(lat, long)
{
    var latlng = new google.maps.LatLng(lat, long);

    geocoder.geocode({"latLng": latlng}, function (data, status)
    {
        if (status == google.maps.GeocoderStatus.OK)
        {
            current_geo_address = data[1].formatted_address; //this is the full address

            document.getElementById("origin").value = current_geo_address;
            $("#progressbar").hide();
        }
        else
        {
            $("#progressbar").hide();
            //alert("Google couldn't find the current location correctly !");
            SUGAR.App.alert.show('location_not_found', {
                level: 'error',
                messages: SUGAR.App.lang.getModString('LBL_ALERT_LOCATION_NOT_FOUND' , 'rt_maps'),
                autoClose: false,
            });
        }
    });
}

function showError(error)
{
    switch (error.code)
    {
        case error.PERMISSION_DENIED:
        //alert("User denied the request for Geolocation.");
        SUGAR.App.alert.show('permission_denied', {
            level: 'error',
            messages: SUGAR.App.lang.getModString('LBL_ALERT_PERMISSION_DENIED' , 'rt_maps'),
            autoClose: false,
        });
        $("#progressbar").hide();
            break;

        case error.POSITION_UNAVAILABLE:
        //alert("Location information is unavailable.");
        SUGAR.App.alert.show('position_unavailable', {
            level: 'error',
            messages: SUGAR.App.lang.getModString('LBL_ALERT_POSITION_UNAVAILABLE' , 'rt_maps'),
            autoClose: false,
        });
        $("#progressbar").hide();
            break;

        case error.TIMEOUT:
        //alert("The request to get user location timed out.");
        SUGAR.App.alert.show('timeout', {
            level: 'error',
            messages: SUGAR.App.lang.getModString('LBL_ALERT_TIMEOUT' , 'rt_maps'),
            autoClose: false,
        });

        $("#progressbar").hide();
        break;

    case error.UNKNOWN_ERROR:
        //alert("An unknown error occurred.");
        SUGAR.App.alert.show('unknown_error', {
            level: 'error',
            messages: SUGAR.App.lang.getModString('LBL_ALERT_UNKNOWN_ERROR' , 'rt_maps'),
            autoClose: false,
        });
        $("#progressbar").hide();
            break;
    }
}

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var radlon1 = Math.PI * lon1 / 180;
    var radlon2 = Math.PI * lon2 / 180;

    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
        dist = dist * 1.609344;
    }
    if (unit == "N") {
        dist = dist * 0.8684;
    }
    return dist
}

/**
 *   This will process the address value of the customer
 */
function process_address(addd)
{
    for (var ad = 0; ad < special_chars.length; ad++)
    {
        addd = replaceAll(addd, special_chars[ad], '', 0);
    }
    addd = addd.replace(/[,]$/, '');
    return addd;
}

/**
 * This will swap two address with each other
 */
function swap_add(source, destination)
{
    var temp = source.value;
    source.value = destination.value;
    destination.value = temp;
}

/**
 *  This will replace all occurrances of a special charactere present in string
 */
function replaceAll(oldStr, removeStr, replaceStr, caseSenitivity)
{
    oldStr = oldStr.replace('+', '');
    if (caseSenitivity == 1) {
        cs = "g";
    } else {
        cs = "gi";
    }
    var myPattern = new RegExp(removeStr, cs);

    RegExp(removeStr, cs);


    newStr = oldStr.replace(myPattern, replaceStr);

    return newStr;
}

/**
 *  This will calculate the route and distance between two addresses
 *  and also count the time of travel between them
 */
function calcRoute(start, end)
{
    $("#progressbar").show();

    if (is_grouped_query == true) // it means route finding request came from double click event listner
    {
        source_add = destination_add;
        destination_add = '';
        $("#scroller").css({"z-index": "99999"});
        $("#scroller").attr({"title": "Close"});
        $("#scroller").show();
        is_grouped_query = false;
    }

    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING,
        provideRouteAlternatives: true

    };

    //Fixes issue 61 - Rehman Ahmad - Start
    /*This is the direction service rendering portion to test retaining of the multiple paths along the map */
    directionsDisplayCount = directionsDisplayCount+1;
    directionsDisplay[directionsDisplayCount] = new google.maps.DirectionsRenderer({map:map, 'draggable': false, preserveViewport: true}); // this will prevent from zoom after finding route
    directionsDisplay[directionsDisplayCount].setMap(map); // it will set the map for displaying directions
    $("#result-panel").show(); // to display the directions-panel if hidden
    directionsDisplay[directionsDisplayCount].setPanel(document.getElementById('directions-panel')); // it will set the panel to web page to show the directions

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay[directionsDisplayCount].setDirections(response);
            $("#progressbar").hide();
            $("#directions-panel").show();
            $("#search-panel").show();
        }
        else
        {
          //Fixes issue 59 - Rehman Ahmad - Start
          SUGAR.App.alert.show('invalid_directions_address', {
              level: 'error',
              messages: SUGAR.App.lang.getModString('LBL_INVALID_DIRECTIONS_ADDRESS' , 'rt_maps'),
              autoClose: false,
          });
          //Fixes issue 59 - Rehman Ahmad - End
          $("#progressbar").hide();
        }
    });
}

/**
* This will clear all routes from the map
*/
function clearRoute()
{
  for(var i=0; i <= directionsDisplayCount; i++)
  {   // Clear past routes
      directionsDisplay[i].setMap(null);
      directionsDisplay[i].setPanel(null);
  }
  directionsDisplay = new Array();
  directionsDisplayCount = -1;
  $("#directions-panel").hide();
}
//Fixes issue 61 - Rehman Ahmad - End


/**
 * This will add Google maps's listener to the input search field upon on focus to * get events whenever triggered
 */
function add_input_listener(input)
{
    var markers = [];
    var searchBox = new google.maps.places.SearchBox(input);

    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces();

        for (var i = 0, marker; marker = markers[i]; i++) {
            marker.setMap(null);
        }

        // For each place, get the icon, place name, and location.
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {

            var image_postalcode = new google.maps.MarkerImage('http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-pushpin.png', new google.maps.Size(37, 32), new google.maps.Point(0, 0), new google.maps.Point(0, 32));

            // Create a marker for each place.
            var cust_addd = process_address(place.name);
            var marker = new google.maps.Marker({
                map: map,
                /*icon: image,*/
                icon: image_postalcode,
                title: cust_addd,
                position: place.geometry.location
            });

            markers.push(marker);

            bounds.extend(place.geometry.location);
        }

    });
}

/**
 * convert to first case upper
 * UCFIRST
 */
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

/**
 *  This is the utility function to check whether an object is empty or not, returns true on empty else false
 */
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function showStatistics(leads, contacts, accounts) {
    SUGAR.App.alert.dismissAll();
    var leadsCount = filterStat.Leads.length;
    var contactsCount = filterStat.Contacts.length;
    var accountsCount = filterStat.Accounts.length;
    var licenseURL = SUGAR.App.api.buildURL('salesMapStatistics/statisics', null, null);
    SUGAR.App.api.call('GET', licenseURL, null, {
        success: _.bind(function (result) {
          //Fixes 40 - Start
          if(leadsCount > 1000){
            leadsCount = '1000+';
          }
          if(contactsCount > 1000){
            contactsCount = '1000+';
          }
          if(accountsCount > 1000){
            accountsCount = '1000+';
          }
          if(filterStat.Leads.length > 1000 || filterStat.Contacts.length > 1000 || filterStat.Accounts.length > 1000){
            SUGAR.App.alert.show('filter_statistics', {
                level: 'info',
                messages: '<table>' +
                        '<tr><td style="width:90px">'+ SUGAR.App.lang.getModString('LBL_TOTAL_LEADS' , 'rt_maps') +' </td><td>' + result.Leads + '</td><td style="width:110px; padding-left:30px">Matched Leads: </td><td>' + leadsCount + '</td></tr>' +
                        '<tr><td >'+ SUGAR.App.lang.getModString('LBL_TOTAL_CONTACTS' , 'rt_maps') +'</td><td>' + result.Contacts + '</td><td style="padding-left:30px">Matched Contacts: </td style="width:110px; padding-left:30px"><td>' + contactsCount + '</td></tr>' +
                        '<tr><td >'+ SUGAR.App.lang.getModString('LBL_TOTAL_ACCOUNTS' , 'rt_maps') +'</td><td>' + result.Accounts + '</td><td style="padding-left:30px">Matched Accounts: </td style="width:110px; padding-left:30px"><td>' + accountsCount + '</td></tr>' +
                        '</table>'+
                        '<table>' +
                        '<tr style="width:200px ">**** </tr>' +
                        '</table>' +
                        '<tr style="width:200px ">'+ SUGAR.App.lang.getModString('LBL_IMPORTANT_NOTICE' , 'rt_maps') +'</tr>' +
                        '<tr style="width:200px ">'+ SUGAR.App.lang.getModString('LBL_RECORD_LIMIT' , 'rt_maps') +'</tr>' +
                        '</table>' +
                        '<table>' +
                        '<tr style="width:200px ">\n**** </tr>' +
                        '</table>' ,
                autoClose: false,
            });
          }
          else
          {
            SUGAR.App.alert.show('filter_statistics', {
                level: 'info',
                messages: '<table>' +
                        '<tr><td style="width:90px">'+ SUGAR.App.lang.getModString('LBL_TOTAL_LEADS' , 'rt_maps') +'</td><td>' + result.Leads + '</td><td style="width:110px; padding-left:30px">'+ SUGAR.App.lang.getModString('LBL_MATCHED_LEADS' , 'rt_maps') +'</td><td>' + filterStat.Leads.length + '</td></tr>' +
                        '<tr><td >'+ SUGAR.App.lang.getModString('LBL_TOTAL_CONTACTS' , 'rt_maps') +'</td><td>' + result.Contacts + '</td><td style="padding-left:30px">'+ SUGAR.App.lang.getModString('LBL_MATCHED_CONTACTS' , 'rt_maps') +'</td style="width:110px; padding-left:30px"><td>' + filterStat.Contacts.length + '</td></tr>' +
                        '<tr><td >'+ SUGAR.App.lang.getModString('LBL_TOTAL_ACCOUNTS' , 'rt_maps') +'</td><td>' + result.Accounts + '</td><td style="padding-left:30px">'+ SUGAR.App.lang.getModString('LBL_MATCHED_ACCOUNTS' , 'rt_maps') +'</td style="width:110px; padding-left:30px"><td>' + filterStat.Accounts.length + '</td></tr>' +
                        '</table>',
                autoClose: false,
            });
          }
          //Fixes 40 - End
        }, this)
    });
}
function checkBounds() {
    centreLimitCrossed = false;
    var C = map.getCenter();
    var Y = C.lng();
    var X = C.lat();

    if (X > 84) {
        X = 84.00;
        centreLimitCrossed = true;
    } else if (X < -84) {
        X = -84.00;
        centreLimitCrossed = true;
    }
    if (centreLimitCrossed) {
        map.setCenter(new google.maps.LatLng(Y, X));
    }
}
