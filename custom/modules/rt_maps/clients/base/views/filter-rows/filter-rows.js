/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({
    events: {
        'click [data-action=add]': 'addRow',
        'click [data-action=remove]': 'removeRow',
        //'change [data-filter=field] input[type=hidden]': 'handleFieldSelected',
        //'change [data-filter=operator] input[type=hidden]': 'handleOperatorSelected'
        'change [data-filter=module] input[type=hidden]': 'handleModuleSelected',
        'change [data-filter=field] input[type=hidden]': 'handleFieldSelected',
        'change [data-filter=type] input[type=hidden]': 'handleTypeSelected'
    },
    className: 'filter-definition-container',
    filterFields: [],
    lastFilterDef: [],
    //custom properties
    leadsFilterDef: null,
    contactsFilterDef: null,
    accountsFilterDef: null,
    fieldTypes: null, // get fields of these types from contacts,leads,accounts modules
    fieldTypeMap: {
        'datetime': 'date',
        'datetimecombo': 'date'
    },
    initialize: function (opts) {
        self1 = this;
        fieldTypes = new Array("enum");
        this.formRowTemplate = app.template.get("filter-rows.filter-row-custom");
        var operators = app.metadata.getFilterOperators();
        if (_.isEmpty(operators)) {
            app.logger.error('Filter operators not found.');
            operators = {};
        }
        this.filterOperatorMap = operators;
        app.view.View.prototype.initialize.call(this, opts);
        this.listenTo(this.layout, "filterpanel:change:module", this.handleFilterChange);
        this.listenTo(this.layout, "filter:create:open", this.openForm);
        this.listenTo(this.layout, "filter:create:close", this.render);
        this.listenTo(this.context, "filter:create:save", this.saveFilter);
        this.listenTo(this.layout, "filter:create:delete", this.confirmDelete);
        //this.on('render', this.populateFilter, this);
        //this.populateFilter();
    },
    handleFilterChange: function (moduleName) {
        var moduleMeta = app.metadata.getModule(moduleName);
        if (!moduleMeta) {
            return;
        }
        this.fieldList = app.data.getBeanClass('Filters').prototype.getFilterableFields(moduleName);
        this.filterFields = {};
        this.moduleName = moduleName;
        _.each(this.fieldList, function (value, key) {
            this.filterFields[key] = app.lang.get(value.vname, moduleName);
        }, this);
//        console.log(this.filterFields);
    },
    openForm: _.debounce(function (filterModel) {
        var template = filterModel.get('filter_template') || filterModel.get('filter_definition');
        if (_.isEmpty(template)) {
            this.render();
            this.addRow();
        } else {
            this.populateFilter();
        }
        this.saveFilterEditState();
        app.shortcuts.register('Filter:Add', '+', function () {
            this.$('[data-action=add]').last().click();
        }, this);
        app.shortcuts.register('Filter:Remove', '-', function () {
            this.$('[data-action=remove]').last().click();
        }, this);
    }, 100, true),
    saveFilter: function (name) {
        var self = this,
                obj = {
                    filter_definition: this.buildFilterDef(true),
                    filter_template: this.buildFilterDef(),
                    name: name || this.context.editingFilter.get('name'),
                    module_name: this.moduleName
                },
        message = app.lang.get('TPL_FILTER_SAVE', this.moduleName, {
            name: name
        });
        this.context.editingFilter.save(obj, {
            success: function (model) {
                self.context.trigger('filter:add', model);
                self.layout.trigger('filter:toggle:savestate', false);

                setTimeout(function () {
                    load_cstm();
                }, 2000);
            },
            showAlerts: {
                'success': {
                    title: app.lang.getAppString('LBL_SUCCESS'),
                    messages: message
                }
            }
        });
    },
    confirmDelete: function () {
        app.alert.show('delete_confirmation', {
            level: 'confirmation',
            messages: app.lang.get('LBL_DELETE_FILTER_CONFIRMATION', this.moduleName),
            onConfirm: _.bind(this.deleteFilter, this)
        });
    },
    deleteFilter: function () {
        var self = this,
                name = this.context.editingFilter.get('name'),
                message = app.lang.get('TPL_DELETE_FILTER_SUCCESS', this.moduleName, {
                    name: name
                });
        this.context.editingFilter.destroy({
            success: function (model) {
                self.layout.trigger('filter:remove', model);
                setTimeout(function () {
                    load_cstm();
                }, 2000);
            },
            showAlerts: {
                'success': {
                    title: app.lang.getAppString('LBL_SUCCESS'),
                    messages: message
                }
            }
        });
        this.layout.trigger('filter:create:close');
    },
    getFilterableFields: function (moduleName) {
        var moduleMeta = app.metadata.getModule(moduleName),
                fieldMeta = moduleMeta.fields,
                fields = {};
        if (moduleMeta.filters) {
            _.each(moduleMeta.filters, function (templateMeta) {
                if (templateMeta.meta && templateMeta.meta.fields) {
                    fields = _.extend(fields, templateMeta.meta.fields);
                }
            });
        }
        _.each(fields, function (fieldFilterDef, fieldName) {
            var fieldMetaData = app.utils.deepCopy(fieldMeta[fieldName]);
            if (_.isEmpty(fieldFilterDef)) {
                fields[fieldName] = fieldMetaData || {};
            } else {
                fields[fieldName] = _.extend({
                    name: fieldName
                }, fieldMetaData, fieldFilterDef);
            }
            delete fields[fieldName]['readonly'];
        });
        return fields;
    },
    createField: function (model, def) {
        var obj = {
            meta: {
                view: "edit"
            },
            def: def,
            model: model,
            context: app.controller.context,
            viewName: "edit",
            view: this
        };
        var field = app.view.createField(obj);
        field.action = 'detail';
        return field;
    },
    addRow: function (e) {
        //alert("added by tayyab");
        var $row, model, field, $fieldValue, $fieldContainer;
        if (e) {
            $row = this.$(e.currentTarget).closest('[data-filter=row]');
            $row.after(this.formRowTemplate());
            $row = $row.next();
            this.layout.trigger('filter:toggle:savestate', true);
        } else {
            $row = $(this.formRowTemplate()).appendTo(this.$el);
        }
        model = app.data.createBean(this.moduleName);
        field = this.createField(model, {
            type: 'enum',
            options: this.filterFields
        });
        $fieldValue = $row.find('[data-filter=module]');
        $fieldContainer = $(field.getPlaceholder().string);
        $fieldContainer.appendTo($fieldValue);
        $row.data('nameField', field);
        this._renderField(field, $fieldContainer);
        return $row;
    },
    removeRow: function (e) {
        var $row = this.$(e.currentTarget).closest('[data-filter=row]'),
                fieldOpts = [{
                        'field': 'nameField',
                        'value': 'name'
                    }, {
                        'field': 'operatorField',
                        'value': 'operator'
                    }, {
                        'field': 'valueField',
                        'value': 'value'
                    }];
        this._disposeRowFields($row, fieldOpts);
        $row.remove();
        this.layout.trigger('filter:toggle:savestate', true);
        if (this.$('[data-filter=row]').length === 0) {
            this.addRow();
        }
    },
    validateRows: function (rows) {
        return _.every(rows, this.validateRow, this);
    },
    validateRow: function (row) {
        var $row = $(row),
                data = $row.data();
        if (data.isDateRange || data.isPredefinedFilter) {
            return true;
        }
        if (_.contains(['$between', '$dateBetween'], data.operator)) {
            if (!_.isArray(data.value) || data.value.length !== 2) {
                return false;
            }
            switch (data.operator) {
                case '$between':
                    return !(_.isNaN(parseFloat(data.value[0])) || _.isNaN(parseFloat(data.value[1])));
                case '$dateBetween':
                    return !_.isEmpty(data.value[0]) && !_.isEmpty(data.value[1]);
                default:
                    return false;
            }
        }
        return _.isNumber(data.value) || !_.isEmpty(data.value);
    },
    populateFilter: function () {
        this.context.editingFilter.revertAttributes();
        var name = this.context.editingFilter.get('name');

        filterOptions = this.context.get('filterOptions') || {},
                populate = this.context.editingFilter.get('is_template') && filterOptions.filter_populate,
                filterDef = this.context.editingFilter.get('filter_template') || this.context.editingFilter.get('filter_definition');


        this.render();
        this.layout.trigger('filter:set:name', name);
        if (populate) {
            filterDef = app.data.getBeanClass('Filters').prototype.populateFilterDefinition(filterDef, populate);
        }
        _.each(filterDef, function (row) {
            this.populateRow(row);
        }, this);
        this.lastFilterDef = this.buildFilterDef(true);
        this.lastFilterTemplate = this.buildFilterDef();
    },
    /*
     populateRow: function(rowObj) {
     var $row = this.addRow(),
     moduleMeta = app.metadata.getModule(this.layout.currentModule),
     fieldMeta = moduleMeta.fields;
     _.each(rowObj, function(value, key) {
     var isPredefinedFilter = (this.fieldList[key] && this.fieldList[key].predefined_filter === true);
     if (key === "$or") {
     var keys = _.reduce(value, function(memo, obj) {
     return memo.concat(_.keys(obj));
     }, []);
     key = _.find(_.keys(this.fieldList), function(key) {
     if (_.has(this.fieldList[key], 'dbFields')) {
     return _.isEqual(this.fieldList[key].dbFields.sort(), keys.sort());
     }
     }, this);
     value = _.values(value[0])[0];
     } else if (!fieldMeta[key] && !isPredefinedFilter) {
     $row.remove();
     return;
     }
     if (!this.fieldList[key]) {
     var relate = _.find(this.fieldList, function(field) {
     return field.id_name === key;
     });
     if (!relate) {
     $row.remove();
     return;
     }
     key = relate.name;
     }
     $row.find('[data-filter=module] input[type=hidden]').select2('val', key).trigger('change');
     if (_.isString(value) || _.isNumber(value)) {
     value = {
     "$equals": value
     };
     }
     _.each(value, function(value, operator) {
     $row.data('value', value);
     $row.find('[data-filter=operator] input[type=hidden]').select2('val', operator === '$dateRange' ? value : operator).trigger('change');
     });
     }, this);
     },
     */
    //persist last state of filters
    /*
     populateRow: function(rowObj) {

     var $row = this.addRow(),
     moduleMeta = SUGAR.App.metadata.getModule(this.layout.currentModule),
     fieldMeta = moduleMeta.fields;
     var arr = $.map(rowObj, function(el) { return el; });
     console.log(arr);
     var module = arr[0]["module"];
     var field = Object.keys(rowObj)[0];
     var type = Object.keys(rowObj[field])[0];
     var moduleval = null;
     if (module == "Accounts")
     {
     moduleval = "account_list";
     }
     else if (module == "Leads")
     {
     moduleval = "lead_list";
     }
     else if (module == "Contacts")
     {
     moduleval = "contact_list";
     }
     $row.find('[data-filter=module] input[type=hidden]').val(moduleval);
     $row.find('[data-filter=module]').find('a').find('.select2-chosen').text(module);
     $row.find('[data-filter=module] input[type=hidden]').change();

     var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(module);
     if (!fieldList[field]) {
     var relate = _.find(fieldList, function(key) {

     if(key.id_name === field){
     return key;
     }
     });
     alt = field;
     field = relate.name;
     if(relate){
     console.log(relate);
     if (typeof SUGAR.App.metadata.getStrings('mod_strings')[module][fieldList[relate.name]["vname"]] !='undefined')
     {
     var val_cstm = SUGAR.App.metadata.getStrings('mod_strings')[module][fieldList[relate.name]["vname"]];
     }

     else
     {
     var val_cstm = SUGAR.App.lang.getAppString([fieldList[relate.name]["vname"]]);

     }
     }
     }
     else{
     var val_cstm = SUGAR.App.metadata.getStrings('mod_strings')[module][fieldList[field]["vname"]];
     }

     $row.find('[data-filter=field] input[type=hidden]').val(field);
     $row.find('[data-filter=field]').find('a').find('.select2-chosen').text(val_cstm);
     $row.find('[data-filter=field] input[type=hidden]').change();
     var fieldName=field;
     var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(module);
     var fieldType = this.fieldTypeMap[fieldList[fieldName].type] || fieldList[fieldName].type;
     var resultKey= SUGAR.App.lang.get(this.filterOperatorMap[fieldType][type], [module, 'Filters']);
     $row.find('[data-filter=type] input[type=hidden]').val(type);
     $row.find('[data-filter=type]').find('a').find('.select2-chosen').text(resultKey);
     $row.find('[data-filter=type] input[type=hidden]').change();

     //var valuearray = rowObj[field][type];
     var keyarray = rowObj[field] || rowObj[alt];
     var valuearray = keyarray[type];
     if(fieldType=="enum"){
     $row.find('[data-filter=value] input[type=hidden]').val(valuearray);
     $row.find('[data-filter=value] input[type=hidden]').change();
     }saveFilterEditState
     else if (fieldType=="bool"){
     if(valuearray=="1"){
     $row.find('[data-filter=value] input[type=hidden]').val("1");
     $row.find('[data-filter=value] input[type=hidden]').change();
     }
     else if(valuearray=="0"){
     $row.find('[data-filter=value] input[type=hidden]').val("0");
     $row.find('[data-filter=value] input[type=hidden]').change();
     }
     }
     else{
     var valueDiv=$row.find('[data-filter=value]');
     valueDiv.find("[type=text]").val(valuearray);
     }

     },
     */

    populateRow: function (rowObj) {
        var moduleval = null;
        var module = Object.keys(rowObj)[0];
        rowObj = rowObj[module];

        if (module == "Accounts")
        {
            moduleval = "account_list";
        } else if (module == "Leads")
        {
            moduleval = "lead_list";
        } else if (module == "Contacts")
        {
            moduleval = "contact_list";
        }

        var $row = this.addRow(),
                moduleMeta = SUGAR.App.metadata.getModule(module),
                fieldMeta = moduleMeta.fields;

        $row.find('[data-filter=module] input[type=hidden]').val(moduleval);
        $row.find('[data-filter=module]').find('a').find('.select2-chosen').text(module);
        $row.find('[data-filter=module] input[type=hidden]').change();

        var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(module);
        _.each(rowObj, function (value, key) {
            var isPredefinedFilter = (fieldList[key] && fieldList[key].predefined_filter === true);
            if (key === "$or") {
                var keys = _.reduce(value, function (memo, obj) {
                    return memo.concat(_.keys(obj));
                }, []);
                key = _.find(_.keys(fieldList), function (key) {
                    if (_.has(fieldList[key], 'dbFields')) {
                        return _.isEqual(fieldList[key].dbFields.sort(), keys.sort());
                    }
                }, this);
                value = _.values(value[0])[0];
            } else if (!fieldMeta[key] && !isPredefinedFilter) {
                $row.remove();
                return;
            }
            if (!fieldList[key]) {
                var relate = _.find(fieldList, function (field) {
                    return field.id_name === key;
                });
                if (!relate) {
                    $row.remove();
                    return;
                }
                key = relate.name;
            }
            $row.find('[data-filter=field] input[type=hidden]').select2('val', key).trigger('change');
            if (_.isString(value) || _.isNumber(value)) {
                value = {
                    "$equals": value
                };
            }
            _.each(value, function (value, operator) {
                $row.data('value', value);
                $row.find('[data-filter=type] input[type=hidden]').select2('val', operator === '$dateRange' ? value : operator).trigger('change');
            });
        }, this);
    },
    handleModuleSelected: function (e) {
        var $el = this.$(e.currentTarget);
        $row = $el.parents('[data-filter=row]');
        data = $row.data();
        var fieldName = $row.find('[data-filter=module] input[type=hidden]').select2('val');
        var relatedModule = null;
        var fieldOpts = [{
                'field': 'operatorField',
                'value': 'operator'
            }, {
                'field': 'valueField',
                'value': 'value'
            }];

        this._disposeRowFields($row, fieldOpts);
        if (fieldName == "lead_list")
        {
            relatedModule = "Leads";
        } else if (fieldName == "account_list")
        {
            relatedModule = "Accounts";
        } else if (fieldName == "contact_list")
        {
            relatedModule = "Contacts";
        } else if (fieldName == "module_list")
        {
            var relatedModules = new Array("Accounts", "Leads", "Contacts");
        }
        //var relatedModule="Accounts";
        var temp = {};
        var count = 0;
        var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
        /*jQuery.each(fieldList, function(category, items) {
         if (typeof fieldList[category]["type"]!='undefined') {
         //	if (fieldList[category]["type"] == "enum") {
         //  if (fieldList[category]["name"] == "account_type" || fieldList[category]["name"] == "industry" || fieldList[category]["name"] == "status" || fieldList[category]["name"] == "lead_source") {
         temp[fieldList[category]["name"]] = SUGAR.App.metadata.getStrings('mod_strings')[relatedModule][fieldList[category]["vname"]]
         //	}
         //   }
         }
         });*/
        jQuery.each(fieldList, function (category, items) {
            //	if (typeof fieldList[category]["type"]!='undefined') {
            //	if (fieldList[category]["type"] == "enum") {
            //  if (fieldList[category]["name"] == "account_type" || fieldList[category]["name"] == "industry" || fieldList[category]["name"] == "status" || fieldList[category]["name"] == "lead_source") {
            if (typeof SUGAR.App.metadata.getStrings('mod_strings')[relatedModule][fieldList[category]["vname"]] != 'undefined')
            {
                temp[fieldList[category]["name"]] = SUGAR.App.metadata.getStrings('mod_strings')[relatedModule][fieldList[category]["vname"]]
            } else
            {
                temp[fieldList[category]["name"]] = SUGAR.App.lang.getAppString([fieldList[category]["vname"]]);
            }
            //	}
            //   }
            //	}
        });
        //$row = $('[data-filter=row]');
        $fieldWrapper = $row.find('[data-filter=field]');
        $fieldWrapper.removeClass('hide').empty();
        var model = app.data.createBean(relatedModule);
        //data = $row.data();
        var field = this.createField(model, {
            type: 'enum',
            //Start Qaiser #58539: Type ahead secrh in type filter field
            name: 'filter_row_name',
            //End Qaiser #58539
            options: temp

        });
        $field = $(field.getPlaceholder().string);
        $field.appendTo($fieldWrapper);
        data['operatorField'] = field;
        this._renderField(field, $field);
    },
    handleFieldSelected: function (e) {
        var $el = this.$(e.currentTarget),
                $row = $el.parents('[data-filter=row]'),
                $fieldWrapper = $row.find('[data-filter=type]'),
                data = $row.data(),
                fieldName = $el.val(),
                fieldOpts = [{'field': 'operatorField', 'value': 'operator'},
                    {'field': 'valueField', 'value': 'value'}];
        var moduleFieldName = $row.find('[data-filter=module] input[type=hidden]').select2('val');
        if (moduleFieldName == "module_list")
        {
            var c = fieldName.lastIndexOf('(');
            var L = fieldName.lastIndexOf(')') + 1;
            fieldName = fieldName.slice(0, c) + fieldName.slice(L);
        }

        data['name'] = fieldName;
        if (!fieldName) {
            return;
        }
        var relatedModule = null;
        if (moduleFieldName == "lead_list")
        {
            relatedModule = "Leads";
        } else if (moduleFieldName == "account_list")
        {
            relatedModule = "Accounts";
        } else if (moduleFieldName == "contact_list")
        {
            relatedModule = "Contacts";
        } else if (moduleFieldName == "module_list") //finding related module of the field selected in case of All
        {
            var selectedFieldName = $el.val();
            var relatedModule = selectedFieldName.substring(selectedFieldName.lastIndexOf("(") + 1, selectedFieldName.lastIndexOf(")"));
        }

        var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
        data.id_name = fieldList[fieldName].id_name;

        if (fieldList[fieldName].predefined_filter === true) {
            data.isPredefinedFilter = true;
            this.fireSearch();
            return;
        }
        var fieldType = this.fieldTypeMap[fieldList[fieldName].type] || fieldList[fieldName].type, payload = {};
        types = _.keys(this.filterOperatorMap[fieldType]);
        $fieldWrapper.removeClass('hide').empty();
        $row.find('[data-filter=value]').addClass('hide').empty();
        _.each(types, function (operand) {
            payload[operand] = SUGAR.App.lang.get(this.filterOperatorMap[fieldType][operand], [this.layout.moduleName, 'Filters']);
        }, this);
        var model = SUGAR.App.data.createBean(this.moduleName);
        var field = this.createField(model, {type: 'enum', searchBarThreshold: 9999, options: payload}),
                $field = $(field.getPlaceholder().string);
        $field.appendTo($fieldWrapper);
        data['operatorField'] = field;
        this._renderField(field, $field);
    },
    handleTypeSelected: function (e) {
        evnt = e;

        var $el = this.$(e.currentTarget),
                $row = $el.parents('[data-filter=row]'),
                data = $row.data(),
                operation = $el.val(),
                fieldOpts = [{
                        'field': 'valueField',
                        'value': 'value'
                    }];
        this._disposeRowFields($row, fieldOpts);
        data['operator'] = operation;
        if (!operation) {
            return;
        }
        var fieldNameBefore = $row.find('[data-filter=field] input[type=hidden]').select2('val'); //contains modulename appended with fieldname
        var moduleFieldName = $row.find('[data-filter=module] input[type=hidden]').select2('val');
        var relatedModule = null;
        if (moduleFieldName == "lead_list")
        {
            relatedModule = "Leads";
        } else if (moduleFieldName == "account_list")
        {
            relatedModule = "Accounts";
        } else if (moduleFieldName == "contact_list")
        {
            relatedModule = "Contacts";
        } else if (moduleFieldName == "module_list") //finding related module of the field selected in case of All
        {
            var selectedFieldName = $row.find('[data-filter=field] input[type=hidden]').select2('val');
            var relatedModule = selectedFieldName.substring(selectedFieldName.lastIndexOf("(") + 1, selectedFieldName.lastIndexOf(")"));
            var c = fieldNameBefore.lastIndexOf('(');
            var L = fieldNameBefore.lastIndexOf(')') + 1;
            fieldNameBefore = fieldNameBefore.slice(0, c) + fieldNameBefore.slice(L);
        }
        var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(relatedModule);
        var moduleName = relatedModule,
                module = SUGAR.App.metadata.getModule(moduleName),
                fields = fieldList;

        var fieldName = fieldNameBefore,
                fieldType = this.fieldTypeMap[fieldList[fieldName].type] || fieldList[fieldName].type,
                fieldDef = fields[fieldName];

	if (fieldType == "multienum") {
            fieldDef.type = "enum";
        }
        switch (fieldType) {
            case 'enum':
                fieldDef.isMultiSelect = true;
                fieldDef.searchBarThreshold = -1;
                break;
            case 'bool':
                fieldDef.type = 'enum';
                fieldDef.options = fieldDef.options || 'filter_checkbox_dom';
                break;
            case 'int':
                fieldDef.auto_increment = false;
                if (operation === '$in') {
                    fieldDef.type = 'varchar';
                    fieldDef.len = 200;
                    if (_.isArray($row.data('value'))) {
                        $row.data('value', $row.data('value').join(','));
                    }
                }
                break;
            case 'teamset':
                fieldDef.type = 'relate';
                break;
            case 'datetimecombo':
            case 'date':
                fieldDef.type = 'date';
                data.isDate = true;
                if (operation.charAt(0) !== '$') {
                    data.isDateRange = true;
                    this.fireSearch();
                    return;
                }
                break;
            case 'relate':
                //Ahsan - #58505 Multi select property added
                fieldDef.isMultiSelect = true;
                fieldDef.auto_populate = true;
                break;
        }
        fieldDef.required = false;
        fieldDef.readonly = false;
        var model = SUGAR.App.data.createBean(moduleName);
        var $fieldValue = $row.find('[data-filter=value]');
        $fieldValue.removeClass('hide').empty();
        var _keyUpCallback = function (e) {
            if ($(e.currentTarget).is(".select2-input")) {
                return;
            }
            this.value = $(e.currentTarget).val();
            model.set(this.name, this.unformat($(e.currentTarget).val()), {
                silent: true
            });
            model.trigger('change');
        };
        if (operation === '$between' || operation === '$dateBetween') {
            var minmax = [],
                    value = $row.data('value') || [];
            model.set(fieldName + '_min', value[0] || '');
            model.set(fieldName + '_max', value[1] || '');
            minmax.push(this.createField(model, _.extend({}, fieldDef, {
                name: fieldName + '_min'
            })));
            minmax.push(this.createField(model, _.extend({}, fieldDef, {
                name: fieldName + '_max'
            })));
            if (operation === '$dateBetween') {
                minmax[0].label = SUGAR.App.lang.get('LBL_FILTER_DATEBETWEEN_FROM');
                minmax[1].label = SUGAR.App.lang.get('LBL_FILTER_DATEBETWEEN_TO');
            } else {
                minmax[0].label = SUGAR.App.lang.get('LBL_FILTER_BETWEEN_FROM');
                minmax[1].label = SUGAR.App.lang.get('LBL_FILTER_BETWEEN_TO');
            }
            data['valueField'] = minmax;
            _.each(minmax, function (field) {
                var fieldContainer = $(field.getPlaceholder().string);
                $fieldValue.append(fieldContainer);
                this.listenTo(field, 'render', function () {
                    field.$('input, select, textarea').addClass('inherit-width');
                    field.$('.input-append').prepend('<span class="add-on">' + field.label + '</span>');
                    field.$('.input-append').addClass('input-prepend');
                    field.$('.input-append').removeClass('date');
                    field.$('input, textarea').on('keyup', _.debounce(_.bind(_keyUpCallback, field), 400));
                });
                this._renderField(field, fieldContainer);
            }, this);
        } else {
            //Ahsan - #58505 Array handling fr relate and enum fields
            var row_value = $row.data('value');
            if (_.isArray(row_value)) {
                if(fieldDef.type === 'enum'){
                    row_value = row_value.join(",");
                }else
                row_value = row_value.join("|");
            }

            model.set(fieldDef.id_name || fieldName, row_value);
            var field = this.createField(model, _.extend({}, fieldDef, {
                name: fieldName
            })),
                    fieldContainer = $(field.getPlaceholder().string);
            $fieldValue.append(fieldContainer);
            data['valueField'] = field;
            this.listenTo(field, 'render', function () {
                field.$('input, select, textarea').addClass('inherit-width');
                field.$('.input-append').removeClass('date');
                field.$('input, textarea').on('keyup', _.debounce(_.bind(_keyUpCallback, field), 400));
            });
            if (fieldDef.type === 'relate' && $row.data('value')) {

                var self = this,
                        findRelatedName = SUGAR.App.data.createBeanCollection(fieldDef.module);
                findRelatedName.fetch({
                    fields: [fieldDef.rname],
                    params: {
                        filter: [{
                                //Ahsan - #58505 Filter updated for Multi select field
                                'id': {'$in': $row.data('value')}
                            }]
                    },
                    complete: function () {
                        if (!self.disposed) {
                             //Ahsan - #58505 Field id and name set for relate field
                            if (findRelatedName.length > 0) {
                                model.set(fieldDef.id_name, findRelatedName.pluck('id'), {silent: true});
                                model.set(fieldName, findRelatedName.pluck(fieldDef.rname), {silent: true});

                            }
                            if (!field.disposed) {
                                self._renderField(field, fieldContainer);
                            }
                        }
                    }
                });
            } else {
                this._renderField(field, fieldContainer);
            }
        }
        this.listenTo(model, "change", function () {
            this._updateFilterData($row);
            this.fireSearch();
        });
        var modelValue = model.get(fieldDef.id_name || fieldName);
        if (!_.isEmpty(modelValue) && modelValue !== $row.data('value')) {
            model.trigger('change');
        }

    },
    _updateFilterData: function ($row) {
        var data = $row.data(),
                field = data['valueField'],
                name = data['name'],
                valueForFilter;
        if (this.fieldList[name] && this.fieldList[name].id_name) {
            name = this.fieldList[name].id_name;
        }
        if (_.isArray(field)) {
            valueForFilter = [];
            _.each(field, function (field) {
                var value = !field.disposed && field.model.has(field.name) ? field.model.get(field.name) : '';
                value = $row.data('isDate') ? (app.date.stripIsoTimeDelimterAndTZ(value) || '') : value;
                valueForFilter.push(value);
            });
        } else {
            var value = !field.disposed && field.model.has(name) ? field.model.get(name) : '';
            valueForFilter = $row.data('isDate') ? (app.date.stripIsoTimeDelimterAndTZ(value) || '') : value;
        }
        $row.data("value", valueForFilter);
    },
    fireSearch: _.debounce(function () {

        var filterDef = this.buildFilterDef(true),
                filterTemplate = this.buildFilterDef(),
                defHasChanged = !_.isEqual(this.lastFilterDef, filterDef),
                templateHasChanged = !_.isEqual(this.lastFilterTemplate, filterTemplate);

        if (defHasChanged || templateHasChanged) {
            this.saveFilterEditState(filterDef, filterTemplate);
            this.lastFilterDef = filterDef;
            this.lastFilterTemplate = filterTemplate;
            this.layout.trigger('filter:toggle:savestate', true);
        }
        if (!defHasChanged) {
            return;
        }
        if (this.context.get('applyFilter') !== false) {
            // this.layout.trigger('filter:apply', null, filterDef);
        }
    }, 400),
    saveFilterEditState: function (filterDef, templateDef) {
        if (!this.context.editingFilter) {
            return;
        }
        this.context.editingFilter.set({
            'filter_definition': filterDef || this.buildFilterDef(true),
            'filter_template': templateDef || this.buildFilterDef()
        });
        var filter = this.context.editingFilter.toJSON();
        if (this.layout.getComponent('filter-actions') && this.layout.getComponent('filter-actions').$('input').length === 1) {
            filter.name = this.layout.getComponent('filter-actions').getFilterName();
        }
        this.layout.getComponent('filter').saveFilterEditState(filter);
    },
    /*
     buildFilterDef: function(onlyValidRows) {
     var $rows = this.$('[data-filter=row]'),
     filter = [];
     _.each($rows, function(row) {
     var rowFilter = this.buildRowFilterDef($(row), onlyValidRows);
     if (rowFilter) {
     filter.push(rowFilter);
     }
     }, this);

     return filter;
     },*/

    buildFilterDef: function (onlyValidRows) {
        var modulename = null;
        var module = null;
        var $rows = this.$('[data-filter=row]'),
                filter = [];
        _.each($rows, function (row) {
            modulename = $(row).find('[data-filter=module] input[type=hidden]').select2('val');
            if (modulename == "account_list")
            {
                module = "Accounts";
            } else if (modulename == "contact_list")
            {
                module = "Contacts";
            } else if (modulename == "lead_list")
            {
                module = "Leads";
            }
            var rowFilter = this.buildRowFilterDef($(row), onlyValidRows, module);
            if (rowFilter) {
                var obj = {};
                obj[module] = rowFilter;
                filter.push(obj);
            }
        }, this);
        return filter;
    },
    /*
     buildRowFilterDef: function($row, onlyIfValid) {
     var data = $row.data();
     if (onlyIfValid && !this.validateRow($row)) {
     return;
     }
     var operator = data['operator'],
     value = data['value'] || '',
     name = data['id_name'] || data['name'],
     filter = {};
     if (_.isEmpty(name)) {
     return;
     }
     if (data.isPredefinedFilter || !this.fieldList) {
     filter[name] = '';
     var modulename = $row.find('[data-filter=module] input[type=hidden]').select2('val');
     var module;
     if (modulename == "account_list")
     {
     module = "Accounts";
     }
     else if (modulename == "contact_list")
     {
     module = "Contacts";
     }
     else if (modulename == "lead_list")
     {
     module = "Leads";
     }
     filter[name]["module"] = module;
     return filter;
     } else {
     if (this.fieldList[name] && _.has(this.fieldList[name], 'dbFields')) {
     var subfilters = [];
     _.each(this.fieldList[name].dbFields, function(dbField) {
     var filter = {};
     filter[dbField] = {};
     filter[dbField][operator] = value;
     subfilters.push(filter);
     });
     filter['$or'] = subfilters;
     var modulename = $row.find('[data-filter=module] input[type=hidden]').select2('val');
     var module;
     if (modulename == "account_list")
     {
     module = "Accounts";
     }
     else if (modulename == "contact_list")
     {
     module = "Contacts";
     }
     else if (modulename == "lead_list")
     {
     module = "Leads";
     }
     filter[name]["module"] = module;
     } else {
     if (operator === '$equals') {
     filter[name] = {};
     filter[name][operator] = value;
     } /*else if (data.isDateRange) {
     filter[name] = {};
     filter[name].$dateRange = operator;
     }*/
    /*
     else if (operator === '$in' || operator === '$not_in') {
     filter[name] = {};
     if (_.isArray(value)) {
     filter[name][operator] = value;
     } else if (!_.isEmpty(value)) {
     filter[name][operator] = (value + '').split(',');
     } else {
     filter[name][operator] = [];
     }
     } else {
     filter[name] = {};
     filter[name][operator] = value;
     }
     var modulename = $row.find('[data-filter=module] input[type=hidden]').select2('val');
     var module;
     if (modulename == "account_list")
     {
     module = "Accounts";
     }
     else if (modulename == "contact_list")
     {
     module = "Contacts";
     }
     else if (modulename == "lead_list")
     {
     module = "Leads";
     }
     //filter[name] = {};
     filter[name]["module"] = module;
     }

     return filter;
     }
     },
     */
    buildRowFilterDef: function ($row, onlyIfValid, modulename) {

        var data = $row.data();
        if (onlyIfValid && !this.validateRow($row)) {
            return;
        }

        var dataId = null;
        if (typeof $row.find('[data-filter=value] input[type=hidden]').attr('data-id') != 'undefined')
        {
            dataId = $row.find('[data-filter=value] input[type=hidden]').attr('data-id');
        } else if (!dataId && typeof $row.find('[data-filter=value] input[type=hidden]').attr('value') != 'undefined')
        {
            dataId = $row.find('[data-filter=value] input[type=hidden]').attr('value');
        }
        var operator = data['operator'],
                //value = data['value'] || '',
                value = dataId || data['value'] || '',
                name = data['id_name'] || data['name'],
                filter = {};
        var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(modulename);
        if (!_.isEmpty(fieldList[name]) && fieldList[name].type == 'currency') {
            value = data['value'];
            //filter['currency_id'] = dataId;
        }
        if (_.isEmpty(name)) {
            return;
        }
        if (data.isPredefinedFilter || !this.fieldList) {
            filter[name] = '';
            return filter;
        } else {
            var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(modulename);

            if (fieldList[name] && _.has(fieldList[name], 'dbFields')) {
                var subfilters = [];
                _.each(fieldList[name].dbFields, function (dbField) {
                    var filter = {};
                    filter[dbField] = {};
                    filter[dbField][operator] = value;
                    subfilters.push(filter);
                });
                filter['$or'] = subfilters;
            } else {
                if (operator === '$equals') {
                    filter[name] = value;
                } else if (data.isDateRange) {
                    filter[name] = {};
                    filter[name].$dateRange = operator;
                } else if (operator === '$in' || operator === '$not_in') {
                    filter[name] = {};
                    if (_.isArray(value)) {
                        filter[name][operator] = value;
                    } else if (!_.isEmpty(value)) {
                         //Ahsan - #58505 Relate field values are separated by '|' and enum values are separated by ','
                        if (value.indexOf('|') > -1)
                        {
                            filter[name][operator] = (value + '').split('|');
                        }else{
                            filter[name][operator] = (value + '').split(',');
                        }

                    } else {
                        filter[name][operator] = [];
                    }
                } else {
                    filter[name] = {};
                    filter[name][operator] = value;
                }
            }
            return filter;
        }
    },
    buildFilterDefCustom: function (onlyValidRows) {

        var self = this;
        var modulename = null;
        var module = null;
        filter = [];
        self.leadsFilterDef = [];
        self.contactsFilterDef = [];
        self.accountsFilterDef = [];
        dbfilters = self.collection.origFilterDef;
        if (_.isUndefined(self.context.attributes.currentFilterId) || _.isNull(self.context.attributes.currentFilterId)) {
            dbfilters = '';
        } else if (self.context.attributes.currentFilterId.length < 32) {
            dbfilters = '';
            initialize();
            return true;
        }

        if (!_.isEmpty(dbfilters)) {
            _.each(dbfilters, function (dbfilter) {
                for (var name in dbfilter) {
                    module = name;
                }
                var rowFilter = this.buildRowFilterDefCustom(dbfilter, onlyValidRows, module);
                if (rowFilter) {

                    if (module == "Accounts")
                    {
                        self.accountsFilterDef.push(rowFilter);
                    } else if (module == "Contacts")
                    {
                        self.contactsFilterDef.push(rowFilter);
                    } else if (module == "Leads")
                    {
                        self.leadsFilterDef.push(rowFilter);
                    }
                }
            }, this);
            return filter;
        } else {
            var $rows = this.$('[data-filter=row]');
            _.each($rows, function (row) {
                modulename = $(row).find('[data-filter=module] input[type=hidden]').select2('val');
                if (modulename == "account_list")
                {
                    module = "Accounts";
                } else if (modulename == "contact_list")
                {
                    module = "Contacts";
                } else if (modulename == "lead_list")
                {
                    module = "Leads";
                }
                onlyValidRows = 'NOTSAVED';
                var rowFilter = this.buildRowFilterDefCustom($(row), onlyValidRows, module);
                if (rowFilter) {
                    if (modulename == "account_list")
                    {
                        self.accountsFilterDef.push(rowFilter);
                    } else if (modulename == "contact_list")
                    {
                        self.contactsFilterDef.push(rowFilter);
                    } else if (modulename == "lead_list")
                    {
                        self.leadsFilterDef.push(rowFilter);
                    }
                    filter.push(rowFilter);
                }
            }, this);
            return filter;
        }

    },
    buildRowFilterDefCustom: function (dbfilter, onlyIfValid, modulename) {


        if (onlyIfValid != 'NOTSAVED') {
            var filObj = dbfilter[modulename];
            //Start - issue 38- Compatibility - Rehman Ahmad
            var filObj2 = dbfilter[modulename];
            //End - issue 38- Compatibility - Rehman Ahmad


            //Start - Ahsan - #60561 Multiselect field filter issue
//            return filObj;
            //End - Ahsan - #60561 Multiselect field filter issue
            for (var fldName in filObj) {
                name = fldName;
            }
            filObj = filObj[name];
            var operator2 = filObj[name];
            for (var fltOp in filObj) {
                operator = fltOp;
            }
            filObj = filObj[operator];

            //Start - Ahsan - #60561 Multiselect field filter issue
            value = filObj;
            //End - Ahsan - #60561 Multiselect field filter issue
            if (operator == '$dateBetween') {
                value = filObj;
            }
            var filter = {};
            if (_.isEmpty(name)) {
                return;
            } else {
                var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(modulename);
                if (fieldList[name] && _.has(fieldList[name], 'dbFields')) {
                    var subfilters = [];
                    _.each(fieldList[name].dbFields, function (dbField) {
                        var filter = {};
                        filter[dbField] = {};
                        filter[dbField][operator] = value;
                        subfilters.push(filter);
                    });
                    filter['$or'] = subfilters;
                } else {
                    if (operator === '$equals') {
                        filter[name] = value;
                        //Start - Ahsan - #60561 Multiselect field filter issue
                    } else if (operator === '$in' || operator === '$not_in' || operator === '$contains' || operator === '$not_contains') {
                        //End - Ahsan - #60561 Multiselect field filter issue
                        filter[name] = {};
                        if (_.isArray(value)) {
                            filter[name][operator] = value;
                        } else if (!_.isEmpty(value)) {
                            filter[name][operator] = (value + '').split(',');
                        } else {
                            filter[name][operator] = [];
                        }
                    } else {
                      filter[name] = {};
                      filter[name][operator] = value;
                      }
                }

                //Start - issue 38- Compatibility - Rehman Ahmad
                //return filter;
                return filObj2;
                //End - issue 38- Compatibility - Rehman Ahmad
            }
        } else {

            $row = dbfilter;


            if (typeof $row == 'undefined') {
                return;
            }
            var data = $row.data();
            if (!this.validateRow($row)) {
                return;
            }
            var dataId = null;
            if (typeof $row.find('[data-filter=value] input[type=hidden]').attr('data-id') != 'undefined')
            {
                dataId = $row.find('[data-filter=value] input[type=hidden]').attr('data-id');
            } else if (!dataId && typeof $row.find('[data-filter=value] input[type=hidden]').attr('value') != 'undefined')
            {
                dataId = $row.find('[data-filter=value] input[type=hidden]').attr('value');
            }
            var operator = data['operator'],
                    //value = data['value'] || '',
                    value = dataId || data['value'] || '',
                    name = data['id_name'] || data['name'];
            var filter = {};

            var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(modulename);
            if (!_.isEmpty(fieldList[name]) && fieldList[name].type == 'currency') {
                value = data['value'];
                //filter['currency_id'] = dataId;
            }

            if (_.isEmpty(name)) {
                return;
            }
            if (data.isPredefinedFilter || !this.fieldList) {
                filter[name] = '';
                return filter;
            } else {
                var fieldList = SUGAR.App.data.getBeanClass('Filters').prototype.getFilterableFields(modulename);
                if (fieldList[name] && _.has(fieldList[name], 'dbFields')) {
                    var subfilters = [];
                    _.each(fieldList[name].dbFields, function (dbField) {
                        var filter = {};
                        filter[dbField] = {};
                        filter[dbField][operator] = value;
                        subfilters.push(filter);
                    });
                    filter['$or'] = subfilters;
                } else {
                    if (operator === '$equals') {
                        filter[name] = value;
                    } else if (data.isDateRange) {
                        filter[name] = {};
                        filter[name].$dateRange = operator;
                    } else if (operator === '$in' || operator === '$not_in') {
                        filter[name] = {};
                        if (_.isArray(value)) {
                            filter[name][operator] = value;
                        } else if (!_.isEmpty(value)) {
                            filter[name][operator] = (value + '').split(',');
                        } else {
                            filter[name][operator] = [];
                        }
                    } else {
                        filter[name] = {};
                        filter[name][operator] = value;

                    }
                }

                return filter;
            }
        }

    },
    resetFilterValues: function () {
        var $rows = this.$('[data-filter=row]');
        _.each($rows, function (row) {
            var $row = $(row);
            var valueField = $row.data('valueField');
            if (!valueField || valueField.disposed) {
                return;
            }
            if (!_.isArray(valueField)) {
                valueField.model.clear();
                return;
            }
            _.each(valueField, function (field) {
                field.model.clear();
            });
        });
    },
    _disposeRowFields: function ($row, opts) {
        var data = $row.data(),
                model;
        if (_.isObject(data) && _.isArray(opts)) {
            _.each(opts, function (val) {
                if (data[val.field]) {
                    var fields = _.isArray(data[val.field]) ? data[val.field] : [data[val.field]];
                    data[val.value] = '';
                    _.each(fields, function (field) {
                        model = field.model;
                        if (val.field === "valueField" && model) {
                            model.clear({
                                silent: true
                            });
                            this.stopListening(model);
                        }
                        field.dispose();
                        field = null;
                    }, this);
                    return;
                }
                if (data.isDateRange && val.value === 'value') {
                    data.value = '';
                }
            }, this);
        }
        data.isDate = false;
        data.isDateRange = false;
        data.isPredefinedFilter = false;
        $row.data(data);
        this.fireSearch();
    }
})
