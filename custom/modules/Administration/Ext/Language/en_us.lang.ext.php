<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Administration/Ext/Language/en_us.RT_MAPS_License.php


$mod_strings['LBL_RT_MAPS_APIKEY'] = 'Google API Key Configuration';
$mod_strings['LBL_RT_MAPS_APIKEY_DESC'] = 'Configure the Google API key for Google maps';
$mod_strings['LBL_RT_MAPS_LICENSE'] = 'RT SalesMap License Verification';
$mod_strings['LBL_RT_MAPS_LICENSE_DESC'] = 'RT SalesMap License Verification';
$mod_strings['LBL_RT_MAPS_CONFIGURE_USERS_TITLE'] = 'RT SalesMap Users Configuration';
$mod_strings['LBL_RT_MAPS_CONFIGURE_USERS'] = 'Manage and configure users for RT SalesMap';


?>
