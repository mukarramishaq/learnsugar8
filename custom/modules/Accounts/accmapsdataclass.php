<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

/**
 * Raise the flag for including geo coding.

 * */
class accmapsdataclass {

    function accmapsdatafunction($bean, $event, $arguments) {
        $bean->non_geo_coded_address = '';
    }

}
