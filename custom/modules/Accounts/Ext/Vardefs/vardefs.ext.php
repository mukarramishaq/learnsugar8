<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Accounts/Ext/Vardefs/customfield_latlong.php


$dictionary['Account']['fields']['maps_lat'] = array(
    'name' => 'maps_lat',
    'vname' => 'LBL_LATTITUDE',
    'type' => 'varchar',
    'len' => '255',
    'audited' => false,
    'required' => false,
    'comment' => '',
    'studio'=> false,
);
$dictionary['Account']['fields']['maps_long'] = array(
    'name' => 'maps_long',
    'vname' => 'LBL_LONGITUDE',
    'type' => 'varchar',
    'len' => '255',
    'audited' => false,
    'required' => false,
    'comment' => '',
    'studio'=> false,
);
$dictionary["Account"]["fields"]["non_geo_coded_address"] = array(
    'name' => 'non_geo_coded_address',
    'vname' => 'LBL_NON_GEO_CODING_ADDRESS',
    'type' => 'varchar',
    'len' => '100',
    'default_value' => '',
    'readonly' => true,
    'studio'=> false,
);

?>
